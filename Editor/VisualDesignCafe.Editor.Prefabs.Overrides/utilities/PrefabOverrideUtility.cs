﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using VisualDesignCafe.Editor.Prefabs.Comparing;

// Ignore warnings of obsolete methods. 
// The PrefabOverride.SetValue is obsolete for end-users but is still used internally.
#pragma warning disable 618  

namespace VisualDesignCafe.Editor.Prefabs.Overrides
{
    using Object = UnityEngine.Object;

    public static class PrefabOverrideUtility
    {

        /// <summary>
        /// List of all components that do not support Prefab Overrides.
        /// </summary>
        internal static readonly HashSet<string> UnsupportedComponents = new HashSet<string>
        {
            "VisualDesignCafe.Editor.Prefabs.Prefab",
            "VisualDesignCafe.Editor.Prefabs.NestedPrefab",
            "VisualDesignCafe.Editor.Prefabs.Guid",
            "VisualDesignCafe.Editor.Prefabs.PrefabOverrides",
            "VisualDesignCafe.Editor.Prefabs.ModelModification",
            "VisualDesignCafe.Editor.Prefabs.ModelComponentModification",
        };

        /// <summary>
        /// List of all properties that do not support Prefab Overrides.
        /// </summary>
        internal static readonly HashSet<string> UnsupportedProperties = new HashSet<string>
        {
            "m_PrefabParentObject",
            "m_PrefabInternal",
            "m_GameObject",
            "m_Father",
            "m_Icon",
            "m_ObjectHideFlags",
            "m_Component",
            "m_Children",
            "m_RootOrder",
            "m_LocalEulerAnglesHint",
            "m_EditorHideFlags",
            "m_Script",
            "m_EditorClassIdentifier",
        };

        #region Apply

        /// <summary>
        /// Updates the value for every property override in the entire hierarchy.
        /// </summary>
        public static void UpdateAll( GameObject gameObject )
        {
            if( Config.DEBUG_INFO )
                Console.Log( "Applying all prefab overrides: " + gameObject.ToLog() );

            foreach( var p in gameObject.GetComponentsInChildren<PrefabOverrides>( true ) )
            {
                Update( p );
            }

            Transform parent = gameObject.transform.parent;
            while( parent != null )
            {
                var overrides = parent.GetComponent<PrefabOverrides>();
                if( overrides != null )
                    Update( overrides );

                parent = parent.parent;
            }
        }

        /// <summary>
        /// Updates the value for every property override.
        /// </summary>
        public static void Update( PrefabOverrides overrides )
        {
            if( Config.DEBUG_INFO && Config.VERBOSE )
                Console.Log(
                    "Updating Prefab Override values for "
                    + overrides.gameObject.ToLog() );

            foreach( var objectOverride in overrides.Objects )
            {
                if( objectOverride.Target == null )
                    continue;

                foreach( var componentOverride in objectOverride.Components )
                {
                    Object target = componentOverride.Target;

                    if( target == null )
                        continue;

                    var serializedObject = new SerializedObject( target );

                    foreach( var propertyOverride in componentOverride.Properties )
                    {
                        bool hasParentOverride =
                            IsOverridden(
                                overrides.transform.parent,
                                target,
                                propertyOverride.Path );

                        if( hasParentOverride )
                        {
                            if( Config.DEBUG_INFO && Config.VERBOSE )
                                Console.Log(
                                    "Skipping property override because it is also overridden in a parent. (" + propertyOverride.Path + ")" );

                            continue;
                        }

                        SerializedProperty serializedProperty =
                            serializedObject.FindProperty( propertyOverride.Path );

                        if( serializedProperty == null )
                        {
                            if( Config.DEBUG_WARNING )
                                Console.Log( "Could not find serialized property for override. (" + propertyOverride.Path + ")" );

                            continue;
                        }

                        if( Config.DEBUG_WARNING && Config.VERBOSE )
                            Console.LogFormat(
                                "Updating value for property override. ({0})",
                                propertyOverride.Path );

                        SetOverrideValue( propertyOverride, serializedProperty );
                    }
                }
            }
        }

        /// <summary>
        /// Applies all prefab overrides in the GameObject and its children.
        /// </summary>
        public static void ApplyAll( GameObject gameObject )
        {
            if( Config.DEBUG_INFO )
                Console.Log(
                    "Applying all prefab overrides: " + gameObject.ToLog() );

            var prefabOverrides =
                gameObject.GetComponentsInChildren<PrefabOverrides>( true );

            if( prefabOverrides == null )
                return;

            PrefabOverrides[] orderedOverrides =
                prefabOverrides
                    .OrderByDescending(
                        p => PrefabHierarchyUtility.GetDepth(
                                p.GetComponent<Prefab>(),
                                true ) )
                    .ToArray();

            foreach( var p in orderedOverrides )
            {
                Apply( p );
            }
        }

        /// <summary>
        /// Applies the prefab overrides.
        /// </summary>
        public static void Apply( PrefabOverrides overrides )
        {
            if( Config.DEBUG_INFO )
                Console.Log(
                    "Applying prefab overrides: " + overrides.gameObject.ToLog() );

            if( overrides == null )
                throw new ArgumentNullException( "PrefabOverrides" );

            if( Prefab.IsNull( overrides.GetComponent<Prefab>() ) )
            {
                if( Config.DEBUG_WARNING )
                    Console.LogWarning(
                        "Prefab Overrides is not attached to a prefab." );

                return;
            }

            if( overrides.GetComponent<NestedPrefab>() == null )
            {
                if( Config.DEBUG_WARNING )
                    Console.LogWarning(
                        "Prefab Overrides is not attached to a nested prefab." );

                return;
            }

            Console.Indent++;

            foreach( var objectOverride in overrides.Objects )
            {
                if( objectOverride.Target == null )
                    continue;

                foreach( var componentOverride in objectOverride.Components )
                {
                    Object target = componentOverride.Target;

                    if( target == null )
                        continue;

                    var serializedObject = new SerializedObject( target );

                    foreach( var propertyOverride in componentOverride.Properties )
                    {
                        SerializedProperty serializedProperty =
                            serializedObject.FindProperty( propertyOverride.Path );

                        if( serializedProperty == null )
                            continue;

                        // Skip overrides that have sub-properties. 
                        // We should apply the sub properties individually instead.
                        switch( serializedProperty.propertyType )
                        {
                            case SerializedPropertyType.Vector2:
                            case SerializedPropertyType.Vector3:
                            case SerializedPropertyType.Vector4:
                            case SerializedPropertyType.Quaternion:
                            case SerializedPropertyType.Rect:
                                continue;
                        }

                        if( SetPropertyValue( serializedProperty, propertyOverride ) )
                        {
                            serializedObject.SaveModifications();
                        }
                    }
                }
            }

            Console.Indent--;
        }

        #endregion

        #region Get/Set

        public static PropertyOverride GetPropertyOverride(
            PrefabOverrides overrides, SerializedProperty property )
        {
            if( overrides == null )
                throw new ArgumentNullException( "Prefab Overrides" );

            string path = property.propertyPath;

            // Extract name of property if the property is a gradient.
            if( property.propertyType == SerializedPropertyType.Color )
            {
                if( new Wildcard( "*.key*" ).IsMatch( property.propertyPath ) )
                {
                    path =
                        path.Substring(
                            0,
                            path.LastIndexOf( ".key", StringComparison.Ordinal ) );
                }
            }

            return overrides.GetPropertyOverride(
                property.serializedObject.targetObject,
                path );
        }

        internal static PropertyOverride GetPropertyOverride(
            PrefabOverrides overrides, PropertyInfo property )
        {
            if( overrides == null )
                throw new ArgumentNullException( "Prefab Overrides" );

            if( property == null )
                throw new ArgumentNullException( "Property" );

            if( property.Component.Serialized == null )
                return null;

            string path = property.PropertyPath;

            // Extract name of property if the property is a gradient.
            if( property.PropertyType == SerializedPropertyType.Color )
            {
                if( new Wildcard( ".key*" ).IsMatch( property.PropertyPath ) )
                {
                    path =
                        path.Substring(
                            0,
                            path.LastIndexOf( ".key", StringComparison.Ordinal ) );
                }
            }

            return overrides.GetPropertyOverride(
                property.Component.Serialized.targetObject,
                path );
        }

        public static PropertyOverride SetPropertyOverride(
            PrefabOverrides overrides,
            Object target,
            string propertyPath,
            bool isOverridden )
        {
            if( overrides == null )
                throw new ArgumentNullException( "PrefabOverrides" );

            if( target == null )
                throw new ArgumentNullException( "Target" );

            if( string.IsNullOrEmpty( propertyPath ) )
                throw new ArgumentNullException( "Path" );

            return SetPropertyOverride(
                overrides, target, propertyPath, isOverridden, true );
        }

        internal static PropertyOverride SetPropertyOverride(
            PrefabOverrides overrides,
            Object target,
            string propertyPath,
            bool isoverridden,
            bool includeSubProperties )
        {
            if( overrides == null )
                throw new ArgumentNullException( "PrefabOverrides" );

            if( target == null )
                throw new ArgumentNullException( "Target" );

            SerializedProperty serializedProperty =
                new SerializedObject( target ).FindProperty( propertyPath );

            if( serializedProperty == null )
                throw new NullReferenceException( "Property could not be found" );

            return SetPropertyOverride(
                overrides, serializedProperty, isoverridden, includeSubProperties );
        }

        public static PropertyOverride SetPropertyOverride(
            PrefabOverrides overrides, SerializedProperty property, bool isOverridden )
        {
            if( overrides == null )
                throw new ArgumentNullException( "PrefabOverrides" );

            if( property == null )
                throw new ArgumentNullException( "Property" );

            return SetPropertyOverride( overrides, property, isOverridden, true );
        }

        public static void SetPropertyOverride(
            SerializedObject overrides, SerializedProperty property, bool isOverridden )
        {
            if( overrides == null )
                throw new ArgumentNullException( "PrefabOverrides" );

            if( property == null )
                throw new ArgumentNullException( "Property" );

            SetPropertyOverride( overrides, property, isOverridden, true );
        }

        /// <summary>
        /// Enables or disables the override for the property.
        /// </summary>
        /// <param name="includeSubProperties">Should sub properties be included? (x,y for Vector2. x,y,z for Vector3 values. etc.)</param>
        /// <returns></returns>
        internal static PropertyOverride SetPropertyOverride(
            PrefabOverrides overrides,
            SerializedProperty property,
            bool isoverridden,
            bool includeSubProperties )
        {
            if( overrides == null )
                throw new ArgumentNullException( "Prefab Overrides" );

            if( property == null )
                throw new ArgumentNullException( "Property" );

            if( isoverridden )
            {
                var @override =
                    overrides.AddPropertyOverride(
                        property.serializedObject.targetObject,
                        property.propertyPath );

                if( includeSubProperties )
                {
                    string[] properties = FindPropertiesToModify( property );

                    foreach( string path in properties )
                    {
                        overrides.AddPropertyOverride(
                            property.serializedObject.targetObject,
                            path );
                    }
                }

                EditorUtility.SetDirty( overrides );

                return @override;
            }
            else
            {
                overrides.Remove(
                    overrides.GetPropertyOverride(
                        property.serializedObject.targetObject,
                        property.propertyPath ) );

                if( includeSubProperties )
                {
                    string[] properties = FindPropertiesToModify( property );

                    foreach( string path in properties )
                    {
                        overrides.Remove(
                            overrides.GetPropertyOverride(
                                property.serializedObject.targetObject,
                                path ) );
                    }
                }

                EditorUtility.SetDirty( overrides );
            }

            return null;
        }

        private static string[] FindPropertiesToModify( SerializedProperty property )
        {
            var properties = new List<string>();

            switch( property.propertyType )
            {
                case SerializedPropertyType.Float:

                    SerializedProperty parentProperty = FindParentProperty( property );

                    if( parentProperty != null )
                    {
                        switch( parentProperty.propertyType )
                        {
                            case SerializedPropertyType.Vector2:
                            case SerializedPropertyType.Vector3:
                            case SerializedPropertyType.Vector4:
                            case SerializedPropertyType.Quaternion:
                            case SerializedPropertyType.Rect:
                                properties.Add( parentProperty.propertyPath );
                                break;
                        }
                    }

                    break;
                case SerializedPropertyType.Vector2:
                    properties.Add( property.propertyPath + ".x" );
                    properties.Add( property.propertyPath + ".y" );
                    break;
                case SerializedPropertyType.Vector3:
                    properties.Add( property.propertyPath + ".x" );
                    properties.Add( property.propertyPath + ".y" );
                    properties.Add( property.propertyPath + ".z" );
                    break;
                case SerializedPropertyType.Vector4:
                    properties.Add( property.propertyPath + ".x" );
                    properties.Add( property.propertyPath + ".y" );
                    properties.Add( property.propertyPath + ".z" );
                    properties.Add( property.propertyPath + ".w" );
                    break;
                case SerializedPropertyType.Quaternion:
                    properties.Add( property.propertyPath + ".x" );
                    properties.Add( property.propertyPath + ".y" );
                    properties.Add( property.propertyPath + ".z" );
                    properties.Add( property.propertyPath + ".w" );
                    break;
                case SerializedPropertyType.Rect:
                    properties.Add( property.propertyPath + ".x" );
                    properties.Add( property.propertyPath + ".y" );
                    properties.Add( property.propertyPath + ".width" );
                    properties.Add( property.propertyPath + ".height" );
                    break;
            }

            return properties.ToArray();
        }

        internal static void SetPropertyOverride(
            SerializedObject properties,
            SerializedProperty property,
            bool isoverridden,
            bool includeSubProperties = true )
        {
            SetPropertyOverride(
                (PrefabOverrides) properties.targetObject,
                property,
                isoverridden,
                includeSubProperties );

            properties.Update();
            properties.ApplyModifiedProperties();
        }

        internal static SerializedProperty FindParentProperty(
            SerializedProperty property )
        {
            int index = property.propertyPath.LastIndexOf( '.' );

            if( index == -1 )
                return null;

            string path = property.propertyPath.Substring( 0, index );

            return property.serializedObject.FindProperty( path );
        }

        #endregion

        #region Is Overridden?

        internal static bool IsOverridden(
            Transform transform, SerializedProperty property )
        {
            while( transform != null )
            {
                PrefabOverrides properties = transform.GetComponent<PrefabOverrides>();

                if( properties != null )
                {
                    if( GetPropertyOverride( properties, property ) != null )
                        return true;
                }

                transform = transform.parent;
            }

            return false;
        }

        internal static bool IsOverridden(
            Transform transform, Object target, string propertyPath )
        {
            while( transform != null )
            {
                PrefabOverrides properties = transform.GetComponent<PrefabOverrides>();

                if( properties != null )
                {
                    if( properties.GetPropertyOverride( target, propertyPath ) != null )
                        return true;
                }

                transform = transform.parent;
            }

            return false;
        }

        internal static bool IsOverridden(
            Transform transform,
            SerializedProperty property,
            out PrefabOverrides overrideProperties )
        {
            overrideProperties = null;
            while( transform != null )
            {
                PrefabOverrides properties = transform.GetComponent<PrefabOverrides>();

                if( properties != null )
                {
                    if( GetPropertyOverride( properties, property ) != null )
                        overrideProperties = properties;
                }

                transform = transform.parent;
            }

            return overrideProperties != null;
        }

        internal static bool IsOverridden(
            PrefabOverrides properties, PropertyInfo property )
        {
            return GetPropertyOverride( properties, property ) != null;
        }

        #endregion


        #region Modify Values

        /// <summary>
        /// Sets the value of the override to the value of the serialized property.
        /// </summary>
        internal static void SetOverrideValue( SerializedProperty @override, SerializedProperty value )
        {
            switch( value.propertyType )
            {
                case SerializedPropertyType.AnimationCurve:
                    @override.FindPropertyRelative( "_animationCurveValue" ).animationCurveValue = value.animationCurveValue;
                    break;
                case SerializedPropertyType.Boolean:
                    @override.FindPropertyRelative( "_booleanValue" ).boolValue = value.boolValue;
                    break;
                case SerializedPropertyType.Bounds:
                    @override.FindPropertyRelative( "_boundsValue" ).boundsValue = value.boundsValue;
                    break;
                case SerializedPropertyType.Color:
                    @override.FindPropertyRelative( "_colorValue" ).colorValue = value.colorValue;
                    break;
                case SerializedPropertyType.Enum:
                    @override.FindPropertyRelative( "_intValue" ).intValue = value.enumValueIndex;
                    break;
                case SerializedPropertyType.Float:
                    @override.FindPropertyRelative( "_floatValue" ).floatValue = value.floatValue;
                    break;
                case SerializedPropertyType.Integer:
                    @override.FindPropertyRelative( "_intValue" ).intValue = value.intValue;
                    break;
                case SerializedPropertyType.LayerMask:
                    @override.FindPropertyRelative( "_intValue" ).intValue = value.intValue;
                    break;
                case SerializedPropertyType.ObjectReference:
                    @override.FindPropertyRelative( "_objectReferenceValue" ).objectReferenceValue = value.objectReferenceValue;
                    break;
                case SerializedPropertyType.Quaternion:
                    @override.FindPropertyRelative( "_quaternionValue" ).quaternionValue = value.quaternionValue;
                    break;
                case SerializedPropertyType.Rect:
                    @override.FindPropertyRelative( "_rectValue" ).rectValue = value.rectValue;
                    break;
                case SerializedPropertyType.String:
                    @override.FindPropertyRelative( "_stringValue" ).stringValue = value.stringValue;
                    break;
                case SerializedPropertyType.Vector2:
                    @override.FindPropertyRelative( "_vector2Value" ).vector2Value = value.vector2Value;
                    break;
                case SerializedPropertyType.Vector3:
                    @override.FindPropertyRelative( "_vector3Value" ).vector3Value = value.vector3Value;
                    break;
                case SerializedPropertyType.Vector4:
                    @override.FindPropertyRelative( "_vector4Value" ).vector4Value = value.vector4Value;
                    break;
                default:
                    throw new InvalidOperationException( "SerializedProperty type is not supported" );
            }
        }

        /// <summary>
        /// Sets the value of the override to the value of the property.
        /// </summary>
        internal static void SetOverrideValue( PropertyOverride @override, SerializedProperty property )
        {
            switch( property.propertyType )
            {
                case SerializedPropertyType.AnimationCurve:
                    @override.SetValue( property.animationCurveValue );
                    break;
                case SerializedPropertyType.Boolean:
                    @override.SetValue( property.boolValue );
                    break;
                case SerializedPropertyType.Bounds:
                    @override.SetValue( property.boundsValue );
                    break;
                case SerializedPropertyType.Character:
                    @override.SetValue( property.intValue );
                    break;
                case SerializedPropertyType.Color:
                    @override.SetValue( property.colorValue );
                    break;
                case SerializedPropertyType.Enum:
                    @override.SetValue( property.enumValueIndex );
                    break;
                case SerializedPropertyType.Float:
                    @override.SetValue( property.floatValue );
                    break;
                case SerializedPropertyType.Integer:
                    @override.SetValue( property.intValue );
                    break;
                case SerializedPropertyType.LayerMask:
                    @override.SetValue( property.intValue );
                    break;
                case SerializedPropertyType.ObjectReference:
                    @override.SetValue( property.objectReferenceValue );
                    break;
                case SerializedPropertyType.Quaternion:
                    @override.SetValue( property.quaternionValue );
                    break;
                case SerializedPropertyType.Rect:
                    @override.SetValue( property.rectValue );
                    break;
                case SerializedPropertyType.String:
                    @override.SetValue( property.stringValue );
                    break;
                case SerializedPropertyType.Vector2:
                    @override.SetValue( property.vector2Value );
                    break;
                case SerializedPropertyType.Vector3:
                    @override.SetValue( property.vector3Value );
                    break;
                case SerializedPropertyType.Vector4:
                    @override.SetValue( property.vector4Value );
                    break;
            }
        }

        /// <summary>
        /// Sets the value of the property to the value of the override.
        /// </summary>
        internal static bool SetPropertyValue(
            SerializedProperty property, PropertyOverride @override )
        {
            switch( property.propertyType )
            {
                case SerializedPropertyType.AnimationCurve:
                    if( property.animationCurveValue == @override.AnimationCurveValue )
                        return false;

                    property.animationCurveValue = @override.AnimationCurveValue;
                    break;
                case SerializedPropertyType.Boolean:
                    if( property.boolValue == @override.BooleanValue )
                        return false;

                    property.boolValue = @override.BooleanValue;
                    break;
                case SerializedPropertyType.Bounds:
                    if( property.boundsValue == @override.BoundsValue )
                        return false;

                    property.boundsValue = @override.BoundsValue;
                    break;
                case SerializedPropertyType.Character:
                    if( property.intValue == @override.IntValue )
                        return false;

                    property.intValue = @override.IntValue;
                    break;
                case SerializedPropertyType.Color:
                    if( property.colorValue == @override.ColorValue )
                        return false;

                    property.colorValue = @override.ColorValue;
                    break;
                case SerializedPropertyType.Enum:
                    if( property.enumValueIndex == @override.IntValue )
                        return false;

                    property.enumValueIndex = @override.IntValue;
                    break;
                case SerializedPropertyType.Float:
                    if( property.floatValue == @override.FloatValue )
                        return false;

                    property.floatValue = @override.FloatValue;
                    break;
                case SerializedPropertyType.Integer:
                    if( property.intValue == @override.IntValue )
                        return false;

                    property.intValue = @override.IntValue;
                    break;
                case SerializedPropertyType.LayerMask:
                    if( property.intValue == @override.IntValue )
                        return false;

                    property.intValue = @override.IntValue;
                    break;
                case SerializedPropertyType.ObjectReference:
                    if( property.objectReferenceValue == @override.ObjectReferenceValue )
                        return false;

                    property.objectReferenceValue = @override.ObjectReferenceValue;
                    break;
                case SerializedPropertyType.Quaternion:
                    if( property.quaternionValue == @override.QuaternionValue )
                        return false;

                    property.quaternionValue = @override.QuaternionValue;
                    break;
                case SerializedPropertyType.Rect:
                    if( property.rectValue == @override.RectValue )
                        return false;

                    property.rectValue = @override.RectValue;
                    break;
                case SerializedPropertyType.String:
                    if( property.stringValue == @override.StringValue )
                        return false;

                    property.stringValue = @override.StringValue;
                    break;
                case SerializedPropertyType.Vector2:
                    if( property.vector2Value == @override.Vector2Value )
                        return false;

                    property.vector2Value = @override.Vector2Value;
                    break;
                case SerializedPropertyType.Vector3:
                    if( property.vector3Value == @override.Vector3Value )
                        return false;

                    property.vector3Value = @override.Vector3Value;
                    break;
                case SerializedPropertyType.Vector4:
                    if( property.vector4Value == @override.Vector4Value )
                        return false;

                    property.vector4Value = @override.Vector4Value;
                    break;
            }

            return true;
        }

        #endregion

        #region Check Supported

        /// <summary>
        /// Checks if overriding a property in the component is supported.
        /// </summary>
        internal static bool CanOverrideComponent(
            Transform root, SerializedObject component )
        {
            return CanOverrideComponent( root, component.targetObject as Component );
        }

        /// <summary>
        /// Checks if overriding a property in the component is supported.
        /// </summary>
        internal static bool CanOverrideComponent(
            Transform root, Component component )
        {
            if( component == null )
                return false;

            if( UnsupportedComponents.Contains( component.GetType().FullName ) )
                return false;

            if( component.GetType() == typeof( Transform )
                || component.GetType() == typeof( RectTransform )
                || component.GetType().GetCustomAttributes( typeof( SaveInParent ), true ).Length > 0 )
            {
                Prefab prefab = component.GetComponent<Prefab>();
                if( !Prefab.IsNull( prefab ) && prefab.CachedTransform == root )
                    return false;
            }

            return true;
        }

        /// <summary>
        /// Checks if overriding the proprerty is supported.
        /// </summary>
        /// <param name="property"></param>
        /// <returns>True if the property can be overridden.</returns>
        internal static bool CanOverrideProperty(
            Transform root, SerializedObject component, SerializedProperty property )
        {
            if( property == null )
                return false;

            if( UnsupportedProperties.Contains( property.propertyPath ) )
                return false;

            if( UnsupportedProperties.Contains( property.serializedObject.targetObject.GetType().FullName ) )
                return false;

            if( UnsupportedProperties.Contains( property.serializedObject.targetObject.GetType().FullName + "." + property.propertyPath ) )
                return false;

            if( property.propertyPath == "m_Name" || property.propertyPath == "m_IsActive" )
            {
                GameObject prefab = component.targetObject as GameObject;
                if( prefab != null && prefab.transform == root )
                    return false;
            }

            return true;
        }

        #endregion
    }
}