﻿using System.Text;

namespace VisualDesignCafe.Editor.Prefabs.Overrides
{
    internal static class PrefabOverrideGuiUtility
    {
        /// <summary>
        /// Returns a string containing all overrides of the object.
        /// </summary>
        internal static string ListOverridesAsString(
            PrefabOverrides prefabOverrides )
        {
            var result = new StringBuilder();

            foreach( var objectOverride in prefabOverrides.Objects )
            {
                if( objectOverride.Target == null )
                    continue;

                foreach( var componentOverride in objectOverride.Components )
                {
                    if( componentOverride.Target == null )
                        continue;

                    result.AppendLine( componentOverride.Type );

                    foreach( var propertyOverride in componentOverride.Properties )
                    {
                        result.Append( "    " );
                        result.AppendLine( propertyOverride.Path );
                    }
                }
            }

            return result.ToString();
        }
    }
}