﻿namespace VisualDesignCafe.Editor.Prefabs.Overrides
{
    internal interface IDrawer
    {
        string DisplayName { get; }
        object Target { get; }
        int ChildCount { get; }
        IDrawer[] Children { get; }
        void OnGui( int depth, ref IDrawer clickedItem );
        bool IsExpanded { get; set; }
        bool IsVisible { get; set; }
        bool IsEditable { get; set; }
        bool HasChildren { get; }
        bool BackgroundIsVisible { get; set; }
        int Depth { get; }

        void Order( bool recursive );
        void Collapse( bool recursive );
        void Expand( bool recursive );

        IDrawer GetChildAt( int index );
    }
}