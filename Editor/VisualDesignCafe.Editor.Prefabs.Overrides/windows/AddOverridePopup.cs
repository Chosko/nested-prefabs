﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using VisualDesignCafe.Editor.Prefabs.Comparing;

namespace VisualDesignCafe.Editor.Prefabs.Overrides
{
    public class AddOverridePopup : PopupWindowContent
    {
        /// <summary>
        /// The current or last active Prefab Overrides Inspector.
        /// </summary>
        public static PrefabOverridesInspector ActiveInspector
        {
            get;
            private set;
        }

        public static NestedPrefabsPostprocessor Postprocessor
        {
            get;
            set;
        }

        public readonly PrefabOverrides PrefabOverrides;
        public readonly PrefabOverridesInspector Inspector;

        private float _maximumWidth = 0.0f;
        private float _maxiumHeight = 330.0f;
        private float _height = 1;
        private float _time = 0;

        private string _searchString = string.Empty;
        private bool _isFirstUpdate = true;
        private Vector2 _scroll;
        private HierarchyDrawer _hierarchyDrawer;

        /// <summary>
        /// Minimum visible depth.
        /// All items with a lower depth are hidden because they did not match the search filter.
        /// </summary>
        private int _minimumDepth = 0;

        /// <summary>
        /// Currently selected tab. (0 = Default List, 1 = Modified Properties)
        /// </summary>
        private int _selectedTab = 0;

        /// <summary>
        /// Should all modified properties be added when clicking Add?
        /// </summary>
        private bool _addAllModifiedProperties = false;

        /// <summary>
        /// Current search filter.
        /// </summary>
        private PropertyFilter _cachedFilter;

        private Stack<IDrawer> _navigationHistory = new Stack<IDrawer>();

        public AddOverridePopup(
            PrefabOverridesInspector inspector,
            PrefabOverrides properties,
            float width )
        {
            ActiveInspector = inspector;
            Inspector = inspector;
            PrefabOverrides = properties;

            _maximumWidth = width;
            _height = 1;
            _time = Time.realtimeSinceStartup;
            _hierarchyDrawer = new HierarchyDrawer( properties );

            CacheHierarchy();
        }

        public override Vector2 GetWindowSize()
        {
            return new Vector2( _maximumWidth, _height );
        }

        public override void OnGUI( Rect rect )
        {
            AnimateHeight();
            DrawTab();

            switch( _selectedTab )
            {
                case 0:

                    DrawSearchField( true );
                    if( _navigationHistory.Count > 0 )
                        DrawHeader( _navigationHistory.Peek().DisplayName );
                    else
                        DrawHeader( PrefabOverrides.gameObject.name );
                    DrawScrollView();

                    break;
                case 1:

                    DrawSearchField( false );
                    DrawAddButton();
                    DrawScrollView();

                    break;
            }

            editorWindow.Repaint();
        }

        private void AnimateHeight()
        {
            float deltaTime = Time.realtimeSinceStartup - _time;
            _time = Time.realtimeSinceStartup;

            _height += deltaTime * 2000.0f;
            if( _height > _maxiumHeight )
                _height = _maxiumHeight;
        }

        private void DrawAddButton()
        {
            GUILayout.Space( -12f );
            GUILayout.BeginHorizontal(
                InternalEditorStyles.InspectorTitlebar, GUILayout.Height( 12f ) );
            {
                EditorGUI.BeginChangeCheck();
                {
                    _addAllModifiedProperties =
                        GUILayout.Toggle( _addAllModifiedProperties, string.Empty );
                }
                if( EditorGUI.EndChangeCheck() )
                {
                    ToggleAll( _addAllModifiedProperties );
                }

                if( GUILayout.Button( "Add", EditorStyles.miniButton ) )
                {
                    AddSelectedPropertiesRecursive( _hierarchyDrawer );
                    Inspector.serializedObject.ApplyModifiedProperties();

                    Postprocessor.HierarchyWindowPostprocessor.OnSelectionChanged();
                    EditorApplication.RepaintHierarchyWindow();

                    Inspector.Refresh();
                    editorWindow.Close();
                }
            }
            GUILayout.EndHorizontal();
        }


        private void ToggleAll( bool isToggled )
        {
            ToggleRecursive( _hierarchyDrawer, isToggled );
        }

        private void ToggleRecursive( IDrawer drawer, bool isToggled )
        {
            var objectDrawer = drawer as ToggleObjectDrawer;
            if( objectDrawer != null )
            {
                objectDrawer.IsToggled = isToggled;

                for( int i = 0; i < objectDrawer.ComponentCount; i++ )
                {
                    ToggleRecursive(
                        objectDrawer.GetComponentAt( i ),
                        isToggled );
                }
            }

            var componentDrawer = drawer as ToggleComponentDrawer;
            if( componentDrawer != null )
            {
                componentDrawer.IsToggled = isToggled;
            }

            var propertyDrawer = drawer as TogglePropertyDrawer;
            if( propertyDrawer != null )
            {
                propertyDrawer.IsToggled = isToggled;
            }

            for( int i = 0; i < drawer.ChildCount; i++ )
            {
                ToggleRecursive( drawer.GetChildAt( i ), isToggled );
            }
        }

        private void AddSelectedPropertiesRecursive( IDrawer drawer )
        {
            var property = drawer as TogglePropertyDrawer;
            if( property != null && property.IsToggled )
            {
                PrefabOverrideUtility.SetPropertyOverride(
                    Inspector.serializedObject,
                    property.Target,
                    true,
                    true );
            }

            var objectDrawer = drawer as ToggleObjectDrawer;
            if( objectDrawer != null )
            {
                for( int i = 0; i < objectDrawer.ComponentCount; i++ )
                {
                    AddSelectedPropertiesRecursive(
                        objectDrawer.GetComponentAt( i ) );
                }
            }

            for( int i = 0; i < drawer.ChildCount; i++ )
            {
                AddSelectedPropertiesRecursive( drawer.GetChildAt( i ) );
            }
        }

        private void DrawTab()
        {
            GUILayout.BeginHorizontal(
                GUILayout.Width( _maximumWidth ),
                GUILayout.Height( 20 ) );
            {
                EditorGUI.BeginChangeCheck();
                {
                    _selectedTab =
                        GUILayout.Toolbar(
                            _selectedTab,
                            new string[] { "Hierarchy", "Modified Properties" },
                            EditorStyles.toolbarButton );
                }
                if( EditorGUI.EndChangeCheck() )
                {
                    _navigationHistory.Clear();

                    if( _selectedTab == 1 )
                    {
                        CacheModifiedProperties();
                    }
                    else
                    {
                        CacheHierarchy();
                    }
                }
            }
            GUILayout.EndHorizontal();
        }

        private void DrawSearchField( bool isEnabled )
        {
            if( !isEnabled )
                GUI.enabled = false;

            EditorGUI.BeginChangeCheck();
            {
                GUILayout.Space( 5 );
                GUILayout.BeginHorizontal( GUILayout.Width( _maximumWidth - 5 - 5 ) );
                {
                    GUILayout.Space( 7 );

                    GUI.SetNextControlName( "SearchField" );
                    _searchString =
                        GUILayout.TextField(
                            _searchString,
                            InternalEditorStyles.SearchField,
                            GUILayout.Height( 30 ) );

                    if( GUILayout.Button(
                        "",
                        string.IsNullOrEmpty( _searchString )
                            ? InternalEditorStyles.SearchFieldCancelButtonEmpty
                            : InternalEditorStyles.SearchFieldCancelButton ) )
                    {
                        _searchString = "";
                        GUI.FocusControl( null );
                    }
                }
                GUILayout.EndHorizontal();
            }
            if( EditorGUI.EndChangeCheck() )
            {
                ApplyFilter( _searchString, out _minimumDepth );

                if( !string.IsNullOrEmpty( _searchString ) )
                {
                    _hierarchyDrawer.Expand( true );
                }
                else
                {
                    _hierarchyDrawer.Collapse( true );
                }
            }

            if( _isFirstUpdate )
            {
                GUI.FocusControl( "SearchField" );
                _isFirstUpdate = false;
            }

            GUI.enabled = true;
        }

        private void DrawHeader( string title )
        {
            GUILayout.Space( -12 );
            if( GUILayout.Button(
                title,
                InternalEditorStyles.InspectorTitlebar,
                GUILayout.Width( _maximumWidth ) ) )
            {
                if( _navigationHistory.Count > 0 )
                    OnItemClicked( _navigationHistory.Peek() );
            }
            GUILayout.Space( -4 );
        }

        private void DrawScrollView()
        {
            _scroll =
                GUILayout.BeginScrollView(
                    _scroll,
                    InternalEditorStyles.Empty,
                    GUILayout.Width( _maximumWidth ) );

            IDrawer clicked = null;

            if( _navigationHistory.Count > 0 )
            {
                _navigationHistory.Peek().OnGui( 0, ref clicked );
            }
            else
            {
                if( _minimumDepth < int.MaxValue && !string.IsNullOrEmpty( _searchString ) )
                    _hierarchyDrawer.OnGui( -_minimumDepth + 1, ref clicked );
                else
                    _hierarchyDrawer.OnGui( ref clicked );
            }

            if( clicked != null )
                OnItemClicked( clicked );

            GUILayout.EndScrollView();
        }

        /// <summary>
        /// Called when the user clicked on an item in the hierarchy.
        /// </summary>
        private void OnItemClicked( IDrawer item )
        {
            if( item == null )
                return;

            // Add property override if the item is a PropertyDrawer.
            if( !item.HasChildren
                && item is PropertyDrawer
                && ( (PropertyDrawer) item ).Target.propertyType != SerializedPropertyType.Generic )
            {
                PrefabOverrideUtility.SetPropertyOverride(
                    Inspector.serializedObject,
                    ( (PropertyDrawer) item ).Target,
                    true,
                    true );

                Inspector.serializedObject.ApplyModifiedProperties();
                Postprocessor.HierarchyWindowPostprocessor.OnSelectionChanged();
                EditorApplication.RepaintHierarchyWindow();
                Inspector.Refresh();
                editorWindow.Close();
                return;
            }

            bool isSelected =
                _navigationHistory.Count > 0 && _navigationHistory.Peek() == item;

            if( isSelected )
            {
                var objectDrawer = item as ObjectDrawer;
                if( objectDrawer != null )
                {
                    item.IsExpanded = true;
                    objectDrawer.ComponentsAreExpanded = false;
                }
                else
                {
                    item.IsExpanded = false;
                }

                item.IsVisible = true;

                _navigationHistory.Pop();
            }
            else
            {
                var objectDrawer = item as ObjectDrawer;
                if( objectDrawer != null )
                {
                    item.IsExpanded = false;
                    objectDrawer.ComponentsAreExpanded = true;
                }
                else
                {
                    item.IsExpanded = true;
                }

                item.IsVisible = false;

                _navigationHistory.Push( item );
            }
        }

        private void CacheHierarchy()
        {
            _hierarchyDrawer.Clear();

            var drawer =
                _hierarchyDrawer.GetChild<ObjectDrawer>(
                    PrefabOverrides.gameObject,
                    true );

            drawer.Padding = new RectOffset( -3, 3, 0, 0 );
            drawer.ComponentsAreExpanded = false;

            CacheComponents( drawer, PrefabOverrides.transform );

            foreach( Transform child in PrefabOverrides.transform )
            {
                // Skip objects that do not have a GUID, they are not part of the prefab.
                if( child.GetComponent<Guid>() == null )
                    continue;

                CacheHierarchyRecursive( child, drawer, PrefabOverrides.transform );
            }
        }

        private void CacheHierarchyRecursive(
            Transform transform, ObjectDrawer drawer, Transform root )
        {
            drawer = drawer.GetChild<ObjectDrawer>( transform.gameObject, true );
            drawer.Padding = new RectOffset( -3, 3, 0, 0 );
            drawer.ComponentsAreExpanded = false;

            CacheComponents( drawer, root );

            foreach( Transform child in transform )
            {
                // Skip objects that do not have a GUID, they are not part of the prefab.
                if( child.GetComponent<Guid>() == null )
                    continue;

                CacheHierarchyRecursive( child, drawer, root );
            }
        }

        /// <summary>
        /// Adds all components on the object. Uses the ObjectDrawer's target as target GameObject.
        /// </summary>
        private void CacheComponents( ObjectDrawer drawer, Transform root )
        {
            // Add GameObject properties.
            var componentDrawer =
                drawer.GetComponentDrawer<ComponentDrawer>(
                    new SerializedObject( drawer.Target ),
                    true );

            componentDrawer.Padding = new RectOffset( -3, 3, 0, 0 );
            componentDrawer.IsExpanded = false;

            CacheProperties( componentDrawer, root );

            // Add components on GameObject.
            Component[] components = drawer.Target.GetComponents<Component>();

            foreach( Component component in components )
            {
                if( !PrefabOverrideUtility.CanOverrideComponent( root, component ) )
                    continue;

                componentDrawer =
                    drawer.GetComponentDrawer<ComponentDrawer>(
                        new SerializedObject( component ),
                        true );

                componentDrawer.Padding = new RectOffset( -3, 3, 0, 0 );
                componentDrawer.IsExpanded = false;

                CacheProperties( componentDrawer, root );
            }
        }

        /// <summary>
        /// Adds all the properties in the component. Uses the ComponentDrawer's target as target Component.
        /// </summary>
        /// <param name="drawer"></param>
        private void CacheProperties( ComponentDrawer drawer, Transform root )
        {
            int LOOP_LIMIT = 999;
            int index = 0;

            bool hasNext = false;

            try
            {
                System.Type componentType = drawer.Target.targetObject.GetType();

                CacheProperty( root, drawer, drawer.Target.FindProperty( "m_Enabled" ), ref hasNext );

                if( componentType == typeof( SpriteRenderer ) )
                {
                    CacheProperty( root, drawer, drawer.Target.FindProperty( "m_SortingOrder" ), ref hasNext );
                    CacheProperty( root, drawer, drawer.Target.FindProperty( "m_SortingLayerID" ), ref hasNext );
                }
                else if( componentType == typeof( MeshRenderer ) )
                {
                    CacheProperty( root, drawer, drawer.Target.FindProperty( "m_AutoUVMaxDistance" ), ref hasNext );
                    CacheProperty( root, drawer, drawer.Target.FindProperty( "m_AutoUVMaxAngle" ), ref hasNext );
                    CacheProperty( root, drawer, drawer.Target.FindProperty( "m_IgnoreNormalsForChartDetection" ), ref hasNext );
                    CacheProperty( root, drawer, drawer.Target.FindProperty( "m_MinimumChartSize" ), ref hasNext );
                    CacheProperty( root, drawer, drawer.Target.FindProperty( "m_ScaleInLightmap" ), ref hasNext );
                    CacheProperty( root, drawer, drawer.Target.FindProperty( "m_ImportantGI" ), ref hasNext );
                }
                else if( componentType == typeof( GameObject ) )
                {
                    if( drawer.Target.targetObject != PrefabOverrides.gameObject )
                        CacheProperty( root, drawer, drawer.Target.FindProperty( "m_IsActive" ), ref hasNext );
                }

                SerializedProperty property = drawer.Target.GetIterator();
                hasNext = property.NextVisible( true );

                if( !hasNext )
                    return;

                do
                {
                    if( index++ > LOOP_LIMIT )
                    {
                        throw new System.StackOverflowException( "Loop limit reached" );
                    }

                    // Skip this property if it does not support overrides.
                    if( !PrefabOverrideUtility.CanOverrideProperty( root, drawer.Target, property ) )
                    {
                        hasNext = property.NextVisible( false );
                        continue;
                    }

                    CacheProperty( root, drawer, property, ref hasNext );
                }
                while( hasNext );
            }
            catch( System.Exception e )
            {
                Debug.LogException( e );
            }
        }

        /// <summary>
        /// Adds the property and children to the drawer. 
        /// </summary>
        /// <param name="property"></param>
        /// <param name="drawer"></param>
        /// <param name="hasNext"></param>
        /// <returns>True if the property iterator moved to the next property</returns>
        private bool CacheProperties(
            Transform root,
            IDrawer parentDrawer,
            SerializedProperty property,
            PropertyDrawer drawer,
            ref bool hasNext )
        {
            if( !property.hasChildren && !property.isArray )
                return false;

            if( !PrefabOverrideUtility.CanOverrideProperty(
                root,
                property.serializedObject,
                property ) )
            {
                return false;
            }

            if( property.isArray && property.propertyType == SerializedPropertyType.Generic )
                return CacheArray( root, parentDrawer, property, drawer, ref hasNext );

            bool addSelf = false;
            SerializedProperty rootProperty = null;

            switch( property.propertyType )
            {
                case SerializedPropertyType.ObjectReference:
                case SerializedPropertyType.AnimationCurve:
                case SerializedPropertyType.Color:
                case SerializedPropertyType.Gradient:
                case SerializedPropertyType.Quaternion:
                    return false;

                case SerializedPropertyType.Rect:
                case SerializedPropertyType.Vector2:
                case SerializedPropertyType.Vector3:
                case SerializedPropertyType.Vector4:
                    addSelf = true;
                    rootProperty = property;
                    break;
                case SerializedPropertyType.String:
                    if( property.isArray )
                    {
                        return false;
                    }
                    break;
            }

            if( addSelf )
                drawer.GetChild<PropertyDrawer>( drawer, rootProperty, null, true );

            try
            {
                string path = property.propertyPath;
                hasNext = property.NextVisible( true );

                if( !hasNext )
                    return true;

                int LOOP_LIMIT = 999;
                int index = 0;
                do
                {
                    if( index++ > LOOP_LIMIT )
                        throw new System.StackOverflowException( "Loop limit reached" );

                    if( !property.propertyPath.StartsWith(
                            path,
                            System.StringComparison.Ordinal ) )
                    {
                        break;
                    }

                    CacheProperty( root, drawer, property, ref hasNext );
                }
                while( hasNext );
            }
            catch( System.Exception e )
            {
                Debug.LogException( e );
            }

            return true;
        }

        private void CacheProperty(
            Transform root,
            IDrawer drawer,
            SerializedProperty property,
            ref bool hasNext )
        {
            if( property == null )
                return;

            var componentDrawer = drawer as ComponentDrawer;
            var parentPropertyDrawer = drawer as PropertyDrawer;

            PropertyDrawer propertyDrawer =
                componentDrawer != null
                    ? componentDrawer.GetChild<PropertyDrawer>(
                        property,
                        null,
                        true )
                    : parentPropertyDrawer.GetChild<PropertyDrawer>(
                        drawer,
                        property,
                        null,
                        true );

            propertyDrawer.Padding = new RectOffset( -3, 3, 0, 0 );
            propertyDrawer.IsExpanded = false;

            // Add sub properties.
            if( !CacheProperties(
                    root, drawer, property, propertyDrawer, ref hasNext ) )
            {
                hasNext = property.NextVisible( false );
            }
        }

        private bool CacheArray(
            Transform root,
            IDrawer parentDrawer,
            SerializedProperty property,
            PropertyDrawer drawer,
            ref bool hasNext )
        {
            if( property.arraySize == 0 )
                return false;

            bool dummyHasNext = false;

            for( int i = 0; i < property.arraySize; i++ )
            {
                SerializedProperty arrayItem =
                    property.GetArrayElementAtIndex( i );

                var propertyDrawer =
                    drawer.GetChild<PropertyDrawer>(
                        parentDrawer, arrayItem, null, true );

                propertyDrawer.Padding = new RectOffset( -3, 3, 0, 0 );
                propertyDrawer.IsExpanded = false;

                CacheProperties(
                    root,
                    propertyDrawer,
                    arrayItem,
                    propertyDrawer,
                    ref dummyHasNext );
            }

            return false;
        }

        private void ApplyFilter( string filter, out int depth )
        {
            _cachedFilter = new PropertyFilter( filter );

            depth = int.MaxValue;
            for( int i = 0; i < _hierarchyDrawer.ChildCount; i++ )
            {
                depth =
                    Mathf.Min(
                        ApplyFilter(
                            _hierarchyDrawer.GetChildAt( i ),
                            filter,
                            string.Empty ),
                        depth );
            }
        }

        private int ApplyFilter( IDrawer drawer, string filter, string path )
        {
            int minDepth = int.MaxValue;

            if( drawer == null )
                return minDepth;

            var objectDrawer = drawer as ObjectDrawer;
            if( objectDrawer != null )
            {
                for( int j = 0; j < objectDrawer.ComponentCount; j++ )
                {
                    minDepth =
                        Mathf.Min(
                            minDepth,
                            ApplyFilter(
                                objectDrawer.GetComponentAt( j ),
                                filter,
                                drawer.DisplayName ) );
                }
            }

            for( int i = 0; i < drawer.ChildCount; i++ )
            {
                minDepth =
                    Mathf.Min(
                        minDepth,
                        ApplyFilter(
                            drawer.GetChildAt( i ),
                            filter,
                            path + "." + drawer.DisplayName ) );
            }

            if( string.IsNullOrEmpty( filter ) )
            {
                drawer.IsVisible = true;
                return minDepth;
            }

            path += "." + drawer.DisplayName;

            drawer.IsVisible = _cachedFilter.IsMatch( path );

            if( drawer.IsVisible )
                minDepth = Mathf.Min( minDepth, drawer.Depth );

            return minDepth;
        }

        private void CacheModifiedProperties()
        {
            _hierarchyDrawer.Clear();
            _addAllModifiedProperties = false;
            ModificationList modifications =
                NestedPrefabUtility.GetModificationsForInstance(
                    PrefabOverrides.GetComponent<Prefab>(),
                    true );

            foreach( Modification modification in modifications )
            {
                if( modification.Type != ModificationType.PropertyValueChanged )
                    continue;

                if( !PrefabOverrideUtility.CanOverrideProperty(
                        PrefabHierarchyUtility.GetPrefab(
                            modification.DestinationObject.targetObject.AsGameObject() ).CachedTransform,
                        modification.DestinationObject,
                        modification.DestinationProperty ) )
                {
                    continue;
                }

                if( modification.DestinationProperty.propertyType == SerializedPropertyType.Generic )
                    continue;

                if( PrefabOverrideUtility.IsOverridden(
                    PrefabOverrides.transform,
                    modification.DestinationProperty ) )
                {
                    continue;
                }

                Component component = modification.DestinationObject.targetObject as Component;
                GameObject gameObject = null;

                if( component != null )
                {
                    gameObject = component.gameObject;
                }
                else
                {
                    gameObject = modification.DestinationObject.targetObject as GameObject;
                }

                ObjectDrawer objectDrawer =
                    _hierarchyDrawer.GetChild<ToggleObjectDrawer>(
                        gameObject, true );

                objectDrawer.Padding = new RectOffset( -3, 3, 0, 0 );
                objectDrawer.ComponentsAreExpanded = true;

                ComponentDrawer componentDrawer =
                    objectDrawer.GetComponentDrawer<ToggleComponentDrawer>(
                        modification.DestinationObject, true );

                componentDrawer.Padding = new RectOffset( -3, 3, 0, 0 );
                componentDrawer.IsExpanded = true;
                componentDrawer.SetDepth( 1 );

                PropertyDrawer propertyDrawer =
                    componentDrawer.GetChild<TogglePropertyDrawer>(
                        modification.DestinationProperty, null, true );

                propertyDrawer.Padding = new RectOffset( -3, 3, 0, 0 );
                propertyDrawer.IsExpanded = false;
                propertyDrawer.SetDepth( 2 );
            }
        }
    }
}