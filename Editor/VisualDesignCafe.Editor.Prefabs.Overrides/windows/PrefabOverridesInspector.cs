﻿using UnityEditor;
using UnityEngine;

namespace VisualDesignCafe.Editor.Prefabs.Overrides
{

    [CustomEditor( typeof( PrefabOverrides ) )]
    public class PrefabOverridesInspector : UnityEditor.Editor
    {

        public static NestedPrefabsPostprocessor Postprocessor
        {
            get;
            set;
        }

        /// <summary>
        /// The Prefab Overrides component being inspected.
        /// </summary>
        public PrefabOverrides Target
        {
            get { return target as PrefabOverrides; }
        }

        private HierarchyDrawer _hierarchyDrawer;
        private SerializedProperty _clickedProperty = null;
        private bool _isFirstUpdate = true;
        private bool _shouldRefresh = false;
        private Rect _popupArea;

        /// <summary>
        /// Refreshes the cached prefab overrides in the Inspector.
        /// </summary>
        public void Refresh()
        {
            _hierarchyDrawer.Clear();

            foreach( var obj in Target.Objects )
            {
                foreach( var component in obj.Components )
                {
                    var serializedComponent = new SerializedObject( component.Target );
                    foreach( var property in component.Properties )
                    {
                        ObjectDrawer gameObjectDrawer =
                            _hierarchyDrawer
                                .GetChild<InspectorObjectDrawer>(
                                    component.Target.AsGameObject(),
                                    true );

                        gameObjectDrawer.Padding = new RectOffset( -17, 0, 0, 0 );
                        gameObjectDrawer.IsEditable = true;

                        ComponentDrawer componentDrawer =
                            gameObjectDrawer
                                .GetComponentDrawer<InspectorComponentDrawer>(
                                    serializedComponent,
                                    true );

                        componentDrawer.Padding = new RectOffset( -17, 0, 0, 0 );
                        componentDrawer.IsEditable = true;

                        PropertyDrawer propertyDrawer =
                            componentDrawer.GetChild<InspectorPropertyDrawer>(
                                serializedComponent.FindProperty( property.Path ),
                                property,
                                true );

                        propertyDrawer.Padding = new RectOffset( -17, 0, 0, 0 );
                        propertyDrawer.IsEditable = true;
                    }
                }
            }

            _hierarchyDrawer.Order( true );

            // TODO: Fix this, it should be possible to properly draw all fields in the first frame.
            // Force an additional repaint so the GUI elements are drawn correctly after loading.
            // The first frame they are messed up because they have to cache the last rect that was drawn.
            _isFirstUpdate = true;
        }

        /// <summary>
        /// Called every frame by Unity to draw the GUI.
        /// </summary>
        public override void OnInspectorGUI()
        {
            GUILayout.Space( 3 );

            EditorGUI.BeginChangeCheck();
            {
                using( new GUILayout.VerticalScope( EditorStyles.inspectorFullWidthMargins ) )
                {
                    DrawOverridesList();
                    DrawAddOverrideButton();
                }
            }
            if( EditorGUI.EndChangeCheck() )
            {
                serializedObject.ApplyModifiedProperties();
            }
            GUILayout.Space( 5 );

            if( _isFirstUpdate )
            {
                _isFirstUpdate = false;
                Repaint();
            }

            if( _shouldRefresh && Event.current.type == EventType.Repaint )
            {
                _shouldRefresh = false;

                if( _clickedProperty != null )
                    HandlePropertyClicked( _clickedProperty );

                Refresh();
                Repaint();
            }
        }

        /// <summary>
        /// Called by Unity when the inspector was created.
        /// </summary>
        private void OnEnable()
        {
            _hierarchyDrawer = new HierarchyDrawer( Target );
            Undo.undoRedoPerformed -= OnUndoRedoPerformed;
            Undo.undoRedoPerformed += OnUndoRedoPerformed;

            Refresh();
            Repaint();
        }

        /// <summary>
        /// Called by Unity when the inspector was destroyed.
        /// </summary>
        private void OnDestroy()
        {
            Undo.undoRedoPerformed -= OnUndoRedoPerformed;
        }

        /// <summary>
        /// Draws the list of prefab overrides.
        /// </summary>
        private void DrawOverridesList()
        {
            IDrawer clickedItem = null;
            _hierarchyDrawer.OnGui( ref clickedItem );

            if( !_shouldRefresh && InspectorPropertyDrawer.TargetDisappeared )
            {
                InspectorPropertyDrawer.TargetDisappeared = false;
                _shouldRefresh = true;

                if( Config.DEBUG_WARNING )
                    Console.LogWarning( "Prefab override target property disappeared. Inspector should refresh its cached data." );
            }

            HandleItemClicked( clickedItem );
        }

        /// <summary>
        /// Gets called when any item in the hierarchy drawer was clicked.
        /// </summary>
        private void HandleItemClicked( IDrawer clickedItem )
        {
            if( clickedItem == null )
                return;

            if( clickedItem is ObjectDrawer )
            {
                ( (ObjectDrawer) clickedItem ).ComponentsAreExpanded =
                    !( (ObjectDrawer) clickedItem ).ComponentsAreExpanded;
            }
            else if( clickedItem is ComponentDrawer )
            {
                clickedItem.IsExpanded = !clickedItem.IsExpanded;
            }
            else
            {
                PropertyDrawer propertyDrawer = clickedItem as PropertyDrawer;
                _clickedProperty = propertyDrawer.Target;

                _shouldRefresh = true;
            }
        }

        /// <summary>
        /// Gets called when a property was clicked. 
        /// In the Inspector a property is clicked when the user chooses "Remove" from the context menu.
        /// </summary>
        /// <param name="property">The property that should be removed.</param>
        private void HandlePropertyClicked( SerializedProperty property )
        {
            if( property == null )
                return;

            bool revertValue = false;

            Prefab prefab =
                PrefabHierarchyUtility.GetPrefab(
                    property.serializedObject.targetObject.AsGameObject() );

            if( !Prefab.IsNullOrMissing( prefab ) )
            {
                revertValue = EditorUtility.DisplayDialog(
                        "Revert Property Value?",
                        "Do you want to revert the property value to the prefab value?",
                        "Revert",
                        "Keep Value" );
            }

            Undo.IncrementCurrentGroup();
            Undo.SetCurrentGroupName( "Remove Prefab Override" );

            switch( property.propertyType )
            {
                case SerializedPropertyType.Rect:
                case SerializedPropertyType.Vector2:
                case SerializedPropertyType.Vector3:
                case SerializedPropertyType.Vector4:
                case SerializedPropertyType.Quaternion:
                    PrefabOverrideUtility.SetPropertyOverride(
                        serializedObject,
                        property,
                        false,
                        true );
                    break;

                default:
                    PrefabOverrideUtility.SetPropertyOverride(
                        serializedObject,
                        property,
                        false );
                    break;
            }

            if( revertValue )
            {
                if( !NestedPrefabUtility.RevertProperty( property ) )
                    Debug.LogWarning( "Could not revert property." );
            }

            serializedObject.ApplyModifiedProperties();

            Undo.CollapseUndoOperations( Undo.GetCurrentGroup() );
            Undo.IncrementCurrentGroup();

            Postprocessor.HierarchyWindowPostprocessor.OnSelectionChanged();
            EditorApplication.RepaintHierarchyWindow();
        }

        /// <summary>
        /// Draws the "Add Override" button and opens a popup when it is clicked.
        /// </summary>
        private void DrawAddOverrideButton()
        {
            if( GUILayout.Button( "Add Override" ) )
            {
                PopupWindow.Show(
                    _popupArea,
                    new AddOverridePopup( this, Target, _popupArea.width ) );
            }

            if( Event.current.type == EventType.Repaint )
                _popupArea = GUILayoutUtility.GetLastRect();
        }

        private void OnUndoRedoPerformed()
        {
            if( serializedObject == null )
                return;

            serializedObject.Update();
            Refresh();
            Repaint();
        }
    }
}