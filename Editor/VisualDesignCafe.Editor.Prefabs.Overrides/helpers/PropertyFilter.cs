﻿using System.Text.RegularExpressions;

namespace VisualDesignCafe.Editor.Prefabs.Overrides
{
    public struct PropertyFilter
    {
        private string[] _filterList;

        public PropertyFilter( string filter )
        {
            _filterList = filter.Split( '.' );
        }

        /// <summary>
        /// Does the full property path match with the filter?
        /// </summary>
        /// <returns>True if the path matches with the filter.</returns>
        public bool IsMatch( string fullPropertyPath )
        {
            string[] fullPath = fullPropertyPath.Split( '.' );

            foreach( string f in _filterList )
            {
                if( !IsMatchSingle( fullPath, f ) )
                    return false;
            }

            return true;
        }

        /// <summary>
        /// Checks if the split path matches with a single value of the filter.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        private bool IsMatchSingle( string[] source, string filter )
        {
            foreach( string s in source )
            {
                if( Regex.IsMatch( s.Trim( ' ', '.' ), filter, RegexOptions.IgnoreCase ) )
                    return true;
            }

            return false;
        }
    }
}