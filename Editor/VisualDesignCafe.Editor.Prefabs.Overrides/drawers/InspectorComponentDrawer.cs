﻿using UnityEditor;
using UnityEngine;

namespace VisualDesignCafe.Editor.Prefabs.Overrides
{
    internal class InspectorComponentDrawer : ComponentDrawer
    {
        private GUIContent _cachedContent;
        private bool _cachedIsExpanded;

        public InspectorComponentDrawer(
            PrefabOverrides prefabOverrides, SerializedObject component, int depth )
                : base( prefabOverrides, component, depth )
        { }

        protected override void OnLayout( int depth, ref IDrawer clickedItem )
        {
            bool isClicked = false;

            ValidateContent();

            GUILayout.BeginHorizontal();
            {
                DrawIcon( _cachedContent );

                if( DrawLabel( _cachedContent ) )
                    isClicked = true;
            }
            GUILayout.EndHorizontal();

            if( isClicked )
                clickedItem = this;
        }

        private void DrawIcon( GUIContent content )
        {
            GUILayout.Space( this.Padding.left );
            GUILayout.Label( content.image, GUILayout.Height( 16 ), GUILayout.Width( 16 ) );
            GUILayout.Space( -3 );
        }

        private bool DrawLabel( GUIContent content )
        {
            return GUILayout.Button(
                content.text,
                IsHovering ? EditorStyles.whiteLabel : EditorStyles.label );
        }

        private void ValidateContent()
        {
            if( _cachedContent == null || _cachedIsExpanded != IsExpanded )
            {
                _cachedContent = GetContent();
                _cachedIsExpanded = IsExpanded;
            }
        }

        private GUIContent GetContent()
        {
            var content =
                new GUIContent(
                    EditorGUIUtility.ObjectContent(
                        Target.targetObject,
                        Target.targetObject.GetType() ) );

            content.text =
                Target.targetObject is Component
                    ? Target.targetObject.GetType().Name
                    : "GameObject Properties";

            if( !IsExpanded )
                content.text += " (" + this.ChildCount + ")";

            return content;
        }
    }
}