﻿using System.Linq;
using UnityEditor;
using UnityEngine;

namespace VisualDesignCafe.Editor.Prefabs.Overrides
{
    internal class InspectorObjectDrawer : ObjectDrawer
    {
        private GUIStyle _extendedBackgroundStyle;
        private Rect _extendedBackgroundRect;

        private string _collapsedDisplayName;
        private GUIContent _cachedContent;

        public InspectorObjectDrawer(
            PrefabOverrides prefabOverrides, GameObject gameObject, int depth )
                : base( prefabOverrides, gameObject, depth )
        {
            _cachedContent = new GUIContent( EditorGUIUtility.ObjectContent( Target, typeof( GameObject ) ) );
            _cachedContent.text = Target.name;

            // Check if the name of the GameObject changed compared to the original object.
            // If it changed we should add the original name as well to make sure the user does not get confused about which object it is.
            if( TargetInOriginalPrefab != null && Target.name != TargetInOriginalPrefab.name )
                _cachedContent.text += " (\"" + TargetInOriginalPrefab.name + "\")";
        }

        protected override void OnLayout( int depth, ref IDrawer clickedItem )
        {
            bool isClicked = false;

            if( _extendedBackgroundStyle == null )
                CreateGuiStyle();

            GUILayout.BeginHorizontal( InternalEditorStyles.Empty );
            {
                GUILayout.Space( -21 );
                GUILayout.BeginHorizontal( _extendedBackgroundStyle );
                {
                    DrawIcon();

                    if( DrawLabel() )
                        isClicked = true;
                }
                GUILayout.EndHorizontal();

                DrawExtendedBackground();
            }
            GUILayout.EndHorizontal();

            if( isClicked )
                clickedItem = this;
        }

        protected override void Draw( Rect rect, int depth )
        {
        }

        private void DrawIcon()
        {
            GUILayout.Space( 21 );
            GUILayout.Space( Padding.left );

            if( GUILayout.Button(
                _cachedContent.image,
                EditorStyles.label,
                GUILayout.Height( 16 ),
                GUILayout.Width( 16 ) ) )
            {
                EditorGUIUtility.PingObject( Target );
            }

            GUILayout.Space( -3 );
        }

        private bool DrawLabel()
        {
            if( !ComponentsAreExpanded )
            {
                _collapsedDisplayName = _cachedContent.text;
                _collapsedDisplayName += " (" + Components.Sum( c => c.ChildCount ) + ")";
            }

            return GUILayout.Button(
                ComponentsAreExpanded
                    ? _cachedContent.text
                    : _collapsedDisplayName,
                IsHovering
                    ? EditorStyles.whiteLabel
                    : EditorStyles.label );
        }

        /// <summary>
        /// Draws an extended background box on the right side of the title.
        /// This will cover the empty space caused by the inspector boundries.
        /// </summary>
        private void DrawExtendedBackground()
        {
            if( Event.current.type == EventType.repaint )
                _extendedBackgroundRect = GUILayoutUtility.GetLastRect();

            GUI.Box(
                new Rect(
                    _extendedBackgroundRect.xMax - 1,
                    _extendedBackgroundRect.y,
                    10,
                    _extendedBackgroundRect.height ),
                string.Empty,
                _extendedBackgroundStyle );
        }

        private void CreateGuiStyle()
        {
            _extendedBackgroundStyle = new GUIStyle( InternalEditorStyles.InspectorTitlebar );
            _extendedBackgroundStyle.contentOffset = Vector2.zero;
            _extendedBackgroundStyle.margin = new RectOffset( 0, 0, 0, 0 );
            _extendedBackgroundStyle.padding = new RectOffset( 0, 0, 0, 0 );
            _extendedBackgroundStyle.alignment = TextAnchor.MiddleLeft;
        }
    }
}