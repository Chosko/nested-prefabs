﻿using UnityEditor;
using UnityEngine;

namespace VisualDesignCafe.Editor.Prefabs.Overrides
{
    internal class TogglePropertyDrawer : PropertyDrawer
    {
        public bool IsToggled
        {
            get;
            set;
        }

        private Rect _rect;

        public TogglePropertyDrawer(
            PrefabOverrides prefabOverrides,
            IDrawer parent,
            SerializedProperty property,
            PropertyOverride propertyOverride,
            int depth )
                : base( prefabOverrides, parent, property, propertyOverride, depth )
        { }

        protected virtual void DrawToggle( Rect rect )
        {
            if( Event.current.type == EventType.Repaint )
                GUI.Toggle(
                    rect.Contract( 32f, 0, 0, 0 ),
                    IsToggled,
                    string.Empty );
        }

        protected override void Draw( Rect rect, int depth )
        {
            DrawBackground( rect );
            DrawToggle( rect );
            DrawLabel( rect, depth );
        }

        protected override void HandleClick( Rect rect, out bool isClicked )
        {
            base.HandleClick( rect, out isClicked );

            if( isClicked )
            {
                isClicked = false;
                IsToggled = !IsToggled;
            }
        }

        protected override void DrawLabel( Rect rect, int depth )
        {
            GUI.Label(
                Padding.Add( new Rect(
                    rect.x + 16f + 32f + 20f,
                    rect.y + 3,
                    rect.width,
                    16f ) ),
                Target.displayName,
                IsHovering ? EditorStyles.whiteLabel : EditorStyles.label );
        }
    }
}