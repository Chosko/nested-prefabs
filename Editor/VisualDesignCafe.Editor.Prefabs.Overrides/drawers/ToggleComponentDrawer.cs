﻿using UnityEditor;
using UnityEngine;

namespace VisualDesignCafe.Editor.Prefabs.Overrides
{

    internal class ToggleComponentDrawer : ComponentDrawer
    {
        public bool IsToggled
        {
            get;
            set;
        }

        private GUIContent _cachedContent;

        public ToggleComponentDrawer(
            PrefabOverrides prefabOverrides, SerializedObject component, int depth )
                : base( prefabOverrides, component, depth )
        {
            _cachedContent =
                new GUIContent(
                    EditorGUIUtility.ObjectContent(
                        Target.targetObject,
                        Target.targetObject.GetType() ) );

            _cachedContent.text =
                Target.targetObject is Component
                    ? Target.targetObject.GetType().Name
                    : "GameObject Properties";
        }

        public override void OnGui( int depth, ref IDrawer clickedItem )
        {
            if( Target == null )
                return;

            bool isEditable =
                !FlagsUtility.IsSet( Target.targetObject.hideFlags, HideFlags.NotEditable );

            if( !isEditable )
            {
                Target.targetObject.hideFlags =
                    FlagsUtility.Unset(
                        Target.targetObject.hideFlags,
                        HideFlags.NotEditable );
            }

            Target.Update();

            base.OnGui( depth + 1, ref clickedItem );

            if( !isEditable )
            {
                Target.targetObject.hideFlags =
                    FlagsUtility.Set(
                        Target.targetObject.hideFlags,
                        HideFlags.NotEditable );
            }
        }

        protected override void Draw( Rect rect, int depth )
        {
            DrawBackground( rect );
            DrawToggle( rect );

            GUI.Label(
                Padding.Add( new Rect( rect.x + 16f + 20f, rect.y + 3, 16f, 16f ) ),
                _cachedContent.image );

            GUI.Label(
                Padding.Add( new Rect( rect.x + 32f + 20f, rect.y + 3, rect.width, 16f ) ),
                _cachedContent.text,
                IsHovering ? EditorStyles.whiteLabel : EditorStyles.label );
        }

        protected override void HandleClick( Rect rect, out bool isClicked )
        {
            base.HandleClick( rect, out isClicked );

            if( isClicked )
            {
                isClicked = false;
                IsToggled = !IsToggled;

                for( int i = 0; i < ChildCount; i++ )
                {
                    var toggleProperty = (TogglePropertyDrawer) GetChildAt( i );
                    toggleProperty.IsToggled = IsToggled;
                }
            }
        }

        private void DrawToggle( Rect rect )
        {
            if( Event.current.type == EventType.Repaint )
                GUI.Toggle(
                    rect.Contract( 16f, 0f, 0f, 0f ),
                    IsToggled,
                    string.Empty );
        }
    }
}