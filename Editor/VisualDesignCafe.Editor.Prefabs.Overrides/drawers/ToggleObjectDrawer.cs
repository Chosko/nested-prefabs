﻿using UnityEditor;
using UnityEngine;

namespace VisualDesignCafe.Editor.Prefabs.Overrides
{

    /// <summary>
    /// Object Drawer with a toggle. 
    /// Used for the list of modified properties in the Prefab Overrides popup.
    /// </summary>
    internal class ToggleObjectDrawer : ObjectDrawer
    {
        public override string DisplayName
        {
            get
            {
                return base.DisplayName;
            }
        }

        public bool IsToggled
        {
            get;
            set;
        }

        private Rect _rect;
        private GUIContent _cachedHierarchyPath;
        private GUIContent _cachedContent;

        public ToggleObjectDrawer(
            PrefabOverrides prefabOverrides, GameObject gameObject, int depth )
            : base( prefabOverrides, gameObject, depth )
        {
            string path =
                PrefabHierarchyUtility.GetHierarchyPath(
                    gameObject,
                    AddOverridePopup.ActiveInspector.Target.transform,
                    false );

            _cachedHierarchyPath = new GUIContent( path, path );
            _cachedContent = EditorGUIUtility.ObjectContent( Target, Target.GetType() );
        }

        public override void OnGui( int depth, ref IDrawer clickedItem )
        {
            if( Target == null )
                return;

            IDrawer clicked = null;

            if( IsVisible )
                OnLayout( depth, ref clicked );

            if( ComponentsAreExpanded )
            {
                for( int i = 0; i < _components.Count; i++ )
                {
                    if( _components[ i ] == null )
                        continue;

                    _components[ i ].OnGui( depth + 1, ref clickedItem );
                }
            }
        }

        protected override void Draw( Rect rect, int depth )
        {
            DrawBackground( rect );
            DrawToggle( rect );

            GUI.Label(
                Padding.Add( new Rect( rect.x + 20f, rect.y + 3, 16f, 16f ) ),
                _cachedContent.image );

            GUI.Label(
                Padding.Add( new Rect( rect.x + 16f + 20f, rect.y - 2, rect.width, 16f ) ),
                DisplayName,
                IsHovering ? EditorStyles.whiteLabel : EditorStyles.label );

            GUI.enabled = false;
            {
                GUI.Label(
                    new Rect( rect.x + 20f + 20f, rect.yMax - 14, rect.width, rect.height ),
                    _cachedHierarchyPath,
                    IsHovering ? EditorStyles.whiteMiniLabel : EditorStyles.miniLabel );
            }
            GUI.enabled = true;
        }

        protected override void HandleClick( Rect rect, out bool isClicked )
        {
            base.HandleClick( rect, out isClicked );

            if( isClicked )
            {
                isClicked = false;
                IsToggled = !IsToggled;

                for( int i = 0; i < _components.Count; i++ )
                {
                    var toggleComponent = (ToggleComponentDrawer) _components[ i ];
                    toggleComponent.IsToggled = IsToggled;

                    for( int j = 0; j < toggleComponent.ChildCount; j++ )
                    {
                        var toggleProperty = (TogglePropertyDrawer) toggleComponent.GetChildAt( j );
                        toggleProperty.IsToggled = IsToggled;
                    }
                }
            }
        }

        private void DrawToggle( Rect rect )
        {
            if( Event.current.type == EventType.Repaint )
                GUI.Toggle(
                    rect.Contract( 6f, 0, 0, 0 ),
                    IsToggled,
                    string.Empty );
        }
    }
}