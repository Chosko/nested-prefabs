﻿using UnityEditor;
using UnityEngine;

namespace VisualDesignCafe.Editor.Prefabs.Overrides
{
    internal partial class InspectorPropertyDrawer : PropertyDrawer
    {
        public static bool TargetDisappeared
        {
            get;
            set;
        }

        private Rect _cachedRect;
        private IDrawer _clickedItem = null;

        private SerializedProperty _targetX;
        private SerializedProperty _targetY;
        private SerializedProperty _targetZ;
        private SerializedProperty _targetW;

        private PropertyOverride _targetOverrideX;
        private PropertyOverride _targetOverrideY;
        private PropertyOverride _targetOverrideZ;
        private PropertyOverride _targetOverrideW;

        public InspectorPropertyDrawer(
            PrefabOverrides prefabOverrides,
            IDrawer parent,
            SerializedProperty property,
            PropertyOverride propertyOverride,
            int depth )
                : base( prefabOverrides, parent, property, propertyOverride, depth )
        {
            _targetX =
                property.FindPropertyRelative( "x" )
                ?? FindPropertySibling( property, "x" );

            _targetY =
                property.FindPropertyRelative( "y" )
                ?? FindPropertySibling( property, "y" );

            _targetZ =
                property.FindPropertyRelative( "z" )
                ?? FindPropertySibling( property, "z" )
                ?? property.FindPropertyRelative( "width" )
                ?? FindPropertySibling( property, "width" );

            _targetW =
                property.FindPropertyRelative( "w" )
                ?? FindPropertySibling( property, "w" )
                ?? property.FindPropertyRelative( "height" )
                ?? FindPropertySibling( property, "height" );

            if( _targetX != null )
            {
                _targetOverrideX =
                    PrefabOverrides.GetPropertyOverride(
                        property.serializedObject.targetObject,
                        _targetX.propertyPath );
            }

            if( _targetY != null )
            {
                _targetOverrideY =
                    PrefabOverrides.GetPropertyOverride(
                        property.serializedObject.targetObject,
                        _targetY.propertyPath );
            }

            if( _targetZ != null )
            {
                _targetOverrideZ =
                    PrefabOverrides.GetPropertyOverride(
                        property.serializedObject.targetObject,
                        _targetZ.propertyPath );
            }

            if( _targetW != null )
            {
                _targetOverrideW =
                    PrefabOverrides.GetPropertyOverride(
                        property.serializedObject.targetObject,
                        _targetW.propertyPath );
            }
        }

        protected override void OnLayout( int depth, ref IDrawer clickedItem )
        {
            if( !TargetExists() )
            {
                TargetDisappeared = true;
                return;
            }

            if( IsShared() )
                return;

            float propertyHeight = CalculatePropertyHeight();
            Rect rect = GUILayoutUtility.GetRect( 10, propertyHeight );

            if( IsParentOfShared() )
            {
                GUILayout.Space( -propertyHeight );
                DrawShared( Target, _cachedRect, depth );
            }
            else
            {
                Draw( _cachedRect, depth );
            }

            if( Event.current.type == EventType.Repaint )
                _cachedRect = rect;

            if( _clickedItem != null )
                clickedItem = _clickedItem;

            return;
        }

        protected override void Draw( Rect rect, int depth )
        {
            try
            {
                if( HasParentOverride )
                {
                    DrawMultipleOverrideWarning( rect );
                }
                else
                {
                    PrefabOverrideUtility.SetOverrideValue( Override, Target );
                    EditorUtility.SetDirty( PrefabOverrides );
                }

                EditorGUI.BeginChangeCheck();
                {
                    DrawLabel( rect );

                    if( HasParentOverride )
                    {
                        bool isEnabled = GUI.enabled;
                        GUI.enabled = false;

                        DrawPropertyField(
                            rect,
                            Target,
                            new GUIContent( Target.displayName, Target.tooltip ) );

                        GUI.enabled = isEnabled;
                    }
                    else
                    {
                        DrawPropertyField(
                            rect,
                            Target,
                            new GUIContent( Target.displayName, Target.tooltip ) );
                    }
                }
                if( EditorGUI.EndChangeCheck() )
                {
                    if( !HasParentOverride )
                    {
                        Target.serializedObject.ApplyModifiedProperties();

                        if( Override != null )
                        {
                            PrefabOverrideUtility.SetOverrideValue( Override, Target );
                            EditorUtility.SetDirty( PrefabOverrides );
                        }
                    }
                }

            }
            catch( System.InvalidOperationException )
            {
                EditorGUI.DrawRect( rect, new Color( 1, 0, 0, 0.3f ) );
            }
        }

        protected virtual void DrawShared( SerializedProperty parent, Rect rect, int depth )
        {
            // Prevent default 'Reset to prefab value' context menu when 
            // right -clicking on a serialized property and replace it with
            // a 'Remove Override' button.
            if( Event.current.type == EventType.MouseUp
                /* && Event.current.button == 1 */
                && new Rect(
                        rect.x,
                        rect.y,
                        EditorGUIUtility.labelWidth,
                        EditorGUIUtility.singleLineHeight )
                    .Contains( Event.current.mousePosition ) )
            {
                Event.current.Use();
                var contextMenu = new GenericMenu();
                contextMenu.AddItem(
                    new GUIContent( "Remove override" ),
                    false,
                    () =>
                    {
                        _clickedItem = this;
                    } );
                contextMenu.ShowAsContext();
            }

            bool guiIsEnabled = GUI.enabled;

            if( HasParentOverride )
                GUI.enabled = false;

            switch( parent.propertyType )
            {
                case SerializedPropertyType.Vector2:
                    Vector2Field( rect, parent );
                    break;
                case SerializedPropertyType.Vector3:
                    Vector3Field( rect, parent );
                    break;
                case SerializedPropertyType.Vector4:
                    Vector4Field( rect, parent );
                    break;
                case SerializedPropertyType.Quaternion:
                    QuaternionField( rect, parent );
                    break;
                case SerializedPropertyType.Rect:
                    parent.rectValue = EditorGUI.RectField( rect, parent.displayName, parent.rectValue );
                    break;
            }

            if( HasParentOverride )
                GUI.enabled = guiIsEnabled;
        }

        private void DrawLabel( Rect rect )
        {
            var labelRect =
                new Rect( rect.x, rect.y, EditorGUIUtility.labelWidth, 16 );

            string toolTip = Target.propertyPath;
            if( !string.IsNullOrEmpty( Target.tooltip ) )
                toolTip += ". " + Target.tooltip;

            GUI.Label(
                labelRect,
                new GUIContent( string.Empty, toolTip ), EditorStyles.label );

            // Prevent default 'Reset to prefab value' context menu when 
            // right -clicking on a serialized property and replace it with
            // a 'Remove Override' button.
            if( Event.current.type == EventType.MouseUp
                /* && Event.current.button == 1 */
                && labelRect.Contains( Event.current.mousePosition ) )
            {
                Event.current.Use();
                var contextMenu = new GenericMenu();
                contextMenu.AddItem(
                    new GUIContent( "Remove override" ),
                    false,
                    () =>
                    {
                        _clickedItem = this;
                    } );
                contextMenu.ShowAsContext();
            }
        }

        private void DrawMultipleOverrideWarning( Rect rect )
        {
            Color storedBackgroundColor = GUI.backgroundColor;
            GUI.backgroundColor = Color.clear;

            var warningRect = new Rect( rect.x - 17, rect.y + 1, rect.height, rect.height );
            EditorGUI.HelpBox( warningRect.Expand( 5 ), string.Empty, MessageType.Warning );
            if( GUI.Button(
                warningRect,
                new GUIContent(
                    string.Empty,
                    "Property has an override in parent prefab" ),
                EditorStyles.label ) )
            {
                EditorGUIUtility.PingObject( ParentPrefabOverrides.gameObject );
            }

            GUI.backgroundColor = storedBackgroundColor;
        }

        private bool IsShared()
        {
            if( Target.propertyType == SerializedPropertyType.Float
                && Target.propertyPath.Contains( "." ) )
            {
                SerializedProperty parentProperty =
                    PrefabOverrideUtility.FindParentProperty( Target );

                switch( parentProperty.propertyType )
                {
                    case SerializedPropertyType.Vector2:
                    case SerializedPropertyType.Vector3:
                    case SerializedPropertyType.Quaternion:
                    case SerializedPropertyType.Vector4:
                    case SerializedPropertyType.Rect:
                        return true;
                }
            }

            return false;
        }

        private bool IsParentOfShared()
        {
            switch( Target.propertyType )
            {
                case SerializedPropertyType.Vector2:
                case SerializedPropertyType.Vector3:
                case SerializedPropertyType.Vector4:
                case SerializedPropertyType.Quaternion:
                case SerializedPropertyType.Rect:

                    return _targetOverrideX != null
                        || _targetOverrideY != null
                        || _targetOverrideZ != null
                        || _targetOverrideW != null;
            }

            return false;
        }

        private float CalculatePropertyHeight()
        {
            return EditorGUI.GetPropertyHeight(
                Target,
                new GUIContent( Target.displayName ),
                false );
        }
    }
}