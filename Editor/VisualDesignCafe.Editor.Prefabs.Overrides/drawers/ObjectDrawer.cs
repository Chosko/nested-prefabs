﻿using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace VisualDesignCafe.Editor.Prefabs.Overrides
{
    internal class ObjectDrawer : Drawer<GameObject>
    {
        public override string DisplayName
        {
            get { return Target.name; }
        }

        public ComponentDrawer[] Components
        {
            get { return _components.ToArray(); }
        }

        public int ComponentCount
        {
            get { return _components.Count; }
        }

        public bool ComponentsAreExpanded
        {
            get { return _componentsAreExpanded; }
            set { _componentsAreExpanded = value; }
        }

        public new readonly GameObject Target;
        public readonly SerializedObject SerializedTarget;
        public readonly GameObject TargetInOriginalPrefab;

        private bool _componentsAreExpanded = true;
        protected List<ComponentDrawer> _components = new List<ComponentDrawer>();
        private GUIContent _cachedContent;

        public ObjectDrawer(
            PrefabOverrides prefabOverrides, GameObject gameObject, int depth )
                : base( prefabOverrides, gameObject, depth )
        {
            Target = gameObject;
            SerializedTarget = new SerializedObject( gameObject );
            TargetInOriginalPrefab = FindGameObjectInAsset( gameObject );

            _cachedContent =
                new GUIContent(
                    EditorGUIUtility.ObjectContent( Target, Target.GetType() ) );
        }

        public void ClearComponents()
        {
            _components.Clear();
        }

        public IDrawer GetComponentAt( int index )
        {
            if( index < 0 || index >= _components.Count )
                throw new System.ArgumentOutOfRangeException( "Index" );

            return _components[ index ];
        }

        public override void Collapse( bool recursive )
        {
            ComponentsAreExpanded = false;
            base.Collapse( recursive );

            foreach( ComponentDrawer component in _components )
            {
                component.Collapse( recursive );
            }

            IsExpanded = true;
        }

        public override void Expand( bool recursive )
        {
            ComponentsAreExpanded = true;
            IsExpanded = true;
            base.Expand( recursive );

            foreach( ComponentDrawer component in _components )
            {
                component.Expand( recursive );
            }
        }

        public override void Order( bool recursive )
        {
            _components = _components.OrderBy( c => c.DisplayName ).ToList();
            base.Order( recursive );
        }

        public ComponentDrawer GetComponentDrawer<T>(
            SerializedObject component, bool createIfNull )
                where T : ComponentDrawer
        {
            for( int i = 0; i < _components.Count; i++ )
            {
                if( _components[ i ] == null )
                    continue;

                if( _components[ i ].Target.targetObject == component.targetObject )
                    return _components[ i ];
            }

            if( createIfNull )
            {
                T drawer =
                    (T) System.Activator.CreateInstance(
                        typeof( T ),
                        PrefabOverrides,
                        component,
                        Depth + 1 );

                _components.Add( drawer );
                return drawer;
            }

            return null;
        }

        public override void OnGui( int depth, ref IDrawer clickedItem )
        {
            if( Target == null )
                return;

            if( IsVisible )
                OnLayout( depth, ref clickedItem );

            if( ComponentsAreExpanded )
                OnComponentLayout( depth, ref clickedItem );

            if( IsExpanded )
                DrawChildren( depth, ref clickedItem );
        }

        protected override void Draw( Rect rect, int depth )
        {
            DrawBackground( rect );

            GUI.Label(
                Padding.Add( new Rect( depth * 16f, rect.y + 3, 16f, 16f ) ),
                _cachedContent.image );

            GUI.Label(
                Padding.Add( new Rect( depth * 16f + 16f, rect.y + 3, rect.width, 16f ) ),
                Target.name,
                IsHovering ? EditorStyles.whiteLabel : EditorStyles.label );

            if( _components.Count > 0 )
            {
                GUI.Label(
                    new Rect( rect.xMax - rect.height, rect.y + 4, rect.height, rect.height ),
                    string.Empty,
                    EditorStyles.foldout );
            }
        }

        protected virtual void OnComponentLayout( int depth, ref IDrawer clickedItem )
        {
            for( int i = 0; i < _components.Count; i++ )
            {
                if( _components[ i ] == null )
                    continue;

                _components[ i ].OnGui( depth, ref clickedItem );

                if( IsEditable )
                    DrawSeparator();
            }
        }

        private void DrawSeparator()
        {
            GUILayout.Space( 10 );

            if( Event.current.type == EventType.repaint )
            {
                Rect rect = GUILayoutUtility.GetLastRect();
                EditorGUI.DrawRect(
                    new Rect(
                        rect.x,
                        rect.center.y,
                        Screen.width - rect.x - 21 - 3,
                        1 ),
                    EditorGUIUtility.isProSkin
                        ? new Color( 1, 1, 1, 0.2f )
                        : new Color( 0, 0, 0, 0.5f ) );
            }
        }

        private GameObject FindGameObjectInAsset( GameObject gameObject )
        {
            Prefab prefab = PrefabHierarchyUtility.GetPrefab( gameObject );

            if( Prefab.IsNullOrMissing( prefab ) )
                return null;

            Prefab prefabAsset = prefab.Asset.GetComponent<Prefab>();

            if( prefabAsset == null )
                return null;

            return prefabAsset
                .ReferenceCollection
                .FindObjectWithGuid( gameObject.GetComponent<Guid>() );
        }
    }
}
