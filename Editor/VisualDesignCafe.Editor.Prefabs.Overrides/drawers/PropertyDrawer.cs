﻿using UnityEditor;
using UnityEngine;

namespace VisualDesignCafe.Editor.Prefabs.Overrides
{
    internal class PropertyDrawer : Drawer<SerializedProperty>
    {
        public override string DisplayName
        {
            get { return Target.displayName; }
        }

        public readonly string TargetPath;
        public readonly SerializedObject TargetObject;
        public new readonly SerializedProperty Target;
        public readonly IDrawer Parent;
        public readonly PropertyOverride Override;
        public readonly bool HasParentOverride;
        public readonly PrefabOverrides ParentPrefabOverrides;

        private GUIStyle _valueBoxStyle;
        private Rect _rect;

        public PropertyDrawer(
            PrefabOverrides prefabOverrides,
            IDrawer parent,
            SerializedProperty property,
            PropertyOverride propertyOverride,
            int depth )
            : base( prefabOverrides, property, depth )
        {
            Parent = parent;
            Target = property;
            TargetPath = property.propertyPath;
            TargetObject = property.serializedObject;
            Override = propertyOverride;

            HasParentOverride =
                PrefabOverrideUtility.IsOverridden(
                    PrefabOverrides.transform.parent,
                    Target,
                    out ParentPrefabOverrides );
        }

        public PropertyDrawer GetChild<T>(
            IDrawer parent,
            SerializedProperty property,
            SerializedProperty @override,
            bool addIfNull )
            where T : PropertyDrawer
        {
            var drawer = GetChild<PropertyDrawer>( property, false );
            if( drawer == null && addIfNull )
            {
                drawer =
                    (T) System.Activator.CreateInstance(
                            typeof( T ),
                            PrefabOverrides,
                            parent,
                            property.Copy(),
                            @override != null ? @override.Copy() : null,
                            Depth + 1 );

                AddChild( drawer );
            }

            return drawer;
        }

        protected bool TargetExists()
        {
            return TargetObject.FindProperty( TargetPath ) != null;
        }

        protected override void Draw( Rect rect, int depth )
        {
            DrawBackground( rect );
            DrawLabel( rect, depth );
            DrawValue( rect );
            DrawFoldout( rect );
        }

        protected virtual void DrawLabel( Rect rect, int depth )
        {
            GUI.Label(
                Padding.Add( new Rect(
                    depth * 16f,
                    rect.y + 3,
                    rect.width,
                    16f ) ),
                Target.displayName,
                IsHovering ? EditorStyles.whiteLabel : EditorStyles.label );
        }

        protected void DrawValue( Rect rect )
        {
            if( !HasChildren && Target.propertyType != SerializedPropertyType.Generic )
            {
                if( _valueBoxStyle == null )
                {
                    _valueBoxStyle = new GUIStyle( GUI.skin.FindStyle( "box" ) );
                    _valueBoxStyle.border = new RectOffset( 1, 1, 1, 1 );
                    _valueBoxStyle.contentOffset = Vector2.zero;
                    _valueBoxStyle.margin = new RectOffset( 0, 0, 0, 0 );
                    _valueBoxStyle.padding = new RectOffset( 0, 0, 0, 0 );
                    _valueBoxStyle.alignment = TextAnchor.MiddleCenter;
                    _valueBoxStyle.wordWrap = false;
                    _valueBoxStyle.clipping = TextClipping.Clip;
                    _valueBoxStyle.normal.textColor = EditorStyles.label.normal.textColor;
                }

                Rect valueRect =
                    new Rect(
                        rect.width - rect.width * 0.3f,
                        rect.y,
                        rect.width * 0.3f,
                        rect.height );

                Color storedBackgroundColor = GUI.backgroundColor;
                GUI.backgroundColor *= new Color( 1, 1, 1, .5f );

                switch( Target.propertyType )
                {
                    case SerializedPropertyType.Float:
                        GUI.Box( valueRect, Target.floatValue.ToString(), _valueBoxStyle );
                        break;
                    case SerializedPropertyType.Integer:
                        GUI.Box( valueRect, Target.intValue.ToString(), _valueBoxStyle );
                        break;
                    case SerializedPropertyType.String:
                        GUI.Box(
                            valueRect,
                            new GUIContent(
                                "\"" + Target.stringValue + "\"",
                                Target.stringValue ),
                            _valueBoxStyle );
                        break;
                    case SerializedPropertyType.Boolean:
                        GUI.Box( valueRect, Target.boolValue.ToString(), _valueBoxStyle );
                        break;
                    case SerializedPropertyType.Color:
                        GUI.Box( valueRect, string.Empty, _valueBoxStyle );

                        Rect colorRect = valueRect.Contract( 1, 1, 1, 1 );

                        EditorGUI.DrawRect(
                            colorRect,
                            new Color(
                                Target.colorValue.r,
                                Target.colorValue.g,
                                Target.colorValue.b,
                                1 ) );

                        Rect alphaRect = colorRect.Contract( 0, 16, 0, 0 );

                        EditorGUI.DrawRect( alphaRect, Color.black );
                        EditorGUI.DrawRect(
                            new Rect(
                                alphaRect.x,
                                alphaRect.y,
                                alphaRect.width * Target.colorValue.a,
                                alphaRect.height ),
                            Color.white );
                        break;
                    case SerializedPropertyType.LayerMask:
                        GUI.Box( valueRect, Target.intValue.ToString(), _valueBoxStyle );
                        break;
                    default:
                        GUI.Box(
                            valueRect,
                            new GUIContent(
                                Target.propertyType.ToString(),
                                Target.propertyType.ToString() ),
                            _valueBoxStyle );
                        break;
                }

                GUI.backgroundColor = storedBackgroundColor;
            }
        }

        protected void DrawFoldout( Rect rect )
        {
            if( HasChildren || Target.propertyType == SerializedPropertyType.Generic )
                GUI.Label(
                    new Rect(
                        rect.xMax - rect.height,
                        rect.y + 4,
                        rect.height,
                        rect.height ),
                    string.Empty,
                    EditorStyles.foldout );
        }
    }
}