﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace VisualDesignCafe.Editor.Prefabs.Overrides
{
    internal class Drawer<T> : IDrawer
    {
        public virtual string DisplayName
        {
            get { return string.Empty; }
        }

        public object Target
        {
            get;
            private set;
        }

        public int Depth
        {
            get;
            private set;
        }

        public bool HasChildren
        {
            get { return _children.Count > 0; }
        }

        public int ChildCount
        {
            get { return _children.Count; }
        }

        public IDrawer[] Children
        {
            get { return _children.ToArray(); }
        }

        public bool IsVisible
        {
            get { return _drawSelf; }
            set { _drawSelf = value; }
        }

        public bool IsExpanded
        {
            get { return _isExpanded; }
            set { _isExpanded = value; }
        }

        public bool IsEditable
        {
            get;
            set;
        }

        public bool BackgroundIsVisible
        {
            get;
            set;
        }

        public bool IsHovering
        {
            get;
            protected set;
        }

        public RectOffset Padding
        {
            get { return _padding ?? ( _padding = new RectOffset() ); }
            set { _padding = value; }
        }

        public virtual int Height
        {
            get { return 22; }
        }

        public virtual int Width
        {
            get { return Screen.width; }
        }

        public readonly PrefabOverrides PrefabOverrides;

        private bool _drawSelf = true;
        private bool _isExpanded = true;
        private RectOffset _padding;
        private List<IDrawer> _children = new List<IDrawer>();

        public Drawer( PrefabOverrides prefabOverrides, T target, int depth )
        {
            PrefabOverrides = prefabOverrides;
            Target = target;
            Depth = depth;
        }

        public void SetDepth( int depth )
        {
            Depth = depth;
        }

        public void Clear()
        {
            _children.Clear();
        }

        public IDrawer GetChildAt( int index )
        {
            if( index < 0 || index >= _children.Count )
                throw new ArgumentOutOfRangeException( "Index" );

            return _children[ index ];
        }

        public virtual void Collapse( bool recursive )
        {
            IsExpanded = false;

            if( recursive )
            {
                for( int i = 0; i < _children.Count; i++ )
                {
                    if( _children[ i ] == null )
                        continue;

                    _children[ i ].Collapse( recursive );
                }
            }
        }

        public virtual void Expand( bool recursive )
        {
            IsExpanded = true;

            if( recursive )
            {
                for( int i = 0; i < _children.Count; i++ )
                {
                    if( _children[ i ] == null )
                        continue;

                    _children[ i ].Expand( recursive );
                }
            }
        }

        public virtual void Order( bool recursive )
        {
            _children = _children.OrderBy( c => c.DisplayName ).ToList();

            if( recursive )
            {
                for( int i = 0; i < _children.Count; i++ )
                {
                    if( _children[ i ] == null )
                        continue;

                    _children[ i ].Order( recursive );
                }
            }
        }

        public virtual Tresult GetChild<Tresult>( object target, bool createIfNull = false )
            where Tresult : IDrawer
        {
            Tresult child = (Tresult) GetChild( target );

            if( child != null )
                return child;

            if( createIfNull )
            {
                child =
                    (Tresult) Activator.CreateInstance(
                        typeof( Tresult ),
                        PrefabOverrides,
                        target,
                        Depth + 1 );

                _children.Add( child );

                return child;
            }

            return default( Tresult );
        }

        public virtual IDrawer GetChild( object target )
        {
            for( int i = 0; i < _children.Count; i++ )
            {
                if( _children[ i ].Target == target )
                    return _children[ i ];
            }

            return null;
        }

        public virtual void AddChild( IDrawer target )
        {
            if( !_children.Contains( target ) )
                _children.Add( target );
        }

        public virtual void RemoveChild( IDrawer target )
        {
            _children.Remove( target );
        }

        public virtual void OnGui( int depth, ref IDrawer clickedItem )
        {
            if( IsVisible )
                OnLayout( depth, ref clickedItem );

            if( IsExpanded )
                DrawChildren( depth, ref clickedItem );
        }

        protected virtual void Draw( Rect rect, int depth )
        {
        }

        protected virtual void DrawBackground( Rect rect )
        {
            if( BackgroundIsVisible )
                EditorGUI.DrawRect( new Rect( 0, rect.y - 1, Screen.width, rect.height + 2 ), Color.black );

            if( IsHovering )
            {
                EditorGUI.DrawRect( rect, new Color32( 62, 125, 231, 255 ) );
            }
        }

        protected virtual void HandleClick( Rect rect, out bool isClicked )
        {
            isClicked = GUI.Button( rect, string.Empty, InternalEditorStyles.Empty );
        }

        protected virtual void HandleHover( Rect rect )
        {
            IsHovering = rect.Contains( Event.current.mousePosition );
        }

        protected virtual void OnLayout( int depth, ref IDrawer clickedItem )
        {
            bool isClicked = false;

            Rect rect = GUILayoutUtility.GetRect( Width, Height );

            HandleClick( rect, out isClicked );
            HandleHover( rect );

            if( isClicked )
                clickedItem = this;

            if( Event.current.type == EventType.repaint )
            {
                Draw( rect, depth );
            }
        }

        protected virtual void DrawChildren( int depth, ref IDrawer clickedItem )
        {
            for( int i = 0; i < _children.Count; i++ )
            {
                if( _children[ i ] == null )
                    continue;

                _children[ i ].OnGui( depth + 1, ref clickedItem );
            }
        }
    }
}