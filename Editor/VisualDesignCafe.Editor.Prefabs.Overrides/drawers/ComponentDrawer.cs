﻿using System;
using UnityEditor;
using UnityEngine;

namespace VisualDesignCafe.Editor.Prefabs.Overrides
{
    internal class ComponentDrawer : Drawer<SerializedObject>
    {
        public override string DisplayName
        {
            get { return Target.targetObject.GetType().Name; }
        }

        public new readonly SerializedObject Target;

        private GUIContent _cachedContent;

        public ComponentDrawer(
            PrefabOverrides prefabOverrides, SerializedObject component, int depth )
                : base( prefabOverrides, component, depth )
        {
            Target = component;

            _cachedContent =
                new GUIContent(
                    EditorGUIUtility.ObjectContent(
                        Target.targetObject,
                        Target.targetObject.GetType() ) );

            if( Target.targetObject as Component == null )
            {
                _cachedContent.text = "GameObject Properties";
            }
            else
            {
                _cachedContent.text = Target.targetObject.GetType().Name;
            }
        }

        public PropertyDrawer GetChild<T>(
            SerializedProperty property, PropertyOverride @override, bool addIfNull )
                where T : PropertyDrawer
        {
            var drawer = GetChild<PropertyDrawer>( property, false );
            if( drawer == null && addIfNull )
            {
                drawer =
                    (T) Activator.CreateInstance(
                            typeof( T ),
                            PrefabOverrides,
                            this,
                            property.Copy(),
                            @override,
                            Depth + 1 );

                AddChild( drawer );
            }

            return drawer;
        }

        public override void OnGui( int depth, ref IDrawer clickedItem )
        {
            if( Target == null )
                return;

            bool isEditable =
                !FlagsUtility.IsSet( Target.targetObject.hideFlags, HideFlags.NotEditable );

            if( !isEditable )
                Target.targetObject.hideFlags =
                    FlagsUtility.Unset( Target.targetObject.hideFlags, HideFlags.NotEditable );

            Target.Update();
            base.OnGui( depth, ref clickedItem );

            if( !isEditable )
                Target.targetObject.hideFlags =
                    FlagsUtility.Set( Target.targetObject.hideFlags, HideFlags.NotEditable );
        }

        protected override void Draw( Rect rect, int depth )
        {
            DrawBackground( rect );

            GUI.Label(
                Padding.Add( new Rect( depth * 16f, rect.y + 3, 16f, 16f ) ),
                _cachedContent.image );

            GUI.Label(
                Padding.Add( new Rect( depth * 16f + 16f, rect.y + 3, rect.width, 16f ) ),
                _cachedContent.text,
                IsHovering ? EditorStyles.whiteLabel : EditorStyles.label );

            if( HasChildren )
                GUI.Label(
                    new Rect( rect.xMax - rect.height, rect.y + 4, rect.height, rect.height ),
                    string.Empty,
                    EditorStyles.foldout );
        }
    }
}