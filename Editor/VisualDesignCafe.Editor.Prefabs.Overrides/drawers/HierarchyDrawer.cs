﻿namespace VisualDesignCafe.Editor.Prefabs.Overrides
{
    internal class HierarchyDrawer : Drawer<IDrawer>
    {
        public HierarchyDrawer( PrefabOverrides prefabOverrides )
            : base( prefabOverrides, null, 0 )
        {
            IsVisible = false;
        }

        public void OnGui( ref IDrawer clickedItem )
        {
            base.OnGui( -1, ref clickedItem );
        }
    }
}