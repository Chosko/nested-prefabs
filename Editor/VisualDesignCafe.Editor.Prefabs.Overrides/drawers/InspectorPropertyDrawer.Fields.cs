﻿using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;

#pragma warning disable 618

namespace VisualDesignCafe.Editor.Prefabs.Overrides
{
    internal partial class InspectorPropertyDrawer : PropertyDrawer
    {
        private int[] _cachedSortingLayerUniqueIDs;
        private GUIContent[] _cachedSortingLayerNames;
        private GUIContent[] _cachedTagList;

        private void DrawPropertyField(
            Rect rect, SerializedProperty property, GUIContent label )
        {
            switch( Target.propertyPath )
            {
                case "m_SortingLayerID":
                    DrawSortingLayerIdField( rect, property, label );
                    break;
                case "m_Layer":
                    property.intValue = EditorGUI.LayerField( rect, label, property.intValue );
                    break;
                case "m_TagString":
                    DrawTagStringField( rect, property, label );
                    break;
                default:
                    EditorGUI.PropertyField( rect, property, label, false );
                    break;
            }
        }

        private void DrawSortingLayerIdField(
            Rect rect, SerializedProperty property, GUIContent label )
        {
            if( _cachedSortingLayerUniqueIDs == null )
            {
                _cachedSortingLayerUniqueIDs =
                    (int[]) typeof( UnityEditorInternal.InternalEditorUtility )
                        .GetProperty(
                            "sortingLayerUniqueIDs",
                            BindingFlags.Static | BindingFlags.NonPublic )
                        .GetValue( null, null );
            }

            if( _cachedSortingLayerNames == null )
            {
                _cachedSortingLayerNames =
                    ( (string[]) typeof( UnityEditorInternal.InternalEditorUtility )
                        .GetProperty(
                            "sortingLayerNames",
                            BindingFlags.Static | BindingFlags.NonPublic )
                        .GetValue( null, null ) )
                        .Select( n => new GUIContent( n ) )
                        .ToArray();
            }

            int index = _cachedSortingLayerUniqueIDs.IndexOf( property.intValue );

            if( index == -1 )
                index = 0;

            EditorGUI.BeginChangeCheck();
            {
                index = EditorGUI.Popup( rect, label, index, _cachedSortingLayerNames );
            }
            if( EditorGUI.EndChangeCheck() )
            {
                property.intValue = _cachedSortingLayerUniqueIDs[ index ];
            }
        }

        private void DrawTagStringField(
            Rect rect, SerializedProperty property, GUIContent label )
        {
            if( _cachedTagList == null )
                _cachedTagList =
                    UnityEditorInternal.InternalEditorUtility.tags
                        .Select( t => new GUIContent( t ) )
                        .ToArray();

            int index = 0;
            for( int i = 0; i < _cachedTagList.Length; i++ )
            {
                if( _cachedTagList[ i ].text == property.stringValue )
                {
                    index = i;
                    break;
                }
            }

            EditorGUI.BeginChangeCheck();
            {
                index = EditorGUI.Popup( rect, label, index, _cachedTagList );
            }
            if( EditorGUI.EndChangeCheck() )
            {
                property.stringValue = _cachedTagList[ index ].text;
            }

        }

        private float FloatField(
            string label,
            float value,
            SerializedProperty property,
            PropertyOverride propertyOverride )
        {
            if( propertyOverride != null && property != null )
            {
                if( property.prefabOverride )
                {
                    EditorStyles.label.fontStyle = FontStyle.Bold;
                    EditorStyles.numberField.fontStyle = FontStyle.Bold;
                }

                value = EditorGUILayout.FloatField( label, value );

                EditorStyles.label.fontStyle = FontStyle.Normal;
                EditorStyles.numberField.fontStyle = FontStyle.Normal;
            }
            else
            {
                bool isEnabled = GUI.enabled;
                GUI.enabled = false;
                EditorGUILayout.FloatField( label, value );
                GUI.enabled = isEnabled;
            }

            return value;
        }

        private void Vector2Field( Rect rect, SerializedProperty property )
        {
            var vectorValue =
                new Vector2(
                    _targetOverrideX != null
                        ? _targetOverrideX.FloatValue
                        : property.vector2Value.x,
                    _targetOverrideY != null
                        ? _targetOverrideY.FloatValue
                        : property.vector2Value.y );

            EditorGUI.BeginChangeCheck();
            {
                if( rect.height > EditorGUIUtility.singleLineHeight )
                    EditorGUILayout.PrefixLabel(
                        new GUIContent( property.displayName, property.tooltip ) );

                EditorGUILayout.BeginHorizontal();
                {
                    if( rect.height <= EditorGUIUtility.singleLineHeight )
                        EditorGUILayout.PrefixLabel(
                            new GUIContent( property.displayName, property.tooltip ) );

                    float storedLabelWidth = EditorGUIUtility.labelWidth;
                    float storedFieldWidth = EditorGUIUtility.fieldWidth;
                    EditorGUIUtility.labelWidth = 12;
                    EditorGUIUtility.fieldWidth = 30;

                    vectorValue.x = FloatField( "X", vectorValue.x, _targetX, _targetOverrideX );
                    vectorValue.y = FloatField( "Y", vectorValue.y, _targetY, _targetOverrideY );

                    EditorGUIUtility.labelWidth = storedLabelWidth;
                    EditorGUIUtility.fieldWidth = storedFieldWidth;
                }
                EditorGUILayout.EndHorizontal();
            }
            if( EditorGUI.EndChangeCheck() )
            {
                if( _targetOverrideX != null )
                    _targetOverrideX.SetValue( vectorValue.x );

                if( _targetOverrideY != null )
                    _targetOverrideY.SetValue( vectorValue.y );

                property.vector2Value = vectorValue;
                property.serializedObject.ApplyModifiedProperties();
            }
        }

        private void Vector3Field( Rect rect, SerializedProperty property )
        {
            var vectorValue =
                new Vector3(
                    _targetOverrideX != null
                        ? _targetOverrideX.FloatValue
                        : property.vector3Value.x,
                    _targetOverrideY != null
                        ? _targetOverrideY.FloatValue
                        : property.vector3Value.y,
                    _targetOverrideZ != null
                        ? _targetOverrideZ.FloatValue
                        : property.vector3Value.z );

            EditorGUI.BeginChangeCheck();
            {
                if( rect.height > EditorGUIUtility.singleLineHeight )
                    EditorGUILayout.PrefixLabel(
                        new GUIContent( property.displayName, property.tooltip ) );

                GUILayout.BeginHorizontal();
                {
                    if( rect.height <= EditorGUIUtility.singleLineHeight )
                        EditorGUILayout.PrefixLabel(
                            new GUIContent( property.displayName, property.tooltip ) );

                    float storedLabelWidth = EditorGUIUtility.labelWidth;
                    float storedFieldWidth = EditorGUIUtility.fieldWidth;
                    EditorGUIUtility.labelWidth = 12;
                    EditorGUIUtility.fieldWidth = 30;

                    vectorValue.x = FloatField( "X", vectorValue.x, _targetX, _targetOverrideX );
                    vectorValue.y = FloatField( "Y", vectorValue.y, _targetY, _targetOverrideY );
                    vectorValue.z = FloatField( "Z", vectorValue.z, _targetZ, _targetOverrideZ );

                    EditorGUIUtility.labelWidth = storedLabelWidth;
                    EditorGUIUtility.fieldWidth = storedFieldWidth;

                }
                GUILayout.EndHorizontal();
            }
            if( EditorGUI.EndChangeCheck() )
            {
                if( _targetOverrideX != null )
                    _targetOverrideX.SetValue( vectorValue.x );

                if( _targetOverrideY != null )
                    _targetOverrideY.SetValue( vectorValue.y );

                if( _targetOverrideZ != null )
                    _targetOverrideZ.SetValue( vectorValue.z );

                property.vector3Value = vectorValue;
                property.serializedObject.ApplyModifiedProperties();
            }
        }

        private void Vector4Field( Rect rect, SerializedProperty property )
        {
            var vectorValue =
                new Vector4(
                    _targetOverrideX != null
                        ? _targetOverrideX.FloatValue
                        : property.vector4Value.x,
                    _targetOverrideY != null
                        ? _targetOverrideY.FloatValue
                        : property.vector4Value.y,
                    _targetOverrideZ != null
                        ? _targetOverrideZ.FloatValue
                        : property.vector4Value.z,
                    _targetOverrideW != null
                        ? _targetOverrideW.FloatValue
                        : property.vector4Value.w );

            EditorGUI.BeginChangeCheck();

            if( rect.height > EditorGUIUtility.singleLineHeight )
                EditorGUILayout.PrefixLabel(
                    new GUIContent( property.displayName, property.tooltip ) );

            EditorGUILayout.BeginHorizontal();
            {
                if( rect.height <= EditorGUIUtility.singleLineHeight )
                    EditorGUILayout.PrefixLabel(
                        new GUIContent( property.displayName, property.tooltip ) );

                float storedLabelWidth = EditorGUIUtility.labelWidth;
                float storedFieldWidth = EditorGUIUtility.fieldWidth;
                EditorGUIUtility.labelWidth = 12;
                EditorGUIUtility.fieldWidth = 30;

                vectorValue.x = FloatField( "X", vectorValue.x, _targetX, _targetOverrideX );
                vectorValue.y = FloatField( "Y", vectorValue.y, _targetY, _targetOverrideY );
                vectorValue.z = FloatField( "Z", vectorValue.z, _targetZ, _targetOverrideZ );
                vectorValue.w = FloatField( "W", vectorValue.w, _targetW, _targetOverrideW );

                EditorGUIUtility.labelWidth = storedLabelWidth;
                EditorGUIUtility.fieldWidth = storedFieldWidth;
            }
            EditorGUILayout.EndHorizontal();

            if( EditorGUI.EndChangeCheck() )
            {
                if( _targetOverrideX != null )
                    _targetOverrideX.SetValue( vectorValue.x );

                if( _targetOverrideY != null )
                    _targetOverrideY.SetValue( vectorValue.y );

                if( _targetOverrideZ != null )
                    _targetOverrideZ.SetValue( vectorValue.z );

                if( _targetOverrideW != null )
                    _targetOverrideW.SetValue( vectorValue.w );

                property.vector4Value = vectorValue;
                property.serializedObject.ApplyModifiedProperties();
            }
        }

        private void QuaternionField( Rect rect, SerializedProperty property )
        {
            var vectorValue =
                new Quaternion(
                    _targetOverrideX != null
                        ? _targetOverrideX.FloatValue
                        : property.quaternionValue.x,
                    _targetOverrideY != null
                        ? _targetOverrideY.FloatValue
                        : property.quaternionValue.y,
                    _targetOverrideZ != null
                        ? _targetOverrideZ.FloatValue
                        : property.quaternionValue.z,
                    _targetOverrideW != null
                        ? _targetOverrideW.FloatValue
                        : property.quaternionValue.w );

            EditorGUI.BeginChangeCheck();

            if( rect.height > EditorGUIUtility.singleLineHeight )
                EditorGUILayout.PrefixLabel(
                    new GUIContent( property.displayName, property.tooltip ) );

            EditorGUILayout.BeginHorizontal();
            {
                if( rect.height <= EditorGUIUtility.singleLineHeight )
                    EditorGUILayout.PrefixLabel(
                        new GUIContent( property.displayName, property.tooltip ) );

                float storedLabelWidth = EditorGUIUtility.labelWidth;
                float storedFieldWidth = EditorGUIUtility.fieldWidth;
                EditorGUIUtility.labelWidth = 12;
                EditorGUIUtility.fieldWidth = 30;

                Vector3 euler = vectorValue.eulerAngles;

                if( property.prefabOverride )
                {
                    EditorStyles.label.fontStyle = FontStyle.Bold;
                    EditorStyles.numberField.fontStyle = FontStyle.Bold;
                }

                euler = EditorGUILayout.Vector3Field( string.Empty, euler );

                EditorStyles.label.fontStyle = FontStyle.Normal;
                EditorStyles.numberField.fontStyle = FontStyle.Normal;

                vectorValue = Quaternion.Euler( euler );

                EditorGUIUtility.labelWidth = storedLabelWidth;
                EditorGUIUtility.fieldWidth = storedFieldWidth;
            }
            EditorGUILayout.EndHorizontal();

            // Save the changes.
            if( EditorGUI.EndChangeCheck() )
            {
                if( _targetOverrideX != null )
                    _targetOverrideX.SetValue( vectorValue.x );

                if( _targetOverrideY != null )
                    _targetOverrideY.SetValue( vectorValue.y );

                if( _targetOverrideZ != null )
                    _targetOverrideZ.SetValue( vectorValue.z );

                if( _targetOverrideW != null )
                    _targetOverrideW.SetValue( vectorValue.w );

                property.quaternionValue = vectorValue;
                property.serializedObject.ApplyModifiedProperties();
            }
        }

        private SerializedProperty FindPropertySibling(
            SerializedProperty property, string path )
        {
            int index = property.propertyPath.LastIndexOf( '.' );

            if( index == -1 )
                return null;

            return property.serializedObject.FindProperty(
                property.propertyPath.Substring( 0, index ) + "." + path );
        }
    }
}
