﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

#pragma warning disable 618

namespace VisualDesignCafe.Editor.Prefabs.Overrides
{
    using Object = UnityEngine.Object;

    public class PrefabOverridesUpdater
    {
        private Queue<Exception> _exceptions = new Queue<Exception>();

        public void Update( Transform root )
        {
            var prefabOverrides =
                root.GetComponentsInChildren<PrefabOverrides>( true );

            PrefabOverrides[] orderedOverrides =
                prefabOverrides
                    .OrderByDescending(
                        p => PrefabHierarchyUtility.GetDepth(
                                p.GetComponent<Prefab>(),
                                true ) )
                    .ToArray();

            foreach( var overrides in orderedOverrides )
                UpdatePrefab( overrides );
        }

        private void UpdatePrefab( PrefabOverrides prefabOverrides )
        {
            if( prefabOverrides == null )
                return;

            UpdatePrefabOverrides( prefabOverrides );

            if( _exceptions.Count > 0 )
            {
                Debug.LogError(
                    "Failed to update prefab overrides. (See below for details)" +
                    string.Join(
                        "\n",
                        _exceptions.Select( e => e.ToString() ).ToArray() ) );

                _exceptions.Clear();
                return;
            }

            if( Config.DEBUG_INFO )
                Console.Log( "Clearing old serialized data..." );

            // Clear the old overrides after saving. 
            // We can use the updated overrides from now on.
            prefabOverrides.GetCompatibilityOverrides().Clear();

            // Mark the component as dirty.
            EditorUtility.SetDirty( prefabOverrides );

            if( Config.DEBUG_INFO )
                Console.Log( "Finished Updating 'Prefab Overrides' component." );
        }

        private void UpdatePrefabOverrides( PrefabOverrides overrides )
        {
            List<PrefabOverrideObject> objects =
                overrides.GetCompatibilityOverrides();

            if( objects.Count == 0 )
                return;

            if( Config.DEBUG_INFO )
                Console.Log( "Updating 'Prefab Overrides' component..." );

            Console.Indent++;
            foreach( PrefabOverrideObject obj in objects )
            {
                if( obj == null )
                    continue;

                try
                {
                    UpdateObject( overrides, obj );
                }
                catch( Exception e )
                {
                    _exceptions.Enqueue( e );
                }
            }
            Console.Indent--;
        }

        private void UpdateObject(
            PrefabOverrides overrides, PrefabOverrideObject obj )
        {
            if( obj == null )
                return;

            if( Config.DEBUG_INFO )
                Console.LogFormat(
                    "Updating Object Override. ({0})",
                    obj.HierarchyPath );

            GameObject target =
                FindTargetGameObject( overrides.transform, obj );

            if( target == null )
            {
                if( Config.DEBUG_WARNING )
                    Console.LogWarning( "Could not find target for override." );
                return;
            }

            overrides.AddObjectOverride( target );

            Console.Indent++;
            for( int i = 0; i < obj.Count; i++ )
            {
                try
                {
                    UpdateComponent(
                        overrides, target, obj.GetComponentAt( i ) );
                }
                catch( Exception e )
                {
                    _exceptions.Enqueue( e );
                }
            }
            Console.Indent--;
        }

        private void UpdateComponent(
            PrefabOverrides overrides,
            GameObject gameObject,
            PrefabOverrideComponent component )
        {
            if( component == null )
                return;

            if( Config.DEBUG_INFO )
                Console.LogFormat(
                    "Updating Component Override. ({0})",
                    component.TypeName );

            Object target = FindTargetObject( gameObject, component );
            if( target == null )
            {
                if( Config.DEBUG_WARNING )
                    Console.LogWarning( "Could not find target for override." );
                return;
            }

            overrides.AddComponentOverride( target );

            Console.Indent++;
            for( int i = 0; i < component.Count; i++ )
            {
                try
                {
                    UpdateProperty(
                        overrides, target, component.GetPropertyAt( i ) );
                }
                catch( Exception e )
                {
                    _exceptions.Enqueue( e );
                }
            }
            Console.Indent--;
        }

        private void UpdateProperty(
            PrefabOverrides overrides,
            Object component,
            PrefabOverride property )
        {
            if( property == null || string.IsNullOrEmpty( property.Path ) )
                return;

            if( Config.DEBUG_INFO )
                Console.LogFormat(
                    "Updating Property Override. ({0})",
                    property.Path );

            PropertyOverride newOverride =
                overrides.AddPropertyOverride( component, property.Path );

            newOverride.SetValue( property.AnimationCurveValue );
            newOverride.SetValue( property.BooleanValue );
            newOverride.SetValue( property.BoundsValue );
            newOverride.SetValue( property.ColorValue );
            newOverride.SetValue( property.DoubleValue );
            newOverride.SetValue( property.FloatValue );
            newOverride.SetValue( property.IntValue );
            newOverride.SetValue( property.LongValue );
            newOverride.SetValue( property.ObjectReferenceValue );
            newOverride.SetValue( property.QuaternionValue );
            newOverride.SetValue( property.RectValue );
            newOverride.SetValue( property.StringValue );
            newOverride.SetValue( property.Vector2Value );
            newOverride.SetValue( property.Vector3Value );
            newOverride.SetValue( property.Vector4Value );
        }

        private GameObject FindTargetGameObject(
            Transform root, PrefabOverrideObject obj )
        {
            if( obj.Target != null )
                return obj.Target;

            Transform target =
                Guid.FindChild( root, obj.ObjectPath )
                    ?? root.Find( obj.HierarchyPath );

            return target != null ? target.gameObject : null;
        }

        private Object FindTargetObject(
            GameObject gameObject, PrefabOverrideComponent component )
        {
            if( component.Target != null )
                return component.Target;

            return
                component.TypeName != "GameObject"
                    ? (Object) gameObject.GetComponent( component.TypeName )
                    : gameObject;
        }
    }
}