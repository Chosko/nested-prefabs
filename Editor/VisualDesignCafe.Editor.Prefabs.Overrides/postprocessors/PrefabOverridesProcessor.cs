﻿using System.Collections.Generic;
using UnityEditor;
using VisualDesignCafe.Editor.Prefabs.Comparing;

namespace VisualDesignCafe.Editor.Prefabs.Overrides
{
    [CustomProcessor( typeof( Prefab ) )]
    internal class PrefabOverridesProcessor
    {

        /// <summary>
        /// Called before applying modifications to a prefab.
        /// </summary>
        /// <param name="prefab">The prefab that will be modified.</param>
        /// <param name="modifications">The modifications that will be applied to the prefab.</param>
        /// <param name="sourcePrefab">The source prefab that modifications are applied from.</param>
        /// <returns></returns>
        public static Modification[] PostprocessModifications(
            Prefab prefab, Modification[] modifications, Prefab sourcePrefab )
        {
            if( Config.DEBUG_INFO )
                Console.Log( "Removing prefab overrides from modifications." );

            modifications = PostprocessModificationsForPrefab( prefab, modifications );
            modifications = PostprocessModificationsForPrefab( sourcePrefab, modifications );

            return modifications;
        }

        private static Modification[] PostprocessModificationsForPrefab(
            Prefab prefab, Modification[] modifications )
        {
            while( !Prefab.IsNull( prefab ) )
            {
                var properties = prefab.CachedGameObject.GetComponent<PrefabOverrides>();

                if( properties != null )
                {
                    modifications = RemoveOverridesFromModifications( properties, modifications );
                }

                prefab = PrefabHierarchyUtility.GetParentPrefab( prefab );
            }

            return modifications;
        }

        private static Modification[] RemoveOverridesFromModifications(
            PrefabOverrides overrides, Modification[] modifications )
        {
            if( overrides.GetComponent<NestedPrefab>() == null )
                return modifications;

            if( Config.DEBUG_INFO && Config.VERBOSE )
                Console.Log( "Removing permanent prefab overrides from modification list." );

            Console.Indent++;

            var modificationList = new List<Modification>();
            modificationList.AddRange( modifications );

            foreach( var obj in overrides.Objects )
            {
                if( obj == null )
                    continue;

                foreach( var component in obj.Components )
                {
                    if( component == null )
                        continue;

                    foreach( var property in component.Properties )
                    {
                        if( property == null )
                            continue;

                        modificationList.RemoveAll(
                            m =>
                            {
                                if( m == null )
                                    return true;

                                if( m.Type != ModificationType.PropertyValueChanged )
                                    return false;

                                if( m.DestinationProperty == null )
                                    return false;

                                if( m.DestinationProperty.propertyPath != property.Path )
                                {
                                    // Gradients have a type of 'color' but have additional sub-properties that should be ignored.
                                    // Therefore the property path does not match with the override and requires an additional check.
                                    if( !IsGradient( m.DestinationProperty, property ) )
                                        return false;
                                }

                                if( m.DestinationProperty.serializedObject.targetObject != component.Target
                                    && m.CachedSourceProperty.Component.Serialized.targetObject != component.Target )
                                    return false;

                                if( Config.DEBUG_INFO && Config.VERBOSE )
                                    Console.Log( "Removed <b>" + property.Path + "</b>" );

                                return true;
                            } );
                    }
                }
            }

            Console.Indent--;

            return modificationList.ToArray();
        }

        private static bool IsGradient(
            SerializedProperty serializedProperty, PropertyOverride propertyOverride )
        {
            return serializedProperty.propertyType == SerializedPropertyType.Color
                && serializedProperty.propertyPath.StartsWith( propertyOverride.Path );
        }
    }
}