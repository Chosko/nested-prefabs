﻿using UnityEditor;
using UnityEngine;

namespace VisualDesignCafe.Editor.Prefabs
{
    [CustomEditor( typeof( NestedPrefab ) )]
    internal sealed class NestedPrefabInspector : UnityEditor.Editor
    {
        /// <summary>
        /// Immediately hides the NestedPrefab components after the inspector (editor) is opened.
        /// </summary>
        private void OnEnable()
        {
            if( target == null || targets == null )
                return;

            foreach( Object t in targets )
            {
                if( t == null )
                    continue;

                Config.SetDefaultHideFlags( t );
            }
        }
    }
}