﻿using UnityEditor;
using UnityEngine;

namespace VisualDesignCafe.Editor.Prefabs
{
    [CustomEditor( typeof( Guid ) )]
    internal sealed class GuidInspector : UnityEditor.Editor
    {
        /// <summary>
        /// Immediately hides the Guid components after the inspector (editor) is opened.
        /// </summary>
        private void OnEnable()
        {
            if( target == null || targets == null )
                return;

            foreach( Object t in targets )
            {
                if( t == null )
                    continue;

                Config.SetDefaultHideFlags( t );
            }
        }
    }
}