﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;
using VisualDesignCafe.Editor.Prefabs.Overrides;

namespace VisualDesignCafe.Editor.Prefabs
{

    public sealed class NestedPrefabHierarchyWindow
    {

        private const string APPLY_MODIFIED_HIERARCHY =
            "Can't apply added children or components locally. " +
            "Apply all changes to the entire prefab hierarchy or modify the original prefab asset (\"{0}\").";

        public static NestedPrefabsPostprocessor Postprocessor
        {
            get;
            set;
        }

        public static bool SelectionIsNestedPrefab
        {
            get;
            set;
        }

        public static GameObject SelectionParent
        {
            get;
            set;
        }

        public static int SelectionDepth
        {
            get;
            set;
        }

        public static HashSet<GameObject> SelectedHierarchy
        {
            get;
            set;
        }

        public static Dictionary<PrefabOverrides, HashSet<GameObject>> SelectedPrefabOverrides =
            new Dictionary<PrefabOverrides, HashSet<GameObject>>();

        public static Texture2D DottedTextureVertical
        {
            get
            {
                if( _dottedTextureVertical == null )
                {
                    _dottedTextureVertical = new Texture2D( 1, 4, TextureFormat.ARGB32, false, false );
                    _dottedTextureVertical.SetPixels(
                        new Color[]
                        {
                            new Color(1, 1, 1, 1),
                            new Color(1, 1, 1, 1),
                            new Color(1,1,1, 0),
                            new Color(1,1,1, 0),
                        } );
                    _dottedTextureVertical.Apply();
                    _dottedTextureVertical.hideFlags = HideFlags.HideAndDontSave;
                }
                return _dottedTextureVertical;
            }
        }

        public static Texture2D DottedTextureHorizontal
        {
            get
            {
                if( _dottedTextureHorizontal == null )
                {
                    _dottedTextureHorizontal = new Texture2D( 4, 1, TextureFormat.ARGB32, false, false );
                    _dottedTextureHorizontal.SetPixels(
                        new Color[]
                        {
                            new Color(1, 1, 1, 1),
                            new Color(1, 1, 1, 1),
                            new Color(1,1,1, 0),
                            new Color(1,1,1, 0),
                        } );
                    _dottedTextureHorizontal.Apply();
                    _dottedTextureHorizontal.hideFlags = HideFlags.HideAndDontSave;
                }
                return _dottedTextureHorizontal;
            }
        }

        private static Texture _nestedPrefabIcon;
        private static Texture _prefabIcon;
        private static Texture _nestedModelPrefabIcon;
        private static Texture _modelPrefabIcon;

        private static Rect _selectionParentRect;

        private static GUIStyle _corruptPrefabLabelStyle;
        private static GUIStyle _missingPrefabLabelStyle;
        private static GUIStyle _prefabLabelStyle;
        private static GUIStyle _prefabLabelSelectedStyle;

        private static Color[] _iconColorCollection = new Color[]
        {
            new Color32( 198, 140, 253, 255 ),
            new Color32( 255, 165, 68, 255 ),
            new Color32( 138, 255, 68, 255 ),
            new Color32( 255, 120, 120, 255 ),
        };

        private static int _refreshCachedValuesId = 0;
        private static Texture2D _dottedTextureVertical = null;
        private static Texture2D _dottedTextureHorizontal = null;

        private static MethodInfo _hasSearchFilter;
        private static FieldInfo _getActiveHierarchyWindow;


        /// <summary>
        /// Initialize the hierarchy window. Should be called at startup
        /// </summary>
        [InitializeOnLoadMethod]
        public static void Initialize()
        {
            _nestedPrefabIcon = EditorGUIUtility.FindTexture( "Prefab Icon" );
            _prefabIcon = EditorGUIUtility.FindTexture( "PrefabNormal Icon" );
            _modelPrefabIcon = EditorGUIUtility.FindTexture( "PrefabModel Icon" );
            _nestedModelPrefabIcon = EditorGUIUtility.FindTexture( "DefaultAsset Icon" );

            EditorApplication.hierarchyWindowItemOnGUI -= HierarchyWindowItemOnGUI;
            EditorApplication.hierarchyWindowItemOnGUI += HierarchyWindowItemOnGUI;

            EditorApplication.hierarchyWindowChanged -= OnHierarchyWindowChanged;
            EditorApplication.hierarchyWindowChanged += OnHierarchyWindowChanged;
        }

        private static void OnHierarchyWindowChanged()
        {
            _refreshCachedValuesId++;
        }


        /// <summary>
        /// OnGUI Callback for every item in the hierarchy
        /// </summary>
        private static void HierarchyWindowItemOnGUI( int instanceID, Rect itemRect )
        {
            if( Application.isPlaying )
                return;

            try
            {
                GameObject currentObject =
                    EditorUtility.InstanceIDToObject( instanceID ) as GameObject;

                if( currentObject == null )
                    return;

                var nestedPrefab = currentObject.GetComponent<NestedPrefab>();
                var prefab = currentObject.GetComponent<Prefab>();

                Rect iconRect = GetPrefabIconRect( itemRect );

                DrawOverrides( itemRect, currentObject );
                DrawHelperLine( itemRect, currentObject, prefab );
                if( prefab != null )
                {
                    DrawPrefabIcon( iconRect, currentObject, prefab, nestedPrefab );
                }
                else if( nestedPrefab != null )
                {
                    DrawMissingPrefabIcon( iconRect, "Corrupt Prefab" );
                }
            }
            catch( Exception e )
            {
                Debug.LogException( e );
            }
        }

        /// <summary>
        /// Draws a helper line to indicate the hierarchy relation for the selected object.
        /// </summary>
        private static void DrawHelperLine(
            Rect itemRect, GameObject currentObject, Prefab prefab )
        {
            if( currentObject == Selection.activeGameObject
                && SelectionParent != null )
            {
                DrawLineToItem( itemRect, prefab, currentObject );
            }
            else if( !SelectionIsNestedPrefab
                        && SelectedHierarchy != null
                        && SelectedHierarchy.Contains( currentObject ) )
            {
                DrawIndentLine( itemRect );
            }
        }

        /// <summary>
        /// Draws a vertical line on the left side of the rect.
        /// Only the 'y' and 'height' value of the rect are used.
        /// </summary>
        private static void DrawIndentLine( Rect itemRect )
        {
            itemRect.x = ( SelectionDepth - 1 ) * 14f + 6f + 3f;
            itemRect.width = 1;

            Color storedColor = GUI.color;
            {
                GUI.color *=
                    EditorGUIUtility.isProSkin
                        ? new Color( 1, 1, 1, .3f )
                        : new Color( 0, 0, 0, .3f );

                GUI.DrawTexture( itemRect, Texture2D.whiteTexture );
            }
            GUI.color = storedColor;
        }

        /// <summary>
        /// Draws a line indicating the hierarchy relation for the current object.
        /// </summary>
        private static void DrawLineToItem(
            Rect itemRect, Prefab prefab, GameObject o )
        {
            if( prefab == null )
            {
                float left = ( SelectionDepth - 1 ) * 14f + 6f + 3f;
                float right =
                    o.transform.childCount > 0
                        ? itemRect.x - itemRect.height
                        : itemRect.x;

                Color storedColor = GUI.color;
                {
                    GUI.color *= new Color32( 220, 220, 220, 255 );
                    GUI.DrawTexture(
                        new Rect( left, itemRect.y, 1, itemRect.height * 0.5f ),
                        Texture2D.whiteTexture );
                    GUI.DrawTexture(
                        new Rect( left, itemRect.center.y, right - left, 1 ),
                        Texture2D.whiteTexture );
                }
                GUI.color = storedColor;
            }
            else
            {
                float width = itemRect.height + 1;
                float right =
                    o.transform.childCount > 0 ? itemRect.x - width : itemRect.x - width * 0.3f;

                Color storedColor = GUI.color;
                {
                    GUI.color *= new Color32( 220, 220, 220, 255 );
                    GUI.DrawTexture(
                        new Rect( right - 20, itemRect.center.y - 1, 5, 1 ),
                        Texture2D.whiteTexture );
                    GUI.DrawTexture(
                        new Rect( right - 20, itemRect.center.y - 1, 1, 4 ),
                        Texture2D.whiteTexture );
                    GUI.DrawTexture(
                        new Rect( right - 20, itemRect.center.y + 3, 20, 1 ),
                        Texture2D.whiteTexture );
                }
                GUI.color = storedColor;
            }
        }

        /// <summary>
        /// Draws the prefab overrides for the current object.
        /// </summary>
        private static void DrawOverrides( Rect itemRect, GameObject currentObject )
        {
            const int LOOP_LIMIT = 21;
            int index = 0;

            PrefabOverrides selectedProperties = null;
            Prefab selectedPrefab = null;
            int selectedDepth = -1;

            Prefab parentPrefab = PrefabHierarchyUtility.GetPrefab( currentObject );
            while( parentPrefab != null )
            {
                if( index++ > LOOP_LIMIT )
                    throw new OverflowException( "Loop limit reached" );

                var properties =
                    parentPrefab.CachedGameObject.GetComponent<PrefabOverrides>();
                if( properties != null )
                {
                    int depth = PrefabHierarchyUtility.GetDepth( properties.transform, null );

                    if( Selection.activeGameObject == properties.gameObject )
                    {
                        selectedProperties = properties;
                        selectedPrefab = parentPrefab;
                        selectedDepth = depth;
                    }
                    else
                    {
                        DrawHelperLineForOverrides(
                            itemRect,
                            currentObject,
                            parentPrefab,
                            properties,
                            depth );
                    }
                }

                parentPrefab =
                    PrefabHierarchyUtility.GetParentPrefab( parentPrefab );
            }

            if( selectedProperties != null )
                DrawHelperLineForOverrides(
                    itemRect,
                    currentObject,
                    selectedPrefab,
                    selectedProperties,
                    selectedDepth );
        }

        /// <summary>
        /// Draws a helper line from the current object to the target of each prefab override.
        /// </summary>
        private static void DrawHelperLineForOverrides(
            Rect itemRect,
            GameObject currentObject,
            Prefab prefab,
            PrefabOverrides overrides,
            int depth )
        {
            HashSet<GameObject> hierarchy;
            if( !SelectedPrefabOverrides.TryGetValue( overrides, out hierarchy ) )
                return;

            if( !hierarchy.Contains( currentObject ) )
                return;

            int prefabDepth =
                PrefabHierarchyUtility.GetDepth(
                    prefab.CachedGameObject.GetComponent<NestedPrefab>() );

            Color prefabColor = GetIconColor( prefabDepth );
            bool currentObjectHasOverride =
                overrides.GetObjectOverride( currentObject ) != null;

            var lineRect =
                new Rect( depth * 14f - 2f, itemRect.y, 1, itemRect.height );

            if( currentObjectHasOverride )
                lineRect.height =
                    lineRect.height
                    * 0.5f
                    - ( Selection.activeGameObject == currentObject ? 3 : 0 );

            Color storedColor = GUI.color;
            {
                if( Selection.activeGameObject == currentObject && currentObjectHasOverride )
                {
                    GUI.color *= prefabColor;
                }
                else
                {
                    GUI.color *=
                        prefabColor
                        * ( EditorGUIUtility.isProSkin
                            ? new Color( .8f, .8f, .8f )
                            : new Color( .8f, .8f, .8f ) );

                    if( Selection.activeGameObject == overrides.gameObject )
                        GUI.color =
                            EditorGUIUtility.isProSkin
                                ? new Color32( 220, 220, 220, 255 )
                                : (Color32) Color.white;
                }

                // Draw vertical dotted line.
                GUI.DrawTextureWithTexCoords(
                    lineRect,
                    DottedTextureVertical,
                    new Rect( 0, 0, 1, itemRect.height * 0.25f ),
                    true );

                if( currentObjectHasOverride )
                {
                    // Draw property override icon.
                    Color storedTextColor = GUI.skin.label.normal.textColor;
                    {
                        GUI.skin.label.normal.textColor = Color.white;
                        GUI.Label(
                            new Rect(
                                itemRect.x - 24,
                                itemRect.y - 9 + lineRect.height,
                                20,
                                itemRect.height ),
                            new GUIContent(
                                "o",
                                PrefabOverrideGuiUtility.ListOverridesAsString( overrides ) ) );
                    }
                    GUI.skin.label.normal.textColor = storedTextColor;

                    // Draw horizontal line.
                    int propertyDepth =
                        PrefabHierarchyUtility.GetDepth(
                            currentObject.transform,
                            overrides.transform );

                    GUI.DrawTextureWithTexCoords(
                        new Rect(
                            lineRect.x + 1,
                            lineRect.yMax,
                            propertyDepth * 14f - 5,
                            1 ),
                        DottedTextureHorizontal,
                        new Rect( 0, 0, lineRect.width * 0.25f, 1 ),
                        true );
                }
            }
            GUI.color = storedColor;
        }

        /// <summary>
        /// Calculates the rect for the prefab icon.
        /// </summary>
        private static Rect GetPrefabIconRect( Rect rect )
        {
            Rect iconRect =
                new Rect( rect.x - rect.height, rect.y, rect.height, rect.height );

            Vector2 center = iconRect.center;
            iconRect.width -= 3;
            iconRect.height -= 3;
            iconRect.center = center;

            iconRect.x -= iconRect.height - 2;

            if( HasSearchFilter() )
            {
                iconRect.x += 10;
            }

            return iconRect;
        }


        /// <summary>
        /// Draws the icon for a prefab.
        /// </summary>
        private static void DrawPrefabIcon(
            Rect iconRect,
            GameObject currentObject,
            Prefab prefab,
            NestedPrefab nestedPrefab )
        {
            PrefabType prefabType =
                PrefabUtility.GetPrefabType( prefab.CachedGameObject );
            bool isNested =
                nestedPrefab != null
                && PrefabHierarchyUtility.FindPrefabRoot( nestedPrefab.CachedGameObject )
                    != nestedPrefab.CachedGameObject;
            bool isConnectedToAsset =
                NestedPrefabUtility.IsConnectedToAsset( prefab );
            bool isModelPrefab =
                ( !string.IsNullOrEmpty( prefab.AssetGuid )
                    && !AssetDatabase.GUIDToAssetPath( prefab.AssetGuid )
                        .EndsWith( ".prefab", StringComparison.OrdinalIgnoreCase ) );
            bool isConnectedToPrefab =
                prefabType != PrefabType.DisconnectedPrefabInstance
                    && prefabType != PrefabType.DisconnectedModelPrefabInstance;

            Color32 iconColor = new Color( 1, 1, 1, 0.8f );
            if( isNested )
                iconColor = GetIconColor( PrefabHierarchyUtility.GetDepth( nestedPrefab ) );

            Color storedColor = GUI.color;
            {
                // Draw black outline
                if( ( currentObject == SelectionParent
                        && !SelectionIsNestedPrefab )
                    || currentObject == Selection.activeGameObject )
                {
                    GUI.color *= new Color( 0, 0, 0, .7f );
                    GUI.DrawTexture(
                        new Rect(
                            iconRect.x - 1,
                            iconRect.y - 1,
                            iconRect.width + 2,
                            iconRect.height + 2 ),
                        _prefabIcon );
                    GUI.color = storedColor;
                }

                if( isConnectedToAsset )
                {
                    if( isModelPrefab )
                        DrawModelIcon( iconRect, isNested, iconColor );
                    else
                        DrawDefaultPrefabIcon( iconRect, isNested, iconColor );

                    DrawReferenceCollectionIcon( iconRect, prefab );

                    if( !isConnectedToPrefab )
                        DrawDisconnectedPrefabIcon( iconRect );
                }
                else
                {
                    DrawMissingPrefabIcon( iconRect );
                }

                string toolTip =
                    GetTooltip(
                        prefab,
                        isConnectedToAsset,
                        isNested,
                        isConnectedToPrefab );

                if( GUI.Button(
                    iconRect,
                    new GUIContent(
                        string.Empty,
                        toolTip ),
                    EditorStyles.label ) )
                {
                    if( Event.current.button == 0 )
                    {
                        if( prefab.Asset != null )
                            EditorGUIUtility.PingObject( prefab.Asset );
                    }
                    else if( Event.current.button == 1 )
                    {
                        OpenContextMenu( currentObject, prefab, isConnectedToAsset );
                    }
                }
                GUI.color = storedColor;
            }
        }

        /// <summary>
        /// Draws a default prefab icon.
        /// </summary>
        private static void DrawDefaultPrefabIcon(
            Rect iconRect, bool isNested, Color iconColor )
        {
            Color storedColor = GUI.color;
            GUI.color *= iconColor;
            GUI.DrawTexture(
                iconRect,
                isNested ? _nestedPrefabIcon : _prefabIcon );
            GUI.color = storedColor;
        }

        /// <summary>
        /// Draws a model icon.
        /// </summary>
        private static void DrawModelIcon(
            Rect iconRect, bool isNested, Color iconColor )
        {
            if( isNested )
            {
                Color storedColor = GUI.color;
                GUI.color *= iconColor;
                GUI.DrawTexture( iconRect, _nestedPrefabIcon );
                GUI.color = storedColor;

                GUI.color = Color.white;
                GUI.DrawTexture(
                    new Rect(
                        iconRect.x + 7,
                        iconRect.y + 6,
                        7,
                        7 ),
                    _nestedModelPrefabIcon );
                GUI.color = storedColor;
            }
            else
            {
                GUI.DrawTexture( iconRect, _modelPrefabIcon );
            }
        }

        /// <summary>
        /// Draws a yellow question mark (?) if the reference collection is invalid.
        /// </summary>
        private static void DrawReferenceCollectionIcon(
            Rect iconRect, Prefab prefab )
        {
            if( prefab.ReferenceCollection == null )
                return;

            if( ReferenceCollectionUtility.IsValidCached(
                    prefab.ReferenceCollection, _refreshCachedValuesId ) )
                return;

            if( _corruptPrefabLabelStyle == null )
            {
                _corruptPrefabLabelStyle = new GUIStyle( EditorStyles.whiteBoldLabel );
                _corruptPrefabLabelStyle.normal.textColor = Color.yellow;
            }

            GUI.Label(
                new Rect( iconRect.x + 1, iconRect.y - 1, 24, 24 ),
                "?",
                _corruptPrefabLabelStyle );
        }

        /// <summary>
        /// Draws a red 'X'.
        /// </summary>
        private static void DrawDisconnectedPrefabIcon( Rect iconRect )
        {
            if( _missingPrefabLabelStyle == null )
            {
                _missingPrefabLabelStyle = new GUIStyle( EditorStyles.whiteBoldLabel );
                _missingPrefabLabelStyle.normal.textColor = Color.red;
            }

            GUI.Label(
                new Rect( iconRect.x - 1, iconRect.y + 1, 24, 24 ),
                "x",
                _missingPrefabLabelStyle );
        }

        /// <summary>
        /// Draws a red 'X' and a white prefab icon.
        /// </summary>
        private static void DrawMissingPrefabIcon( Rect iconRect, string tooltip = null )
        {
            if( _missingPrefabLabelStyle == null )
            {
                _missingPrefabLabelStyle = new GUIStyle( EditorStyles.whiteBoldLabel );
                _missingPrefabLabelStyle.normal.textColor = Color.red;
            }

            GUI.DrawTexture( iconRect, _nestedPrefabIcon );

            GUI.Label(
                new Rect( iconRect.x - 1, iconRect.y + 1, 24, 24 ),
                "x",
                _missingPrefabLabelStyle );

            if( !string.IsNullOrEmpty( tooltip ) )
            {
                GUI.Button(
                        iconRect,
                        new GUIContent(
                            string.Empty,
                            tooltip ),
                        EditorStyles.label );
            }
        }

        /// <summary>
        /// Creates a tooltip for the prefab.
        /// </summary>
        private static string GetTooltip(
            Prefab prefab,
            bool isConnected,
            bool isNested,
            bool isConnectedToPrefab )
        {
            string toolTip = "Missing Prefab";

            if( !isConnectedToPrefab )
                return "Disconnected Prefab Instance";

            if( isConnected )
            {
                if( isNested )
                {
                    toolTip =
                        string.Join(
                            "/",
                            PrefabHierarchyUtility.GetPrefabhHierarchy(
                                prefab.CachedTransform,
                                PrefabHierarchyUtility.FindPrefabRoot( prefab.CachedGameObject ).transform,
                                true )
                                    .Select( p => p != null ? p.name : "?" )
                                    .ToArray() )
                            + "/"
                            + prefab.Asset.name;
                }
                else
                {
                    toolTip = prefab.Asset.name;
                }
            }

            return toolTip;
        }

        /// <summary>
        /// Opens the context menu for the object.
        /// </summary>
        private static void OpenContextMenu(
            GameObject currentObject, Prefab prefab, bool isConnected )
        {
            if( Prefab.IsNull( prefab ) )
                return;

            GenericMenu menu = new GenericMenu();

            GameObject asset = prefab.Asset;
            if( asset == null || !isConnected )
            {
                AddRemoveConnection( menu, true, currentObject, prefab, false );
                menu.AddSeparator( string.Empty );
                AddHelp( menu, prefab );
                menu.ShowAsContext();
                return;
            }

            bool hasModifications = false;
            bool hasModificationsSelf = false;
            bool hierarchyIsConnected = HierarchyIsConnected( prefab, false );
            bool hasModificationsRevert = false;
            bool hasModificationsSelfRevert = false;
            bool isRootPrefab = PrefabHierarchyUtility.FindPrefabRoot( prefab.CachedGameObject ) == prefab.CachedGameObject;
            bool isNestedModelPrefab = NestedPrefabUtility.IsModelPrefab( prefab );
            bool isRootAndModelPrefab = isRootPrefab && isNestedModelPrefab;

            try
            {
                hasModifications =
                    NestedPrefabUtility.HasModifications( prefab, true, false );
                hasModificationsSelf =
                    NestedPrefabUtility.HasModifications( prefab, false, false );
                hasModificationsRevert =
                    NestedPrefabUtility.HasModifications( prefab, true, true, false );
                hasModificationsSelfRevert =
                    NestedPrefabUtility.HasModifications( prefab, false, true, false );
            }
            catch( Exception e )
            {
                Debug.LogException( e );
            }

            if( !isRootAndModelPrefab )
            {
                if( !isNestedModelPrefab )
                {
                    AddApplySelf(
                        menu,
                        hierarchyIsConnected
                            && hasModificationsSelf
                            && !isRootAndModelPrefab
                            && !isNestedModelPrefab,
                        currentObject,
                        prefab );

                    AddApply(
                        menu,
                        hierarchyIsConnected
                            && hasModifications
                            && !isRootAndModelPrefab
                            && !isNestedModelPrefab,
                        currentObject,
                        prefab );
                }

                AddApplyAll(
                    menu,
                    !isRootAndModelPrefab,
                    currentObject,
                    prefab );

                menu.AddSeparator( string.Empty );
            }

            AddRevertSelf(
                menu,
                hierarchyIsConnected && hasModificationsSelfRevert,
                currentObject,
                prefab,
                isNestedModelPrefab );

            AddRevert(
                menu,
                hierarchyIsConnected && hasModificationsRevert,
                currentObject,
                prefab,
                isNestedModelPrefab );

            AddRevertAll( menu, true, currentObject, prefab );

            menu.AddSeparator( string.Empty );

            AddRemoveConnection(
                menu,
                hierarchyIsConnected || isRootPrefab,
                currentObject,
                prefab,
                isNestedModelPrefab );

            if( !ReferenceCollectionUtility.IsValid( prefab.ReferenceCollection ) )
            {
                menu.AddSeparator( string.Empty );
                AddFix( menu, hierarchyIsConnected, currentObject, prefab );
            }

            menu.AddSeparator( string.Empty );
            AddHelp( menu, prefab );

            menu.ShowAsContext();
        }

        /// <summary>
        /// Adds an item to the context menu for 'Apply All'
        /// </summary>
        private static void AddApplyAll(
            GenericMenu menu,
            bool isEnabled,
            GameObject currentObject,
            Prefab prefab )
        {
            if( !isEnabled )
            {
                menu.AddDisabledItem( new GUIContent( "Apply All" ) );
                return;
            }

            menu.AddItem(
                new GUIContent( "Apply All" ),
                false,
                ( data ) =>
                {
                    try
                    {
                        GameObject root =
                            PrefabHierarchyUtility.FindPrefabRoot( currentObject );

                        NestedPrefabUtility.ReplacePrefab(
                            root,
                            PrefabUtility.GetPrefabParent( root ),
                            ReplacePrefabOptions.ConnectToPrefab );
                    }
                    catch( Exception e )
                    {
                        Debug.LogException( e );
                    }

                },
                null );
        }

        /// <summary>
        /// Adds an item to the context menu for Apply.
        /// </summary>
        private static void AddApply(
            GenericMenu menu,
            bool isEnabled,
            GameObject currentObject,
            Prefab prefab )
        {
            if( !isEnabled )
            {
                menu.AddDisabledItem( new GUIContent( "Apply Prefab and Children" ) );
                return;
            }

            menu.AddItem(
                new GUIContent( "Apply Prefab and Children" ),
                false,
                ( data ) =>
                {
                    try
                    {
                        if( NestedPrefabUtility.HasModifiedHierarchy(
                                prefab,
                                true,
                                false ) )
                        {
                            int result =
                                EditorUtility.DisplayDialogComplex(
                                    "Modified Hierarchy",
                                    string.Format( APPLY_MODIFIED_HIERARCHY, prefab.Asset.name ),
                                    "Ok",
                                    "Apply All",
                                    "Cancel" );

                            if( result == 1 )
                            {
                                GameObject root =
                                    PrefabHierarchyUtility.FindPrefabRoot( currentObject );

                                NestedPrefabUtility.ReplacePrefab(
                                    root,
                                    PrefabUtility.GetPrefabParent( root ),
                                    ReplacePrefabOptions.ConnectToPrefab );
                            }
                        }
                        else
                        {
                            Postprocessor.ApplyChangesDelayed(
                                currentObject.GetComponent<Prefab>(),
                                true );
                        }
                    }
                    catch( Exception e )
                    {
                        Debug.LogException( e );
                    }

                },
                null );
        }

        /// <summary>
        /// Adds an item to the context menu for Apply Self.
        /// </summary>
        private static void AddApplySelf(
            GenericMenu menu,
            bool isEnabled,
            GameObject currentObject,
            Prefab prefab )
        {
            if( !isEnabled )
            {
                menu.AddDisabledItem( new GUIContent( "Apply Prefab" ) );
                return;
            }

            menu.AddItem(
                new GUIContent( "Apply Prefab" ),
                false,
                ( data ) =>
                {
                    try
                    {
                        if( NestedPrefabUtility.HasModifiedHierarchy(
                                prefab,
                                false,
                                false ) )
                        {
                            int result =
                                EditorUtility.DisplayDialogComplex(
                                    "Modified Hierarchy",
                                    string.Format( APPLY_MODIFIED_HIERARCHY, prefab.Asset.name ),
                                    "Ok",
                                    "Apply All",
                                    "Cancel" );

                            if( result == 1 )
                            {
                                GameObject root =
                                    PrefabHierarchyUtility.FindPrefabRoot( currentObject );

                                NestedPrefabUtility.ReplacePrefab(
                                    root,
                                    PrefabUtility.GetPrefabParent( root ),
                                    ReplacePrefabOptions.ConnectToPrefab );
                            }
                        }
                        else
                        {
                            Postprocessor.ApplyChangesDelayed(
                                currentObject.GetComponent<Prefab>(),
                                false );
                        }
                    }
                    catch( Exception e )
                    {
                        Debug.LogException( e );
                    }

                },
                null );
        }

        /// <summary>
        /// Adds an item to the context menu for Revert.
        /// </summary>
        private static void AddRevertAll(
            GenericMenu menu,
            bool isEnabled,
            GameObject currentObject,
            Prefab prefab )
        {
            if( !isEnabled )
            {
                menu.AddDisabledItem( new GUIContent( "Revert All" ) );
                return;
            }

            menu.AddItem(
                new GUIContent( "Revert All" ),
                false,
                ( data ) =>
                {
                    try
                    {
                        GameObject root =
                            PrefabHierarchyUtility.FindPrefabRoot( currentObject );

                        PrefabUtility.ReconnectToLastPrefab( root );
                        PrefabUtility.RevertPrefabInstance( root );
                    }
                    catch( Exception e )
                    {
                        Debug.LogException( e );
                    }
                },
                null );
        }

        /// <summary>
        /// Adds an item to the context menu for Revert.
        /// </summary>
        private static void AddRevert(
            GenericMenu menu,
            bool isEnabled,
            GameObject currentObject,
            Prefab prefab,
            bool isModelPrefab )
        {
            if( !isEnabled )
            {
                menu.AddDisabledItem(
                    new GUIContent(
                        isModelPrefab
                            ? "Revert to Prefab"
                            : "Revert Prefab and Children" ) );
                return;
            }

            menu.AddItem(
                new GUIContent(
                    isModelPrefab
                        ? "Revert to Prefab"
                        : "Revert Prefab and Children" ),
                false,
                ( data ) =>
                {
                    try
                    {
                        NestedPrefabUtility.ReloadPrefab(
                            prefab, prefab.Asset, true, true, true, false );

                        if( isModelPrefab )
                            NestedPrefabUtility.RevertModelToPrefab( prefab );
                    }
                    catch( Exception e )
                    {
                        Debug.LogException( e );
                    }
                },
                null );
        }

        /// <summary>
        /// Adds an item to the context menu for Revert Self.
        /// </summary>
        private static void AddRevertSelf(
            GenericMenu menu,
            bool isEnabled,
            GameObject currentObject,
            Prefab prefab,
            bool isModelPrefab )
        {
            if( !isEnabled )
            {
                menu.AddDisabledItem(
                    new GUIContent(
                        isModelPrefab
                            ? "Revert to Model"
                            : "Revert Prefab" ) );
                return;
            }

            menu.AddItem(
                new GUIContent(
                    isModelPrefab
                        ? "Revert to Model"
                        : "Revert Prefab" ),
                false,
                ( data ) =>
                {
                    try
                    {
                        NestedPrefabUtility.ReloadPrefab(
                            prefab, prefab.Asset, true, false, true, true );
                    }
                    catch( Exception e )
                    {
                        Debug.LogException( e );
                    }
                },
                null );
        }

        /// <summary>
        /// Adds an item to the context menu for 'Remove Prefab Connection'.
        /// </summary>
        private static void AddRemoveConnection(
            GenericMenu menu,
            bool isEnabled,
            GameObject currentObject,
            Prefab prefab,
            bool isModelPrefab )
        {
            if( !isEnabled )
            {
                menu.AddDisabledItem(
                    new GUIContent(
                        isModelPrefab
                            ? "Break Model Connection"
                            : "Break Prefab Connection" ) );
                return;
            }

            menu.AddItem(
                new GUIContent(
                    isModelPrefab
                        ? "Break Model Connection"
                        : "Break Prefab Connection" ),
                false,
                ( data ) =>
                {
                    try
                    {
                        NestedPrefabUtility.RemovePrefabConnection(
                            prefab, Postprocessor.Database );
                    }
                    catch( Exception e )
                    {
                        Debug.LogException( e );
                    }

                    Postprocessor.HierarchyWindowPostprocessor.OnSelectionChanged();
                }, null );
        }

        /// <summary>
        /// Adds an item to the context menu for Fix.
        /// </summary>
        private static void AddFix(
            GenericMenu menu,
            bool isEnabled,
            GameObject currentObject,
            Prefab prefab )
        {
            if( !isEnabled )
            {
                menu.AddDisabledItem( new GUIContent( "Fix" ) );
                return;
            }

            menu.AddItem( new GUIContent( "Fix" ), false,
                        ( data ) =>
                        {
                            NestedPrefabUtility.FixPrefab( prefab );
                            if( !ReferenceCollectionUtility.IsValid( prefab.ReferenceCollection ) )
                            {
                                EditorUtility.DisplayDialog(
                                    "Could not fix prefab.",
                                    "Could not fix missing children in prefab. Please try reimporting the prefab asset.",
                                    "Ok" );
                            }
                        }, null );
        }

        private static void AddHelp( GenericMenu menu, Prefab prefab )
        {
            bool hasUnusualIcon = false;

            if( prefab.Asset == null )
            {
                hasUnusualIcon = true;
            }
            else if( !prefab.ReferenceCollection.IsValidCached )
            {
                hasUnusualIcon = true;
            }
            else
            {
                PrefabType prefabType = PrefabUtility.GetPrefabType( prefab.CachedGameObject );
                if( prefabType == PrefabType.DisconnectedPrefabInstance
                    || prefabType == PrefabType.DisconnectedModelPrefabInstance )
                {
                    hasUnusualIcon = true;
                }
            }

            menu.AddItem(
                new GUIContent( "Help" ),
                false,
                ( data ) =>
                {
                    if( hasUnusualIcon )
                    {
                        Application.OpenURL(
                            "https://www.visualdesigncafe.com/nestedprefabs/documentation/icons/" );
                    }
                    else
                    {
                        Application.OpenURL(
                            "https://www.visualdesigncafe.com/nestedprefabs/documentation/" );

                    }
                },
                null );
        }

        private static bool HierarchyIsConnected(
            Prefab prefab, bool includeSelf = true )
        {
            if( Prefab.IsNullOrMissing( prefab ) )
                return false;

            PrefabType type = PrefabUtility.GetPrefabType( prefab.CachedGameObject );

            if( type == PrefabType.DisconnectedModelPrefabInstance
                || type == PrefabType.DisconnectedPrefabInstance
                || type == PrefabType.MissingPrefabInstance )
            {
                return false;
            }

            return true;
        }

        public static bool HasSearchFilter()
        {
            try
            {
                if( _hasSearchFilter == null )
                    _hasSearchFilter =
                        typeof( SearchableEditorWindow )
                            .GetMethod(
                                "get_hasSearchFilter",
                                BindingFlags.NonPublic | BindingFlags.Instance );

                if( _getActiveHierarchyWindow == null )
                    _getActiveHierarchyWindow =
                        typeof( SearchableEditorWindow )
                            .Assembly
                            .GetType( "UnityEditor.SceneHierarchyWindow" )
                            .GetField(
                                "s_LastInteractedHierarchy",
                                BindingFlags.NonPublic | BindingFlags.Static );

                if( _hasSearchFilter == null || _getActiveHierarchyWindow == null )
                    return false;

                object hierarchy = _getActiveHierarchyWindow.GetValue( null );
                if( hierarchy == null )
                    return false;

                return (bool) _hasSearchFilter.Invoke( hierarchy, null );
            }
            catch( NullReferenceException )
            {
                return false;
            }
        }

        private static Color GetIconColor( int prefabDepth )
        {
            return _iconColorCollection[
                ( prefabDepth - 1 ) % _iconColorCollection.Length ];
        }
    }
}