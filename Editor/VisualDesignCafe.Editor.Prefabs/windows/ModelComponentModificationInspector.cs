﻿using UnityEditor;
using UnityEngine;

namespace VisualDesignCafe.Editor.Prefabs
{
    [CustomEditor( typeof( ModelComponentModification ) )]
    internal sealed class ModelComponentModificationInspector : UnityEditor.Editor
    {
        /// <summary>
        /// Immediately hides the ModelComponentModification components after the inspector (editor) is opened.
        /// </summary>
        private void OnEnable()
        {
            if( target == null || targets == null )
                return;

            foreach( Object t in targets )
            {
                if( t == null )
                    continue;

                Config.SetDefaultHideFlags( t );
            }
        }
    }
}