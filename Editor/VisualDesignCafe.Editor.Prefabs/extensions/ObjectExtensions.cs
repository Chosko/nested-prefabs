﻿using System.Text;
using UnityEditor;
using UnityEngine;

namespace VisualDesignCafe.Editor.Prefabs
{
    internal static class ObjectExtensions
    {
        public static string ToLog(
            this GameObject gameObject )
        {
            if( gameObject == null )
                return "null";

            string color = EditorGUIUtility.isProSkin ? "ffffff66" : "00000066";

            var message = new StringBuilder();

            // Type
            message.Append(
               string.Format(
                   "<color=#{0}>{1} </color> ",
                   color,
                   GetPrefabType( gameObject ) ) );

            // Name
            message.Append(
                string.Format(
                    "<b>{0}</b>",
                    gameObject != null ? gameObject.name : "null" ) );

            // Instance ID
            message.Append(
                string.Format(
                    "<color=#{0}> ({1})</color>",
                    color,
                    gameObject != null ? gameObject.GetInstanceID() : -1 ) );

            // Path
            message.Append(
               string.Format(
                   "<color=#{0}> [<i>\"{1}\"</i>]</color>",
                   color,
                   GetHierarchyPath( gameObject ) ) );

            // GUID
            Guid guid = gameObject.GetComponent<Guid>();
            message.Append(
              string.Format(
                  "<color=#{0}> [<i>\"{1}\"</i>]</color>",
                  color,
                  guid != null ? guid.Value : "???-???-???-???" ) );

            // Prefab GUID
            if( guid != null && guid.Prefab != null )
            {
                message.Append(
                  string.Format(
                      "<color=#{0}> [<i>\"{1}\"</i>]</color>",
                      color,
                      guid.Prefab.PrefabGuid ) );
            }

            return message.ToString();
        }

        public static string PrintHierarchy(
            this GameObject gameObject, string indent = "" )
        {
            var message = new StringBuilder();

            message.AppendLine( indent + gameObject.ToLog() );

            indent += "    ";

            foreach( Transform child in gameObject.transform )
                message.Append( child.gameObject.PrintHierarchy( indent ) );

            return message.ToString();
        }

        public static GameObject AsGameObject( this Object source )
        {
            if( source == null )
                return null;

            GameObject gameObject = source as GameObject;

            if( gameObject != null )
                return gameObject;

            Component component = source as Component;

            if( component != null )
                return component.gameObject;

            return null;
        }

        private static string GetPrefabType( GameObject prefab )
        {
            if( prefab == null )
                return "Unknown";

            if( EditorUtility.IsPersistent( prefab ) )
                return "Asset";

            if( PrefabOperation.IsAssetInstance( prefab ) )
                return "Asset Instance";

            if( prefab.GetComponent<NestedPrefab>() != null && prefab.transform.parent != null )
                return "Nested";

            PrefabType type = PrefabUtility.GetPrefabType( prefab );
            if( type != PrefabType.None )
                return "Root";

            return "Unknown";
        }

        private static string GetHierarchyPath( GameObject gameObject )
        {
            string path = string.Empty;
            Transform parent = gameObject.transform;
            while( parent != null )
            {
                path = parent.name + "/" + path;
                parent = parent.parent;
            }

            return path.Remove( path.Length - 1 );
        }
    }
}
