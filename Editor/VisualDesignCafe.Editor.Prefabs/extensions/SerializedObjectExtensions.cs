﻿using System;
using System.Reflection;
using UnityEditor;

namespace VisualDesignCafe.Editor.Prefabs
{
    internal static class SerializedObjectExtensions
    {
        private static MethodInfo _applyModifiedPropertiesWithoutUndo;
        private static MethodInfo _updateIfRequiredOrScript;

        /// <summary>
        /// Compatibility method for ApplyModifiedPropertiesWithoutUndo because it is not publicly accessible in Unity 5.0
        /// </summary>
        public static void SaveModifications( this SerializedObject serializedObject )
        {
            if( serializedObject == null )
                throw new ArgumentNullException();

            if( _applyModifiedPropertiesWithoutUndo == null )
            {
                _applyModifiedPropertiesWithoutUndo =
                    serializedObject.GetType().GetMethod(
                        "ApplyModifiedPropertiesWithoutUndo",
                        BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic );

                if( _applyModifiedPropertiesWithoutUndo == null )
                    throw new NullReferenceException( "Could not find method 'ApplyModifiedPropertiesWithoutUndo'." );
            }

            _applyModifiedPropertiesWithoutUndo.Invoke( serializedObject, null );
        }

        /// <summary>
        /// Compatibility method for UpdateIfRequiredOrScript because it has different names between Unity versions.
        /// </summary>
        public static void UpdateIfRequired( this SerializedObject serializedObject )
        {
            if( serializedObject == null )
                throw new ArgumentNullException();

            if( _updateIfRequiredOrScript == null )
            {
                _updateIfRequiredOrScript =
                    serializedObject.GetType().GetMethod(
                        "UpdateIfRequiredOrScript",
                        BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic );

                if( _updateIfRequiredOrScript == null )
                    _updateIfRequiredOrScript =
                        serializedObject.GetType().GetMethod(
                            "UpdateIfDirtyOrScript",
                            BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic );

                if( _updateIfRequiredOrScript == null )
                    throw new NullReferenceException( "Could not find method 'UpdateIfRequiredOrScript'." );
            }

            _updateIfRequiredOrScript.Invoke( serializedObject, null );
        }
    }
}
