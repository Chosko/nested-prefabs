﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace VisualDesignCafe.Editor.Prefabs
{
    using Object = UnityEngine.Object;

    public class HierarchyWindowPostprocessor
    {
        public event Action OnTrialPrefabLimitReached;

        protected readonly PrefabDatabase Database;

        private GameObject _activeGameObject;

        public HierarchyWindowPostprocessor(
            PostprocessorEventDispatcher eventDispatcher,
            PrefabDatabase database )
        {
            Database = database;
            eventDispatcher.HierarchyWindowChanged += OnHierarchyWindowChanged;
            eventDispatcher.EditorUpdate += OnEditorUpdate;
            eventDispatcher.PostprocessAllAssets += OnPostprocessAllAssets;
            eventDispatcher.UndoRedoPerformed += OnUndoRedoPerformed;
            eventDispatcher.PrefabInstanceUpdated += OnPrefabInstanceUpdated;
        }

        /// <summary>
        /// Called whenever any prefab instance in the scene was updated.
        /// </summary>
        private void OnPrefabInstanceUpdated( GameObject instance )
        {
            foreach( GameObject gameObject in Selection.gameObjects )
            {
                if( gameObject.transform.IsChildOf( instance.transform ) )
                {
                    SetHideFlagsForModelComponents();
                    break;
                }
            }
        }

        /// <summary>
        /// Called every time an Undo or Redo operation was performed
        /// </summary>
        private void OnUndoRedoPerformed()
        {
            if( Application.isPlaying )
                return;

            if( NestedPrefabsPostprocessor.IsDestroyed )
                return;

            if( !NestedPrefabsPostprocessor.IsEnabled )
                return;

            OnHierarchyWindowChanged();
            OnSelectionChanged();
        }

        /// <summary>
        /// Called whenever any asset was imported, moved, or deleted.
        /// </summary>
        private void OnPostprocessAllAssets(
            string[] importedAssets,
            string[] deletedAssets,
            string[] movedAssets,
            string[] movedFromAssetPaths )
        {
            // Setting the active gameobject to null will force a refresh.
            _activeGameObject = null;
        }

        /// <summary>
        /// Called every frame in the editor
        /// </summary>
        private void OnEditorUpdate()
        {
            if( Application.isPlaying )
                return;

            if( NestedPrefabsPostprocessor.IsDestroyed )
                return;

            if( Selection.activeGameObject != _activeGameObject )
            {
                _activeGameObject = Selection.activeGameObject;
                OnSelectionChanged();
            }
        }

        /// <summary>
        /// Called whenever anything in the hierarchy window changed.
        /// </summary>
        private void OnHierarchyWindowChanged()
        {
            if( Application.isPlaying )
                return;

            if( NestedPrefabsPostprocessor.IsDestroyed )
                return;

            if( !NestedPrefabsPostprocessor.IsEnabled )
                return;

            bool isEnabled = NestedPrefabsPostprocessor.IsEnabled;
            NestedPrefabsPostprocessor.IsEnabled = false;
            SetHideFlagsForModelComponents();
            NestedPrefabsPostprocessor.IsEnabled = isEnabled;
            CachePrefabsInSelection( Selection.gameObjects );
        }


        /// <summary>
        /// Loops through the objects and checks for any changes within the hierarchy,
        /// creating or deleting Nested Prefabs where necessary.
        /// </summary>
        private void CachePrefabsInSelection( GameObject[] selection )
        {
            var prefabs = new List<GameObject>();

            // Find all prefabs in the current selection,
            // this will only find the prefab roots.
            foreach( GameObject obj in selection )
            {
                if( obj == null )
                    continue;

                // Make sure we skip assets in the selection.
                if( EditorUtility.IsPersistent( obj ) )
                    continue;

                try
                {
                    // Check for any nested prefabs that were dragged 
                    // outside of its parent's hierarchy.
                    var currentNestedPrefab = obj.GetComponent<NestedPrefab>();
                    if( currentNestedPrefab != null
                        && currentNestedPrefab.CachedTransform.parent == null )
                    {
                        // Not supported in Unity 5.0
                        if( currentNestedPrefab.Asset != null
#if DEV
                            && !Config.IS_UNIT_TEST
#endif
                            )
                        {
                            NestedPrefabUtility.ConnectGameObjectToPrefab(
                                currentNestedPrefab.CachedGameObject,
                                currentNestedPrefab.Asset );
                        }

                        Component.DestroyImmediate( currentNestedPrefab );
                    }
                    else
                    {
                        FindPrefabs( obj.transform, prefabs );
                    }
                }
                catch( Exception e )
                {
                    Debug.LogException( e );
                }
            }

            // Loop through all the prefabs and check if they are nested.
            for( int i = 0; i < prefabs.Count; i++ )
            {
                if( prefabs[ i ] == null )
                    continue;

                try
                {
                    Transform parent;
                    NestedPrefabType parentType =
                        PrefabHierarchyUtility.FindValidPrefab(
                            prefabs[ i ].transform.parent,
                            out parent );

                    switch( parentType )
                    {
                        case NestedPrefabType.None:

                            // The prefab is not part of any prefab hierarchy anymore, 
                            // which means it was dragged outside of its parent's hierarchy.
                            // We have to reconnect it to the original prefab asset.
                            ReconnectToOriginalAsset( prefabs[ i ] );

                            break;
                        case NestedPrefabType.Prefab:
                        case NestedPrefabType.NestedPrefab:

                            var prefab = prefabs[ i ].GetComponent<Prefab>();
                            if( prefab != null )
                            {
                                EnsureNestedPrefabComponentExists( prefabs[ i ] );
                                PreventCircularReference( prefab );
                            }

#if TRIAL
                            CheckNestedPrefabLimit( prefabs[ i ] );
#endif

                            break;
                    }
                }
                catch( Exception e )
                {
                    if( Config.RETHROW )
                        throw;
                    else
                        Debug.LogException( e );
                }
            }
        }

        private void ReconnectToOriginalAsset( GameObject gameObject )
        {
            var nestedPrefab = gameObject.GetComponent<NestedPrefab>();
            if( nestedPrefab != null )
            {
                if( nestedPrefab.Asset != null
#if DEV
                            && !Config.IS_UNIT_TEST
#endif
                            )
                {
                    // Not supported in Unity 5.0
                    NestedPrefabUtility.ConnectGameObjectToPrefab(
                        gameObject, nestedPrefab.Asset );
                }

                Component.DestroyImmediate( nestedPrefab );
            }
        }

        private void PreventCircularReference( Prefab prefab )
        {
            if( !Config.PREVENT_CIRCULAR_REFERENCES )
                return;

            if( !PrefabHierarchyUtility.HasCircularReference( prefab ) )
                return;

            if( Config.STRICT )
                throw new InvalidOperationException( "Nested prefab has a circular reference" );
            else if( Config.DEBUG_ERROR )
                Debug.LogError( "Nested prefab has a circular reference" );

            Component.DestroyImmediate( prefab.gameObject.GetComponent<NestedPrefab>() );
            NestedPrefabsPostprocessor.IsEnabled = false;
            Undo.PerformUndo();
            Selection.objects = new Object[ 0 ];

            if( !Prefab.IsNull( prefab )
                && PrefabHierarchyUtility.HasCircularReference( prefab ) )
            {
                prefab.CachedTransform.parent = null;
            }

            NestedPrefabsPostprocessor.IsEnabled = true;
        }

        private void EnsureNestedPrefabComponentExists( GameObject gameObject )
        {
            var nestedPrefab = gameObject.GetComponent<NestedPrefab>();
            if( nestedPrefab == null )
            {
                nestedPrefab = gameObject.AddComponent<NestedPrefab>();
                Config.SetDefaultHideFlags( nestedPrefab );
            }
        }

        private void SetHideFlagsForModelComponents()
        {
            foreach( GameObject gameObject in Selection.gameObjects )
            {
                if( gameObject == null )
                    continue;

                Component[] components;

                Prefab prefab;
                if( !ObjectIsModel( gameObject, out prefab ) )
                {
                    components = gameObject.GetComponents<Component>();

                    foreach( Component component in components )
                    {
                        // Make sure components with a missing script reference are skipped.
                        if( component == null )
                            continue;

                        // Ignore internal components.
                        if( component is NestedPrefab
                            || component is Prefab
                            || component is ModelModification
                            || component is ModelComponentModification
                            || component is Guid )
                        {
                            Config.SetDefaultHideFlags( component );
                            continue;
                        }

                        if( FlagsUtility.IsSet(
                            component.hideFlags, HideFlags.NotEditable ) )
                        {
                            component.hideFlags =
                                FlagsUtility.Unset(
                                    component.hideFlags, HideFlags.NotEditable );
                        }
                    }

                    continue;
                }

                var componentModifications =
                    gameObject.GetComponent<ModelComponentModification>();

                Component[] modifications =
                    componentModifications != null
                        ? componentModifications.Components
                        : new Component[ 0 ];

                components = gameObject.GetComponents<Component>();

                foreach( Component component in components )
                {
                    // Make sure components with a missing script reference are skipped.
                    if( component == null )
                        continue;

                    // Ignore internal components, they are already hidden.
                    if( component is NestedPrefab
                        || component is Prefab
                        || component is ModelModification
                        || component is ModelComponentModification
                        || component is Guid )
                    {
                        Config.SetDefaultHideFlags( component );
                        continue;
                    }

                    // Ignore if the component was added to the asset in the prefab.
                    if( modifications.Contains( component ) )
                    {
                        if( FlagsUtility.IsSet(
                            component.hideFlags, HideFlags.NotEditable ) )
                        {
                            component.hideFlags =
                                FlagsUtility.Unset(
                                    component.hideFlags, HideFlags.NotEditable );
                        }

                        continue;
                    }

                    // Skip transforms for nested prefab root.
                    if( gameObject == prefab.CachedGameObject
                        && ( component is Transform
                            || component.GetType().GetCustomAttributes(
                                typeof( SaveInParent ), true ).Length > 0 ) )
                    {
                        if( FlagsUtility.IsSet(
                            component.hideFlags, HideFlags.NotEditable ) )
                        {
                            component.hideFlags =
                                FlagsUtility.Unset(
                                    component.hideFlags, HideFlags.NotEditable );
                        }

                        continue;
                    }

                    // Ignore if the component is a prefab override.
                    if( PrefabUtility.IsComponentAddedToPrefabInstance( component ) )
                    {
                        if( FlagsUtility.IsSet(
                            component.hideFlags, HideFlags.NotEditable ) )
                        {
                            component.hideFlags =
                                FlagsUtility.Unset(
                                    component.hideFlags, HideFlags.NotEditable );
                        }

                        continue;
                    }

                    component.hideFlags |= HideFlags.NotEditable;
                }
            }
        }

        private bool ObjectIsModel( GameObject gameObject, out Prefab prefab )
        {
            prefab = null;

            if( PrefabUtility.GetPrefabType( gameObject ) == PrefabType.None )
                return false;

            prefab = PrefabHierarchyUtility.GetPrefab( gameObject );

            if( Prefab.IsNull( prefab ) )
                return false;

            var nestedPrefab = prefab.CachedGameObject.GetComponent<NestedPrefab>();
            if( nestedPrefab == null )
                return false;

            if( !NestedPrefabUtility.IsModelPrefab( prefab ) )
                return false;

            if( NestedPrefabUtility.IsModelModification( gameObject ) )
                return false;

            return true;
        }


        /// <summary>
        /// Collects the entire hierarchy of the transform and stores it in the HashSet
        /// </summary>
        private void CacheHierarchy(
            Transform root, Transform transform, HashSet<GameObject> hierarchy )
        {
            if( transform.parent == null || transform == root )
                return;

            if( !hierarchy.Contains( transform.gameObject ) )
                hierarchy.Add( transform.gameObject );

            int siblingIndex = transform.GetSiblingIndex();
            for( int i = 0; i < siblingIndex; i++ )
            {
                CacheHierarchyRecursive( transform.parent.GetChild( i ), hierarchy );
            }

            CacheHierarchy( root, transform.parent, hierarchy );
        }

        /// <summary>
        /// Collects the entire hierarchy of the transform and stores it in the HashSet.
        /// </summary>
        private void CacheHierarchyRecursive(
            Transform transform, HashSet<GameObject> hierarchy )
        {
            hierarchy.Add( transform.gameObject );

            foreach( Transform child in transform )
                CacheHierarchyRecursive( child, hierarchy );
        }

        /// <summary>
        /// Finds all prefabs in the given transform and its children
        /// </summary>
        private void FindPrefabs( Transform transform, List<GameObject> prefabs )
        {
            PrefabType type = PrefabUtility.GetPrefabType( transform.gameObject );

            if( type != PrefabType.None
                && PrefabHierarchyUtility.FindPrefabRoot( transform.gameObject ) == transform.gameObject )
            {
                prefabs.Add( transform.gameObject );
            }

            foreach( Transform child in transform )
            {
                FindPrefabs( child, prefabs );
            }
        }

        /// <summary>
        /// Called when the active object in the selection changed
        /// </summary>
        public void OnSelectionChanged()
        {
            GameObject activeGameObject = Selection.activeGameObject;

            NestedPrefabHierarchyWindow.SelectedHierarchy = null;
            NestedPrefabHierarchyWindow.SelectionParent = null;
            NestedPrefabHierarchyWindow.SelectionIsNestedPrefab = false;
            NestedPrefabHierarchyWindow.SelectionDepth = -1;

            if( activeGameObject != null )
            {
                Transform parent;
                NestedPrefabType type =
                    PrefabHierarchyUtility.FindValidPrefab(
                        activeGameObject.transform, out parent );

                if( type != NestedPrefabType.None )
                {
                    NestedPrefabHierarchyWindow.SelectionParent =
                        parent.gameObject;

                    NestedPrefabHierarchyWindow.SelectionIsNestedPrefab =
                        activeGameObject.GetComponent<NestedPrefab>() != null;

                    NestedPrefabHierarchyWindow.SelectionDepth =
                        PrefabHierarchyUtility.GetDepth( parent );

                    var hierarchy = new HashSet<GameObject>();
                    CacheHierarchy( parent, activeGameObject.transform, hierarchy );
                    NestedPrefabHierarchyWindow.SelectedHierarchy = hierarchy;
                }

                NestedPrefabHierarchyWindow.SelectedPrefabOverrides.Clear();
                CollectPrefabOverrides(
                    PrefabHierarchyUtility.FindRoot( activeGameObject.transform ),
                    NestedPrefabHierarchyWindow.SelectedPrefabOverrides );
            }

            SetHideFlagsForModelComponents();
        }

        private void CollectPrefabOverrides(
            Transform root, Dictionary<PrefabOverrides, HashSet<GameObject>> collection )
        {
            var properties = root.GetComponent<PrefabOverrides>();
            if( properties != null )
            {
                HashSet<GameObject> hashSet;
                if( !collection.TryGetValue( properties, out hashSet ) )
                {
                    hashSet = new HashSet<GameObject>();
                    collection[ properties ] = hashSet;
                }

                foreach( var objectOverride in properties.Objects )
                {
                    if( objectOverride.Target == null )
                        continue;

                    CacheHierarchy(
                        properties.transform,
                        objectOverride.Target.transform,
                        hashSet );
                }
            }

            foreach( Transform child in root )
            {
                CollectPrefabOverrides( child, collection );
            }
        }

#if TRIAL
        private int GetNestedPrefabCount()
        {
            return Database.GetAllReferencedPrefabAssets().Length;
        }

        internal void CheckNestedPrefabLimit( GameObject prefab )
        {
            int count = GetNestedPrefabCount();

            if( count < 10 )
                return;

            // Check if the asset has already been nested before. 
            // We can allow renesting prefabs, just don't allow any new ones.
            try
            {
                GameObject asset = PrefabUtility.GetPrefabParent( prefab ) as GameObject;

                if( asset != null )
                {
                    if( AssetDatabaseUtility.FindAssetsWithReferenceTo(
                        asset,
                        Database,
                        false,
                        1,
                        true
                        ).Length > 0 )
                    {
                        return;
                    }
                }
            }
            catch( Exception e )
            {
                Debug.LogException( e );
            }

            // Remove Nested Prefab component.
            NestedPrefab nestedPrefabObject = prefab.GetComponent<NestedPrefab>();

            if( nestedPrefabObject != null )
                Component.DestroyImmediate( nestedPrefabObject );

            // Undo parenting
            //Undo.PerformUndo();

            // Change the parent to null, just in case.
            if( prefab.transform.parent != null )
                prefab.transform.SetParent( null );

            if( OnTrialPrefabLimitReached != null )
                OnTrialPrefabLimitReached.Invoke();

        }
#endif
    }
}
