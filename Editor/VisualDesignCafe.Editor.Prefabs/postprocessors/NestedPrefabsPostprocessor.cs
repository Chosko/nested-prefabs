﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using VisualDesignCafe.Editor.Prefabs.Overrides;

namespace VisualDesignCafe.Editor.Prefabs
{
    public sealed class NestedPrefabsPostprocessor
    {
        public static string[] IGNORED_FOLDERS = new string[]
        {
            //"Assets/Example Ignored Folder/",
        };

        public static event Action<long> OnPrefabDatabaseCached;
        public static event Action<long> OnApplied;

        /// <summary>
        /// Is the post processor enabled? 
        /// When disabled, prefabs will still be processed and validated, but no changes will be applied to nested prefabs in the project.
        /// The postprocessor will be disabled while installing and when restoring the prefab database after building.
        /// </summary>
        public static bool IsEnabled
        {
            get;
            set;
        }

        /// <summary>
        /// Is the post processor destroyed?
        /// When destroyed, all postprocess operations will be ignored.
        /// The postprocessor will be destroyed when uninstalling Nested Prefabs.
        /// </summary>
        internal static bool IsDestroyed
        {
            get;
            private set;
        }

        private static double _pendingChangesTimer = 0;
        private static float _pendingChangesDelay = 0.5f;

        public PrefabPostprocessor PrefabPostprocessor
        {
            get;
            private set;
        }

        public ModelPostprocessor ModelPostprocessor
        {
            get;
            private set;
        }

        public HierarchyWindowPostprocessor HierarchyWindowPostprocessor
        {
            get;
            private set;
        }

        public ScenePostprocessor ScenePostprocessor
        {
            get;
            private set;
        }

        public ApplyPool ApplyPool
        {
            get;
            private set;
        }

        private PostprocessorEventDispatcher CallbackProvider
        {
            get;
            set;
        }

        private PrefabOverridesUpdater PrefabOverridesUpdater
        {
            get;
            set;
        }

        public readonly PrefabDatabase Database;

        public static bool IsIgnored( string path )
        {
            if( IGNORED_FOLDERS == null || IGNORED_FOLDERS.Length == 0 )
                return false;

            foreach( var ignored in IGNORED_FOLDERS )
            {
                if( path.StartsWith( ignored, StringComparison.OrdinalIgnoreCase ) )
                {
                    return true;
                }
            }
            return false;
        }

        public NestedPrefabsPostprocessor(
            PostprocessorEventDispatcher callbackProvider, PrefabDatabase database )
        {
            IsEnabled = true;

            CallbackProvider = callbackProvider;

            Database = database;
            ApplyPool = new ApplyPool();
            PrefabPostprocessor =
                new PrefabPostprocessor( callbackProvider, ApplyPool );
            ModelPostprocessor =
                new ModelPostprocessor( callbackProvider, PrefabPostprocessor );
            HierarchyWindowPostprocessor =
                new HierarchyWindowPostprocessor( callbackProvider, database );
            ScenePostprocessor =
                new ScenePostprocessor( callbackProvider );
            PrefabOverridesUpdater =
                new PrefabOverridesUpdater();

            callbackProvider.EditorUpdate += EditorUpdate;
            callbackProvider.PostprocessAllAssets += OnPostprocessAllAssets;

            PrefabPostprocessor.OnPrefabAssetUpdated += OnPrefabAssetUpdated;

            Prefab.GetAssetForPrefab =
                new Prefab.GetAsset(
                    guid => AssetDatabase.LoadAssetAtPath(
                                AssetDatabase.GUIDToAssetPath( guid ),
                                typeof( GameObject ) )
                            as GameObject );
        }

        private void OnPrefabAssetUpdated( string path )
        {
            RemovePrefabFromDatabase( path );
        }

        /// <summary>
        /// Destroys the postprocessor. Should only be used during Uninstall.
        /// </summary>
        public void Destroy()
        {
            CallbackProvider.EditorUpdate -= EditorUpdate;

            IsDestroyed = true;
        }

        public void Create()
        {
            CallbackProvider.EditorUpdate -= EditorUpdate;
            CallbackProvider.EditorUpdate += EditorUpdate;

            IsDestroyed = false;
        }

        public void ClearPrefabDatabase()
        {
            Database.Clear();
        }

        internal void RemovePrefabFromDatabase( string path )
        {
            string guid = AssetDatabase.AssetPathToGUID( path );
            Database.Remove( guid );
        }

        internal void RefreshPrefabInDatabase( string path )
        {
            GameObject prefab =
                AssetDatabase.LoadAssetAtPath(
                    path,
                    typeof( GameObject ) ) as GameObject;

            if( prefab == null )
            {
                if( Config.DEBUG_WARNING )
                    Console.LogWarningFormat( "Could not load prefab at path. ({0})", path );

                return;
            }

            RefreshPrefabInDatabase( path, prefab );
        }

        internal void RefreshPrefabInDatabase( string path, GameObject prefab )
        {
            try
            {
                Database.Add( prefab );
            }
            catch( Exception e )
            {
                Debug.LogException( e );
            }
        }

        internal void FlushDatabase()
        {
            double startTime = EditorApplication.timeSinceStartup;
            Database.Flush(
                    ( float progress ) =>
                    {
                        if( EditorApplication.timeSinceStartup - startTime >= 2 )
                        {
                            EditorUtility.DisplayProgressBar(
                                "Hold On",
                                "Saving prefab database...",
                                progress );
                        }
                    } );
        }

        internal void OnPostprocessAllAssets(
            string[] importedAssets,
            string[] deletedAssets,
            string[] movedAssets,
            string[] movedFromAssetPaths )
        {
            if( IsDestroyed )
                return;

            // Apply changes for all nested prefabs when a prefab asset was changed
            foreach( string path in importedAssets )
            {
                if( path.EndsWith( ".prefab", System.StringComparison.OrdinalIgnoreCase ) )
                {
                    if( IsIgnored( path ) )
                        continue;

                    GameObject prefab =
                        AssetDatabase.LoadAssetAtPath(
                            path,
                            typeof( GameObject ) ) as GameObject;

                    if( prefab == null )
                        continue;

                    RefreshPrefabInDatabase( path, prefab );

                    try
                    {
                        var processedAssets = new List<string>();
                        processedAssets.Add( AssetDatabase.AssetPathToGUID( path ) );

                        PrefabPostprocessor.OnPostprocessPrefabAsset( prefab, processedAssets );
                        PrefabOverridesUpdater.Update( prefab.transform );
                    }
                    catch( Exception e )
                    {
                        Debug.LogException( e );
                    }
                }
            }

            foreach( string path in movedAssets )
            {
                if( IsIgnored( path ) )
                    continue;

                if( path.EndsWith( ".prefab", StringComparison.OrdinalIgnoreCase ) )
                    RefreshPrefabInDatabase( path );
            }

            foreach( string path in deletedAssets )
            {
                if( IsIgnored( path ) )
                    continue;

                if( path.EndsWith( ".prefab", StringComparison.OrdinalIgnoreCase ) )
                    RemovePrefabFromDatabase( path );
            }
        }

        internal void EnsurePrefabDatabaseIsValid()
        {
            try
            {
                var stopwatch = new System.Diagnostics.Stopwatch();
                stopwatch.Start();

                double startTime = EditorApplication.timeSinceStartup;
                Database.Refresh(
                    ( float progress ) =>
                    {
                        if( EditorApplication.timeSinceStartup - startTime >= 2 )
                        {
                            EditorUtility.DisplayProgressBar(
                                "Hold On",
                                "Caching prefab database...",
                                progress );
                        }
                    } );

                stopwatch.Stop();

                if( OnPrefabDatabaseCached != null )
                    OnPrefabDatabaseCached.Invoke( stopwatch.ElapsedMilliseconds );
            }
            catch( Exception e )
            {
                Debug.LogException( e );
            }
        }

        /// <summary>
        /// Applies changes to the prefab in the next update loop.
        /// </summary>
        internal void ApplyChangesDelayed( Prefab prefab, bool includeNestedChildren )
        {
            // The prefab is not actually saved and postprocessed so we 
            // have to manually update the Prefab Overrides values.
            PrefabOverrideUtility.UpdateAll( prefab.CachedGameObject );

            ApplyPool.Add( prefab, includeNestedChildren, true, false );
        }

        /// <summary>
        /// Called every frame in the editor
        /// </summary>
        internal void EditorUpdate()
        {
            if( Application.isPlaying )
                return;

            if( IsDestroyed )
                return;

            if( ApplyPool.Count > 0
                && ( !Config.DELAY_APPLY_CHANGES || EditorApplication.timeSinceStartup - _pendingChangesTimer >= _pendingChangesDelay ) )
            {
                ApplyPendingChanges();
            }

            if( Database.IsDirty )
                FlushDatabase();
        }

        private void ApplyPendingChanges()
        {
            var applyBatchStopwatch = new System.Diagnostics.Stopwatch();

            try
            {
                EnsurePrefabDatabaseIsValid();

                applyBatchStopwatch.Start();

                // The postprocessor should be disabled so that prefabs that are saved after being modified are not added to the apply pool again.
                IsEnabled = false;

                PrefabOperation.BeginBatch();

                KeyValuePair<GameObject, List<Prefab>>[] queue = ApplyPool.GetSortedQueue();

                if( Config.DEBUG_INFO )
                {
                    Console.Log( "Applying pending changes:\n" +
                        string.Join(
                            "\n",
                            queue
                                .Select(
                                    q =>
                                        q.Key
                                        + ":\n"
                                        + ( q.Value != null
                                             ? string.Join(
                                                 "\n",
                                                 q.Value.Select(
                                                     v =>
                                                         "    "
                                                         + v.CachedGameObject.name
                                                         + " ("
                                                         + PrefabHierarchyUtility.GetDepth( v, true )
                                                         + ")" )
                                                    .ToArray() )
                                             : string.Empty ) )
                                .ToArray() ) );
                }

                // Apply all changes to the nested prefabs first...
                for( int i = 0; i < queue.Length; i++ )
                {
                    if( queue[ i ].Value == null )
                        continue;

                    NestedPrefabUtility.ApplyChanges( queue[ i ].Value.ToArray(), false );
                }

                // ...Then reload all instances.
                for( int i = 0; i < queue.Length; i++ )
                {
                    if( queue[ i ].Key == null )
                        continue;

                    NestedPrefabUtility.ReloadInstancesForAsset(
                        queue[ i ].Key,
                        Database,
                        false );

                    if( Config.APPLY_OVERRIDES_TO_ASSET )
                        PrefabOverrideUtility.ApplyAll( queue[ i ].Key );
                }
            }
            catch( Exception e )
            {
                if( Config.RETHROW )
                    throw e;
                else if( Config.DEBUG_EXCEPTION )
                {
                    Debug.LogException( e );
                }
            }
            finally
            {
                EditorUtility.ClearProgressBar();
                PrefabOperation.EndBatch();

                if( Config.SAVE_ASSETS_AFTER_APPLY )
                {
#if DEV
                    if( !Config.IS_UNIT_TEST )
#endif
                        AssetDatabase.SaveAssets();
                }

                ApplyPool.Clear();

                // Send the time it took to apply the entire batch to analytics so we can check 
                // if the apply time is reasonable and if it goes up or down between versions.
                applyBatchStopwatch.Stop();

                if( OnApplied != null )
                    OnApplied.Invoke( applyBatchStopwatch.ElapsedMilliseconds );

                IsEnabled = true;
            }
        }
    }
}