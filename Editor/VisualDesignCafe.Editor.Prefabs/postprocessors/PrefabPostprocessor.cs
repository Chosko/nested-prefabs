﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using VisualDesignCafe.Editor.Prefabs.Overrides;

namespace VisualDesignCafe.Editor.Prefabs
{
    public class PrefabPostprocessor
    {

        public delegate void PostprocessModelAssetCallback( Prefab prefab );

        public event PostprocessModelAssetCallback PostprocessModelPrefab;
        public event Action<string> OnPrefabAssetUpdated;

        private readonly ApplyPool _applyPool;

        public PrefabPostprocessor(
            PostprocessorEventDispatcher callbackProvider, ApplyPool applyPool )
        {
            _applyPool = applyPool;

            callbackProvider.PrefabInstanceUpdated += OnPrefabInstanceUpdated;
        }

        public void OnPostprocessPrefabAsset(
            GameObject asset, List<string> processedAssets )
        {
            if( asset == null )
                throw new ArgumentNullException( "asset" );

            if( processedAssets == null )
                processedAssets = new List<string>();

            if( Config.DEBUG_INFO && Config.VERBOSE )
                Console.LogFormat( "Postprocess prefab asset. ({0})", asset.name );

            Console.Indent++;

            // Make sure the root prefab does not have any Nested Prefab component.
            // Since we add Nested Prefab components to temporary copies 
            // for editing it is possible that one will end up in the asset.
            RemoveNestedPrefabComponent( asset );

            // Make sure the prefab has a Prefab component.
            // Every prefab root should have a Prefab component to 
            // identify the object as a prefab.
            Prefab prefabObject = GetAddPrefabComponent( asset );

            // Cached GameObject, Transform and Asset for prefab are not 
            // always correct when applying after duplicating or breaking a prefab connection.
            // Therefore we should refresh the cached values.
            prefabObject.Cache();

            // Postprocess the prefab object to make sure it has valid 
            // values and all its children are registered.
            PostprocessPrefabObject( processedAssets, prefabObject, 0 );

            // Validate the HideFlags for components in the prefab.
            if( ValidateHideFlags( asset ) )
            {
                if( Config.DEBUG_INFO )
                    Console.LogFormat( "Fixed HideFlags for prefab. ({0})", asset.name );
            }

            if( Config.APPLY_CHANGES_TO_ASSET )
            {
                AddAssetToPool( asset );
            }

            Console.Indent--;
        }

        /// <summary>
        /// Postprocess this prefab. Will create new Guid components where necessary
        /// and will also update the reference collection with all children of this Prefab.
        /// </summary>
        private void PostprocessPrefabObject(
            List<string> processedAssets, Prefab prefab, int depth = 0 )
        {
            if( Config.DEBUG_INFO && Config.VERBOSE )
                Console.LogFormat( "Postprocess prefab. ({0})", prefab );

            if( depth > 0 && prefab.GetComponent<NestedPrefab>() == null )
            {
                if( Config.DEBUG_WARNING )
                    Console.LogWarning( "Could not find Nested Prefab component on prefab." );

                NestedPrefab nestedPrefab = prefab.gameObject.AddComponent<NestedPrefab>();
                Config.SetDefaultHideFlags( nestedPrefab );
            }

            // Checks if the asset GUID changed. 
            // Should only happen when the prefab is first created and the stored GUID is still empty.
            string assetPath = AssetDatabase.GetAssetPath( prefab.gameObject );
            string assetGuid = AssetDatabase.AssetPathToGUID( assetPath );

            if( AssetGuidChanged( prefab, assetGuid ) )
            {
                if( Config.DEBUG_INFO )
                    Console.LogFormat(
                        "Clearing reference collection. Asset GUID changed from {0} to {1}",
                        prefab.AssetGuid,
                        assetGuid );

                prefab.OverrideAsset( assetGuid );
                prefab.ReferenceCollection.Clear();
            }
            else
            {
                prefab.ReferenceCollection.Clean();
            }

            // Make sure the prefab has an empty Prefab GUID string if it is a root prefab.
            // Nested Prefabs are identified by the Prefab GUID, if the prefab root has a non-empty GUID
            // then it is recognized as a nested prefab and children can't be found correctly.
            if( prefab.transform.parent == null )
                prefab.OverrideGuid( string.Empty );

            // Make sure the root of the (nested) prefab is added to the reference collection.
            prefab.ReferenceCollection.Add( prefab.gameObject );

            // Register all children of the prefab.
            // Add the asset guid of the current prefab to the list of processed asset guids. 
            // This way a nested prefab with the same asset guid will be ignored to prevent circular references.
            if( !string.IsNullOrEmpty( prefab.AssetGuid ) )
                processedAssets.Add( prefab.AssetGuid );

            foreach( Transform child in prefab.transform )
                RegisterChildren(
                    processedAssets,
                    prefab,
                    child,
                    depth,
                    NestedPrefabUtility.IsModelPrefab( prefab ) );

            if( !string.IsNullOrEmpty( prefab.AssetGuid ) )
                processedAssets.Remove( prefab.AssetGuid );

            ReferenceCollectionUtility.Validate( prefab.ReferenceCollection );
            UpdatePrefabOverrides( prefab );
        }

        /// <summary>
        /// Registers all children of the Transform to the reference collection.
        /// Will create new Guid components where necessary.
        /// </summary>
        private void RegisterChildren(
            List<string> processedAssets,
            Prefab rootPrefab,
            Transform child,
            int depth,
            bool rootIsModelPrefab )
        {
            bool isNewChild =
                !rootPrefab.ReferenceCollection.Contains( child.gameObject );

            rootPrefab.ReferenceCollection.Add( child.gameObject );

            var nestedPrefab = child.GetComponent<NestedPrefab>();
            var prefab = child.GetComponent<Prefab>();

            if( prefab != null )
            {
                if( HasCircularReference( prefab, processedAssets ) )
                {
                    Debug.LogWarning(
                        "Nested Prefab has a circular reference. Removing prefab connection." );

                    RemoveCircularReference( prefab, nestedPrefab );

                    // The nested prefab structure changed so we have to 
                    // make sure the prefab is refreshed in the database.
                    if( OnPrefabAssetUpdated != null )
                        OnPrefabAssetUpdated.Invoke(
                            AssetDatabase.GUIDToAssetPath( processedAssets[ 0 ] ) );
                }
                // Make sure the prefab child has a Nested Prefab component.
                else if( nestedPrefab == null )
                {
                    nestedPrefab = prefab.gameObject.AddComponent<NestedPrefab>();

                    // TODO: this looks outdated. Check if it can be removed.
                    if( nestedPrefab.Asset != null )
                        nestedPrefab.CachedGameObject.GetComponent<Guid>().CopyFrom(
                            nestedPrefab.Asset.GetComponent<Guid>() );

                    // NestedPrefab components are cached in the database, 
                    // since we added a new component we have to refresh the prefab in the cache.
                    if( OnPrefabAssetUpdated != null )
                        OnPrefabAssetUpdated.Invoke( AssetDatabase.GUIDToAssetPath( processedAssets[ 0 ] ) );
                }
            }

            if( !rootIsModelPrefab )
            {
                RemoveModelModifications( child, prefab );
            }

            // Check if the object is a Nested Prefab. 
            // If it is we should stop the loop here and continue by calling PostProcess on the Nested Prefab. 
            // Otherwise the children will be registered to the wrong prefab.
            if( nestedPrefab != null )
            {
                if( nestedPrefab.Prefab != null )
                {
                    // Create a new prefab Guid for the prefab if it doesn't have one.
                    // Only prefabs with a depth of 1 (default value) will be handled here. 
                    // Nested prefabs with a depth greater than 1 should copy the GUID from the asset where the Nested Prefab has a depth of 1. 
                    // If we create a new GUID for prefabs with a depth greater than one we will end up with a corrupt reference collection. 
                    // Since the GUID is used to identify objects, they should be shared between nested prefabs.
                    if( isNewChild || string.IsNullOrEmpty( nestedPrefab.Prefab.PrefabGuid ) )
                    {
                        if( depth < Prefab.PostprocessPrefabGuidDepthLimit
                            || Prefab.PostprocessPrefabGuidDepthLimit <= 0 )
                        {
                            nestedPrefab.Prefab.CreateGuid();
                            EditorUtility.SetDirty( nestedPrefab.Prefab );
                        }
                    }

                    string sourceAssetPath =
                        AssetDatabase.GUIDToAssetPath( nestedPrefab.Prefab.AssetGuid );

                    bool isModelPrefab =
                        !string.IsNullOrEmpty( sourceAssetPath )
                        && !sourceAssetPath.EndsWith(
                            ".prefab",
                            StringComparison.OrdinalIgnoreCase );

                    PostprocessPrefabObject(
                        processedAssets, nestedPrefab.Prefab, depth + 1 );

                    if( isModelPrefab )
                    {
                        if( PostprocessModelPrefab != null )
                            PostprocessModelPrefab.Invoke( nestedPrefab.Prefab );
                    }
                }
                else
                {
                    if( Config.DEBUG_ERROR )
                        Debug.LogError(
                            "Could not find Prefab object for Nested Prefab. Please make sure there are no corrupt prefabs in your hierarchy. This can happen if you manually destroy a prefab component using Destroy().",
                            nestedPrefab.CachedGameObject );
                }

                return;
            }

            foreach( Transform c in child )
                RegisterChildren(
                    processedAssets, rootPrefab, c, depth, rootIsModelPrefab );
        }


        /// <summary>
        /// Called when a user hit 'Apply' on a prefab instance
        /// </summary>
        private void OnPrefabInstanceUpdated( GameObject instance )
        {
            if( Application.isPlaying )
                return;

            if( NestedPrefabsPostprocessor.IsDestroyed )
                return;

            // Check the reference collections.
            Prefab[] prefabs = instance.GetComponentsInChildren<Prefab>( true );

            foreach( Prefab prefab in prefabs )
            {
                if( Prefab.IsNullOrMissing( prefab ) )
                    continue;

                ReferenceCollectionUtility.Update( prefab.ReferenceCollection );

                // Automatically try to fix any invalid reference collections.
                if( !ReferenceCollectionUtility.IsValid( prefab.ReferenceCollection ) )
                {
                    NestedPrefabUtility.FixPrefab( prefab );

                    if( !ReferenceCollectionUtility.IsValid( prefab.ReferenceCollection ) )
                    {
                        Debug.LogWarning( "Prefab contains unregistered children. Please try reimporting the prefab asset.", instance );
                    }
                }
            }

            PrefabOverrideUtility.ApplyAll( instance );
        }

        private void RemoveNestedPrefabComponent( GameObject gameObject )
        {
            NestedPrefab nestedPrefab = gameObject.GetComponent<NestedPrefab>();
            if( nestedPrefab != null )
                Component.DestroyImmediate( nestedPrefab, true );
        }

        private Prefab GetAddPrefabComponent( GameObject gameObject )
        {
            Prefab prefabObject = gameObject.GetComponent<Prefab>();

            if( prefabObject == null )
                prefabObject = gameObject.AddComponent<Prefab>();

            return prefabObject;
        }

        private bool ValidateHideFlags( GameObject gameObject )
        {
            if( gameObject == null )
                return false;

            bool fixedHideFlags = false;

            Component[] components = gameObject.GetComponents<Component>();

            foreach( Component component in components )
            {
                if( component == null )
                    continue;

                if( component is Prefab
                    || component is Guid
                    || component is NestedPrefab
                    || component is ModelModification
                    || component is ModelComponentModification )
                {
                    if( Config.SetDefaultHideFlags( component ) )
                        fixedHideFlags = true;
                }
            }

            foreach( Transform child in gameObject.transform )
            {
                if( ValidateHideFlags( child.gameObject ) )
                    fixedHideFlags = true;
            }

            return fixedHideFlags;
        }

        private bool AssetGuidChanged( Prefab prefab, string guid )
        {
            if( string.IsNullOrEmpty( guid ) )
                return false;

            return prefab.AssetGuid != guid
                && prefab.GetComponent<NestedPrefab>() == null
                && PrefabHierarchyUtility.FindPrefabRoot( prefab.gameObject ) == prefab.gameObject;
        }

        private void UpdatePrefabOverrides( Prefab prefab )
        {
            var overrides = prefab.GetComponent<PrefabOverrides>();

            if( overrides == null )
                return;

            overrides.UpdateAll();

            if( NestedPrefabsPostprocessor.IsEnabled )
            {
                if( !PrefabOperation.IsInBatch )
                    PrefabOverrideUtility.Update( overrides );
            }
        }

        private bool HasCircularReference( Prefab prefab, List<string> assetGuids )
        {
            if( string.IsNullOrEmpty( prefab.AssetGuid ) )
                return false;

            return assetGuids.IndexOf( prefab.AssetGuid ) > -1;
        }

        private void RemoveCircularReference( Prefab prefab, NestedPrefab nestedPrefab )
        {
            GameObject gameObject = prefab.gameObject;

            Component.DestroyImmediate( prefab, true );

            if( nestedPrefab != null )
            {
                Component.DestroyImmediate( nestedPrefab, true );
                nestedPrefab = null;
            }

            // Create a new GUID for the child. If there were multiple duplicates of the prefab it needs a new GUID, 
            // otherwise there would be multiple objects with the same GUID and they would be removed.
            Guid prefabObjectGuid = gameObject.GetComponent<Guid>();
            if( prefabObjectGuid != null )
                prefabObjectGuid.CreateGuid();
        }

        private void RemoveModelModifications( Transform child, Prefab prefab )
        {
            var modelModification = child.GetComponent<ModelModification>();

            if( modelModification != null )
            {
                if( Config.DEBUG_INFO && Config.VERBOSE )
                    Console.Log(
                        "Removing ModelModification component from "
                        + child.gameObject.ToLog() );

                Component.DestroyImmediate( modelModification, true );
            }

            if( prefab == null || !NestedPrefabUtility.IsModelPrefab( prefab ) )
            {
                var modelComponentModification =
                    child.GetComponent<ModelComponentModification>();

                if( modelComponentModification != null )
                {
                    if( Config.DEBUG_INFO && Config.VERBOSE )
                        Console.Log(
                            "Removing ModelComponentModification component from "
                            + child.gameObject.ToLog() );

                    Component.DestroyImmediate( modelComponentModification, true );
                }
            }
        }

        internal void AddAssetToPool( GameObject prefab )
        {
            if( !NestedPrefabsPostprocessor.IsEnabled )
            {
                if( Config.DEBUG_WARNING )
                    Console.LogWarning( "Postprocessor is disabled." );
                return;
            }

            Prefab prefabComponent = prefab.GetComponent<Prefab>();
            if( prefabComponent != null )
            {
                _applyPool.Add( prefabComponent, true, false, true );
            }
            else
            {
                if( Config.DEBUG_WARNING )
                    Console.LogWarning( "Asset does not have a Prefab component." );
            }
        }
    }
}