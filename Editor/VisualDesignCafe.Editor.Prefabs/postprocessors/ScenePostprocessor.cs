﻿using UnityEditor;
using UnityEngine;

namespace VisualDesignCafe.Editor.Prefabs
{
    public class ScenePostprocessor
    {
        public ScenePostprocessor( PostprocessorEventDispatcher eventDispatcher )
        {
            eventDispatcher.PostprocessScene += OnPostProcessScene;
        }

        /// <summary>
        /// Called for each scene when the game starts or when starting a build
        /// </summary>
        private void OnPostProcessScene()
        {
            if( NestedPrefabsPostprocessor.IsDestroyed )
                return;

            NestedPrefabsPostprocessor.IsEnabled = false;

            RemoveAllComponents<NestedPrefab>();
            RemoveAllComponents<Guid>();
            RemoveAllComponents<Prefab>();
            RemoveAllComponents<PrefabOverrides>();
            RemoveAllComponents<ModelModification>();
            RemoveAllComponents<ModelComponentModification>();

            NestedPrefabsPostprocessor.IsEnabled = true;
        }

        /// <summary>
        /// Removes all components in the scene of the given type.
        /// </summary>
        private void RemoveAllComponents<T>()
            where T : Component
        {
            T[] components = Resources.FindObjectsOfTypeAll<T>();

            foreach( T component in components )
            {
                if( component == null )
                    continue;

                if( EditorUtility.IsPersistent( component ) )
                    continue;

                Component.DestroyImmediate( component );
            }
        }
    }
}