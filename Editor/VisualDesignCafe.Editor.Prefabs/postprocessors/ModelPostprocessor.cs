﻿using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace VisualDesignCafe.Editor.Prefabs
{
    public class ModelPostprocessor
    {
        public static bool Force
        {
            get;
            set;
        }

        public static bool SkipPreProcessor
        {
            get;
            set;
        }

        public PrefabPostprocessor PrefabPostprocessor
        {
            get;
            private set;
        }

        public ModelPostprocessor(
            PostprocessorEventDispatcher eventDispatcher,
            PrefabPostprocessor prefabPostprocessor )
        {
            eventDispatcher.PostprocessModel += OnPostprocessModel;

            PrefabPostprocessor = prefabPostprocessor;
            PrefabPostprocessor.PostprocessModelPrefab += OnPostprocessModelPrefab;
        }

        public void OnPostprocessModelPrefab( Prefab prefab )
        {
            if( Prefab.IsNullOrMissing( prefab ) )
                return;

            Guid[] assetGuids = prefab.Asset.GetComponentsInChildren<Guid>( true );

            SetModelComponentModifications( prefab, prefab.CachedTransform, assetGuids );

            foreach( Transform child in prefab.CachedTransform )
            {
                SetModelModifications( prefab, child, assetGuids );
                SetModelComponentModifications( prefab, child, assetGuids );
            }
        }

        /// <summary>
        /// Checks the model prefab for any modifications made compared to 
        /// the original model asset and marks the modifications with the ModelModification flag.
        /// </summary>
        private static void SetModelModifications(
            Prefab prefab, Transform transform, Guid[] assetGuids )
        {
            var modelModification = transform.GetComponent<ModelModification>();

            bool isModelObject = IsModelObject( transform, prefab, assetGuids );

            // Add a ModelModification component if the transform is not part of the original model asset.
            // Based on these components we can reload the model asset without deleting the modifications.
            // If the transform is part of the original asset we will have to delete the component.
            if( modelModification == null )
            {
                if( !isModelObject )
                {
                    if( Config.DEBUG_INFO && Config.VERBOSE )
                        Console.Log( "Adding ModelModification flag to " + transform.gameObject.ToLog() );

                    modelModification =
                        transform.gameObject.AddComponent<ModelModification>();

                    // We only have to add the component to the root game object, 
                    // all children of the modification are automatically ignored as well.
                    // So we can exit the loop here and skip checking all children.
                    return;
                }
            }
            else
            {
                if( isModelObject )
                {
                    if( Config.DEBUG_INFO && Config.VERBOSE )
                        Console.Log( "Removing ModelModification flag from " + transform.gameObject.ToLog() );

                    Component.DestroyImmediate( modelModification, true );
                }
                else
                {
                    // The object is a modification of the model and it already has the ModelModification flag. 
                    // The rest of the loop can be ignored.
                    return;
                }
            }

            // Exit loop if we have reached a nested prefab. 
            // The nested prefab is handled separately.
            if( transform.GetComponent<Prefab>() != null )
                return;

            foreach( Transform child in transform )
                SetModelModifications( prefab, child, assetGuids );
        }

        /// <summary>
        /// Checks the model prefab for components added to the game objects of the original asset.
        /// These added components will be marked with the ModelComponentModification flag so they are ignored when reloading changes from the asset.
        /// If the flag is not added the components will be removed when reloading the asset.
        /// </summary>
        private static void SetModelComponentModifications(
            Prefab prefab, Transform transform, Guid[] assetGuids )
        {
            var modelComponentModification =
                transform.GetComponent<ModelComponentModification>();

            Transform objectInModel =
                FindModelObject( transform, prefab, assetGuids );

            // Remove the ModelComponentModification flag if the object is not part of the original model. 
            // It should already have a ModelModification for the entire object, which also includes all components.
            if( objectInModel == null )
            {
                if( modelComponentModification != null )
                    Component.DestroyImmediate( modelComponentModification, true ); ;
            }
            // Loop through all components on the game object and find out which ones were added in the prefab and which ones are part of the original model asset.
            // All added components in the prefab will be marked with the ModelComponentModification flag so they are ignored when reloading the model.
            // If the flag is not added the components will be removed during the next reload.
            else
            {
                List<Component> modelComponents =
                    objectInModel.GetComponents<Component>().ToList();

                List<Component> components =
                    transform.GetComponents<Component>().ToList();

                while( components.Count > 0 )
                {
                    if( components[ 0 ] == null )
                    {
                        components.RemoveAt( 0 );
                        continue;
                    }

                    System.Type componentType = components[ 0 ].GetType();

                    int index = modelComponents.FindIndex( c => c != null && c.GetType() == componentType );

                    if( index == -1 )
                    {
                        // Ignore internal components.
                        if( componentType != typeof( Prefab )
                            && componentType != typeof( NestedPrefab )
                            && componentType != typeof( Guid )
                            && componentType != typeof( ModelModification )
                            && componentType != typeof( ModelComponentModification )
                            && componentType != typeof( PrefabOverrides )
                            )
                        {
                            if( modelComponentModification == null )
                                modelComponentModification =
                                    transform.gameObject.AddComponent<ModelComponentModification>();

                            modelComponentModification.AddComponent( components[ 0 ] );
                        }
                    }
                    else
                    {
                        if( modelComponentModification != null )
                            modelComponentModification.RemoveComponent( components[ 0 ] );

                        modelComponents.RemoveAt( index );
                    }

                    components.RemoveAt( 0 );
                }

                if( modelComponentModification != null && modelComponentModification.Count == 0 )
                    Component.DestroyImmediate( modelComponentModification, true );
            }

            foreach( Transform child in transform )
                SetModelComponentModifications( prefab, child, assetGuids );
        }

        /// <summary>
        /// Finds the transform in the original model asset of the prefab.
        /// Searches based on object GUID.
        /// </summary>
        /// <param name="transform">Transform to find in the asset</param>
        /// <param name="prefab">Parent prefab of the tranform</param>
        /// <param name="assetGuids">Collection of GUID components in the asset</param>
        /// <returns></returns>
        private static Transform FindModelObject(
            Transform transform, Prefab prefab, Guid[] assetGuids )
        {
            Guid guid = transform.GetComponent<Guid>();
            if( guid != null )
            {
                Guid result =
                    assetGuids.FirstOrDefault( g => g != null && g.Value == guid.Value );

                if( result == null )
                {
                    string path =
                        PrefabHierarchyUtility.GetHierarchyPath(
                            transform.gameObject,
                            prefab.CachedTransform,
                            false );

                    return prefab.Asset.transform.Find( path );
                }

                return result != null ? result.transform : null;
            }
            else
            {
                string path =
                    PrefabHierarchyUtility.GetHierarchyPath(
                        transform.gameObject,
                        prefab.CachedTransform,
                        false );

                return prefab.Asset.transform.Find( path );
            }
        }

        /// <summary>
        /// Checks if the transform is part of the original asset of the prefab.
        /// </summary>
        /// <param name="transform">Transform to find in the asset</param>
        /// <param name="prefab">Parent prefab of the tranform</param>
        /// <param name="assetGuids">Collection of GUID components in the asset</param>
        /// <returns></returns>
        private static bool IsModelObject(
            Transform transform, Prefab prefab, Guid[] assetGuids )
        {
            return FindModelObject( transform, prefab, assetGuids ) != null;
        }

        /// <summary>
        /// Updates the asset GUID of the prefab at the given path.
        /// </summary>
        internal static GameObject UpdateModelPrefabGuid( string path )
        {
            if( string.IsNullOrEmpty( path ) )
                throw new System.ArgumentNullException( "Path" );

            GameObject prefab =
                AssetDatabase.LoadAssetAtPath( path, typeof( GameObject ) ) as GameObject;

            if( prefab != null )
            {
                Prefab prefabObject = prefab.GetComponent<Prefab>();

                if( prefabObject != null )
                {
                    string guid = AssetDatabase.AssetPathToGUID( path );

                    if( !string.IsNullOrEmpty( guid ) )
                        prefabObject.OverrideAsset( guid );
                }
                else if( Config.DEBUG_WARNING )
                    Console.LogWarning( "Model does not have a Prefab component." );
            }

            if( Config.DEBUG_INFO )
                Console.Log(
                    "UpdateModelPrefabGuid: "
                    + prefab + ". " + path + ". "
                    + AssetDatabase.AssetPathToGUID( path ) );

            return prefab;
        }

        /// <summary>
        /// Called by the Unity asset postprocessor after importing a model asset.
        /// </summary>
        public void OnPostprocessModel(
            UnityEditor.AssetPostprocessor sender, GameObject gameObject )
        {
            // The model postprocessor is an extenion on the nested prefab postprocessor. 
            // Ignore this postprocessor if the nested prefab postprocessor is destroyed.
            if( NestedPrefabsPostprocessor.IsDestroyed && !Force )
                return;

            if( NestedPrefabsPostprocessor.IsIgnored( sender.assetPath ) )
                return;

            if( Config.DEBUG_INFO )
                Console.Log( "OnPostprocessModel: " + gameObject.ToLog() );

            string guid = AssetDatabase.AssetPathToGUID( sender.assetPath );

            // Postprocess the model like a prefab. (They are basically the same)
            List<string> processedAssets = new List<string>();
            processedAssets.Add( guid );

            PrefabPostprocessor.OnPostprocessPrefabAsset( gameObject, processedAssets );

            // Make sure the asset GUID is valid.
            Prefab prefabComponent = gameObject.GetComponent<Prefab>();
            if( prefabComponent != null )
            {
                if( !string.IsNullOrEmpty( guid ) )
                {
                    prefabComponent.OverrideAsset( guid );
                }
                else if( Config.DEBUG_WARNING )
                    Console.LogWarning( "Asset GUID is empty" );
            }
            else if( Config.DEBUG_WARNING )
            {
                Console.LogWarning( "Could not find prefab component on model" );
            }

            // Make sure the GUID value is empty. It is assigned later. 
            Guid[] guids = gameObject.GetComponentsInChildren<Guid>( true );
            foreach( Guid g in guids )
            {
                g.OverrideGuid( string.Empty );
            }

            var children = new List<string>();

            // Make sure the object GUID is valid for all children.
            if( SetConsistentGuid(
                gameObject.transform, gameObject.transform, children ) )
            {
                Debug.LogWarning( "Model '" + sender.assetPath + "' has multiple children with the same name. This can cause incorrect references in nested prefabs." );
            }

            // Add the asset to the Apply Pool.
            PrefabPostprocessor.AddAssetToPool( gameObject );
        }

        /// <summary>
        /// Makes sure the objects in the model prefab have a consistent GUID.
        /// </summary>
        private bool SetConsistentGuid(
            Transform root, Transform transform, List<string> children )
        {
            bool hasDuplicateChildren = false;

            var guid = transform.GetComponent<Guid>();
            if( guid != null )
            {
                string path = transform == root
                    ? transform.gameObject.name
                    : PrefabHierarchyUtility.GetHierarchyPath( transform.gameObject, root, false );

                int sum = children.Sum( c => c == path ? 1 : 0 );

                children.Add( path );

                if( sum > 0 )
                {
                    path += sum.ToString();
                    hasDuplicateChildren = true;
                }

                string value = ( (uint) path.GetStableHashCode() ).ToString();

                if( value != guid.Value )
                {
                    guid.OverrideGuid( value );
                }
            }

            foreach( Transform child in transform )
                hasDuplicateChildren = SetConsistentGuid( root, child, children ) || hasDuplicateChildren;

            return hasDuplicateChildren;
        }
    }
}
