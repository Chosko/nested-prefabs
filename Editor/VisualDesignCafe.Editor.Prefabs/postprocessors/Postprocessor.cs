﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEditor;
using VisualDesignCafe.Editor.Prefabs.Comparing;

namespace VisualDesignCafe.Editor.Prefabs
{

    internal class Postprocessor
    {
        private static Postprocessor[] _cachedProcessors = new Postprocessor[ 0 ];

        /// <summary>
        /// The target type for this processor.
        /// </summary>
        public readonly Type Target;

        /// <summary>
        /// Method that gets called after all modifications to a nested prefab have been found and before applying them.
        /// </summary>
        public readonly MethodInfo PostprocessModificationsMethod;

        /// <summary>
        /// Called after reloading scripts. (after compile finished)
        /// </summary>
        [InitializeOnLoadMethod]
        internal static void OnReloadedScripts()
        {
            _cachedProcessors = FindAllProcessors();
        }

        internal static Postprocessor[] FindAllProcessors()
        {
            if( Config.DEBUG_INFO )
                Console.Log( "Finding custom processors in project." );

            var processors = new List<Postprocessor>();

            foreach( Type type in FindAllTypes() )
            {
                if( type == null )
                    continue;

                try
                {
                    var attributes =
                        type.GetCustomAttributes( typeof( CustomProcessor ), true ) as CustomProcessor[];

                    foreach( CustomProcessor attribute in attributes )
                        processors.Add( new Postprocessor( type, attribute.Type ) );
                }
                catch( TypeLoadException )
                {
                    // This exception is thrown if the assembly is dynamic. 
                    // Not sure how and why this happens, but we should be able to ignore it.
                    if( Config.DEBUG_WARNING )
                        Console.LogWarning( "Type in dynamic assembly is not supported" );
                }
                catch( ReflectionTypeLoadException )
                {
                    // This exception is thrown if the assembly is dynamic. 
                    // Not sure how and why this happens, but we should be able to ignore it.
                    if( Config.DEBUG_WARNING )
                        Console.LogWarning( "Type in dynamic assembly is not supported" );
                }
                catch( NotSupportedException )
                {
                    // This exception is thrown if the assembly is dynamic. 
                    // Not sure how and why this happens, but we should be able to ignore it.
                    if( Config.DEBUG_WARNING )
                        Console.LogWarning( "Type in dynamic assembly is not supported" );
                }
            }

            return processors.ToArray();
        }

        internal static Postprocessor[] GetCachedPostprocessorsForType( Type type )
        {
            var processors = new List<Postprocessor>();

            if( _cachedProcessors == null || _cachedProcessors.Length == 0 )
                FindAllProcessors();

            foreach( Postprocessor processor in _cachedProcessors )
            {
                if( processor.Target == type )
                {
                    processors.Add( processor );
                }
            }

            return processors.ToArray();
        }

        private static Type[] FindAllTypes()
        {
            Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();
            List<Type> typesInAssembly = new List<Type>();

            foreach( Assembly assembly in assemblies )
            {
                try
                {
                    typesInAssembly.AddRange( assembly.GetTypes() );
                }
                catch( TypeLoadException )
                {
                    // This exception is thrown if the assembly is dynamic. 
                    // Not sure how and why this happens, but we should be able to ignore it.
                    if( Config.DEBUG_WARNING )
                        Console.LogWarning( "Type in dynamic assembly is not supported" );
                }
                catch( ReflectionTypeLoadException )
                {
                    // This exception is thrown if the assembly is dynamic. 
                    // Not sure how and why this happens, but we should be able to ignore it.
                    if( Config.DEBUG_WARNING )
                        Console.LogWarning( "Type in dynamic assembly is not supported" );
                }
                catch( NotSupportedException )
                {
                    // This exception is thrown if the assembly is dynamic. 
                    // Not sure how and why this happens, but we should be able to ignore it.
                    if( Config.DEBUG_WARNING )
                        Console.LogWarning( "Type in dynamic assembly is not supported" );
                }
            }

            return typesInAssembly.Distinct().ToArray();
        }

        public Postprocessor( Type processor, Type target )
        {
            if( processor == null || target == null )
                throw new ArgumentNullException();

            Target = target;

            PostprocessModificationsMethod =
                processor
                .GetMethod(
                    "PostprocessModifications",
                    BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic,
                    null,
                    new Type[] { typeof( Prefab ), typeof( Modification[] ), typeof( Prefab ) },
                    null );

            // The PostprocessModifications method used to be called PreprocessModifications. 
            // We will also search for the old method name for compatibility with older versions.
            if( PostprocessModificationsMethod == null )
            {
                PostprocessModificationsMethod =
                    processor
                    .GetMethod(
                        "PreprocessModifications",
                        BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic,
                        null,
                        new Type[] { typeof( Prefab ), typeof( Modification[] ), typeof( Prefab ) },
                        null );

                if( PostprocessModificationsMethod != null )
                    UnityEngine.Debug.LogWarning( "'PreprocessModifications' is obsolete. Use 'PostprocessModifications' instead." );
            }
        }
    }
}
