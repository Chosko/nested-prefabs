﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using VisualDesignCafe.Serialization;

namespace VisualDesignCafe.Editor.Prefabs
{
    using Debug = UnityEngine.Debug;

    public class DatabaseReader : IDisposable
    {
        public float Progress
        {
            get;
            private set;
        }

        public bool FinishedReading
        {
            get;
            private set;
        }

        public long ElapsedMilliseconds
        {
            get { return _stopwatch.ElapsedMilliseconds; }
        }

        private AsyncAction _action;
        private string _location;
        private Action<JsonObject> _callback;
        private Action _finishedCallback;
        private Stopwatch _stopwatch = new Stopwatch();

        public DatabaseReader( string location )
        {
            if( string.IsNullOrEmpty( location ) )
                throw new ArgumentNullException( "Location" );

            _location = location;
            _action = new AsyncAction( Read );
        }

        public void Dispose()
        {
            _action.Dispose();
        }

        public void ReadAsync(
            Action<JsonObject> callback,
            Action finishedCallback )
        {
            if( !Directory.Exists( _location ) )
                return;

            _callback = callback;
            _finishedCallback = finishedCallback;
            _action.Start();
        }

        private void Read()
        {
            _stopwatch.Start();

            int index = 0;
            string[] files =
                Directory.GetFiles(
                    _location,
                    "*.db",
                    SearchOption.TopDirectoryOnly );

            foreach( string file in files )
            {
                try
                {
                    ReadFile( file );
                    Progress = (float) index++ / (float) files.Length;
                }
                catch( Exception e )
                {
                    Debug.LogException( e );
                }
            }

            _stopwatch.Stop();
            FinishedReading = true;

            if( _finishedCallback != null )
                _finishedCallback.Invoke();
        }

        private void ReadFile( string file )
        {
            List<JsonObject> json =
                new JsonObject( File.ReadAllText( file ) )
                .AsArray;

            foreach( var entry in json )
            {
                try
                {
                    _callback( entry );
                }
                catch( Exception e )
                {
                    Debug.LogException( e );
                }
            }
        }
    }
}