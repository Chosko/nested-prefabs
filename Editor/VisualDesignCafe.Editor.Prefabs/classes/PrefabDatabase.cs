﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;
using VisualDesignCafe.Serialization;

namespace VisualDesignCafe.Editor.Prefabs
{

    public class PrefabDatabase
    {
        public readonly string Location;
        public readonly SerializedPrefabDatabase SerializedDatabase;

        public bool IsDirty
        {
            get { return _dirtyAssets.Count > 0; }
        }

        private Dictionary<string, PrefabAsset> _collection =
            new Dictionary<string, PrefabAsset>();
        private List<string> _dirtyAssets = new List<string>();
        private DatabaseReader _importer;

        public PrefabAsset this[ string guid ]
        {
            get
            {
                return _collection[ guid ];
            }
        }

        public PrefabDatabase(
            string location, SerializedPrefabDatabase serializedDatabase )
        {
            PrefabAsset.AssetPathToGUID = AssetDatabase.AssetPathToGUID;
            PrefabAsset.GetAssetPath = AssetDatabase.GetAssetPath;

            Location = location;
            SerializedDatabase = serializedDatabase;

            if( SerializedDatabase != null )
            {
                SerializedDatabase.OnSerialize +=
                    () =>
                    {
                        if( _collection.Count > 0 )
                            SerializedDatabase.Serialize( _collection.Values.ToArray() );
                    };

                PrefabAsset[] collection;
                var stopwatch = new System.Diagnostics.Stopwatch();
                stopwatch.Start();
                SerializedDatabase.Deserialize( out collection );
                stopwatch.Stop();

                if( Config.DEBUG_INFO )
                    Console.LogFormat(
                        "Deserialized Prefab Database in {0} ms ({1:#,#} entries)",
                        stopwatch.ElapsedMilliseconds,
                        collection.Length
                        );

                if( collection.Length > 0 )
                {
                    foreach( var asset in collection )
                        _collection[ asset.Guid ] = asset;

                    Import( true );
                    return;
                }
            }

            Import( false );
        }

        public void Clear()
        {
            EnsureImportingFinished();

            _collection.Clear();
            _dirtyAssets.Clear();
        }

        public string[] GetAllReferencedPrefabAssets()
        {
            EnsureImportingFinished();

            List<string> assets = new List<string>();

            var enumerator = _collection.GetEnumerator();
            while( enumerator.MoveNext() )
            {
                assets.AddRange( enumerator.Current.Value.References );
            }

            return assets.Distinct().ToArray();
        }

        public void Refresh( Action<float> progressCallback = null )
        {
            EnsureImportingFinished();

            string[] prefabAssets =
                AssetDatabase.GetAllAssetPaths()
                    .Where(
                        p => p.EndsWith(
                                ".prefab",
                                StringComparison.OrdinalIgnoreCase ) )
                    .ToArray();

            int index = 0;
            int count = prefabAssets.Length;

            foreach( string path in prefabAssets )
            {
                index++;
                string guid = AssetDatabase.AssetPathToGUID( path );

                if( Contains( guid ) )
                    continue;

                GameObject prefab =
                    (GameObject) AssetDatabase.LoadAssetAtPath(
                        path,
                        typeof( GameObject ) );

                if( prefab == null )
                    continue;

                if( progressCallback != null )
                    progressCallback( (float) index / (float) count );

                Add( prefab );
            }
        }

        public void Flush( Action<float> progressCallback )
        {
            EnsureImportingFinished();

            var stopwatch = new System.Diagnostics.Stopwatch();
            stopwatch.Start();

            var groups = _dirtyAssets.GroupBy( guid => guid[ 0 ] );
            var assets = _collection.Values;

            foreach( var group in groups )
            {
                char key = group.Key;
                var text = new JsonObject();

                foreach( var asset in assets )
                {
                    if( asset.Guid[ 0 ] != key )
                        continue;

                    text.Add( asset.ToJson() );
                }

                WriteToFile(
                     Location + "/" + key.ToString() + ".db",
                     text.ToString()
                    );
            }

            stopwatch.Stop();
            if( Config.DEBUG_INFO )
                Console.LogFormat(
                    "Flushed prefab database in {0} ms",
                    stopwatch.ElapsedMilliseconds );

            _dirtyAssets.Clear();
        }

        public bool Contains( string guid )
        {
            EnsureImportingFinished();

            return _collection.ContainsKey( guid );
        }

        public bool TryGetAsset( string guid, out PrefabAsset asset )
        {
            EnsureImportingFinished();

            return _collection.TryGetValue( guid, out asset );
        }

        public PrefabAsset Add( GameObject asset )
        {
            if( asset == null )
                throw new ArgumentNullException( "Asset" );

            var assetInfo = new PrefabAsset( asset );
            Add( assetInfo );

            return assetInfo;
        }

        public void Add( PrefabAsset asset, bool checkDirty = true )
        {
            if( asset == null )
                throw new ArgumentNullException( "Asset" );

            if( string.IsNullOrEmpty( asset.Guid ) )
                throw new InvalidOperationException( "Asset does not have a valid GUID." );

            if( checkDirty )
            {
                PrefabAsset entry;
                if( _collection.TryGetValue( asset.Guid, out entry ) && entry != null )
                {
                    if( !PrefabAssetEquals( entry, asset ) )
                    {
                        SetDirty( asset );
                    }
                }
                else
                {
                    SetDirty( asset );
                }
            }

            _collection[ asset.Guid ] = asset;
        }

        private bool PrefabAssetEquals( PrefabAsset a, PrefabAsset b )
        {
            if( object.ReferenceEquals( a, b ) )
                return true;

            if( a == null || b == null )
                return false;

            if( a.Path != b.Path )
                return false;

            if( !a.References.SequenceEqual( b.References ) )
                return false;

            return true;
        }

        public bool Remove( GameObject asset )
        {
            if( asset == null )
                throw new ArgumentNullException( "Asset" );

            string path = AssetDatabase.GetAssetPath( asset );

            if( string.IsNullOrEmpty( path ) )
                throw new InvalidOperationException( "GameObject is not an asset" );

            return Remove( AssetDatabase.AssetPathToGUID( path ) );
        }

        public bool Remove( string guid )
        {
            if( string.IsNullOrEmpty( guid ) )
                throw new ArgumentNullException( "GUID" );

            if( _collection.Remove( guid ) )
            {
                _dirtyAssets.Remove( guid );
                return true;
            }

            return false;
        }

        public void SetDirty( PrefabAsset asset )
        {
            if( _dirtyAssets.Contains( asset.Guid ) )
                return;

            _dirtyAssets.Add( asset.Guid );
        }

        public PrefabAsset[] FindPrefabsWithReferenceTo( string guid )
        {
            EnsureImportingFinished();

            var result = new List<PrefabAsset>();
            var enumerator = _collection.GetEnumerator();
            while( enumerator.MoveNext() )
            {
                if( enumerator.Current.Value.HasReferenceTo( guid ) )
                    result.Add( enumerator.Current.Value );
            }

            return result.ToArray();
        }

        private void WriteToFile( string path, string text )
        {
            File.WriteAllText( path, text );
        }

        private void EnsureDirectoryExists( string path )
        {
            if( !Directory.Exists( path ) )
                Directory.CreateDirectory( path );
        }

        public void EnsureImportingFinished()
        {
            if( _importer == null )
                return;

            while( _importer != null && !_importer.FinishedReading )
            {
                EditorUtility.DisplayProgressBar(
                    "Hold On",
                    "Importing prefab database...",
                    _importer.Progress );

                System.Threading.Thread.Sleep( 200 );
            }

            EditorUtility.ClearProgressBar();

            _importer.Dispose();
            _importer = null;
        }

        private void Import( bool skipExistingEntries )
        {
            EnsureDirectoryExists( Location );

            if( Config.DEBUG_INFO )
                Console.Log( "Importing Prefab Database..." );

            _importer = new DatabaseReader( Location );
            _importer.ReadAsync(
                ( JsonObject json ) =>
                {
                    var asset = new PrefabAsset( json );

                    if( skipExistingEntries && _collection.ContainsKey( asset.Guid ) )
                        return;

                    Add( asset, false );
                },
                () =>
                {
                    if( Config.DEBUG_INFO )
                        Console.LogFormat(
                            "Finished importing Prefab Database in {0:#,#} ms ({1:#,#} entries)",
                            _importer.ElapsedMilliseconds,
                            _collection.Count );
                } );
        }
    }
}