﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace VisualDesignCafe.Editor.Prefabs
{
    using Collection = Dictionary<GameObject, List<Prefab>>;
    using Entry = KeyValuePair<GameObject, List<Prefab>>;
    using GroupList = IEnumerable<IGrouping<GameObject, Prefab>>;

    public class ApplyPool
    {

        /// <summary>
        /// The number of entries (prefab assets) in the apply pool.
        /// </summary>
        public int Count
        {
            get { return _collection.Count; }
        }

        private Collection _collection = new Collection();

        /// <summary>
        /// Clears all entries in the apply pool.
        /// </summary>
        public void Clear()
        {
            _collection.Clear();
        }

        /// <summary>
        /// Returns all prefabs in the queue ordered by depth.
        /// </summary>
        /// <returns></returns>
        public Entry[] GetSortedQueue()
        {
            List<Entry> sortedList = new List<Entry>();

            foreach( Entry entry in _collection )
            {
                List<Prefab> sourcePrefabs =
                    entry.Value
                    .Where( p => !Prefab.IsNull( p ) )
                    .ToList();

                if( sourcePrefabs == null || sourcePrefabs.Count == 0 )
                {
                    sortedList.Add( new Entry( entry.Key, null ) );
                    continue;
                }

                foreach( var group in GroupByRoot( sourcePrefabs ) )
                {
                    sortedList.Add( new Entry( entry.Key, group.ToList() ) );
                }
            }

            return sortedList
                .OrderByDescending( entry => MaxDepth( entry.Value ) )
                .ToArray();
        }

        /// <summary>
        /// Adds the prefab to the pool.
        /// </summary>
        /// <param name="prefab"></param>
        /// <param name="includeChildren"></param>
        public void Add(
            Prefab prefab,
            bool includeChildren,
            bool includeSelf,
            bool includeAsset )
        {
            Trace.AssertArgumentNotNull( prefab, "Prefab" );

            if( !includeChildren && !includeSelf && !includeAsset )
                throw new InvalidOperationException();

            if( includeAsset )
            {
                if( Prefab.IsMissing( prefab ) )
                {
                    if( Config.DEBUG_WARNING )
                        Console.LogWarning( "Prefab asset is missing." );
                }
                else
                {
                    AddToCollection( prefab.Asset, null );
                }
            }

            if( includeSelf )
            {
                AddNestedPrefab( prefab );
            }

            if( includeChildren )
            {
                foreach( Transform child in prefab.CachedTransform )
                {
                    AddRecursive( child );
                }
            }
        }

        /// <summary>
        /// Adds all Prefab components on the transform and all its children to the pool.
        /// </summary>
        /// <param name="transform"></param>
        private void AddRecursive( Transform transform )
        {
            Prefab prefab = transform.GetComponent<Prefab>();

            if( prefab != null )
            {
                AddNestedPrefab( prefab );
            }

            foreach( Transform child in transform )
            {
                AddRecursive( child );
            }
        }

        private void AddNestedPrefab( Prefab sourcePrefab )
        {
            if( Prefab.IsMissing( sourcePrefab ) )
            {
                if( Config.DEBUG_WARNING )
                    Console.LogWarningFormat(
                        "No asset found for prefab. ({0})",
                        sourcePrefab );

                return;
            }

            if( NestedPrefabUtility.IsModelPrefab( sourcePrefab ) )
            {
                AddModelPrefab( sourcePrefab );
                return;
            }

            if( !NestedPrefabUtility.HasModifications( sourcePrefab, false, false ) )
            {
                if( Config.DEBUG_INFO )
                    Console.LogFormat(
                        "Ignoring prefab because no modifications were found. ({0})",
                        sourcePrefab );

                return;
            }

            AddToCollection( sourcePrefab.Asset, sourcePrefab );
        }

        private void AddModelPrefab( Prefab sourcePrefab )
        {
            Prefab parentPrefab =
                PrefabHierarchyUtility.GetParentPrefab( sourcePrefab, true );

            if( Prefab.IsNullOrMissing( parentPrefab ) )
            {
                if( Config.DEBUG_WARNING )
                    Console.LogWarning(
                        "No parent prefab found for nested model prefab" );

                return;
            }

            GameObject nestedPrefabInParentAsset =
                PrefabHierarchyUtility.FindNestedPrefabWithGuid(
                    parentPrefab.Asset.transform,
                    sourcePrefab.ObjectGuid );

            if( nestedPrefabInParentAsset == null )
            {
                if( Config.DEBUG_WARNING )
                    Console.LogWarning(
                        "No alternative asset found for nested model prefab" );

                return;
            }

            if( NestedPrefabUtility.HasModifications(
                    sourcePrefab.CachedGameObject,
                    nestedPrefabInParentAsset,
                    false,
                    false,
                    null ) )
            {
                AddToCollection( parentPrefab.Asset, parentPrefab );
                return;
            }
            else
            {
                if( Config.DEBUG_INFO )
                    Console.LogFormat(
                        "Ignoring prefab because no modifications were found. ({0})",
                        sourcePrefab.CachedGameObject );

                return;
            }
        }

        private void AddToCollection( GameObject asset, Prefab sourcePrefab )
        {
            if( Config.DEBUG_INFO )
                Console.LogFormat(
                    "Adding entry to apply pool. (Asset: {0}, Source: {1})",
                    asset.name,
                    sourcePrefab );

            if( !_collection.ContainsKey( asset ) )
                _collection[ asset ] = new List<Prefab>();

            if( sourcePrefab != null
                && !_collection[ asset ].Contains( sourcePrefab ) )
            {
                _collection[ asset ].Add( sourcePrefab );
            }
        }

        private int MaxDepth( List<Prefab> list )
        {
            if( list == null )
                return -1;

            int maxDepth = -1;
            int depth = -1;

            foreach( Prefab prefab in list )
            {
                depth = PrefabHierarchyUtility.GetDepth( prefab, true );
                if( depth > maxDepth )
                    maxDepth = depth;
            }

            return maxDepth;
        }

        private GroupList GroupByRoot( List<Prefab> sourcePrefabs )
        {
            return sourcePrefabs
                .GroupBy(
                    prefab =>
                        PrefabHierarchyUtility.FindPrefabRoot(
                            prefab.CachedGameObject ),
                    prefab => prefab );
        }
    }
}