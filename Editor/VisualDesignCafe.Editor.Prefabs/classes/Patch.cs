﻿using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace VisualDesignCafe.Editor.Prefabs
{
    internal class Patch
    {
        private class PrefabPatch
        {
            private Dictionary<string, string> _guidCollection =
                new Dictionary<string, string>();

            public void OverwriteGuid( string currentGuid, string newGuid )
            {
                _guidCollection[ currentGuid ] = newGuid;
            }

            public bool TryGetGuid( string currentGuid, out string newGuid )
            {
                return _guidCollection.TryGetValue( currentGuid, out newGuid );
            }
        }

        private Dictionary<string, PrefabPatch> _prefabPatchCollection =
            new Dictionary<string, PrefabPatch>();

        private List<string> _removedPrefabs = new List<string>();

        /// <summary>
        /// Removes the prefab connection.
        /// </summary>
        public void RemovePrefabConnection( Prefab prefab )
        {
            _removedPrefabs.Add( prefab.PrefabGuid );
        }

        /// <summary>
        /// Overwrites the GUID in the prefab with the new GUID.
        /// </summary>
        public void OverwriteGuid( Prefab prefab, string currentGuid, string newGuid )
        {
            GetCreatePatchForPrefab( prefab ).OverwriteGuid( currentGuid, newGuid );
        }

        /// <summary>
        /// Applies the patch to the prefab.
        /// </summary>
        public void Apply( Prefab prefab )
        {
            if( prefab == null )
                throw new System.NullReferenceException();

            if( Config.DEBUG_INFO )
                Console.Log( "Applying patch to " + prefab.CachedGameObject.ToLog() );

            Console.Indent++;
            ApplyRecursive( prefab, prefab.CachedTransform );
            Console.Indent--;
        }

        /// <summary>
        /// Applies the patch to the transform and all its children recursively.
        /// </summary>
        private void ApplyRecursive( Prefab prefab, Transform transform )
        {
            RemovePrefabConnections( prefab, transform );
            ApplyToGuid( prefab, transform );
            ApplyToPrefab( prefab, transform );

            foreach( Transform child in transform )
            {
                ApplyRecursive( child.GetComponent<Prefab>() ?? prefab, child );
            }
        }

        private bool RemovePrefabConnections( Prefab root, Transform transform )
        {
            Prefab prefab = transform.GetComponent<Prefab>();

            if( prefab == null )
                return false;

            if( !_removedPrefabs.Contains( prefab.PrefabGuid ) )
                return false;

            // Append the reference collection of the prefab to its parent.
            Prefab parent = PrefabHierarchyUtility.FindValidParentPrefab( transform.gameObject );

            if( parent != null )
            {
                if( Config.DEBUG_INFO )
                    Console.Log( "Parent: " + parent.gameObject.ToLog() );

                for( int i = 0; i < prefab.ReferenceCollection.Count; i++ )
                {
                    GameObject child = prefab.ReferenceCollection[ i ];

                    if( child == null || EditorUtility.IsPersistent( child ) )
                        continue;

                    if( Config.DEBUG_INFO )
                        Console.Log( "Adding " + child + " to parent reference collection" );

                    parent.ReferenceCollection.Add( child, true );
                }
            }

            string prefabGuid = prefab.PrefabGuid;

            // Destroy the prefab components.
            Component.DestroyImmediate( prefab );

            var nestedPrefab = transform.GetComponent<NestedPrefab>();
            if( nestedPrefab != null )
                Component.DestroyImmediate( nestedPrefab );

            // Remove prefab overrides. They can only exist on Nested Prefabs.
            var prefabOverrides = transform.GetComponent<PrefabOverrides>();
            if( prefabOverrides != null )
                Component.DestroyImmediate( prefabOverrides );

            // Update the GUID in all Prefab Overrides, 
            // otherwise the override will point to the wrong object.
            ReplaceGuidInOverrides( transform, prefabGuid, string.Empty );

            if( Config.DEBUG_INFO )
                Console.Log( "Removed prefab connection of " + transform.gameObject.ToLog() );

            return true;
        }

        /// <summary>
        /// Applies the patch to the GUID of the transform.
        /// Ignored if there is no GUID component on the Transform.
        /// </summary>
        private void ApplyToGuid( Prefab root, Transform transform )
        {
            Guid guid = transform.GetComponent<Guid>();

            // The child does not have any GUID. Maybe it is a new object and not part of the prefab.
            // We should ignore it.
            if( guid == null || string.IsNullOrEmpty( guid.Value ) )
                return;

            PrefabPatch patch = GetPatchForPrefab( root );
            if( patch == null )
                return;

            string newGuid = string.Empty;
            if( !patch.TryGetGuid( guid.Value, out newGuid ) )
                return;

            string oldGuid = guid.Value;
            guid.OverrideGuid( newGuid );

            // Update the GUID in all Prefab Overrides, 
            // otherwise the override will point to the wrong object.
            ReplaceGuidInOverrides( transform, oldGuid, newGuid );
        }

        /// <summary>
        /// Applies the patch to the Prefab of the transform. 
        /// Ignored if there is no Prefab component on the Transform.
        /// </summary>
        private void ApplyToPrefab( Prefab root, Transform transform )
        {
            Prefab prefab = transform.GetComponent<Prefab>();

            // The child does not have any GUID. Maybe it is a new object and not part of the prefab.
            // We should ignore it.
            if( prefab == null || string.IsNullOrEmpty( prefab.PrefabGuid ) )
                return;

            PrefabPatch patch = GetPatchForPrefab( root );
            if( patch == null )
                return;

            string newGuid = string.Empty;
            if( !patch.TryGetGuid( prefab.PrefabGuid, out newGuid ) )
                return;

            string oldGuid = prefab.PrefabGuid;
            prefab.OverrideGuid( newGuid );

            // Update the GUID in all Prefab Overrides, 
            // otherwise the override will point to the wrong object.
            ReplaceGuidInOverrides( transform, oldGuid, newGuid );
        }

        /// <summary>
        /// Replaces all occurances of the GUID value in all Prefab Overrides components on all parents of the transform.
        /// </summary>
        private void ReplaceGuidInOverrides(
            Transform transform, string guid, string newGuid )
        {
            while( transform != null )
            {
                var overrides = transform.GetComponent<PrefabOverrides>();
                if( overrides != null )
                {
                    foreach( var obj in overrides.Objects )
                    {
                        string[] path = obj.GuidPath;
                        int index = path.IndexOf( guid );

                        if( index == -1 )
                            continue;

                        if( !string.IsNullOrEmpty( newGuid ) )
                        {
                            path[ index ] = newGuid;
                            obj.UpdateGuidPath( path );
                        }
                        else
                        {
                            obj.UpdateGuidPath(
                                path.Where( p => p != guid ).ToArray() );
                        }

                        EditorUtility.SetDirty( overrides );
                    }
                }

                transform = transform.parent;
            }
        }

        private PrefabPatch GetCreatePatchForPrefab( Prefab prefab )
        {
            PrefabPatch patch = null;
            string key = prefab.PrefabGuid;

            if( !_prefabPatchCollection.TryGetValue( key, out patch ) )
            {
                patch = new PrefabPatch();
                _prefabPatchCollection[ key ] = patch;
            }

            return patch;
        }

        private PrefabPatch GetPatchForPrefab( Prefab prefab )
        {
            PrefabPatch patch = null;
            string key = prefab.PrefabGuid;

            if( !_prefabPatchCollection.TryGetValue( key, out patch ) )
                return null;

            return patch;
        }
    }
}