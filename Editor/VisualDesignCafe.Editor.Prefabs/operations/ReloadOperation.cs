﻿using System;
using System.Linq;
using UnityEngine;
using VisualDesignCafe.Editor.Prefabs.Comparing;

namespace VisualDesignCafe.Editor.Prefabs
{
    internal class ReloadOperation : PrefabOperation
    {
        public readonly GameObject[] SourcePrefabs;
        public readonly bool RevertLocalChanges;
        public readonly bool RevertModelModifications;
        public readonly bool IncludeNestedChildren;
        public readonly bool IncludeSceneObjects;

        private readonly Postprocessor[] Processors;

        public static ModificationList FindModifications(
            Prefab gameObject, Prefab source, bool includePrefabOverride )
        {
            return Compare(
                gameObject,
                source,
                includePrefabOverride,
                false,
                false );
        }

        public static ModificationList FindModifications(
            Prefab gameObject,
            Prefab source,
            bool includePrefabOverride,
            bool includeNestedChildren )
        {
            return Compare(
                gameObject,
                source,
                includePrefabOverride,
                includeNestedChildren,
                false );
        }

        private static ModificationList Compare(
            Prefab instanceObject,
            Prefab sourceObject,
            bool includePrefabOverride = false,
            bool includeNestedChildren = false,
            bool isReloadedFromModelPrefab = false,
            bool includeSceneObjects = false,
            bool isModelPrefab = false )
        {
            if( instanceObject == null )
                throw new ArgumentNullException( "Destination GameObject" );

            if( sourceObject == null )
                throw new ArgumentNullException( "Source GameObject" );

            var sourcePrefab = sourceObject.GetComponent<Prefab>();
            var instancePrefab = instanceObject.GetComponent<Prefab>();

            if( sourcePrefab == null )
                throw new ArgumentException( "Source is not a prefab" );

            if( instancePrefab == null )
                throw new ArgumentException( "Destination is not a prefab" );

            bool hasDifferences = false;
            PrefabComparer comparer;

            if( Config.DEBUG_INFO && Config.VERBOSE )
                Console.LogFormat(
                    "Reloading {0} from {1}",
                    instanceObject.gameObject.ToLog(),
                    sourceObject.gameObject.ToLog() );

            comparer =
                new PrefabComparer(
                    instanceObject,
                    sourceObject,
                    PrefabComparer.DefaultIgnoreList,
                    includeNestedChildren,
                    isReloadedFromModelPrefab,
                    isModelPrefab );

            PrefabComparison flags =
                includePrefabOverride
                    ? PrefabComparison.AsInstance
                    : PrefabComparison.AsPrefab;

            if( includeSceneObjects )
                FlagsUtility.Set( ref flags, PrefabComparison.IncludeSceneReferences );

            ModificationList modifications =
                comparer.Compare(
                    flags,
                    ( modification ) =>
                    {
                        hasDifferences = true;

                        if( NestedPrefabUtility.ModificationCallback != null )
                            NestedPrefabUtility.ModificationCallback( modification );

                        if( Config.DEBUG_INFO )
                            Console.Log( modification.ToString() );

                        return true;
                    } );

            if( !hasDifferences && Config.DEBUG_INFO )
                Console.Log( "No Differences found" );

            return modifications;
        }

        public ReloadOperation(
            Prefab prefab,
            GameObject sourcePrefab,
            bool revertLocalChanges,
            bool includeNestedChildren,
            bool includeSceneObjects,
            bool revertModelModifications )
                : base( prefab )
        {
            if( Prefab.IsNull( prefab ) )
                throw new ArgumentNullException( "Prefab" );

            if( sourcePrefab == null )
                throw new ArgumentNullException( "Source Prefab" );

            SourcePrefabs = new GameObject[] { sourcePrefab };
            RevertLocalChanges = revertLocalChanges;
            IncludeNestedChildren = includeNestedChildren;
            IncludeSceneObjects = includeSceneObjects;
            RevertModelModifications = revertModelModifications;

            Processors = Postprocessor.GetCachedPostprocessorsForType( typeof( Prefab ) );
        }

        public ReloadOperation(
            Prefab prefab,
            GameObject[] sourcePrefabs,
            bool revertLocalChanges,
            bool includeNestedChildren,
            bool includeSceneObjects,
            bool revertModelModifications )
                : base( prefab )
        {
            if( Prefab.IsNull( prefab ) )
                throw new ArgumentNullException( "Prefab" );

            if( sourcePrefabs == null || sourcePrefabs.Length == 0 )
                throw new ArgumentNullException( "Source Prefabs" );

            foreach( GameObject sourcePrefab in sourcePrefabs )
            {
                if( sourcePrefab == null )
                    throw new ArgumentNullException( "Source Prefab is null" );

                if( Target.CachedGameObject == sourcePrefab )
                    throw new ArgumentException( "Source prefab is self" );
            }

            SourcePrefabs = sourcePrefabs;
            RevertLocalChanges = revertLocalChanges;
            IncludeNestedChildren = includeNestedChildren;
            IncludeSceneObjects = includeSceneObjects;
            RevertModelModifications = revertModelModifications;

            Processors = Postprocessor.GetCachedPostprocessorsForType( typeof( Prefab ) );
        }

        public override void Do()
        {
            if( Config.DEBUG_INFO && Config.VERBOSE )
                Console.LogFormat( "Reloading {0} from\n{1}",
                    Target.CachedGameObject.ToLog(),
                    string.Join( "\n", SourcePrefabs.Select( g => g.ToLog() ).ToArray() ) );

            Console.Indent++;
            ModifyPrefab( FindAllModifications(), RevertLocalChanges );

            if( IncludeNestedChildren )
                ReloadChildren();

            ReferenceCollectionUtility.IsValid( Target.ReferenceCollection );
            Console.Indent--;
        }

        public void ModifyPrefab(
            ModificationList modifications,
            bool revertLocalChanges = false )
        {
            modifications.Sort();

            var modifier = new PrefabModifier( Target );
            modifier.ApplyModifications( modifications, !revertLocalChanges );
        }

        public ModificationList FindModifications(
            Prefab prefab,
            GameObject sourcePrefab,
            bool includePrefabOverride = false,
            bool isReloadedFromModelPrefab = false,
            bool isModelPrefab = false )
        {
            return Compare(
                    prefab,
                    sourcePrefab.GetComponent<Prefab>(),
                    includePrefabOverride,
                    false,
                    isReloadedFromModelPrefab,
                    IncludeSceneObjects,
                    isModelPrefab );
        }

        private ModificationList FindAllModifications()
        {
            var modifications = new ModificationList();
            foreach( GameObject sourcePrefab in SourcePrefabs )
            {
                if( sourcePrefab == Target.gameObject )
                {
                    if( Config.DEBUG_WARNING )
                        Console.LogWarning( "Destination equals source. Ignoring reload operation." );

                    continue;
                }

                if( Config.DEBUG_INFO )
                    Console.Log( "Searching for modifications in " + sourcePrefab.ToLog() );

                Console.Indent++;

                // Make sure the guid of the nested prefab is correct.
                // Children of the prefab are completely replaced if the GUID is not right and the resulting prefab will be correct.
                // However the root will never be completely replaced, so if the GUID is wrong we will just copy the GUID from the original asset.
                Guid assetGuid =
                    Target.Asset != null
                        ? Target.Asset.GetComponent<Guid>()
                        : null;

                if( assetGuid != null
                    && Target.ObjectGuid != null
                    && Target.ObjectGuid.Value != assetGuid.Value )
                {
                    Target.ObjectGuid.OverrideGuid( assetGuid.Value );
                }

                // Find all the modification of the prefab compared to the original prefab.
                bool isModelPrefab =
                    Target.Asset != null
                        ? NestedPrefabUtility.IsModelPrefab( Target.Asset )
                        : false;

                bool isReloadedFromModelPrefab =
                    NestedPrefabUtility.IsModelPrefab( sourcePrefab )
                        && ( !RevertLocalChanges || !RevertModelModifications );

                ModificationList modificationCollection =
                    FindModifications(
                        Target,
                        sourcePrefab,
                        RevertLocalChanges,
                        isReloadedFromModelPrefab,
                        isModelPrefab );

                CallPostprocessors( ref modificationCollection, sourcePrefab );

                modifications.AddRange( modificationCollection );

                Console.Indent--;
            }

            return modifications;
        }

        private void CallPostprocessors(
            ref ModificationList modificationCollection, GameObject sourcePrefab )
        {
            // Call modification postprocessors.
            if( Processors.Length > 0 )
            {
                foreach( Postprocessor processor in Processors )
                {
                    try
                    {
                        if( processor.PostprocessModificationsMethod == null )
                            continue;

                        modificationCollection =
                            new ModificationList(
                                (Modification[]) processor.PostprocessModificationsMethod.Invoke(
                                    null,
                                    new object[]
                                    {
                                            Target,
                                            (Modification[]) modificationCollection,
                                            sourcePrefab.GetComponent<Prefab>(),
                                    } ) );
                    }
                    catch( Exception e )
                    {
                        Debug.LogException( e );
                    }
                }
            }
            else
            {
                if( Config.DEBUG_WARNING )
                    Console.LogWarning( "No custom processors found for prefab. There should be a prefab overrides processor." );
            }

            // Call custom modification postprocessors for each component.
            for( int i = modificationCollection.Count - 1; i > -1; i-- )
            {
                ComponentInfo componentInfo = modificationCollection[ i ].CachedSourceComponent;

                if( componentInfo == null && modificationCollection[ i ].CachedSourceProperty != null )
                    componentInfo = modificationCollection[ i ].CachedSourceProperty.Component;

                if( componentInfo == null || componentInfo.Processors.Length == 0 )
                    continue;

                foreach( Postprocessor processor in componentInfo.Processors )
                {
                    try
                    {
                        if( processor.PostprocessModificationsMethod == null )
                            continue;

                        modificationCollection =
                            new ModificationList(
                                (Modification[]) processor.PostprocessModificationsMethod.Invoke(
                                    null,
                                    new object[]
                                    {
                                            Target,
                                            (Modification[]) modificationCollection,
                                            sourcePrefab.GetComponent<Prefab>(),
                                    } ) );
                    }
                    catch( Exception e )
                    {
                        Debug.LogException( e );
                    }
                }

                i = Mathf.Min( i, modificationCollection.Count );
            }
        }

        private void ReloadChildren()
        {
            NestedPrefab[] nestedPrefabs =
                Target.CachedGameObject.GetComponentsInChildren<NestedPrefab>( true );

            foreach( NestedPrefab nestedPrefab in nestedPrefabs )
            {
                if( nestedPrefab == null || nestedPrefab.Asset == null )
                    continue;

                if( !PrefabHierarchyUtility.IsDirectChildOf( nestedPrefab.Prefab, Target ) )
                    continue;

                // Check if the nested prefab is an instance of a model prefab.
                bool isReloadedFromModelPrefab =
                    NestedPrefabUtility.IsModelPrefab( nestedPrefab.Prefab )
                        && ( !RevertLocalChanges || !RevertModelModifications );

                // If the prefab is a model prefab and we are not directly 
                // reloading the nested model from an asset due to a reimport 
                // of the asset then we have to reload it from the parent prefab.
                // Otherwise the added objects and components are not included.
                bool isReloaded = false;
                if( isReloadedFromModelPrefab )
                {
                    if( Config.DEBUG_INFO && Config.VERBOSE )
                        Console.Log( "Source is a model prefab" );

                    Guid nestedPrefabGuid =
                        nestedPrefab.CachedGameObject.GetComponent<Guid>();

                    GameObject[] sources =
                        SourcePrefabs
                            .Select(
                                sp => PrefabHierarchyUtility.FindNestedPrefabWithGuid(
                                        sp.transform, nestedPrefabGuid ) )
                            .Where( s => s != null )
                            .ToArray();

                    if( sources.Length > 0 )
                    {
                        isReloaded = true;

                        new ReloadOperation(
                            nestedPrefab.Prefab,
                            sources,
                            RevertLocalChanges,
                            IncludeNestedChildren,
                            IncludeSceneObjects,
                            RevertModelModifications ).Do();
                    }
                }

                // If the nested prefab was not reloaded from a different nested prefab (above) 
                // then we have to reload it from the original asset like usual.
                if( !isReloaded )
                {
                    new ReloadOperation(
                        nestedPrefab.Prefab,
                        HasInstanceForAsset( nestedPrefab.Asset )
                            ? GetInstanceForAsset( nestedPrefab.Asset )
                            : nestedPrefab.Asset,
                        RevertLocalChanges,
                        IncludeNestedChildren,
                        IncludeSceneObjects,
                        RevertModelModifications ).Do();
                }
            }
        }
    }
}