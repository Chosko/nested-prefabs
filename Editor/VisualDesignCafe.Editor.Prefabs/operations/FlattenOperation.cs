﻿using System;
using UnityEditor;
using UnityEngine;

namespace VisualDesignCafe.Editor.Prefabs
{
    internal class FlattenOperation : PrefabOperation
    {
        protected readonly PrefabDatabase Database;

        public FlattenOperation( Prefab prefab, PrefabDatabase database )
            : base( prefab )
        {
            if( Prefab.IsNull( prefab ) )
                throw new ArgumentNullException( "Prefab" );

            if( database == null )
                throw new ArgumentNullException( "Database" );

            Database = database;
        }

        public override void Do()
        {
            RemovePrefabConnection( Target );
        }

        /// <summary>
        /// Removes the prefab connection.
        /// </summary>
        private void RemovePrefabConnection( Prefab prefab )
        {
            var nestedPrefab = prefab.CachedGameObject.GetComponent<NestedPrefab>();
            Prefab parent = PrefabHierarchyUtility.FindValidParentPrefab( prefab.CachedGameObject );

            if( nestedPrefab == null
                || Prefab.IsNull( parent )
                || PrefabHierarchyUtility.FindPrefabRoot( nestedPrefab.CachedGameObject )
                    != PrefabHierarchyUtility.FindPrefabRoot( parent.gameObject ) )
            {
                RemoveRootPrefabConnection( prefab );
                return;
            }

            if( parent == null )
                throw new NullReferenceException();

            var patch = CreatePatch( prefab );

            GameObject asset = parent.Asset;

            // Apply the patch to nested prefabs in all assets.
            if( asset != null )
            {
                BeginBatch();

                GameObject[] references =
                    AssetDatabaseUtility.FindAssetsWithReferenceTo(
                        asset,
                        Database,
                        true,
                        -1 );

                foreach( GameObject assetWithReference in references )
                {
                    if( assetWithReference == null )
                        continue;

                    if( Config.DEBUG_INFO )
                        Console.Log( "Reference: " + assetWithReference.ToLog() );

                    ApplyPatchRecursive( assetWithReference, patch, prefab );
                }

                patch.Apply( prefab );

                // There is no need to postprocess any changes, since we manually applied all changes already.
                // Therefore we can disable the Postprocessor to save performance.
                NestedPrefabsPostprocessor.IsEnabled = false;
                EndBatch();
                NestedPrefabsPostprocessor.IsEnabled = true;
            }
            else
            {
                patch.Apply( prefab );

                if( Config.DEBUG_WARNING )
                    Console.LogWarning( "Could not load asset for " + parent );
            }
        }

        private void ApplyPatchRecursive(
            GameObject asset, Patch patch, Prefab sourcePrefab )
        {
            GameObject instance = GetInstanceForAsset( asset );

            Prefab[] nestedPrefabs = instance.GetComponentsInChildren<Prefab>( true );
            foreach( Prefab nestedPrefab in nestedPrefabs )
            {
                if( nestedPrefab.PrefabGuid != sourcePrefab.PrefabGuid
                    || nestedPrefab.ObjectGuid != sourcePrefab.ObjectGuid )
                {
                    continue;
                }

                patch.Apply( nestedPrefab );

            }

            SaveAssetInstance( instance, asset );
        }

        private Patch CreatePatch( Prefab prefab )
        {
            if( Prefab.IsNull( prefab ) )
                throw new ArgumentNullException( "prefab" );

            Prefab parent =
                PrefabHierarchyUtility.FindValidParentPrefab( prefab.CachedGameObject );

            if( parent == null )
                throw new NullReferenceException();

            var patch = new Patch();

            patch.RemovePrefabConnection( prefab );

            // Loop through all objects in the nested prefab to reassign the GUID and parent prefab.
            for( int i = prefab.ReferenceCollection.Count - 1; i > -1; i-- )
            {
                GameObject child = prefab.ReferenceCollection[ i ];

                if( child == null || EditorUtility.IsPersistent( child ) )
                    continue;

                Guid childGuid = child.GetComponent<Guid>();
                if( childGuid == null )
                    continue;

                string currentValue = childGuid.Value;

                parent.ReferenceCollection.Add( child, true );

                // Add a GUID overwrite to the patch. 
                // This will give the object a new GUID in all nested prefabs/assets that contain this object.
                patch.OverwriteGuid( prefab, currentValue, Guid.NewGuid() );

                // Also reset the GUID of any nested prefabs.
                Prefab childPrefab = child.GetComponent<Prefab>();

                if( childPrefab != null && childPrefab != prefab )
                {
                    patch.OverwriteGuid( prefab, childPrefab.PrefabGuid, Guid.NewGuid() );
                }
            }

            return patch;
        }

        private void RemoveRootPrefabConnection( Prefab prefab )
        {
            if( Config.DEBUG_INFO )
                Console.Log( "Removing root prefab connection" );

            Undo.IncrementCurrentGroup();
            Undo.SetCurrentGroupName( "Remove Prefab Connection" );

            PrefabUtility.DisconnectPrefabInstance( prefab.CachedGameObject );

            RemovePrefabConnectionRecursive( prefab.CachedTransform );

            Undo.CollapseUndoOperations( Undo.GetCurrentGroup() );
        }

        private void RemovePrefabConnectionRecursive( Transform transform )
        {
            Prefab childPrefab = transform.GetComponent<Prefab>();
            Guid childGuid = transform.GetComponent<Guid>();
            NestedPrefab childNestedPrefab = transform.GetComponent<NestedPrefab>();

            if( childGuid != null )
                Undo.DestroyObjectImmediate( childGuid );

            if( childPrefab != null )
                Undo.DestroyObjectImmediate( childPrefab );

            if( childNestedPrefab != null )
                Undo.DestroyObjectImmediate( childNestedPrefab );

            foreach( Transform child in transform )
            {
                if( child.GetComponent<Prefab>() != null )
                    continue;

                RemovePrefabConnectionRecursive( child );
            }
        }
    }
}
