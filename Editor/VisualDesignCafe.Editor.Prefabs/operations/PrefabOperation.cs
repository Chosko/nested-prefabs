﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using VisualDesignCafe.Editor.Prefabs.Overrides;

namespace VisualDesignCafe.Editor.Prefabs
{
    public class PrefabOperation
    {
        public Prefab Target
        {
            get;
            protected set;
        }

        public static bool IsInBatch
        {
            get { return _isInBatch; }
        }

        public static Dictionary<GameObject, GameObject> BatchedAssets
        {
            get { return _batchedAssets; }
        }

        private static bool _isInBatch = false;
        private static Dictionary<GameObject, GameObject> _batchedAssets =
            new Dictionary<GameObject, GameObject>();

        /// <summary>
        /// Begins a new batch. 
        /// Should be used before reloading any Nested Prefabs or assets.
        /// </summary>
        internal static void BeginBatch()
        {
            if( Config.VERBOSE && Config.DEBUG_INFO )
            {
                Console.Log( "Starting Batch" );
                Console.Indent++;
            }

            _isInBatch = true;
            _batchedAssets.Clear();
        }

        /// <summary>
        /// Ends the current batch.
        /// Should be called after all Nested Prefabs and/or assets were reloaded.
        /// </summary>
        internal static void EndBatch()
        {
            if( Config.VERBOSE && Config.DEBUG_INFO )
            {
                Console.Indent--;
                Console.Log( "Ending Batch" );
            }

            _isInBatch = false;

            AssetDatabase.StartAssetEditing();
            foreach( KeyValuePair<GameObject, GameObject> pair in _batchedAssets )
                SaveAssetInstance( pair.Value, pair.Key );

            _batchedAssets.Clear();
            AssetDatabase.StopAssetEditing();
            _batchedAssets.Clear();
        }

        /// <summary>
        /// Checks if an instance for the asset is stored in the current batch.
        /// </summary>
        internal static bool HasInstanceForAsset( GameObject asset )
        {
            return _batchedAssets.ContainsKey( asset );
        }

        internal static bool HasInstanceForAsset( GameObject asset, out GameObject instance )
        {
            return _batchedAssets.TryGetValue( asset, out instance ) && instance != null;
        }

        /// <summary>
        /// Creates an instance of the asset for editing
        /// </summary>
        internal static GameObject GetInstanceForAsset(
            GameObject asset, bool asNestedPrefab = false )
        {
            if( asset == null )
                throw new System.ArgumentNullException( "asset" );

            GameObject instance;

            if( !_isInBatch || !HasInstanceForAsset( asset, out instance ) )
            {
                instance = InstantiateAsset( asset );

                if( Config.DEBUG_INFO && Config.VERBOSE )
                    Console.LogFormat(
                        "Created temporary instance for asset. (Asset: {0}, Instance: {1})",
                        asset.ToLog(),
                        instance.ToLog() );
            }
            else
            {
                if( Config.DEBUG_INFO && Config.VERBOSE )
                    Console.LogFormat(
                        "Loaded cached temporary instance for asset. (Asset: {0}, Instance: {1})",
                        asset.ToLog(),
                        instance.ToLog() );
            }

            if( Config.DEBUG_WARNING && instance.GetComponent<Prefab>() == null )
                Console.LogWarning( "No Prefab component found on instance" );

            if( asNestedPrefab )
            {
                NestedPrefab nestedPrefab = instance.GetComponent<NestedPrefab>();
                if( nestedPrefab == null )
                    nestedPrefab = instance.AddComponent<NestedPrefab>();
            }

            return instance;
        }

        /// <summary>
        /// Saves the changes to the asset instance to the given prefab
        /// </summary>
        internal static bool SaveAssetInstance(
            GameObject instance, GameObject asset, bool force = false )
        {
            if( EditorUtility.IsPersistent( instance ) )
                return false;

            if( _isInBatch && !force )
                return false;

            if( Config.DEBUG_INFO )
                Console.LogFormat(
                    "Saving {0} to <b><i>\"{1}\"</i></b>",
                    instance.ToLog(),
                    AssetDatabase.GetAssetPath( asset ) );

            if( Config.APPLY_OVERRIDES_TO_ASSET )
                PrefabOverrideUtility.ApplyAll( instance );

            // A NestedPrefab component will be added to the asset during editing.
            // We have to make sure to delete it before saving the asset.
            NestedPrefab nestedPrefab = instance.GetComponent<NestedPrefab>();

            if( nestedPrefab != null )
                Component.DestroyImmediate( nestedPrefab );

            NestedPrefabUtility.ReplacePrefab(
                instance, asset, ReplacePrefabOptions.ConnectToPrefab );

            GameObject.DestroyImmediate( instance );
            return true;
        }

        internal static bool IsAssetInstance( GameObject instance )
        {
            GameObject objectInAsset =
                PrefabUtility.GetPrefabParent( instance ) as GameObject;

            if( objectInAsset == null )
                return false;

            Transform asset = objectInAsset.transform.root;

            GameObject assetInstance = null;
            if( !_batchedAssets.TryGetValue( asset.gameObject, out assetInstance ) )
                return false;

            return instance == assetInstance
                || instance.transform.IsChildOf( assetInstance.transform );
        }

        private static GameObject InstantiateAsset( GameObject asset )
        {
            GameObject instance = (GameObject) PrefabUtility.InstantiatePrefab( asset );
            instance.hideFlags = HideFlags.HideAndDontSave;

            if( _isInBatch )
                _batchedAssets[ asset ] = instance;

            return instance;
        }

        public PrefabOperation( Prefab target )
        {
            Target = target;
        }

        public virtual void Do()
        { }
    }
}