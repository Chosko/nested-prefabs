﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using VisualDesignCafe.Editor.Prefabs.Comparing;

namespace VisualDesignCafe.Editor.Prefabs
{
    using Object = UnityEngine.Object;

    public sealed class ApplyOperation : PrefabOperation
    {
        public static event Action<long> OnPrefabApplied;

        public readonly bool IncludeNestedChildren;
        public readonly Prefab[] Prefabs;

        /// <summary>
        /// Creates a new Apply Operation for the prefab.
        /// </summary>
        public ApplyOperation( Prefab prefab, bool includeNestedChildren )
            : base( null )
        {
            if( Prefab.IsNull( prefab ) )
                throw new ArgumentNullException( "Prefab" );

            IncludeNestedChildren = includeNestedChildren;
            Prefabs = new Prefab[] { prefab };
        }

        /// <summary>
        /// Creates a new Apply Operation for the prefabs.
        /// </summary>
        public ApplyOperation( Prefab[] prefabs, bool includeNestedChildren )
            : base( null )
        {
            if( prefabs == null || prefabs.Length == 0 )
                throw new ArgumentNullException( "Prefabs" );

            if( prefabs.Any( p => Prefab.IsNull( p ) ) )
                throw new ArgumentNullException( "Prefab element" );

            if( prefabs.Any( p => p.AssetGuid != prefabs[ 0 ].AssetGuid ) )
                throw new ArgumentException( "Mixed Prefabs list" );

            Prefabs = prefabs;
            IncludeNestedChildren = includeNestedChildren;
        }

        /// <summary>
        /// Applies the changes to the prefabs.
        /// </summary>
        public override void Do()
        {
            if( Config.DEBUG_INFO )
                Console.Log( "Applying changes to\n"
                    + string.Join( "\n", Prefabs.Select( n => n.CachedGameObject.ToLog() ).ToArray() )
                    + "\n\nInclude children: " + IncludeNestedChildren );

            Console.Indent++;

            var stopwatch = new System.Diagnostics.Stopwatch();
            stopwatch.Start();

            Prefab temporaryPrefab = null;

            if( IncludeNestedChildren )
            {
                foreach( Prefab prefab in Prefabs )
                {
                    var groups =
                        GroupByAsset(
                            prefab.CachedGameObject.GetComponentsInChildren<Prefab>( true )
                                .Where(
                                    n => n.Asset != null
                                        && PrefabHierarchyUtility.IsDirectChildOf( n, prefab ) )
                                .ToArray() );

                    foreach( var group in groups )
                    {
                        Prefab[] prefabList = group.Value.ToArray();

                        if( prefabList.Length > 0 )
                            new ApplyOperation( prefabList, true ).Do();
                    }
                }
            }

            foreach( Prefab prefab in Prefabs )
                temporaryPrefab = PreprocessPrefab( prefab );

            if( temporaryPrefab == null )
            {
                if( Config.DEBUG_WARNING )
                    Console.LogWarning( "Failed to preprocess prefab. Skipping Apply." );

                Console.Indent--;
                return;
            }

            var sourcePrefabs =
                Prefabs
                    .Select( n => n.CachedGameObject )
                    .Where( go => go != temporaryPrefab.CachedGameObject )
                    .ToArray();

            if( sourcePrefabs.Length > 0 )
            {
                var operation =
                    new ReloadOperation(
                        temporaryPrefab,
                        sourcePrefabs,
                        false,
                        true,
                        false,
                        false );

                operation.Do();
            }

            foreach( Prefab prefab in Prefabs )
                PostprocessPrefab( temporaryPrefab, prefab );

            foreach( Prefab prefab in Prefabs )
                RemovePrefabOverride( prefab.CachedGameObject, prefab.CachedGameObject );

            if( Config.DEBUG_INFO )
                Console.Log( "Result: \n" + temporaryPrefab.CachedGameObject.PrintHierarchy() );

            stopwatch.Stop();

            if( OnPrefabApplied != null )
                OnPrefabApplied.Invoke( stopwatch.ElapsedMilliseconds );

            Console.Indent--;
        }

        /// <summary>
        /// Groups the list of prefabs by asset.
        /// </summary>
        private Dictionary<GameObject, List<Prefab>> GroupByAsset( Prefab[] prefabs )
        {
            var groups = new Dictionary<GameObject, List<Prefab>>();

            foreach( Prefab prefab in prefabs )
            {
                if( prefab.Asset == null )
                    continue;

                if( !groups.ContainsKey( prefab.Asset ) )
                    groups[ prefab.Asset ] = new List<Prefab>();

                groups[ prefab.Asset ].Add( prefab );
            }

            return groups;
        }

        /// <summary>
        /// Preprocesses the (Nested) Prefab before applying changes
        /// </summary>
        private Prefab PreprocessPrefab( Prefab prefab )
        {
            GameObject temporaryInstance =
                GetInstanceForAsset( prefab.Asset, true );

            var temporaryPrefab = temporaryInstance.GetComponent<Prefab>();
            if( temporaryPrefab == null )
            {
                if( Config.STRICT )
                    throw new NullReferenceException(
                        "Asset Instance does not have a prefab component" );
                else
                    Debug.LogErrorFormat(
                        "Asset Instance does not have a prefab component. Please try reimporting the asset. ({0})",
                        AssetDatabase.GetAssetPath( prefab.Asset ) );
            }

            return temporaryPrefab;
        }

        /// <summary>
        /// Postprocesses the Nested Prefab after applying changes
        /// </summary>
        private void PostprocessPrefab( Prefab temporaryPrefab, Prefab prefab )
        {
            // Destroy the temporary Nested Prefab component, we don't want to accidentally save it.
            NestedPrefab nestedPrefab = temporaryPrefab.CachedGameObject.GetComponent<NestedPrefab>();
            if( nestedPrefab != null )
                Component.DestroyImmediate( nestedPrefab );

            ReferenceCollectionUtility.UpdateRecursive( prefab.ReferenceCollection );
        }

        /// <summary>
        /// Removes the (unity) prefab overrides from the serialized properties.
        /// </summary>
        private void RemovePrefabOverride( GameObject gameObject, GameObject root )
        {
            // Remove the prefab override from the internal GameObject properties (name, isActive, layer, etc.)
            RemovePrefabOverridesForObject( gameObject, root );

            // Remove overrides for each component.
            foreach( var component in gameObject.GetComponents<Component>() )
            {
                if( component == null )
                    continue;

                // Ignore components that are saved in the parent.
                if( gameObject == root
                    && ( component is Transform
                        || component is RectTransform
                        || component.GetType().GetCustomAttributes(
                            typeof( SaveInParent ), true ).Length > 0 ) )
                    continue;

                RemovePrefabOverridesForObject( component, root );
            }

            // Remove overrides for all children recursively.
            foreach( Transform child in gameObject.transform )
            {
                if( child == null )
                    continue;

                if( child.GetComponent<Prefab>() != null )
                {
                    // Remove overrides for components that are saved in the parent.
                    RectTransform rectTransform = child.GetComponent<RectTransform>();
                    if( rectTransform != null )
                        RemovePrefabOverridesForObject( rectTransform, root );
                    else
                        RemovePrefabOverridesForObject( child.GetComponent<Transform>(), root );

                    Component[] childComponents = child.GetComponents<Component>();
                    foreach( Component childComponent in childComponents )
                    {
                        if( childComponent == null )
                            continue;

                        if( childComponent.GetType().GetCustomAttributes(
                                typeof( SaveInParent ), true ).Length == 0 )
                        {
                            continue;
                        }

                        RemovePrefabOverridesForObject( childComponent, root );
                    }

                    // Ignore the rest of the nested prefab.
                    continue;
                }

                RemovePrefabOverride( child.gameObject, root );
            }
        }

        /// <summary>
        /// Removes the (unity) prefab overrides from the serialized properties.
        /// </summary>
        private void RemovePrefabOverridesForObject( Object obj, GameObject root )
        {
            var serialized = new SerializedObject( obj );
            GameObject gameObject = obj as GameObject;

            SerializedProperty property = serialized.GetIterator();
            while( property.Next( true ) )
            {
                if( PrefabComparer.DefaultIgnoreList.IsIgnored(
                        property.propertyPath, obj ) )
                {
                    continue;
                }

                // Skip properties that are saved in the parent.
                if( gameObject != null
                    && gameObject == root
                    && ( property.propertyPath == "m_Name" || property.propertyPath == "m_IsActive" ) )
                {
                    continue;
                }

                if( property.propertyType == SerializedPropertyType.ObjectReference
                    && IsSceneObject( property.objectReferenceValue, root ) )
                {
                    continue;
                }

                property.prefabOverride = false;
            }

            serialized.SaveModifications();
        }

        private bool IsSceneObject( Object obj, GameObject root )
        {
            GameObject gameObject = obj.AsGameObject();

            if( gameObject == null )
                return false;

            if( gameObject.transform.IsChildOf( root.transform ) )
                return false;

            if( EditorUtility.IsPersistent( gameObject ) )
                return false;

            return true;
        }
    }
}
