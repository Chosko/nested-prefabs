﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace VisualDesignCafe.Editor.Prefabs
{
    public sealed class ReloadAssetOperation : PrefabOperation
    {
        public static event Action<long> OnApplied;

        private const int REFERENCE_LIMIT = 1024;

        private static bool _allowRefresh = true;

        public readonly GameObject Asset;
        public readonly PrefabDatabase PrefabDatabase;
        private GameObject _switchedInstance;

        public ReloadAssetOperation(
            GameObject asset, PrefabDatabase nestedPrefabCollection )
                : base( null )
        {
            if( asset == null )
                throw new ArgumentNullException( "Asset" );

            if( !EditorUtility.IsPersistent( asset ) )
                throw new ArgumentException( "Prefab is not an asset" );

            if( asset.transform.parent != null )
                throw new ArgumentException( "GameObject is not a prefab root" );

            Asset = asset;
            PrefabDatabase = nestedPrefabCollection;
        }

        public override void Do()
        {
            if( Config.DEBUG_INFO )
                Console.Log( "Applying changes to " + Asset.ToLog() );

            if( !_allowRefresh )
                return;

            _allowRefresh = false;

            int totalAssetCount = 0;
            int referenceCount = 0;
            var stopwatch = new System.Diagnostics.Stopwatch();
            stopwatch.Start();

            try
            {
                // Create a list of modified prefabs so we can keep track of which prefabs have been modified
                // We should reload all nested prefabs for these modified prefabs.
                var modifiedAssets = new List<GameObject>();
                modifiedAssets.Add( Asset );
                totalAssetCount++;

                int depth = 0;
                while( modifiedAssets.Count > 0 )
                {
                    if( Config.DEBUG_INFO )
                        Console.LogFormat(
                            "Reloading Nested Prefabs for {0}",
                            modifiedAssets[ 0 ].ToLog() );

                    Console.Indent++;

                    // Find all prefab assets in the project that contain a direct reference to the modified prefab
                    // All nested prefabs with a depth larger than 1 are excluded.
                    GameObject[] references =
                        AssetDatabaseUtility.FindAssetsWithReferenceTo(
                            modifiedAssets[ 0 ],
                            PrefabDatabase,
                            false,
                            1,
                            false,
                            true );

                    if( Config.DEBUG_INFO && references.Length == 0 )
                        Console.Log( "No references to asset" );

                    int index = 0;

                    foreach( GameObject asset in references )
                    {
                        if( asset == null )
                            continue;

                        index++;
                        referenceCount++;

                        if( Config.DEBUG_INFO )
                            Console.Log( "Prefab is referenced from " + asset.ToLog() );

                        GameObject assetInstance =
                            EditorUtility.IsPersistent( asset )
                                ? GetInstanceForAsset( asset )
                                : asset;

                        ReloadNestedPrefabsRecursive(
                            assetInstance,
                            modifiedAssets[ 0 ] );

                        if( EditorUtility.IsPersistent( asset ) )
                        {
                            SaveAssetInstance( assetInstance, asset );

                            // Add the asset to the list of modified assets
                            // so any prefabs that references to this asset will be updated as well.
                            if( !modifiedAssets.Contains( asset ) )
                            {
                                modifiedAssets.Add( asset );
                                totalAssetCount++;
                            }
                        }
                    }

                    // Update modified prefab list and continue with the next prefab
                    modifiedAssets.RemoveAt( 0 );

                    Console.Indent--;

                    // Make sure we are not stuck in an endless loop because of a circular reference
                    depth++;
                    if( depth > REFERENCE_LIMIT )
                    {
                        Console.LogErrorFormat(
                            "Nested Prefab reference limit reached ({0})",
                            REFERENCE_LIMIT );

                        break;
                    }
                }
            }
            catch( Exception e )
            {
                if( Config.RETHROW )
                    throw e;
                else if( Config.DEBUG_EXCEPTION )
                {
                    Debug.LogException( e );
                }
            }
            finally
            {
                _allowRefresh = true;

                stopwatch.Stop();
                if( referenceCount > 0 )
                {
                    if( OnApplied != null )
                        OnApplied.Invoke( stopwatch.ElapsedMilliseconds );
                }
            }
        }

        private void ReloadNestedPrefabsRecursive(
            GameObject gameObject, GameObject targetAsset )
        {
            NestedPrefab[] nestedPrefabs = gameObject.GetComponentsInChildren<NestedPrefab>( true );
            foreach( NestedPrefab nestedPrefab in nestedPrefabs )
            {
                if( nestedPrefab == null || nestedPrefab.Asset == null )
                    continue;

                if( nestedPrefab.Asset != targetAsset )
                    continue;

                GameObject source = null;
                BatchedAssets.TryGetValue( nestedPrefab.Asset, out source );
                source = source ?? nestedPrefab.Asset;

                if( nestedPrefab.gameObject == source )
                    continue;

                var operation =
                    new ReloadOperation(
                        nestedPrefab.Prefab,
                        source,
                        false,
                        true,
                        false,
                        false );

                operation.Do();
            }
        }
    }
}
