﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace VisualDesignCafe.Editor.Prefabs
{
    public enum NestedPrefabType
    {
        None,
        Prefab,
        NestedPrefab,
    }

    public static class PrefabHierarchyUtility
    {
        /// <summary>
        /// Returns the topmost prefab in the hierarchy.
        /// </summary>
        public static GameObject FindPrefabRoot( GameObject gameObject )
        {
            if( EditorUtility.IsPersistent( gameObject ) )
            {
                return FindRoot( gameObject.transform ).gameObject;
            }
            else
            {
                return PrefabUtility.FindValidUploadPrefabInstanceRoot( gameObject )
                    ?? PrefabUtility.FindRootGameObjectWithSameParentPrefab( gameObject );
            }
        }

        /// <summary>
        /// Returns the prefab object that the gameobject is part of.
        /// </summary>
        public static Prefab GetPrefab( GameObject gameObject )
        {
            if( gameObject == null )
                throw new ArgumentNullException( "GameObject" );

            return GetPrefab( gameObject.transform );
        }

        /// <summary>
        /// Returns the parent prefab of the given prefab.
        /// </summary>
        public static Prefab GetParentPrefab( Prefab prefab )
        {
            return GetPrefab( prefab.transform.parent );
        }

        /// <summary>
        /// Returns the prefab object that the transform is part of.
        /// </summary>
        public static Prefab GetPrefab( Transform transform )
        {
            while( transform != null )
            {
                var prefab = transform.GetComponent<Prefab>();
                if( prefab != null )
                    return prefab;

                transform = transform.parent;
            }

            return null;
        }

        /// <summary>
        /// Returns the parent of the given prefab.
        /// </summary>
        /// <param name="ignoreModelPrefabs">Should model prefabs be ignored?</param>
        public static Prefab GetParentPrefab( Prefab prefab, bool ignoreModelPrefabs )
        {
            Transform transform = prefab.transform.parent;

            while( transform != null )
            {
                var parentPrefab = transform.GetComponent<Prefab>();
                if( parentPrefab != null )
                {
                    if( !ignoreModelPrefabs || !NestedPrefabUtility.IsModelPrefab( parentPrefab ) )
                        return parentPrefab;
                }

                transform = transform.parent;
            }

            return null;
        }

        /// <summary>
        /// Finds the topmost transform in the hierarchy.
        /// </summary>
        public static Transform FindRoot( Transform transform )
        {
            Transform root = transform;
            while( root.parent != null )
            {
                root = root.parent;
            }

            return root;
        }

        /// <summary>
        /// Returns the number of nested prefab parents of the given prefab.
        /// </summary>
        public static int GetDepth(
            NestedPrefab prefab, bool ignorePrefabModels = false )
        {
            if( prefab == null )
                return -1;

            int depth = 1;
            Transform parent = prefab.CachedTransform.parent;
            while( parent != null )
            {
                var nestedPrefab = parent.gameObject.GetComponent<NestedPrefab>();
                if( nestedPrefab != null )
                {

                    bool isModelPrefab =
                        NestedPrefabUtility.IsModelPrefab( nestedPrefab.Prefab );

                    if( !ignorePrefabModels || !isModelPrefab )
                    {
                        if( FindPrefabRoot( nestedPrefab.CachedGameObject )
                            != nestedPrefab.CachedGameObject )
                        {
                            depth++;
                        }
                        else
                        {
                            break;
                        }
                    }
                }

                parent = parent.parent;
            }

            return depth;
        }

        /// <summary>
        /// Returns the number of nested prefab parents of the given prefab.
        /// </summary>
        public static int GetDepth( Prefab prefab, bool includeNonNested )
        {
            if( prefab == null )
                return -1;

            int depth = 1;
            Transform parent = prefab.CachedTransform.parent;
            while( parent != null )
            {
                if( includeNonNested )
                {
                    if( parent.gameObject.GetComponent<Prefab>() != null )
                        depth++;
                }
                else
                {
                    if( parent.gameObject.GetComponent<NestedPrefab>() != null )
                        depth++;
                }

                parent = parent.parent;
            }

            return depth;
        }


        /// <summary>
        /// Returns the total number of parents of the given transform.
        /// </summary>
        internal static int GetDepth( Transform transform )
        {
            return GetDepth( transform, null );
        }

        /// <summary>
        /// Returns the total number of parents until the root transform.
        /// </summary>
        public static int GetDepth( Transform transform, Transform root )
        {
            int depth = 0;
            while( transform != root && transform != null )
            {
                depth++;
                transform = transform.parent;
            }

            return depth;
        }

        /// <summary>
        /// Returns the parent prefab for the given GameObject. 
        /// </summary>
        public static NestedPrefabType FindValidParentPrefab(
            GameObject gameObject, out Transform parentTransform )
        {
            Prefab prefab = GetPrefab( gameObject );

            if( prefab == null || prefab.transform.parent == null )
            {
                parentTransform = null;
                return NestedPrefabType.None;
            }

            return FindValidPrefab( prefab.transform.parent, out parentTransform );
        }

        /// <summary>
        /// Returns the parent prefab for the given GameObject.
        /// </summary>
        public static Prefab FindValidParentPrefab( GameObject gameObject )
        {
            Transform parent = null;
            NestedPrefabType type = FindValidParentPrefab( gameObject, out parent );

            if( type != NestedPrefabType.None )
                return parent.GetComponent<Prefab>();

            return null;
        }

        /// <summary>
        /// Finds the parent prefab for the given transform. 
        /// Either the transform of a NestedPrefab or a prefab will be returned, whatever is found first.
        /// </summary>
        public static NestedPrefabType FindValidPrefab(
            Transform transform, out Transform prefabTransform )
        {
            if( transform == null )
            {
                prefabTransform = null;
                return NestedPrefabType.None;
            }

            Transform parent = transform;
            while( parent != null )
            {
                if( parent.GetComponent<Prefab>() != null )
                {
                    prefabTransform = parent;
                    return FindPrefabRoot( parent.gameObject ) == parent.gameObject
                        ? NestedPrefabType.Prefab
                        : NestedPrefabType.NestedPrefab;
                }

                parent = parent.parent;
            }

            parent = transform;
            while( parent != null )
            {
                PrefabType type = PrefabUtility.GetPrefabType( parent.gameObject );
                if( type != PrefabType.None )
                {
                    prefabTransform = FindPrefabRoot( parent.gameObject ).transform;
                    return NestedPrefabType.Prefab;
                }

                parent = parent.parent;
            }

            prefabTransform = null;
            return NestedPrefabType.None;
        }

        /// <summary>
        /// Checks if the nested prefab has a circular reference in its hierarchy.
        /// </summary>
        public static bool HasCircularReference( Prefab prefab )
        {
            if( Prefab.IsNull( prefab ) )
                throw new ArgumentNullException();

            GameObject asset = prefab.Asset;

            if( asset == null )
                return false;

            Transform parent;
            NestedPrefabType parentType;

            parentType = FindValidParentPrefab( prefab.CachedGameObject, out parent );
            while( parentType != NestedPrefabType.None )
            {
                if( parentType == NestedPrefabType.NestedPrefab )
                {
                    if( parent.GetComponent<Prefab>().Asset == asset )
                        return true;
                }
                else if( parentType == NestedPrefabType.Prefab )
                {
                    if( PrefabUtility.GetPrefabParent( parent.gameObject ) == asset )
                        return true;
                }

                parentType = FindValidParentPrefab( parent.gameObject, out parent );
            }

            foreach( Transform child in prefab.CachedTransform )
            {
                if( HasCircularReference( child, asset ) )
                    return true;
            }

            return false;
        }

        /// <summary>
        /// Finds the GameObject that is a child of 'root' in the given 'other root' object.
        /// Uses the sibling index of the GameObject (and its parents) to find the object.
        /// </summary>
        public static GameObject FindObjectBySiblingIndex(
            GameObject gameObject, GameObject root, GameObject otherRoot )
        {
            return FindSiblingEndNode(
                otherRoot,
                GetSiblingIndex( gameObject, root ) );
        }

        private static int[] GetSiblingIndex( GameObject gameObject, GameObject root )
        {
            List<int> path = new List<int>();

            Transform parent = gameObject.transform;
            while( parent != null && parent.gameObject != root )
            {
                path.Add( parent.GetSiblingIndex() );
                parent = parent.parent;
            }

            path.Reverse();

            return path.ToArray();
        }

        private static GameObject FindSiblingEndNode( GameObject root, int[] path )
        {
            Transform endNode = root.transform;
            for( int i = 0; i < path.Length; i++ )
            {
                if( endNode.childCount <= path[ i ] )
                    return null;

                endNode = endNode.GetChild( path[ i ] );
            }

            return endNode.gameObject;
        }

        /// <summary>
        /// Gets an array containing the prefab hierarchy of the given transform.
        /// </summary>
        internal static GameObject[] GetPrefabhHierarchy(
            Transform transform,
            Transform root = null,
            bool selectAssets = false,
            bool includeRoot = true )
        {
            var hierarchy = new List<GameObject>();
            Transform parent = transform.parent;
            while( parent != null
                && ( parent != root
                    || ( includeRoot && ( root == null || parent != root.parent ) ) ) )
            {
                Prefab prefab = parent.gameObject.GetComponent<Prefab>();
                if( prefab != null && prefab.Asset != null )
                {
                    if( selectAssets )
                        hierarchy.Insert( 0, prefab.Asset );
                    else
                        hierarchy.Insert( 0, prefab.CachedGameObject );
                }
                else if( FindPrefabRoot( parent.gameObject ) == parent.gameObject )
                {
                    if( selectAssets )
                        hierarchy.Insert( 0, (GameObject) PrefabUtility.GetPrefabParent( parent.gameObject ) );
                    else
                        hierarchy.Insert( 0, parent.gameObject );
                }

                if( parent == root )
                    break;

                parent = parent.parent;
            }

            return hierarchy.ToArray();
        }

        /// <summary>
        /// Returns a string containing the path from the root to the GameObject.
        /// </summary>
        public static string GetHierarchyPath(
            GameObject gameObject, Transform root, bool includeRoot )
        {
            string path = string.Empty;
            Transform parent = gameObject.transform;

            if( parent == root && !includeRoot )
                return string.Empty;

            while( parent != null )
            {
                if( !includeRoot && parent == root )
                    break;

                path = parent.name + "/" + path;

                if( parent == root )
                    break;

                parent = parent.parent;
            }

            if( string.IsNullOrEmpty( path ) )
                return string.Empty;

            return path.Remove( path.Length - 1 );
        }

        /// <summary>
        /// Is the prefab a direct child of the parent?
        /// Or are there any other nested prefabs in between?
        /// </summary>
        public static bool IsDirectChildOf(
            Prefab prefab, Prefab parent, bool ignoreModelPrefabs = false )
        {
            return IsDirectChildOf(
                prefab.CachedTransform.parent, parent, ignoreModelPrefabs );
        }

        private static bool IsDirectChildOf(
            Transform transform, Prefab parent, bool ignoreModelPrefabs = false )
        {
            if( transform == null )
                return false;

            var nestedPrefab = transform.GetComponent<Prefab>();
            if( nestedPrefab != null )
            {
                if( !ignoreModelPrefabs || !NestedPrefabUtility.IsModelPrefab( nestedPrefab ) )
                    return nestedPrefab == parent;
            }

            return IsDirectChildOf( transform.parent, parent );
        }


        /// <summary>
        /// Checks if the transform has a circular reference to the asset in its hierarchy.
        /// </summary>
        private static bool HasCircularReference( Transform transform, GameObject asset )
        {
            Prefab nestedPrefab = transform.GetComponent<Prefab>();
            if( nestedPrefab != null )
            {
                return HasCircularReference( nestedPrefab );
            }

            foreach( Transform child in transform )
            {
                if( HasCircularReference( child, asset ) )
                    return true;
            }

            return false;
        }

        /// <summary>
        /// Finds a nested prefab in the hierarchy of the root transform with the given GUID.
        /// </summary>
        internal static GameObject FindNestedPrefabWithGuid( Transform root, Guid guid )
        {
            Guid[] guids = root.GetComponentsInChildren<Guid>( true );
            foreach( Guid g in guids )
            {
                if( g.GetComponent<NestedPrefab>() == null )
                    continue;

                if( g == guid )
                    return g.gameObject;
            }

            return null;
        }
    }
}