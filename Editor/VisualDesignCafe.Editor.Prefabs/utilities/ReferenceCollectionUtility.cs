﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using VisualDesignCafe.Editor.Prefabs.Comparing;

namespace VisualDesignCafe.Editor.Prefabs
{

    internal static class ReferenceCollectionUtility
    {

        /// <summary>
        /// Validates the reference collection by removing any empty entries 
        /// and ensuring that all references in the collection are relative.
        /// </summary>
        public static void Validate( ReferenceCollection collection )
        {
            if( collection == null || collection.Prefab == null )
                throw new ArgumentNullException();

            Prefab rootPrefab =
                PrefabHierarchyUtility.FindPrefabRoot( collection.Prefab.gameObject )
                    .GetComponent<Prefab>();

            Prefab prefab = collection.Prefab;

            collection.Validate(
                ( GameObject gameObject ) =>
                {
                    var reference = new ObjectReference( rootPrefab, prefab, gameObject );

                    switch( reference.Type )
                    {
                        case ObjectReferenceType.Asset:

                            GameObject relativeReference =
                                ObjectReference.FindRelative(
                                    gameObject,
                                    prefab.CachedTransform ) as GameObject;

                            if( Config.DEBUG_WARNING )
                                Console.LogWarning( "Reference Collection entry points to asset. Fixing... (" + relativeReference + ")" );

                            if( relativeReference != null )
                                return relativeReference;

                            break;
                        case ObjectReferenceType.Invalid:
                        case ObjectReferenceType.Null:
                        case ObjectReferenceType.ObjectAsset:
                        case ObjectReferenceType.ObjectInstance:
                        case ObjectReferenceType.SceneObject:

                            if( Config.DEBUG_WARNING )
                                Console.LogWarning( "Reference Collection entry is invalid." );

                            return null;
                    }

                    return gameObject;
                } );
        }

        /// <summary>
        /// Gets the cached result of the last GetIsValid check.
        /// Updates the result if it is outdated.
        /// </summary>
        public static bool IsValidCached(
            ReferenceCollection collection, int id )
        {
            if( collection == null )
                throw new ArgumentNullException();

            if( collection.CacheId < id )
            {
                collection.CacheId = id;
                IsValid( collection );
            }

            return collection.IsValidCached;
        }

        /// <summary>
        /// Checks if the reference collection is valid.
        /// </summary>
        public static bool IsValid( ReferenceCollection collection )
        {
            if( collection == null || collection.Prefab == null )
                throw new ArgumentNullException();

            collection.IsValidCached =
                IsValid(
                    collection.Prefab.CachedTransform,
                    collection.Prefab.transform,
                    collection );

            return collection.IsValidCached;
        }

        /// <summary>
        /// Checks if the transform is a valid entry in the reference collection.
        /// </summary>
        private static bool IsValid(
            Transform root, Transform transform, ReferenceCollection collection )
        {
            // Check if the transform is part of a prefab. 
            // If it is not then we can skip this object and return true.
            PrefabType prefabType = PrefabUtility.GetPrefabType( transform );

            if( prefabType == PrefabType.None
                 || prefabType == PrefabType.DisconnectedPrefabInstance
                 || prefabType == PrefabType.DisconnectedModelPrefabInstance
                 || prefabType == PrefabType.ModelPrefab
                 || prefabType == PrefabType.ModelPrefabInstance )
            {
                return true;
            }

            // Check if the nested prefab has a valid prefab GUID 
            bool isNestedPrefab = false;
            if( transform != root )
            {
                var nestedPrefab = transform.GetComponent<Prefab>();
                if( nestedPrefab != null )
                {
                    if( PrefabHierarchyUtility.FindPrefabRoot( nestedPrefab.CachedGameObject )
                        != nestedPrefab.CachedGameObject )
                    {
                        if( string.IsNullOrEmpty( nestedPrefab.PrefabGuid ) )
                            return false;
                    }

                    return true;
                }
            }

            // Check if the transform has a GUID and is stored in the reference collection.
            var guid = transform.GetComponent<Guid>();
            if( guid != null )
            {
                if( !collection.Contains( transform.gameObject ) )
                    return false;
            }
            else
            {
                return false;
            }

            // Skip children of nested prefab.
            if( isNestedPrefab )
                return true;

            // Check all children.
            foreach( Transform child in transform )
            {
                if( !IsValid( root, child, collection ) )
                    return false;
            }

            return true;
        }

        /// <summary>
        /// Updates the reference collection. 
        /// Makes sure there are no prefab overrides in the collection.
        /// When a child of a prefab is deleted in Unity and applied, 
        /// the array element that used to contain a reference to that child will be marked as overridden.
        /// </summary>
        public static void Update( ReferenceCollection collection )
        {
            if( collection.Prefab == null )
                return;

            if( Config.DEBUG_INFO && Config.VERBOSE )
                Console.Log(
                    "Updating reference collection for "
                    + collection.Prefab.gameObject.ToLog() );

            var serializedProperties = new List<SerializedProperty>();
            var serialized = new SerializedObject( collection.Prefab );

            SerializedProperty serializedReferenceCollection =
                serialized.FindProperty( "_referenceCollection" );
            SerializedProperty serializedCollection =
                serializedReferenceCollection.FindPropertyRelative( "_collection" );

            serializedProperties.Add( serializedReferenceCollection );
            serializedProperties.Add( serializedCollection );
            serializedProperties.Add(
                serializedReferenceCollection
                    .FindPropertyRelative( "_collection.Array" ) );
            serializedProperties.Add(
                serializedReferenceCollection
                    .FindPropertyRelative( "_collection.Array.size" ) );

            for( int i = 0; i < serializedCollection.arraySize; i++ )
            {
                serializedProperties.Add(
                    serializedCollection.GetArrayElementAtIndex( i ) );
            }

            for( int i = serializedProperties.Count - 1; i > -1; i-- )
            {
                if( serializedProperties[ i ].prefabOverride )
                {
                    serializedProperties[ i ].prefabOverride = false;

                    if( Config.DEBUG_WARNING )
                        Console.LogWarning(
                            "Reference Collection entry was marked as prefab override. ("
                            + serializedProperties[ i ].propertyPath + ")" );
                }
            }

            serialized.SaveModifications();
        }

        /// <summary>
        /// Updates the reference collection and all reference collections 
        /// of nested prefabs in the prefab's hierarchy.
        /// </summary>
        public static void UpdateRecursive( ReferenceCollection collection )
        {
            if( collection.Prefab == null )
                return;

            Update( collection );

            Prefab[] prefabs = collection.Prefab.GetComponentsInChildren<Prefab>( true );
            foreach( Prefab prefab in prefabs )
            {
                if( prefab == null )
                    continue;

                if( prefab == collection.Prefab )
                    continue;

                Update( prefab.ReferenceCollection );
            }
        }
    }
}