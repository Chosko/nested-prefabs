﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;
using VisualDesignCafe.Editor.Prefabs.Comparing;
using VisualDesignCafe.Editor.Prefabs.Overrides;

namespace VisualDesignCafe.Editor.Prefabs
{
    using Object = UnityEngine.Object;

    public static class NestedPrefabUtility
    {

        /// <summary>
        /// Gets called for each modification found in a prefab.
        /// </summary>
        internal static Action<Modification> ModificationCallback;

        private static MethodInfo _connectGameObjectToPrefabMethod;

        /// <summary>
        /// Reverts the serialized property back to the value in the original prefab.
        /// </summary>
        public static bool RevertProperty( SerializedProperty property )
        {
            if( property == null )
                throw new ArgumentNullException( "Property" );

            GameObject gameObject =
                property.serializedObject.targetObject.AsGameObject();

            Prefab prefab = PrefabHierarchyUtility.GetPrefab( gameObject );

            if( prefab == null )
                throw new InvalidOperationException( "Property is not part of a prefab" );

            Guid guid = gameObject.GetComponent<Guid>();

            Type componentType =
                property.serializedObject.targetObject.GetType();

            GameObject gameObjectInAsset =
                prefab
                    .Asset
                    .GetComponent<Prefab>()
                    .ReferenceCollection
                    .FindObjectWithGuid( guid );

            if( gameObjectInAsset == null )
                return false;

            Component componentInAsset =
                gameObjectInAsset.GetComponent( componentType );

            if( componentInAsset == null )
                return false;

            var serializedComponent = new SerializedObject( componentInAsset );

            property.serializedObject.CopyFromSerializedProperty(
                serializedComponent.FindProperty( property.propertyPath ) );

            property.serializedObject.ApplyModifiedProperties();
            return true;
        }

        /// <summary>
        /// Is the GameObject added to a model prefab?
        /// </summary>
        public static bool IsModelModification( GameObject gameObject )
        {
            Transform parent = gameObject.transform;

            while( parent != null )
            {
                if( parent.GetComponent<ModelModification>() != null )
                    return true;

                parent = parent.parent;
            }

            return false;
        }

        /// <summary>
        /// Is the prefab a model prefab?
        /// </summary>
        /// <param name="prefab"></param>
        /// <returns></returns>
        public static bool IsModelPrefab( Prefab prefab )
        {
            if( string.IsNullOrEmpty( prefab.AssetGuid ) )
                return false;

            string path = AssetDatabase.GUIDToAssetPath( prefab.AssetGuid );

            return IsModelPrefab( path );
        }

        /// <summary>
        /// Is the prefab a model prefab?
        /// </summary>
        public static bool IsModelPrefab( GameObject prefab )
        {
            if( !EditorUtility.IsPersistent( prefab ) )
                return false;

            string path = AssetDatabase.GetAssetPath( prefab );

            return IsModelPrefab( path );
        }

        /// <summary>
        /// Is the prefab a model prefab?
        /// </summary>
        public static bool IsModelPrefab( string path )
        {
            if( string.IsNullOrEmpty( path ) )
                return false;

            if( path.EndsWith( ".prefab", StringComparison.OrdinalIgnoreCase ) )
                return false;

            return true;
        }

        /// <summary>
        /// Is the component an internal component?
        /// </summary>
        internal static bool IsInternalComponent( Type componentType )
        {
            if( componentType == typeof( Prefab ) )
                return true;

            if( componentType == typeof( NestedPrefab ) )
                return true;

            if( componentType == typeof( ModelModification ) )
                return true;

            if( componentType == typeof( ModelComponentModification ) )
                return true;

            if( componentType == typeof( Guid ) )
                return true;

            return false;
        }

        /// <summary>
        /// Returns the nested prefab in the asset of the parent prefab.
        /// </summary>
        public static GameObject FindAlternativeAsset( Prefab prefab )
        {
            Prefab parentPrefab = PrefabHierarchyUtility.GetParentPrefab( prefab );
            if( !Prefab.IsNullOrMissing( parentPrefab ) )
            {
                return PrefabHierarchyUtility.FindNestedPrefabWithGuid(
                    parentPrefab.Asset.transform, prefab.ObjectGuid );
            }

            return null;
        }

        /// <summary>
        /// Checks if the prefab is connected to the asset. Will also check all parents of the prefab.
        /// </summary>
        public static bool IsConnectedToAsset( Prefab prefab, bool checkParents = true )
        {
            if( Prefab.IsNull( prefab ) )
                throw new ArgumentNullException();

            if( Prefab.IsMissing( prefab ) )
                return false;

            if( checkParents )
            {
                Prefab parent = PrefabHierarchyUtility.FindValidParentPrefab( prefab.CachedGameObject );
                while( !Prefab.IsNull( parent ) )
                {
                    if( parent.Asset == null )
                        return false;

                    parent = PrefabHierarchyUtility.FindValidParentPrefab( parent.CachedGameObject );
                }
            }

            return true;
        }

        /// <summary>
        /// Safe wrapper for NestedPrefabUtility.ReplacePrefab.
        /// For internal use.
        /// </summary>
        internal static GameObject ReplacePrefab(
            GameObject go, Object targetPrefab, ReplacePrefabOptions options )
        {
            HideFlags storedHideFlags = go.hideFlags;
            HideFlags hideFlags = go.hideFlags;

            if( FlagsUtility.IsSet( hideFlags, HideFlags.DontSave ) )
                FlagsUtility.Unset( ref hideFlags, HideFlags.DontSave );

            go.hideFlags = hideFlags;

            GameObject asset =
                PrefabUtility.ReplacePrefab(
                    go,
                    targetPrefab,
                    ReplacePrefabOptions.ConnectToPrefab );

            go.hideFlags = storedHideFlags;

            return asset;
        }

        /// <summary>
        /// Unity 5.0 Compatibility method for PrefabUtility.ConnectGameObjectToPrefab.
        /// Connects the GameObject to the prefab in later versions of Unity. 
        /// Is ignored if PrefabUtility.ConnectGameObjectToPrefab is not supported by Unity.
        /// </summary>
        public static bool ConnectGameObjectToPrefab(
            GameObject go, GameObject sourcePrefab )
        {
            if( _connectGameObjectToPrefabMethod == null )
            {
                _connectGameObjectToPrefabMethod =
                    typeof( PrefabUtility ).GetMethod(
                        "ConnectGameObjectToPrefab",
                        BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public );
            }

            if( _connectGameObjectToPrefabMethod == null )
                return false;

            Vector3 storedPosition = go.transform.position;
            Quaternion storedRotation = go.transform.rotation;
            Vector3 storedScale = go.transform.lossyScale;

            go =
                _connectGameObjectToPrefabMethod.Invoke(
                    null,
                    new object[] { go, sourcePrefab } ) as GameObject;

            if( go == null )
                return false;

            go.transform.position = storedPosition;
            go.transform.rotation = storedRotation;
            go.transform.localScale = storedScale;

            return true;
        }

        /// <summary>
        /// Reloads the Nested Prefab from the original asset.
        /// </summary>
        public static void ReloadPrefab( Prefab prefab )
        {
            if( Prefab.IsNull( prefab ) )
                throw new ArgumentNullException( "Prefab" );

            ReloadPrefab( prefab, prefab.Asset );
        }

        /// <summary>
        /// Reloads the Nested Prefab from the given source.
        /// </summary>
        public static void ReloadPrefab(
            Prefab prefab,
            GameObject sourcePrefab,
            bool revertLocalChanges = false,
            bool includeNestedChildren = true,
            bool includeSceneObjects = false,
            bool revertModelModifications = false )
        {
            if( Prefab.IsNull( prefab ) )
                throw new ArgumentNullException( "Prefab" );

            if( sourcePrefab == null )
                throw new ArgumentNullException( "Source Prefab" );

            var operation =
                new ReloadOperation(
                    prefab,
                    sourcePrefab,
                    revertLocalChanges,
                    includeNestedChildren,
                    includeSceneObjects,
                    revertModelModifications );

            operation.Do();
        }

        /// <summary>
        /// Reloads the Nested Prefab from the given source.
        /// </summary>
        public static void ReloadPrefab(
            Prefab prefab,
            GameObject[] sourcePrefabs,
            bool revertLocalChanges = false,
            bool includeNestedChildren = true,
            bool includeSceneObjects = false,
            bool revertModelModifications = false )
        {
            if( Prefab.IsNull( prefab ) )
                throw new ArgumentNullException( "Prefab" );

            if( sourcePrefabs == null || sourcePrefabs.Length == 0 )
                throw new ArgumentNullException( "Source Prefabs" );

            var operation =
                new ReloadOperation(
                    prefab,
                    sourcePrefabs,
                    revertLocalChanges,
                    includeNestedChildren,
                    includeSceneObjects,
                    revertModelModifications );

            operation.Do();
        }

        /// <summary>
        /// Gets the modifications for the prefab instance. 
        /// </summary>
        public static ModificationList GetModificationsForInstance(
            Prefab prefab, bool includeNestedChildren )
        {
            if( Prefab.IsNull( prefab ) )
                throw new ArgumentNullException( "Prefab" );

            return ReloadOperation.FindModifications(
                prefab,
                prefab.Asset.GetComponent<Prefab>(),
                true,
                includeNestedChildren );
        }

        /// <summary>
        /// Checks if the Nested Prefab has any modifications.
        /// </summary>
        public static bool HasModifications(
            Prefab prefab,
            bool includeNestedChildren,
            bool includeSceneObjects,
            bool useAlternativeAssetForModel = true )
        {
            if( Prefab.IsNull( prefab ) )
                throw new ArgumentNullException( "Prefab" );

            if( useAlternativeAssetForModel && IsModelPrefab( prefab ) )
            {
                GameObject asset = FindAlternativeAsset( prefab );
                if( asset != null )
                    return HasModifications(
                        prefab.CachedGameObject,
                        asset,
                        includeNestedChildren,
                        includeSceneObjects,
                        null );
            }

            if( Prefab.IsMissing( prefab ) )
                return false;

            return HasModifications(
                prefab.CachedGameObject,
                prefab.Asset,
                includeNestedChildren,
                includeSceneObjects,
                null );
        }

        /// <summary>
        /// Does the prefab has modifications to its hierarchy?
        /// </summary>
        public static bool HasModifiedHierarchy(
            Prefab prefab,
            bool includeNestedChildren,
            bool includeSceneObjects,
            bool useAlternativeAssetForModel = true )
        {
            if( Prefab.IsNull( prefab ) )
                throw new ArgumentNullException( "Prefab" );

            if( useAlternativeAssetForModel && IsModelPrefab( prefab ) )
            {
                GameObject asset = FindAlternativeAsset( prefab );
                if( asset != null )
                    return HasModifiedHierarchy(
                        prefab.CachedGameObject,
                        asset,
                        includeNestedChildren,
                        includeSceneObjects );
            }

            return HasModifiedHierarchy(
                prefab.CachedGameObject,
                prefab.Asset,
                includeNestedChildren,
                includeSceneObjects );
        }

        /// <summary>
        /// Does the prefab have modifications to its hierarchy?
        /// </summary>
        public static bool HasModifiedHierarchy(
            GameObject prefabInstance,
            GameObject asset,
            bool includeNestedChildren,
            bool includeSceneObjects )
        {
            if( asset == null )
                return false;

            bool hasModifiedHierarchy = false;

            Prefab prefabInstanceComponent = prefabInstance.GetComponent<Prefab>();
            Prefab prefabAssetComponent = asset.GetComponent<Prefab>();

            PrefabComparer comparer =
                new PrefabComparer(
                    prefabInstanceComponent,
                    prefabAssetComponent,
                    PrefabComparer.DefaultIgnoreList,
                    includeNestedChildren );

            comparer.Compare(
                includeSceneObjects
                    ? ( PrefabComparison.AsInstance | PrefabComparison.IncludeSceneReferences )
                    : PrefabComparison.AsInstance,
                m =>
                {
                    if( m.Type == ModificationType.PropertyAdded
                        || m.Type == ModificationType.PropertyRemoved )
                    {
                        return true;
                    }

                    bool isModifiedHierarchy = ( m.Type == ModificationType.ChildAdded
                        || m.Type == ModificationType.ChildRemoved
                        || m.Type == ModificationType.ComponentAdded
                        || m.Type == ModificationType.ComponentRemoved
                        || ( m.Type == ModificationType.PropertyValueChanged && m.CachedSourceProperty.PropertyPath == "m_RootOrder" )
                        || ( m.Type == ModificationType.PropertyValueChanged && m.CachedSourceProperty.PropertyPath == "m_Father" )
                        || ( m.Type == ModificationType.PropertyValueChanged && m.CachedSourceProperty.PropertyPath == "m_ComponentIndex" ) );

                    if( !isModifiedHierarchy )
                        return true;

                    if( ModificationIsOverride( m ) )
                        return true;

                    hasModifiedHierarchy = true;
                    return false;
                } );

            return hasModifiedHierarchy;
        }

        /// <summary>
        /// Checks if the Nested Prefab has any of the given modifications.
        /// </summary>
        public static bool HasModifications(
            Prefab prefab,
            bool includeNestedChildren,
            bool includeSceneObjects,
            bool useAlternativeAssetForModel = true,
            params ModificationType[] modifications )
        {
            if( Prefab.IsNull( prefab ) )
                throw new ArgumentNullException( "Prefab" );

            if( useAlternativeAssetForModel && IsModelPrefab( prefab ) )
            {
                GameObject asset = FindAlternativeAsset( prefab );
                if( asset != null )
                    return HasModifications(
                        prefab.CachedGameObject,
                        asset,
                        includeNestedChildren,
                        includeSceneObjects,
                        modifications );
            }

            return HasModifications(
                prefab.CachedGameObject,
                prefab.Asset,
                includeNestedChildren,
                includeSceneObjects,
                modifications );
        }

        /// <summary>
        /// Checks if the GameObject instance has any of the given modifications, compared to the given asset.
        /// </summary>
        internal static bool HasModifications(
            GameObject prefabInstance,
            GameObject asset,
            bool includeNestedChildren,
            bool includeSceneObjects,
            params ModificationType[] modifications )
        {
            Trace.AssertArgumentNotNull( asset, "Asset" );
            Trace.AssertArgumentNotNull( prefabInstance, "Prefab Instance" );

            bool hasModifiedHierarchy = false;

            Prefab prefabInstanceComponent = prefabInstance.GetComponent<Prefab>();
            Prefab prefabAssetComponent = asset.GetComponent<Prefab>();

            if( prefabInstanceComponent == null )
                throw new InvalidOperationException( "Instance is not a prefab." );

            if( prefabAssetComponent == null )
            {
                if( Config.STRICT )
                    throw new InvalidOperationException( "Asset is not a prefab." );
                else
                    Debug.LogErrorFormat(
                        "Asset is not a prefab. Please reimport the asset if this message keeps showing. ({0}",
                        AssetDatabase.GetAssetPath( asset )
                        );
            }

            PrefabComparer comparer =
                new PrefabComparer(
                    prefabInstanceComponent,
                    prefabAssetComponent,
                    PrefabComparer.DefaultIgnoreList,
                    includeNestedChildren );

            comparer.Compare(
                includeSceneObjects
                    ? ( PrefabComparison.AsInstance | PrefabComparison.IncludeSceneReferences )
                    : PrefabComparison.AsInstance,
                m =>
                {
                    if( m.Type == ModificationType.PropertyAdded || m.Type == ModificationType.PropertyRemoved )
                        return true;

                    if( modifications != null && !modifications.Contains( m.Type ) )
                        return true;

                    if( ModificationIsOverride( m ) )
                        return true;

                    hasModifiedHierarchy = true;
                    return false;
                } );

            return hasModifiedHierarchy;
        }

        /// <summary>
        /// Applies changes to the Nested Prefab without applying changes to its parent(s).
        /// </summary>
        public static void ApplyChanges(
            Prefab prefab, bool includeNestedChildren = true )
        {
            var operation = new ApplyOperation( prefab, includeNestedChildren );
            operation.Do();
        }

        /// <summary>
        /// Applies changes to the Nested Prefab without applying changes to its parent(s).
        /// </summary>
        public static void ApplyChanges(
            Prefab[] prefabs, bool includeNestedChildren = true )
        {
            var operation = new ApplyOperation( prefabs, includeNestedChildren );
            operation.Do();
        }

        /// <summary>
        /// Applies all changes made to the asset to the nested prefabs in the project
        /// with a reference to the asset.
        /// </summary>
        public static void ReloadInstancesForAsset(
            GameObject prefabAsset,
            PrefabDatabase nestedPrefabCollection = null,
            bool includeNestedChildren = true )
        {
            var operation = new ReloadAssetOperation( prefabAsset, nestedPrefabCollection );
            operation.Do();
        }

        /// <summary>
        /// Removes a prefab connection from the prefab, 
        /// its asset, and all other instances and nested prefabs.
        /// </summary>
        public static void RemovePrefabConnection(
            Prefab prefab, PrefabDatabase database )
        {
            var operation = new FlattenOperation( prefab, database );
            operation.Do();
        }

        /// <summary>
        /// Returns a string containing a description of the prefab type. 
        /// (Unknown, Asset, Asset Instance, Nested, Root)
        /// </summary>
        internal static string GetPrefabType( GameObject prefab )
        {
            if( prefab == null )
                return "Unknown";

            if( EditorUtility.IsPersistent( prefab ) )
                return "Asset";

            if( PrefabOperation.IsAssetInstance( prefab ) )
                return "Asset Instance";

            if( prefab.GetComponent<NestedPrefab>() != null && prefab.transform.parent != null )
                return "Nested";

            PrefabType type = PrefabUtility.GetPrefabType( prefab );
            if( type != PrefabType.None )
                return "Root";

            return "Unknown";
        }

        /// <summary>
        /// Fixes the reference collection of the nested prefab by copying it from the original asset, 
        /// or any Nested Prefab in the scene with a valid collection.
        /// </summary>
        internal static void FixPrefab( Prefab prefab )
        {
            if( ReferenceCollectionUtility.IsValid( prefab.ReferenceCollection ) )
                return;

            Prefab prefabAsset = prefab.Asset.GetComponent<Prefab>();

            if( prefabAsset == null )
                return;

            if( ReferenceCollectionUtility.IsValid( prefabAsset.ReferenceCollection ) )
            {
                ReferenceCollectionUtility.Update( prefab.ReferenceCollection );
            }
            else
            {
                Prefab[] prefabs = GameObject.FindObjectsOfType<Prefab>();
                foreach( Prefab p in prefabs )
                {
                    if( p.Asset != prefab.Asset )
                        continue;

                    if( !ReferenceCollectionUtility.IsValid( p.ReferenceCollection ) )
                        continue;

                    ReferenceCollectionUtility.Update( prefab.ReferenceCollection );

                    if( ReferenceCollectionUtility.IsValid( prefab.ReferenceCollection ) )
                        break;
                }
            }
        }

        /// <summary>
        /// Reverts all modified properties of the model back to the saved properties in the prefab.
        /// </summary>
        internal static void RevertModelToPrefab( Prefab prefab )
        {
            GameObject root =
                PrefabHierarchyUtility.FindPrefabRoot( prefab.CachedGameObject );
            GameObject rootAsset =
                PrefabUtility.GetPrefabParent( root ) as GameObject;
            GameObject modelPrefabInRoot =
                PrefabHierarchyUtility.FindNestedPrefabWithGuid(
                    rootAsset.transform,
                    prefab.ObjectGuid );

            PropertyModification[] modifications =
                PrefabUtility.GetPropertyModifications( root );

            var modificationList = new List<PropertyModification>();

            foreach( PropertyModification modification in modifications )
            {
                Object target = modification.target;

                Transform targetTransform = target.AsGameObject().transform;

                // Skip the object if it is not part of the model prefab.
                if( !targetTransform.IsChildOf( modelPrefabInRoot.transform )
                    && targetTransform != modelPrefabInRoot.transform )
                {
                    modificationList.Add( modification );
                    continue;
                }

                // Remove the modification from the modification list if the target is a model modification.
                if( ObjectIsAddedToModel( target ) )
                {
                    continue;
                }

                modificationList.Add( modification );
            }

            PrefabUtility.SetPropertyModifications( root, modificationList.ToArray() );
        }

        private static bool ObjectIsAddedToModel( Object target )
        {
            GameObject gameObject = target.AsGameObject();
            Component component = target as Component;

            var modelComponentModifications = gameObject.GetComponent<ModelComponentModification>();

            if( component != null && modelComponentModifications != null )
            {
                if( modelComponentModifications.Contains( component ) )
                    return true;
            }

            Transform transform = gameObject.transform;

            while( transform != null )
            {
                var modelModification = transform.GetComponent<ModelModification>();
                if( modelModification != null )
                    return true;

                transform = transform.parent;
            }

            return false;
        }

        /// <summary>
        /// Checks if the modification is a permanent prefab override.
        /// </summary>
        /// <returns>True if the modification is a permanent prefab override.</returns>
        private static bool ModificationIsOverride( Modification modification )
        {
            if( modification.Type != ModificationType.PropertyValueChanged )
                return false;

            if( modification.DestinationProperty == null )
                return false;

            Prefab prefab =
                PrefabHierarchyUtility.GetPrefab(
                    modification.DestinationProperty.serializedObject.targetObject.AsGameObject() );

            return PrefabOverrideUtility.IsOverridden(
                prefab.CachedTransform, modification.DestinationProperty );
        }
    }
}