﻿using UnityEngine;

namespace VisualDesignCafe.Editor.Prefabs
{
    internal static class ComponentUtility
    {
        public static int GetDuplicateComponentIndex( Component component )
        {
            if( component == null )
                return -1;

            Component[] components =
                component.gameObject.GetComponents( component.GetType() );

            for( int i = 0; i < components.Length; i++ )
            {
                if( components[ i ] == component )
                    return i;
            }

            return -1;
        }

        public static Component GetComponentAtIndex(
            GameObject gameObject, System.Type type, int duplicateIndex )
        {
            Component[] components = gameObject.GetComponents( type );

            if( components.Length == 0 )
                return null;

            return components[ Mathf.Min( duplicateIndex, components.Length - 1 ) ];
        }
    }
}