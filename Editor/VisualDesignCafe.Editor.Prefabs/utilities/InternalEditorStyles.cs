﻿using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace VisualDesignCafe.Editor.Prefabs
{
    public static class InternalEditorStyles
    {
        public static GUIStyle Empty
        {
            get
            {
                if( _empty == null )
                {
                    _empty = new GUIStyle( EditorStyles.label );
                    _empty.border = new RectOffset( 0, 0, 0, 0 );
                    _empty.contentOffset = Vector2.zero;
                    _empty.margin = new RectOffset( 0, 0, 0, 0 );
                    _empty.padding = new RectOffset( 0, 0, 0, 0 );
                    _empty.alignment = TextAnchor.MiddleLeft;
                }

                return _empty;
            }
        }

        /// <summary>
        /// Big header background
        /// </summary>
        public static GUIStyle InspectorTitlebar
        {
            get
            {
                if( _inspectorTitlebar == null )
                {
                    _inspectorTitlebar =
                        typeof( EditorStyles )
                            .GetProperty(
                                "inspectorBig",
                                BindingFlags.Static | BindingFlags.NonPublic )
                            .GetValue( null, null ) as GUIStyle;

                    _inspectorTitlebar.fontStyle = FontStyle.Bold;
                    _inspectorTitlebar.fontSize = 11;
                }

                return _inspectorTitlebar;
            }
        }

        /// <summary>
        /// Search field 
        /// </summary>
        public static GUIStyle SearchField
        {
            get
            {
                if( _searchField == null )
                {
                    _searchField =
                        typeof( EditorStyles )
                        .GetProperty(
                            "searchField",
                            BindingFlags.Static | BindingFlags.NonPublic )
                        .GetValue( null, null ) as GUIStyle;
                }

                return _searchField;
            }
        }

        /// <summary>
        /// Search field cancel button, should be drawn next to a search field.
        /// </summary>
        public static GUIStyle SearchFieldCancelButton
        {
            get
            {
                if( _searchFieldCancelButton == null )
                {
                    _searchFieldCancelButton =
                        typeof( EditorStyles )
                        .GetProperty(
                            "searchFieldCancelButton",
                            BindingFlags.Static | BindingFlags.NonPublic )
                        .GetValue( null, null ) as GUIStyle;
                }

                return _searchFieldCancelButton;
            }
        }

        /// <summary>
        /// Search field cancel button, should be drawn next to a search field.
        /// </summary>
        public static GUIStyle SearchFieldCancelButtonEmpty
        {
            get
            {
                if( _searchFieldCancelButtonEmpty == null )
                {
                    _searchFieldCancelButtonEmpty =
                        typeof( EditorStyles )
                        .GetProperty(
                            "searchFieldCancelButtonEmpty",
                            BindingFlags.Static | BindingFlags.NonPublic )
                        .GetValue( null, null ) as GUIStyle;
                }

                return _searchFieldCancelButtonEmpty;
            }
        }

        private static GUIStyle _inspectorTitlebar;
        private static GUIStyle _searchField;
        private static GUIStyle _searchFieldCancelButton;
        private static GUIStyle _searchFieldCancelButtonEmpty;
        private static GUIStyle _empty;
    }
}