﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

#pragma warning disable 618

namespace VisualDesignCafe.Editor.Prefabs
{
    public class AssetDatabaseUtility
    {
        public static bool IsPrefabAsset( string path )
        {
            return path.EndsWith( ".prefab", StringComparison.OrdinalIgnoreCase );
        }

        public static bool IsModelAsset( string path )
        {
            return path.EndsWith( ".fbx", StringComparison.OrdinalIgnoreCase )
                || path.EndsWith( ".dae", StringComparison.OrdinalIgnoreCase )
                || path.EndsWith( ".3ds", StringComparison.OrdinalIgnoreCase )
                || path.EndsWith( ".dxf", StringComparison.OrdinalIgnoreCase )
                || path.EndsWith( ".obj", StringComparison.OrdinalIgnoreCase )
                || path.EndsWith( ".skp", StringComparison.OrdinalIgnoreCase );
        }

        /// <summary>
        /// Finds and loads all prefab assets in the project.
        /// </summary>
        public static GameObject[] FindPrefabsInProject()
        {
            return AssetDatabase.GetAllAssetPaths()
                .Where( p => p.EndsWith( ".prefab", StringComparison.OrdinalIgnoreCase ) )
                .Select( p => AssetDatabase.LoadAssetAtPath( p, typeof( GameObject ) ) as GameObject )
                .Where( p => p != null )
                .ToArray();
        }

        /// <summary>
        /// Returns all the assets that contain a reference to the given prefab asset
        /// </summary>
        internal static GameObject[] FindAssetsWithReferenceTo(
            GameObject prefabAsset,
            PrefabDatabase prefabDatabase,
            bool includeSelf = false,
            int maximumDepth = 1,
            bool onlyIncludeAssets = false,
            bool ignoreModelPrefabs = false )
        {
            if( prefabAsset == null )
                throw new ArgumentNullException( "Prefab Asset" );

            if( !EditorUtility.IsPersistent( prefabAsset ) )
                throw new ArgumentException( "Prefab is not an asset" );

            if( prefabDatabase == null )
                throw new ArgumentNullException( "PrefabDatabase" );

            var prefabs = new List<GameObject>();

            if( includeSelf )
                prefabs.Add( prefabAsset );

            string path = AssetDatabase.GetAssetPath( prefabAsset );
            string guid = AssetDatabase.AssetPathToGUID( path );

            PrefabAsset[] assetsWithReference =
                prefabDatabase.FindPrefabsWithReferenceTo( guid );

            foreach( var asset in assetsWithReference )
            {
                GameObject prefab =
                    (GameObject) AssetDatabase.LoadAssetAtPath(
                        asset.Path,
                        typeof( GameObject ) );

                if( prefab == null )
                    continue;

                prefabs.Add( prefab );
            }

            // Search through the currently edited prefabs in the batch and
            // check if one has a reference to the given prefab asset.
            if( !onlyIncludeAssets )
            {
                foreach( var pair in PrefabOperation.BatchedAssets )
                {
                    NestedPrefab[] nestedPrefabs =
                        pair.Value.GetComponentsInChildren<NestedPrefab>( true );

                    foreach( NestedPrefab nestedPrefab in nestedPrefabs )
                    {
                        if( nestedPrefab == null )
                            continue;

                        if( !HasReferenceToAsset(
                                nestedPrefab,
                                prefabAsset,
                                maximumDepth,
                                true,
                                ignoreModelPrefabs ) )
                        {
                            continue;
                        }

                        GameObject asset =
                            PrefabHierarchyUtility.FindPrefabRoot(
                                nestedPrefab.CachedGameObject );

                        if( !prefabs.Contains( asset ) )
                            prefabs.Add( asset );
                    }
                }
            }

            return prefabs.ToArray();
        }

        /// <summary>
        /// Checks if the NestedPrefab has a reference to the asset.
        /// </summary>
        private static bool HasReferenceToAsset(
            NestedPrefab nestedPrefab,
            GameObject asset,
            int maximumDepth,
            bool allowNonPersistent = false,
            bool ignoreModelPrefabs = false )
        {
            if( nestedPrefab == null || nestedPrefab.Asset == null )
                return false;

            // Skip instances
            if( !allowNonPersistent && !EditorUtility.IsPersistent( nestedPrefab ) )
                return false;

            GameObject root =
                PrefabHierarchyUtility.FindPrefabRoot( nestedPrefab.CachedGameObject );

            // Skip self
            if( root == asset )
                return false;

            // Skip if the prefab does not have the asset as target
            if( nestedPrefab.Asset != asset )
                return false;

            // Skip if the depth is greater than the maxium allowed depth.
            int depth = PrefabHierarchyUtility.GetDepth( nestedPrefab, ignoreModelPrefabs );
            if( depth > maximumDepth && maximumDepth > 0 )
                return false;

            return true;
        }

        /// <summary>
        /// Executes the action for each prefab in the asset database.
        /// </summary>
        public static void ForEachPrefabInAssetDatabase(
            Action<GameObject, int, int> action,
            Action<Exception> exceptionCallback = null )
        {

            ForEachPrefabInAssetDatabase(
                ( string path, int index, int count ) =>
                {
                    GameObject asset =
                        AssetDatabase.LoadAssetAtPath( path, typeof( GameObject ) ) as GameObject;

                    if( asset != null )
                        action( asset, index, count );
                },
                exceptionCallback );
        }

        /// <summary>
        /// Executes the action for each path of a prefab asset in the asset database.
        /// </summary>
        public static void ForEachPrefabInAssetDatabase(
            Action<string, int, int> action,
            Action<Exception> exceptionCallback )
        {
            string[] prefabAssets =
                AssetDatabase.GetAllAssetPaths()
                    .Where( p => IsPrefabAsset( p ) )
                    .ToArray();

            int index = 0;
            int count = prefabAssets.Length;

            NestedPrefabsPostprocessor.IsEnabled = false;
            AssetDatabase.StartAssetEditing();
            {
                foreach( string path in prefabAssets )
                {
                    try
                    {
                        action( path, index++, count );
                    }
                    catch( Exception e )
                    {
                        if( exceptionCallback != null )
                        {
                            exceptionCallback( e );
                        }
                        else
                        {
                            Debug.LogException( e );
                        }
                    }
                }
            }
            AssetDatabase.StopAssetEditing();
            NestedPrefabsPostprocessor.IsEnabled = true;
        }

        /// <summary>
        /// Executes the action for each path of a model asset in the asset database.
        /// </summary>
        public static void ForEachModelInAssetDatabase(
            Action<string, int, int> action,
            Action<Exception> exceptionCallback )
        {
            string[] prefabAssets =
                AssetDatabase.GetAllAssetPaths()
                    .Where( p => IsModelAsset( p ) )
                    .ToArray();

            int index = 0;
            int count = prefabAssets.Length;

            NestedPrefabsPostprocessor.IsEnabled = false;
            AssetDatabase.StartAssetEditing();
            {
                foreach( string path in prefabAssets )
                {
                    try
                    {
                        action( path, index++, count );
                    }
                    catch( Exception e )
                    {
                        if( exceptionCallback != null )
                        {
                            exceptionCallback( e );
                        }
                        else
                        {
                            Debug.LogException( e );
                        }
                    }
                }
            }
            AssetDatabase.StopAssetEditing();
            NestedPrefabsPostprocessor.IsEnabled = true;
        }

        /// <summary>
        /// Executes the action for each model asset in the asset database.
        /// </summary>
        public static void ForEachModelInAssetDatabase(
            Action<GameObject, int, int> action,
            Action<Exception> exceptionCallback )
        {
            NestedPrefabsPostprocessor.IsEnabled = false;
            AssetDatabase.StartAssetEditing();

            ForEachModelInAssetDatabase(
                ( string path, int index, int count ) =>
                {
                    GameObject asset =
                        AssetDatabase.LoadAssetAtPath( path, typeof( GameObject ) ) as GameObject;

                    if( asset != null )
                        action( asset, index, count );
                },
                exceptionCallback );

            NestedPrefabsPostprocessor.IsEnabled = false;
            AssetDatabase.StopAssetEditing();
            NestedPrefabsPostprocessor.IsEnabled = true;
        }

        /// <summary>
        /// Executes the action for each scene in the project. Will temporarily load the scene.
        /// </summary>
        // TODO: Remove obsolete code.
        public static void ForEachSceneInProject(
            Action<string, int, int> action,
            Action<Exception> exceptionCallback = null )
        {
            string[] sceneList =
                AssetDatabase.GetAllAssetPaths()
                    .Where( path => path.EndsWith( ".unity" ) )
                    .ToArray();

            string activeScenePath = EditorApplication.currentScene;

            int count = sceneList.Length;
            int index = 0;

            for( int i = 0; i < sceneList.Length; i++ )
            {
                try
                {
                    if( sceneList[ i ] != EditorApplication.currentScene )
                        EditorApplication.OpenScene( sceneList[ i ] );

                    action( sceneList[ i ], index++, count );
                }
                catch( Exception e )
                {
                    if( exceptionCallback != null )
                    {
                        exceptionCallback( e );
                    }
                    else
                        Debug.LogException( e );
                }
            }

            EditorApplication.OpenScene( activeScenePath );
        }
    }
}
