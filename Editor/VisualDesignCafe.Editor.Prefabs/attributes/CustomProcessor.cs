﻿using System;

namespace VisualDesignCafe.Editor.Prefabs
{
    /// <summary>
    /// Custom processor for a component or prefab.
    /// Can be used to change the modifications that are applied to a nested prefab.
    /// </summary>
    [AttributeUsage( AttributeTargets.Class, AllowMultiple = true, Inherited = true )]
    public class CustomProcessor : Attribute
    {
        /// <summary>
        /// The target component type to which this processor should be applied.
        /// Use 'Prefab' in order to apply the processor for all components in a prefab.
        /// </summary>
        public readonly Type Type;

        public CustomProcessor( Type type )
        {
            this.Type = type;
        }
    }
}