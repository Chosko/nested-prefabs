﻿using System;
using System.Collections.Generic;

namespace VisualDesignCafe.Editor.Prefabs.Comparing
{
    internal class ComponentComparer
    {
        public Func<Modification, bool> ModificationCallback
        {
            get;
            set;
        }

        private SequenceComparer<PropertyInfo>.EqualityComparer _matchByPath
        {
            get
            {
                return ( a, b ) =>
                    a.Equals( b, PrefabComparison.PropertyPath );
            }
        }

        private Dictionary<SequenceMatch, ModificationType> _matchLookup =
            new Dictionary<SequenceMatch, ModificationType>()
       {
                { SequenceMatch.Added, ModificationType.PropertyAdded },
                { SequenceMatch.Removed, ModificationType.PropertyRemoved },
       };

        /// <summary>
        /// Compares the component to the other component.
        /// </summary>
        public ModificationList Compare(
            ComponentInfo source,
            ComponentInfo other,
            PrefabComparison comparison,
            out bool shouldExit )
        {
            var modifications = new ModificationList();
            bool exit = false;
            shouldExit = false;

            var comparer = new SequenceComparer<PropertyInfo>( _matchByPath );
            comparer.MatchByEquality(
                source.Properties,
                other.Properties,
                ( match, sourceProperty, destinationProperty ) =>
                {
                    Modification modification = null;

                    if( match == SequenceMatch.Equal )
                    {
                        if( !sourceProperty.Equals(
                                destinationProperty,
                                comparison ) )
                        {
                            modification =
                                new Modification(
                                    ModificationType.PropertyValueChanged,
                                    sourceProperty,
                                    destinationProperty );
                        }
                    }
                    else
                    {
                        if( !( sourceProperty ?? destinationProperty )
                                .PropertyPath
                                .Contains( ".Array" ) )
                        {
                            modification =
                                new Modification(
                                    _matchLookup[ match ],
                                    sourceProperty,
                                    destinationProperty );
                        }
                    }

                    if( modification != null )
                    {
                        modifications.Add( modification );

                        InvokeModificationCallback(
                            modification,
                            out exit );

                        if( exit )
                            comparer.Exit();
                    }
                } );

            shouldExit = exit;
            return modifications;
        }

        /// <summary>
        /// Checks if this component equals the other component.
        /// </summary>
        /// <returns>True if the components are equal.</returns>
        public bool AreEqual(
            ComponentInfo source,
            ComponentInfo other,
            PrefabComparison comparison )
        {
            if( FlagsUtility.IsSet( comparison, PrefabComparison.ComponentType ) )
            {
                if( source.Type == other.Type )
                    return true;
            }

            bool areEqual = true;

            if( source.Index != other.Index )
                return false;

            var comparer = new SequenceComparer<PropertyInfo>( _matchByPath );

            comparer.MatchByEquality(
                source.Properties,
                other.Properties,
                ( match, sourceProperty, destinationProperty ) =>
                {
                    if( match != SequenceMatch.Equal
                        || !sourceProperty.Equals(
                                destinationProperty,
                                comparison ) )
                    {
                        areEqual = false;
                        comparer.Exit();
                    }
                } );

            return areEqual;
        }

        private void InvokeModificationCallback(
            Modification modification,
            out bool shouldExit )
        {
            shouldExit = false;

            if( ModificationCallback == null )
                return;

            if( !ModificationCallback( modification ) )
                shouldExit = true;

        }
    }
}
