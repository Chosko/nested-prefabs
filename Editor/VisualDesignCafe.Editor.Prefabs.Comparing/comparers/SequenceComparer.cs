﻿using System;
using System.Collections.Generic;

namespace VisualDesignCafe.Editor.Prefabs.Comparing
{
    public enum SequenceMatch
    {
        Added,
        Removed,
        Equal,
    }

    internal class SequenceComparer<T>
        where T : class
    {
        public delegate bool EqualityComparer( T a, T b );

        private EqualityComparer Comparer
        {
            get;
            set;
        }

        private Func<T, bool> Exclude
        {
            get;
            set;
        }

        public bool ShouldExit
        {
            get;
            private set;
        }

        public SequenceComparer(
            EqualityComparer equalityComparer,
            Func<T, bool> exclude = null )
        {
            Comparer = equalityComparer;
            Exclude = exclude;
        }

        public void Exit()
        {
            ShouldExit = true;
        }

        public void MatchByEquality(
            T[] source,
            T[] destination,
            Action<SequenceMatch, T, T> callback )
        {
            var collectionA = new List<T>();
            collectionA.AddRange( source );

            var collectionB = new List<T>();
            collectionB.AddRange( destination );

            for( int i = collectionA.Count - 1; i > -1; i-- )
            {
                T sourceObj = collectionA[ i ];

                if( Exclude != null && Exclude( sourceObj ) )
                    continue;

                int destinationIndex = IndexOf( sourceObj, collectionB );

                if( destinationIndex == -1 )
                {
                    callback( SequenceMatch.Removed, sourceObj, null );

                    if( ShouldExit )
                        return;

                    continue;
                }

                T destinationObj = collectionB[ destinationIndex ];

                callback( SequenceMatch.Equal, sourceObj, destinationObj );

                if( ShouldExit )
                    return;

                collectionB.RemoveAt( destinationIndex );
            }

            for( int i = 0; i < collectionB.Count; i++ )
            {
                if( Exclude != null && Exclude( collectionB[ i ] ) )
                    continue;

                callback( SequenceMatch.Added, (T) null, collectionB[ i ] );

                if( ShouldExit )
                    return;
            }
        }

        private int IndexOf( T obj, List<T> objects )
        {
            return objects.FindLastIndex( c => Comparer( c, obj ) );
        }
    }
}