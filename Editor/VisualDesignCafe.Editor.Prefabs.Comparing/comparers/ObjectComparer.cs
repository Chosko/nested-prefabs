﻿using System;
using System.Collections.Generic;

namespace VisualDesignCafe.Editor.Prefabs.Comparing
{
    internal class ObjectComparer
    {
        public Func<Modification, bool> ModificationCallback
        {
            get;
            set;
        }

        private bool ShouldExit
        {
            get;
            set;
        }

        private Dictionary<SequenceMatch, ModificationType> _objectMatchLookup =
            new Dictionary<SequenceMatch, ModificationType>()
        {
                { SequenceMatch.Added, ModificationType.ChildAdded },
                { SequenceMatch.Removed, ModificationType.ChildRemoved },
        };

        private Dictionary<SequenceMatch, ModificationType> _componentMatchLookup =
            new Dictionary<SequenceMatch, ModificationType>()
        {
                { SequenceMatch.Added, ModificationType.ComponentAdded },
                { SequenceMatch.Removed, ModificationType.ComponentRemoved },
        };

        private SequenceComparer<ObjectInfo>.EqualityComparer _matchByGuid
        {
            get
            {
                return ( a, b ) =>
                    a.Equals( b, PrefabComparison.Guid );
            }
        }

        private SequenceComparer<ComponentInfo>.EqualityComparer _matchByType
        {
            get
            {
                return ( a, b ) =>
                    a.Equals( b, PrefabComparison.ComponentType );
            }
        }

        public ModificationList Compare(
            ObjectInfo source,
            ObjectInfo other,
            PrefabComparison comparison,
            out bool shouldExit )
        {
            shouldExit = false;

            var modifications = new ModificationList();

            if( FlagsUtility.IsSet( comparison, PrefabComparison.GameObject ) )
            {
                modifications.AddRange(
                    CompareGameObject( source, other, comparison ) );

                if( ShouldExit )
                    return modifications;
            }

            if( FlagsUtility.IsSet( comparison, PrefabComparison.Components ) )
            {
                modifications.AddRange(
                    CompareComponents( source, other, comparison ) );

                if( shouldExit )
                    return modifications;
            }

            if( FlagsUtility.IsSet( comparison, PrefabComparison.Children ) )
            {
                modifications.AddRange(
                    CompareChildren(
                        source,
                        other,
                        FlagsUtility.Unset( comparison, PrefabComparison.Children ) ) );

                if( shouldExit )
                    return modifications;
            }

            return modifications;
        }

        public bool AreEqual(
            ObjectInfo source,
            ObjectInfo other,
            PrefabComparison comparison )
        {
            if( FlagsUtility.IsSet( comparison, PrefabComparison.Guid )
                && source.Guid != other.Guid )
            {
                return false;
            }

            if( FlagsUtility.IsSet( comparison, PrefabComparison.GameObject ) )
            {
                if( !source.Object.Equals( other.Object, comparison ) )
                    return false;
            }

            if( FlagsUtility.IsSet( comparison, PrefabComparison.Components ) )
            {
                if( !ComponentsAreEqual( source, other, comparison ) )
                    return false;
            }

            if( FlagsUtility.IsSet( comparison, PrefabComparison.Children ) )
            {
                if( !ChildrenAreEqual(
                        source,
                        other,
                        FlagsUtility.Unset( comparison, PrefabComparison.Children ) ) )
                {
                    return false;
                }
            }

            return true;
        }

        private ModificationList CompareGameObject(
            ObjectInfo source,
            ObjectInfo other,
            PrefabComparison comparison )
        {
            bool exit = false;
            var result =
                source.Object.Compare(
                    other.Object,
                    ModificationCallback,
                    FlagsUtility.Unset( comparison, PrefabComparison.Children ),
                    out exit );

            ShouldExit = exit;
            return result;
        }

        private ModificationList CompareComponents(
            ObjectInfo source,
            ObjectInfo destination,
            PrefabComparison comparison )
        {
            bool exit = false;
            bool componentOrderChanged = false;
            var result = new ModificationList();
            var comparer = new SequenceComparer<ComponentInfo>( _matchByType );

            comparer.MatchByEquality(
                source.Components,
                destination.Components,
                ( match, sourceComponent, destinationComponent ) =>
                {
                    if( match == SequenceMatch.Equal )
                    {
                        if( sourceComponent.Index != destinationComponent.Index )
                            componentOrderChanged = true;

                        result.AddRange(
                            sourceComponent.Compare(
                                destinationComponent,
                                ModificationCallback,
                                comparison,
                                out exit ) );
                    }
                    else
                    {
                        var modification =
                            new Modification(
                                _componentMatchLookup[ match ],
                                sourceComponent ?? source.Object,
                                destinationComponent ?? destination.Object );

                        result.Add( modification );

                        InvokeModificationCallback( modification, out exit );
                    }

                    if( exit )
                        comparer.Exit();
                } );

            if( componentOrderChanged )
            {
                var modification =
                    new Modification(
                        ModificationType.PropertyValueChanged,
                        new PropertyInfo(
                            source.Components[ 0 ],
                            "m_ComponentIndex",
                            0 ),
                        new PropertyInfo(
                            destination.Components[ 0 ],
                            "m_ComponentIndex",
                            0 ) );

                result.Add( modification );

                InvokeModificationCallback( modification, out exit );
            }

            ShouldExit = exit;
            return result;
        }

        private ModificationList CompareChildren(
            ObjectInfo source,
            ObjectInfo other,
            PrefabComparison comparison )
        {
            var comparer = new SequenceComparer<ObjectInfo>( _matchByGuid );
            var result = new ModificationList();
            bool exit = false;

            comparer
                .MatchByEquality(
                    source.CachedChildren,
                    other.CachedChildren,
                    ( match, sourceObj, destinationObj ) =>
                    {
                        if( match == SequenceMatch.Equal )
                        {
                            result.AddRange(
                                sourceObj.Compare(
                                    destinationObj,
                                    ModificationCallback,
                                    comparison,
                                    out exit ) );
                        }
                        else
                        {
                            var modification =
                                new Modification(
                                    _objectMatchLookup[ match ],
                                    sourceObj ?? source,
                                    destinationObj ?? other );

                            result.Add( modification );

                            InvokeModificationCallback( modification, out exit );
                        }

                        if( exit )
                            comparer.Exit();
                    } );

            ShouldExit = exit;
            return result;
        }

        private bool ComponentsAreEqual(
            ObjectInfo source,
            ObjectInfo other,
            PrefabComparison flags )
        {
            var comparer = new SequenceComparer<ComponentInfo>( _matchByType );
            bool areEqual = true;

            comparer.MatchByEquality(
                source.Components,
                other.Components,
                ( match, sourceComponent, destinationComponent ) =>
                {
                    if( match != SequenceMatch.Equal
                        || !sourceComponent
                            .Equals( destinationComponent, flags ) )
                    {
                        areEqual = false;
                        comparer.Exit();
                    }
                } );

            return areEqual;
        }

        private bool ChildrenAreEqual(
            ObjectInfo source,
            ObjectInfo other,
            PrefabComparison comparison )
        {
            var comparer = new SequenceComparer<ObjectInfo>( _matchByGuid );
            bool areEqual = true;

            comparer.MatchByEquality(
                source.Children,
                other.Children,
                ( match, sourceObj, destinationObj ) =>
                {
                    if( match != SequenceMatch.Equal
                        || !sourceObj
                            .Equals( destinationObj, comparison ) )
                    {
                        areEqual = false;
                        comparer.Exit();
                    }
                } );

            return areEqual;
        }

        private void InvokeModificationCallback(
            Modification modification,
            out bool shouldExit )
        {
            shouldExit = false;

            if( ModificationCallback == null )
                return;

            if( !ModificationCallback( modification ) )
                shouldExit = true;
        }
    }
}