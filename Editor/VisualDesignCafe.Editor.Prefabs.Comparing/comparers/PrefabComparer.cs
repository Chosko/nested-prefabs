﻿using System;
using UnityEngine;

namespace VisualDesignCafe.Editor.Prefabs.Comparing
{
    [Flags]
    internal enum PrefabComparison
    {
        None = 0,

        /// <summary>
        /// Compare components by type only.
        /// </summary>
        ComponentType = 1,

        /// <summary>
        /// Compare properties only by property path and not by value
        /// </summary>
        PropertyPath = 2,

        /// <summary>
        /// Ignore properties marked as prefab override
        /// </summary>
        IgnorePrefabOverrides = 8,

        /// <summary>
        /// Compare the root object
        /// </summary>
        GameObject = 16,

        /// <summary>
        /// Compare components
        /// </summary>
        Components = 32,

        /// <summary>
        /// Compare children
        /// </summary>
        Children = 64,

        /// <summary>
        /// Match objects by using the object's GUID.
        /// </summary>
        Guid = 128,

        /// <summary>
        /// Include references to other objects in the scene. 
        /// Scene object references are usually ignored since they are always an override.
        /// </summary>
        IncludeSceneReferences = 1024,

        AsInstance = 16 | 32 | 64 | 4,
        AsPrefab = 16 | 32 | 64 | 4 | 8,
    }

    internal sealed class PrefabComparer
    {

        public static IgnoreList DefaultIgnoreList
        {
            get
            {
                if( _defaultIgnoreList == null )
                {
                    _defaultIgnoreList = new IgnoreList();

                    _defaultIgnoreList.AddProperty( "m_ObjectHideFlags" );
                    _defaultIgnoreList.AddProperty( "m_PrefabParentObject" );
                    _defaultIgnoreList.AddProperty( "m_PrefabParentObject.m_FileID" );
                    _defaultIgnoreList.AddProperty( "m_PrefabParentObject.m_PathID" );
                    _defaultIgnoreList.AddProperty( "m_PrefabInternal" );
                    _defaultIgnoreList.AddProperty( "m_PrefabInternal.m_FileID" );
                    _defaultIgnoreList.AddProperty( "m_PrefabInternal.m_PathID" );
                    _defaultIgnoreList.AddProperty( "m_GameObject" );
                    _defaultIgnoreList.AddProperty( "m_GameObject.m_FileID" );
                    _defaultIgnoreList.AddProperty( "m_GameObject.m_PathID" );
                    _defaultIgnoreList.AddProperty( "m_Father.m_FileID" );
                    _defaultIgnoreList.AddProperty( "m_Father.m_PathID" );
                    _defaultIgnoreList.AddProperty( "m_Component" );
                    _defaultIgnoreList.AddProperty( "m_Children" );
                    _defaultIgnoreList.AddProperty( "randomSeed" );

                    _defaultIgnoreList.AddProperty( new Wildcard( "*.Array" ) );
                    _defaultIgnoreList.AddProperty( new Wildcard( "*.Array.size" ) );
                    _defaultIgnoreList.AddProperty( new Wildcard( "*.m_FileID" ) );
                    _defaultIgnoreList.AddProperty( new Wildcard( "*.m_PathID" ) );
                    _defaultIgnoreList.AddProperty( new Wildcard( "*.m_Curve" ) );
                    _defaultIgnoreList.AddProperty( new Wildcard( "*.r" ) );
                    _defaultIgnoreList.AddProperty( new Wildcard( "*.g" ) );
                    _defaultIgnoreList.AddProperty( new Wildcard( "*.b" ) );
                    _defaultIgnoreList.AddProperty( new Wildcard( "*.a" ) );

                    _defaultIgnoreList.AddProperty( new Wildcard( "m_Children.*" ) );
                    _defaultIgnoreList.AddProperty( new Wildcard( "m_Component.*" ) );

                    _defaultIgnoreList.AddComponent( typeof( NestedPrefab ) );
                    _defaultIgnoreList.AddComponent( typeof( Prefab ) );
                    _defaultIgnoreList.AddComponent( typeof( Guid ) );

                    _defaultIgnoreList.AddProperty(
                        "serializationData.Prefab",
                        "Sirenix.OdinInspector.SerializedMonoBehaviour" );
                }

                return _defaultIgnoreList;
            }
        }

        private static IgnoreList _defaultIgnoreList;
        private ObjectInfo _destinationInfo;
        private ObjectInfo _sourceInfo;

        /// <summary>
        /// Creates a new prefab comparer that can compare the given prefabs.
        /// </summary>
        public PrefabComparer(
            Prefab destination,
            Prefab source,
            IgnoreList ignoreList,
            bool includeNestedChildren = true,
            bool isReloadedFromModel = false,
            bool isModelPrefab = false )
        {
            _destinationInfo =
                new ObjectInfo(
                    destination.gameObject,
                    destination.transform,
                    null,
                    ignoreList,
                    includeNestedChildren,
                    isReloadedFromModel,
                    isModelPrefab );

            _sourceInfo =
                new ObjectInfo(
                    source.gameObject,
                    source.transform,
                    null,
                    ignoreList,
                    includeNestedChildren,
                    isReloadedFromModel,
                    isModelPrefab );
        }

#if DEV
        /// <summary>
        /// Special constructor for tests. 
        /// Allows to compare GameObjects instead of prefabs.
        /// </summary>
        public PrefabComparer(
            GameObject destination,
            GameObject source,
            IgnoreList ignoreList,
            bool includeNestedChildren = true,
            bool isReloadedFromModel = false,
            bool isModelPrefab = false )
        {
            _destinationInfo =
                new ObjectInfo(
                    destination,
                    destination.transform,
                    null,
                    ignoreList,
                    includeNestedChildren,
                    isReloadedFromModel,
                    isModelPrefab );

            _sourceInfo =
                new ObjectInfo(
                    source,
                    source.transform,
                    null,
                    ignoreList,
                    includeNestedChildren,
                    isReloadedFromModel,
                    isModelPrefab );
        }
#endif

        public ModificationList Compare(
            PrefabComparison comparison,
            Func<Modification, bool> modificationCallback )
        {
            bool shouldExit;
            ModificationList modifications =
                _destinationInfo.Compare(
                    _sourceInfo,
                    modificationCallback,
                    comparison,
                    out shouldExit );

            return modifications;
        }

        /// <summary>
        /// Returns true if the objects are equal to each other based on the comparison method.
        /// </summary>
        public bool AreEqual(
            PrefabComparison method,
            IgnoreList ignoreList )
        {
            return
                _destinationInfo.Equals(
                    _sourceInfo,
                    method );
        }
    }
}