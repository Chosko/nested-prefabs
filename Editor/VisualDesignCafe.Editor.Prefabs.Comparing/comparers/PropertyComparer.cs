﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace VisualDesignCafe.Editor.Prefabs.Comparing
{
    using EqualityComparer = Func<PropertyInfo, PrefabComparison, bool>;
    using Object = UnityEngine.Object;

    internal class PropertyComparer
    {
        private static Dictionary<DrivenTransformProperties, string[]> _drivenPropertiesLookup =
            new Dictionary<DrivenTransformProperties, string[]>()
        {
                {
                    DrivenTransformProperties.AnchoredPosition,
                    new string[]
                    {
                        "m_AnchoredPosition",
                        "m_AnchoredPosition.x",
                        "m_AnchoredPosition.y"
                    }
                },
                {
                    DrivenTransformProperties.AnchoredPositionX,
                    new string[]{ "m_AnchoredPosition.x" }
                },
                {
                    DrivenTransformProperties.AnchoredPositionY,
                    new string[]{ "m_AnchoredPosition.y" }
                },
                {
                    DrivenTransformProperties.AnchoredPositionZ,
                    new string[]{ "m_AnchoredPosition.z" }
                },
                {
                    DrivenTransformProperties.AnchorMax,
                    new string[]
                    {
                        "m_AnchorMax",
                        "m_AnchorMax.x",
                        "m_AnchorMax.y"
                    }
                },
                {
                    DrivenTransformProperties.AnchorMaxX,
                    new string[]{ "m_AnchorMax.x" }
                },
                {
                    DrivenTransformProperties.AnchorMaxY,
                    new string[]{ "m_AnchorMax.y" }
                },
                {
                    DrivenTransformProperties.AnchorMin,
                    new string[]
                    {
                        "m_AnchorMin",
                        "m_AnchorMin.x",
                        "m_AnchorMin.y",
                    }
                },
                {
                    DrivenTransformProperties.AnchorMinX,
                    new string[]{ "m_AnchorMin.x" }
                },
                {
                    DrivenTransformProperties.AnchorMinY,
                    new string[]{ "m_AnchorMin.y" }
                },
                {
                    DrivenTransformProperties.Anchors,
                    new string[]
                    {
                        "m_AnchorMin",
                        "m_AnchorMin.x",
                        "m_AnchorMin.y",
                        "m_AnchorMax",
                        "m_AnchorMax.x",
                        "m_AnchorMax.y"
                    }
                },
                {
                    DrivenTransformProperties.Pivot,
                    new string[]
                    {
                        "m_Pivot",
                        "m_Pivot.x",
                        "m_Pivot.y",
                    }
                },
                {
                    DrivenTransformProperties.PivotX,
                    new string[]{ "m_Pivot.x" }
                },
                {
                    DrivenTransformProperties.PivotY,
                    new string[]{ "m_Pivot.y" }
                },
                {
                    DrivenTransformProperties.Rotation,
                    new string[]
                    {
                        "m_LocalRotation",
                        "m_LocalRotation.x",
                        "m_LocalRotation.y",
                        "m_LocalRotation.z",
                        "m_LocalRotation.w",
                    }
                },
                {
                    DrivenTransformProperties.Scale,
                    new string[]
                    {
                        "m_LocalScale",
                        "m_LocalScale.x",
                        "m_LocalScale.y",
                        "m_LocalScale.z"
                    }
                },
                {
                    DrivenTransformProperties.ScaleX,
                    new string[]{ "m_LocalScale.x" }
                },
                {
                    DrivenTransformProperties.ScaleY,
                    new string[]{ "m_LocalScale.y" }
                },
                {
                    DrivenTransformProperties.ScaleZ,
                    new string[]{ "m_LocalScale.z" }
                },
                {
                    DrivenTransformProperties.SizeDelta,
                    new string[]
                    {
                        "m_SizeDelta",
                        "m_SizeDelta.x",
                        "m_SizeDelta.y",
                    }
                },
                {
                    DrivenTransformProperties.SizeDeltaX,
                    new string[]{ "m_SizeDelta.x" }
                },
                {
                    DrivenTransformProperties.SizeDeltaY,
                    new string[]{ "m_SizeDelta.y" }
                },
        };

        public static bool FloatEquals( float a, float b )
        {
            if( Mathf.Approximately( a, b ) )
                return true;

            if( Double.IsPositiveInfinity( a ) && Double.IsPositiveInfinity( b ) )
                return true;

            if( Double.IsNegativeInfinity( a ) && Double.IsNegativeInfinity( b ) )
                return true;

            return false;
        }

        public static bool Vector2Equals( Vector2 a, Vector2 b )
        {
            if( !FloatEquals( a.x, b.x ) )
                return false;

            if( !FloatEquals( a.y, b.y ) )
                return false;

            return true;
        }

        public static bool Vector3Equals( Vector3 a, Vector3 b )
        {
            if( !FloatEquals( a.x, b.x ) )
                return false;

            if( !FloatEquals( a.y, b.y ) )
                return false;

            if( !FloatEquals( a.z, b.z ) )
                return false;

            return true;
        }

        public static bool Vector4Equals( Vector4 a, Vector4 b )
        {
            if( !FloatEquals( a.x, b.x ) )
                return false;

            if( !FloatEquals( a.y, b.y ) )
                return false;

            if( !FloatEquals( a.z, b.z ) )
                return false;

            if( !FloatEquals( a.w, b.w ) )
                return false;

            return true;
        }

        public static bool QuaternionEquals( Quaternion a, Quaternion b )
        {
            if( !FloatEquals( a.x, b.x ) )
                return false;

            if( !FloatEquals( a.y, b.y ) )
                return false;

            if( !FloatEquals( a.z, b.z ) )
                return false;

            if( !FloatEquals( a.w, b.w ) )
                return false;

            return true;
        }

        public static bool GradientEquals( Gradient a, Gradient b )
        {
            if( object.ReferenceEquals( a, b ) )
                return true;

            if( a == null || b == null )
                return false;

            return ( ArrayEquals( a.alphaKeys, b.alphaKeys )
                && ArrayEquals( a.colorKeys, b.colorKeys ) );
        }

        public static bool CurveEquals( AnimationCurve a, AnimationCurve b )
        {
            if( object.ReferenceEquals( a, b ) )
                return true;

            if( a == null || b == null )
                return false;

            return ArrayEquals( a.keys, b.keys );
        }

        public static bool ArrayEquals( Array a1, Array a2 )
        {
            if( ReferenceEquals( a1, a2 ) )
                return true;

            if( a1 == null || a2 == null )
                return false;

            if( a1.Length != a2.Length )
                return false;

            for( int i = 0; i < a1.Length; i++ )
            {
                if( !object.Equals( a1.GetValue( i ), a2.GetValue( i ) ) )
                    return false;
            }

            return true;
        }

        public static EqualityComparer CreateEqualityComparer(
            PropertyInfo property,
            SerializedProperty serialized,
            out object value )
        {
            value = null;

            switch( property.PropertyType )
            {
                case SerializedPropertyType.ArraySize:
                    value = serialized.intValue;
                    return new EqualityComparer(
                        ( p, f ) =>
                            (int) property.Value == (int) p.Value );

                case SerializedPropertyType.AnimationCurve:
                    value = serialized.animationCurveValue;
                    return new EqualityComparer(
                        ( p, f ) =>
                        PropertyComparer.CurveEquals(
                            (AnimationCurve) property.Value,
                            (AnimationCurve) p.Value ) );

                case SerializedPropertyType.Boolean:
                    value = serialized.boolValue;
                    return new EqualityComparer(
                        ( p, f ) =>
                            (bool) property.Value == (bool) p.Value );

                case SerializedPropertyType.Bounds:
                    value = serialized.boundsValue;
                    return new EqualityComparer(
                        ( p, f ) =>
                            (Bounds) property.Value == (Bounds) p.Value );

                case SerializedPropertyType.Character:
                    value = serialized.intValue;
                    return new EqualityComparer(
                        ( p, f ) =>
                            (int) property.Value == (int) p.Value );

                case SerializedPropertyType.Color:
                    value = serialized.colorValue;
                    return new EqualityComparer(
                        ( p, f ) =>
                            (Color) property.Value == (Color) p.Value );

                case SerializedPropertyType.Enum:
                    value = serialized.enumValueIndex;
                    return new EqualityComparer(
                        ( p, f ) =>
                            (int) property.Value == (int) p.Value );

                case SerializedPropertyType.Float:
                    value = serialized.floatValue;
                    return new EqualityComparer(
                        ( p, f ) =>
                        FloatEquals( (float) property.Value, (float) p.Value ) );

                case SerializedPropertyType.Generic:
                    if( property.IsArray )
                    {
                        value = serialized.Copy();

                        return new EqualityComparer(
                            ( p, f ) =>
                                property.ArraySize == p.ArraySize );
                    }
                    else
                    {
                        value = null;
                        return new EqualityComparer(
                                ( p, f ) => true );
                    }

                case SerializedPropertyType.Gradient:
                    value = (Gradient) property.Value;
                    return new EqualityComparer(
                        ( p, f ) =>
                        GradientEquals(
                            (Gradient) property.Value,
                            (Gradient) p.Value ) );

                case SerializedPropertyType.Integer:
                    value = serialized.intValue;
                    return new EqualityComparer(
                        ( p, f ) =>
                        (int) property.Value == (int) p.Value );

                case SerializedPropertyType.LayerMask:
                    value = serialized.intValue;
                    return new EqualityComparer(
                        ( p, f ) =>
                        (int) property.Value == (int) p.Value );

                case SerializedPropertyType.ObjectReference:
                    value = serialized.objectReferenceValue;
                    break;

                case SerializedPropertyType.Quaternion:
                    value = serialized.quaternionValue;
                    return new EqualityComparer(
                        ( p, f ) =>
                        QuaternionEquals(
                            (Quaternion) property.Value,
                            (Quaternion) p.Value ) );

                case SerializedPropertyType.Rect:
                    value = serialized.rectValue;
                    return new EqualityComparer(
                        ( p, f ) =>
                            (Rect) property.Value == (Rect) p.Value );

                case SerializedPropertyType.String:
                    value = serialized.stringValue;
                    return new EqualityComparer(
                        ( p, f ) =>
                        (string) property.Value == (string) p.Value );

                case SerializedPropertyType.Vector2:
                    value = serialized.vector2Value;
                    return new EqualityComparer(
                        ( p, f ) =>
                        Vector2Equals(
                            (Vector2) property.Value,
                            (Vector2) p.Value ) );

                case SerializedPropertyType.Vector3:
                    value = serialized.vector3Value;
                    return new EqualityComparer(
                        ( p, f ) =>
                        Vector3Equals(
                            (Vector3) property.Value,
                            (Vector3) p.Value ) );

                case SerializedPropertyType.Vector4:
                    value = serialized.vector4Value;
                    return new EqualityComparer(
                        ( p, f ) =>
                            Vector4Equals(
                                (Vector4) property.Value,
                                (Vector4) p.Value ) );
            }

            return null;
        }

        public bool AreEqual(
            PropertyInfo source,
            PropertyInfo destination,
            PrefabComparison comparison )
        {
            ObjectInfo root = source.Component.Object.PrefabInfo;
            ObjectInfo otherRoot = destination.Component.Object.PrefabInfo;
            ObjectInfo sourceInfo = source.Component.Object;
            ObjectInfo otherInfo = destination.Component.Object;

            if( source.PropertyType != destination.PropertyType )
                return false;

            if( source.PropertyPath != destination.PropertyPath )
                return false;

            if( FlagsUtility.IsSet( comparison, PrefabComparison.PropertyPath ) )
                return true;

            // Skip properties marked as prefab override when comparing a prefab.
            if( source.PrefabOverride
                && FlagsUtility.IsSet( comparison, PrefabComparison.IgnorePrefabOverrides ) )
            {
                return true;
            }

            // Skip properties that are saved in the parent.
            if( source.Component.Object.IsRootObject || destination.Component.Object.IsRootObject )
            {
                if( source.PropertyPath == "m_Father" )
                    return true;

                if( source.PropertyPath == "m_Name" )
                    return true;

                if( source.PropertyPath == "m_IsActive" )
                    return true;

                if( source.Component.Type == typeof( Transform ) )
                    return true;

                if( source.Component.Type == typeof( RectTransform ) )
                    return true;

                // Skip properties that are marked as "Save in Parent".
                if( source.Component.SaveInParent )
                    return true;
            }

            // Skip some properties when reloading from the original model asset.
            if( source.Component.Object.IsReloadedFromModelPrefab )
            {
                if( source.PropertyPath == "m_StaticEditorFlags" )
                    return true;

                if( source.PropertyPath == "m_TagString" )
                    return true;

                if( source.PropertyPath == "m_Layer" )
                    return true;
            }

            // Skip driven properties in RectTransform.
            if( source.Component.Type == typeof( RectTransform ) )
            {
                if( source.PropertyPath.StartsWith( "m_LocalPosition" ) )
                    return true;
            }

            if( source.Component.DrivenProperties != DrivenTransformProperties.None
                || destination.Component.DrivenProperties != DrivenTransformProperties.None )
            {
                if( IsDrivenProperty( source, source.Component.DrivenProperties )
                    || IsDrivenProperty( destination, destination.Component.DrivenProperties ) )
                {
                    return true;
                }
            }

            if( source.PropertyType == SerializedPropertyType.ObjectReference )
            {
                // Ignore scene object references since they are always an override.
                // Unless there is a specific flag set to include them (for reverting the prefab).
                if( !FlagsUtility.IsSet( comparison, PrefabComparison.IncludeSceneReferences ) )
                {
                    if( source.ObjectReference.Type == ObjectReferenceType.SceneObject
                        || destination.ObjectReference.Type == ObjectReferenceType.SceneObject )
                    {
                        return true;
                    }
                }

                return
                    ObjectReferenceEquals(
                        source,
                        destination,
                        source.ObjectReference,
                        destination.ObjectReference,
                        root );
            }
            else
            {
                // Compare root order index without any ignored objects.
                if( source.PropertyPath == "m_RootOrder"
                    && source.Component.Object.IsReloadedFromModelPrefab )
                {
                    return
                        source.SubtractIgnoredFromRootOrder()
                        == destination.SubtractIgnoredFromRootOrder();
                }

                return source.CachedEquals != null
                    ? source.CachedEquals( destination, comparison )
                    : true;
            }
        }

        private bool ObjectReferenceEquals(
            PropertyInfo source,
            PropertyInfo destination,
            ObjectReference a,
            ObjectReference b,
            ObjectInfo root )
        {
            if( a.Type != b.Type )
                return false;

            switch( a.Type )
            {
                case ObjectReferenceType.Asset:
                case ObjectReferenceType.ObjectAsset:
                case ObjectReferenceType.ObjectInstance:
                case ObjectReferenceType.SceneObject:
                case ObjectReferenceType.Null:
                    return Object.ReferenceEquals( a.Reference, b.Reference );

                case ObjectReferenceType.Relative:

                    // Relative references should never be exactly the same.
                    if( Object.ReferenceEquals( a.Reference, b.Reference ) )
                        return false;

                    GameObject xObj = a.Reference as GameObject;
                    GameObject yObj = b.Reference as GameObject;

                    if( xObj != null && yObj != null )
                    {
                        return Guid.HierarchyEquals(
                                xObj.GetComponent<Guid>(),
                                yObj.GetComponent<Guid>(),
                                source.Component.Object.PrefabTransform );
                    }

                    Component xCom = a.Reference as Component;
                    Component yCom = b.Reference as Component;

                    if( xCom != null && yCom != null )
                    {
                        return Guid.HierarchyEquals(
                            xCom.GetComponent<Guid>(),
                            yCom.GetComponent<Guid>(),
                            source.Component.Object.PrefabTransform );
                    }

                    break;
                case ObjectReferenceType.Invalid:
                    return false;
            }

            if( a.Reference == null || b.Reference == null )
                return false;

            return a.Reference.Equals( b.Reference );
        }

        private static bool IsDrivenProperty(
            PropertyInfo info, DrivenTransformProperties flags )
        {
            if( info.PropertyPath.StartsWith( "m_LocalPosition" ) )
                return true;

            if( info.PropertyPath.StartsWith( "m_AnchoredPosition" ) )
                return true;

            if( info.PropertyPath.StartsWith( "m_SizeDelta" ) )
                return true;

            var enumerator = _drivenPropertiesLookup.GetEnumerator();
            while( enumerator.MoveNext() )
            {
                if( FlagsUtility.IsSet( flags, enumerator.Current.Key )
                    && enumerator.Current.Value.Contains( info.PropertyPath ) )
                {
                    return true;
                }
            }

            return false;
        }
    }
}