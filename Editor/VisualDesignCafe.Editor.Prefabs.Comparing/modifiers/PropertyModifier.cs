﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace VisualDesignCafe.Editor.Prefabs.Comparing
{
    using Object = UnityEngine.Object;

    internal class PropertyModifier
    {
        private GameObject _sourceGameObject;
        private GameObject _destinationGameObject;
        private Prefab _destinationPrefab;

        public void ModifyProperty(
            SerializedProperty source,
            SerializedProperty destination,
            SerializedObject destinationObject,
            SerializedObject sourceObject,
            PropertyInfo info,
            bool keepPrefabOverride )
        {
            // 'm_ComponentIndex' is a fake property that is returned 
            // if a difference in the component order was found.
            // We have to check it first because it doesn't contain any
            // valid data and will throw an exception during further checks.
            if( info.PropertyPath == "m_ComponentIndex" )
            {
                ReorderComponents(
                    sourceObject.targetObject.AsGameObject(),
                    destinationObject.targetObject.AsGameObject() );

                return;
            }

            if( source.serializedObject.targetObject ==
                destination.serializedObject.targetObject )
            {
                if( Config.DEBUG_WARNING )
                    Console.LogWarningFormat(
                        "Source equals destination. Ignoring property modification. ({0})",
                        source.propertyPath );

                return;
            }

            _sourceGameObject =
                source.serializedObject.targetObject.AsGameObject();

            _destinationGameObject =
                destination.serializedObject.targetObject.AsGameObject();

            _destinationPrefab =
                PrefabHierarchyUtility.GetPrefab( _destinationGameObject );

            if( info.IsArray )
            {
                ModifyArray( info, destination, keepPrefabOverride );
                return;
            }

            if( source.propertyPath == "m_RootOrder" )
            {
                ModifyRootOrder( source, destination );
            }
            else if( source.propertyPath == "m_Father" )
            {
                ModifyParent( source, destination, info );
            }
            else
            {
                CopySerializedProperty( info, source, destination );
            }

            if( !keepPrefabOverride )
            {
                ResetOverride(
                    destination.serializedObject,
                    destination.propertyPath );
            }
        }

        private void ModifyParent(
            SerializedProperty sourceProperty,
            SerializedProperty destinationProperty,
            PropertyInfo sourcePropertyInfo )
        {
            if( sourceProperty.objectReferenceValue == null )
            {
                if( Config.DEBUG_WARNING )
                    Console.LogWarningFormat(
                        "Child parent pointer 'm_Father' is null. ({0})",
                        _sourceGameObject );

                return;
            }

            Transform destinationTransform =
                (Transform) destinationProperty.serializedObject.targetObject;

            Guid sourceParentGuid =
                ( (Transform) sourceProperty.objectReferenceValue )
                    .GetComponent<Guid>();

            if( sourceParentGuid == null )
            {
                if( Config.STRICT )
                    throw new NullReferenceException( "Parent Object is null" );
                else if( Config.DEBUG_ERROR )
                    Console.LogErrorFormat(
                        "Could not find object reference. ({0})",
                        sourceProperty.objectReferenceValue );
            }

            Transform destinationParent =
                Guid.FindChildWithGuid(
                    _destinationPrefab.CachedTransform,
                    sourceParentGuid );

            // Make sure the parent of the prefab root is not changed.
            if( sourcePropertyInfo.Component.Object.IsRootObject )
                destinationParent = null;

            if( destinationParent == null )
            {
                if( Config.STRICT )
                    throw new NullReferenceException( "Parent Object is null" );
                else if( Config.DEBUG_ERROR )
                    Debug.LogError( "Failed to change parent. Parent does not exist." );

                return;
            }

            destinationTransform.SetParent(
                destinationParent.transform,
                false );

            destinationProperty.serializedObject.SaveModifications();

            if( destinationTransform.parent != destinationParent.transform )
            {
                if( Config.DEBUG_ERROR )
                    Debug.LogError( "Failed to change parent" );
            }
        }

        private void ModifyRootOrder(
            SerializedProperty sourceProperty,
            SerializedProperty destinationProperty )
        {
            try
            {
                ( (Transform) destinationProperty.serializedObject.targetObject )
                    .SetSiblingIndex( sourceProperty.intValue );
            }
            catch( NullReferenceException )
            {
                if( Config.DEBUG_WARNING )
                    Console.LogWarning(
                        "Failed to modify property. Target object has been destroyed." );
            }
        }

        private void ModifyArray(
            PropertyInfo sourcePropertyInfo,
            SerializedProperty destinationProperty,
            bool keepPrefabOverride )
        {
            SerializedProperty sourceArray =
                sourcePropertyInfo.Component.Serialized
                    .FindProperty( sourcePropertyInfo.PropertyPath );

            SerializedProperty destinationArray = destinationProperty;

            SetArraySize( destinationArray, sourceArray.arraySize );

            for( int i = 0; i < sourceArray.arraySize; i++ )
            {
                CopyArrayElement(
                    sourcePropertyInfo.Component,
                    destinationProperty.serializedObject,
                    sourceArray.GetArrayElementAtIndex( i ),
                    destinationArray.GetArrayElementAtIndex( i ),
                    keepPrefabOverride );

                if( !keepPrefabOverride )
                {
                    ResetOverride(
                        destinationProperty.serializedObject,
                        sourcePropertyInfo.PropertyPath + ".Array.data[" + i + "]" );
                }
            }

            if( !keepPrefabOverride )
            {
                destinationArray.prefabOverride = false;
            }
        }

        private void SetArraySize( SerializedProperty array, int size )
        {
            array.arraySize = size;
            array.serializedObject.SaveModifications();
        }

        private void CopyArrayElement(
            ComponentInfo sourceComponent,
            SerializedObject destinationComponent,
            SerializedProperty sourceProperty,
            SerializedProperty destinationProperty,
            bool keepPrefabOverride )
        {
            ModifyProperty(
                sourceProperty,
                destinationProperty,
                destinationProperty.serializedObject,
                sourceProperty.serializedObject,
                new PropertyInfo( sourceComponent, sourceProperty ),
                keepPrefabOverride );

            if( sourceProperty.hasChildren )
            {
                int depth = sourceProperty.depth;
                bool enterChildren = !IsString( sourceProperty );

                while( sourceProperty.Next( enterChildren )
                    && destinationProperty.Next( enterChildren ) )
                {
                    // Do not enter children if the property is a string,
                    // otherwise all individual characters of the string will be checked.
                    enterChildren = !IsString( sourceProperty );

                    if( sourceProperty.depth < depth )
                        break;

                    if( PrefabComparer.DefaultIgnoreList.IsIgnored(
                         sourceProperty.propertyPath,
                         sourceComponent.Serialized.targetObject ) )
                    {
                        continue;
                    }

                    ModifyProperty(
                        sourceProperty,
                        destinationProperty,
                        destinationProperty.serializedObject,
                        sourceProperty.serializedObject,
                        new PropertyInfo( sourceComponent, sourceProperty ),
                        keepPrefabOverride );
                }
            }

        }

        private bool IsString( SerializedProperty property )
        {
            return property.isArray
                && property.propertyType == SerializedPropertyType.String;
        }

        private void CopySerializedProperty(
            PropertyInfo info,
            SerializedProperty source,
            SerializedProperty destination )
        {
            CopySerializedProperty( info, source, destination, info.PropertyPath );
        }

        private void CopySerializedProperty(
            PropertyInfo info,
            SerializedProperty source,
            SerializedProperty destination,
            string propertyPath )
        {
            if( source == null || destination == null )
                throw new NullReferenceException( "Could not find serialized property." );

            if( source.propertyType != SerializedPropertyType.ObjectReference )
            {
                destination.serializedObject.CopyFromSerializedProperty( source );
                return;
            }

            Object reference = source.objectReferenceValue;

            if( reference == null )
            {
                destination.objectReferenceValue = null;
                return;
            }

            if( !IsValidReference( info ) )
            {
                string path =
                    PrefabHierarchyUtility.GetHierarchyPath(
                        info.Component.Object.Object.Serialized.targetObject.AsGameObject(),
                        info.Component.Object.PrefabTransform,
                        true );

                Debug.LogWarningFormat(
                    "Object Reference is invalid."
                    + " Make sure relative references point to an object within the nested prefab and not to a parent of the nested prefab."
                    + " In order to reference a parent of a nested prefab you have to add a Prefab Override for the reference."
                    + "\n(\"{0}\" in \"{1} ({2})\")\nReference was: {3}\n",
                    info.DisplayName,
                    path,
                    info.Component.Type,
                    info.ObjectReference.Reference );

                destination.objectReferenceValue = null;
                return;
            }

            switch( info.ObjectReference.Type )
            {
                case ObjectReferenceType.Asset:
                    CopyAssetReference( info, source, destination );
                    break;
                case ObjectReferenceType.ObjectAsset:
                case ObjectReferenceType.ObjectInstance:
                    destination.objectReferenceValue = source.objectReferenceValue;
                    break;
                case ObjectReferenceType.Relative:
                    CopyRelativeReference( info, source, destination, reference );
                    break;
                case ObjectReferenceType.Null:
                    destination.objectReferenceValue = null;
                    break;
            }
        }

        private void CopyAssetReference(
            PropertyInfo info,
            SerializedProperty source,
            SerializedProperty destination )
        {
            destination.objectReferenceValue = source.objectReferenceValue;

            GameObject reference =
                source.objectReferenceValue.AsGameObject();

            if( !IsValidAssetReference( info, source, destination, reference ) )
            {
                GameObject destinationGameObject =
                    destination.serializedObject.targetObject.AsGameObject();

                string path =
                    PrefabHierarchyUtility.GetHierarchyPath(
                        destinationGameObject,
                        PrefabHierarchyUtility.FindRoot(
                            destinationGameObject.transform ),
                        true );

                Debug.LogWarning(
                    string.Format(
                        "Object Reference is invalid."
                        + " A prefab asset can't be referenced if it is part of the parent hierarchy of the nested prefab instance."
                        + "\n('{0} ({1})' in '{2} ({3})')\nReference was: {4} ({5})",
                        destination.displayName,
                        destination.propertyPath,
                        path,
                        destination.serializedObject.targetObject.GetType(),
                        info.ObjectReference.Reference,
                        AssetDatabase.GetAssetPath( info.ObjectReference.Reference ) ) );

                destination.objectReferenceValue = null;
            }
        }

        private bool IsValidAssetReference(
            PropertyInfo info,
            SerializedProperty source,
            SerializedProperty destination,
            GameObject reference )
        {
            if( reference == null )
                return true;

            Prefab referencedPrefab = PrefabHierarchyUtility.GetPrefab( reference );
            if( referencedPrefab == null )
                return true;

            bool isReferenceToAssetOfParent = false;

            Prefab parent = _destinationPrefab;

            // Prefab Overrides are saved in the parent prefab and are not saved in the original prefab,
            // so object references can be set to one parent higher.
            if( info.Component.Type == typeof( PrefabOverrides ) )
            {
                parent = PrefabHierarchyUtility.GetParentPrefab( parent );
            }

            while( parent != null )
            {
                if( parent.Asset != null )
                {
                    if( referencedPrefab.CachedTransform.IsChildOf(
                            parent.Asset.transform ) )
                    {
                        isReferenceToAssetOfParent = true;
                        break;
                    }
                }

                parent = PrefabHierarchyUtility.GetParentPrefab( parent );
            }

            if( !isReferenceToAssetOfParent )
                return true;

            return false;
        }

        private void CopyRelativeReference(
            PropertyInfo info,
            SerializedProperty source,
            SerializedProperty destination,
            Object reference )
        {
            if( info.ObjectReference == null
                || info.ObjectReference.Type != ObjectReferenceType.Relative )
            {
                destination.objectReferenceValue = source.objectReferenceValue;
                return;
            }

            Transform root =
                    FindRootForRelativeReference(
                        _destinationGameObject,
                        info );

            destination.objectReferenceValue =
                    ObjectReference.FindRelative( reference, root );
        }

        private bool IsValidReference( PropertyInfo src )
        {
            return src.ObjectReference == null
                || src.ObjectReference.Type != ObjectReferenceType.Invalid;
        }

        private void ReorderComponents( GameObject source, GameObject destination )
        {
            if( source == null || destination == null )
                return;

            Component[] sourceComponents = GetComponents( source );
            Component[] destinationComponents = GetComponents( destination );

            List<Type> sourceComponentTypes =
                sourceComponents
                    .Select( s => s != null ? s.GetType() : null )
                    .ToList();

            destinationComponents =
                destinationComponents.OrderBy(
                    c =>
                    {
                        if( c == null )
                            return -1;

                        int index = sourceComponentTypes.IndexOf( c.GetType() );

                        if( index > -1 )
                            sourceComponentTypes[ index ] = null;

                        return index;
                    } ).ToArray();

            SetComponents( destination, destinationComponents );
        }

        private void SetComponents(
            GameObject gameObject,
            Component[] components )
        {
            var serializedObject =
                new SerializedObject( gameObject );

            SerializedProperty componentsArray =
                serializedObject.FindProperty( "m_Component" );

            for( int i = 0; i < components.Length; i++ )
            {
                SerializedProperty element =
                    componentsArray.GetArrayElementAtIndex( i );

                // Unity 5.0 uses 'second' while later versions use 'component'.
                SerializedProperty component =
                    element.FindPropertyRelative( "component" ) ??
                    element.FindPropertyRelative( "second" );

                if( component == null )
                {
                    if( Config.DEBUG_WARNING )
                        Console.LogWarning(
                            "Could not find relative serialized property 'component'." );

                    continue;
                }

                component.objectReferenceValue = components[ i ];
            }

            serializedObject.SaveModifications();
        }

        private Component[] GetComponents( GameObject gameObject )
        {
            var serializedObject = new SerializedObject( gameObject );

            SerializedProperty components =
                serializedObject.FindProperty( "m_Component" );

            if( components == null )
            {
                if( Config.DEBUG_WARNING )
                    Console.LogWarning(
                        "Could not find serialized property 'm_Component'." );

                return new Component[ 0 ];
            }

            Component[] result = new Component[ components.arraySize ];

            for( int i = 0; i < result.Length; i++ )
            {
                SerializedProperty property =
                    components.GetArrayElementAtIndex( i );

                // Unity 5.0 uses 'second' while later versions use 'component'.
                property =
                    property.FindPropertyRelative( "component" ) ??
                    property.FindPropertyRelative( "second" );

                if( property == null )
                {
                    result[ i ] = null;

                    if( Config.DEBUG_WARNING )
                        Console.LogWarning(
                            "Could not find relative serialized property 'component'." );

                    continue;
                }

                result[ i ] = property.objectReferenceValue as Component;
            }

            return result;
        }

        private Transform FindRootForRelativeReference( GameObject destination, PropertyInfo sourceProperty )
        {
            return ObjectReference.FindRootForRelativeReference(
                destination,
                sourceProperty.Component.Serialized.targetObject );
        }

        private void ResetOverride( SerializedObject obj, string propertyPath )
        {
            obj.FindProperty( propertyPath ).prefabOverride = false;
        }
    }
}
