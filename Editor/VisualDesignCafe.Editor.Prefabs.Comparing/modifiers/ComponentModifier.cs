﻿using System;
using UnityEditor;
using UnityEngine;

namespace VisualDesignCafe.Editor.Prefabs.Comparing
{
    internal class ComponentModifier
    {
        /// <summary>
        /// Adds a component to the destination gameobject.
        /// Copies the component data from the source component.
        /// </summary>
        /// <param name="transformTypeChanged">True if the transform type changes to/from RectTransform.</param>
        public void AddComponent(
            GameObject destination,
            Component sourceComponent,
            ComponentInfo componentInfo,
            out bool transformTypeChanged )
        {
            transformTypeChanged = false;

            Trace.AssertArgumentNotNull( destination, sourceComponent, componentInfo );

            Type type = sourceComponent.GetType();

            if( type == typeof( Transform ) )
            {
                if( ReplaceRectWithTransform( destination ) )
                    transformTypeChanged = true;
            }
            else
            {
                Component component = destination.AddComponent( type );

                if( component != null )
                {
                    EditorUtility.CopySerialized( sourceComponent, component );

                    if( type == typeof( RectTransform ) )
                        transformTypeChanged = true;

                    SetRelativeReferences( destination, component, componentInfo );
                }
            }

            EnsureComponentWasAdded( destination, type );

            if( Config.DEBUG_INFO && Config.VERBOSE )
                Console.LogFormat(
                    "Added component of type {0} to {1}",
                    type,
                    destination.ToLog() );
        }

        /// <summary>
        /// Removes the component.
        /// </summary>
        public void RemoveComponent( Component component )
        {
            if( component == null )
                return;

            if( component.GetType() == typeof( Transform )
                || component.GetType() == typeof( RectTransform ) )
            {
                return;
            }

            Component.DestroyImmediate( component );
        }

        private bool ReplaceRectWithTransform( GameObject destination )
        {
            var rectTransform = destination.GetComponent<RectTransform>();

            if( rectTransform == null )
                return false;

            Undo.DestroyObjectImmediate( rectTransform );

            return true;
        }

        private void EnsureComponentWasAdded( GameObject destination, Type type )
        {
            if( destination.GetComponent( type ) != null )
                return;

            throw new NullReferenceException(
                string.Format( "Failed to copy component. ({0})", type.Name ) );
        }

        private void SetRelativeReferences(
            GameObject destination,
            Component component,
            ComponentInfo componentInfo )
        {
            if( Config.VERBOSE && Config.DEBUG_INFO )
                Console.Log( "Setting relative references for new component." );

            SerializedObject serializedComponent = null;

            for( int i = 0; i < componentInfo.Properties.Length; i++ )
            {
                PropertyInfo property = componentInfo.Properties[ i ];

                if( !IsRelativeReference( property ) )
                    continue;

                if( serializedComponent == null )
                    serializedComponent = new SerializedObject( component );

                SerializedProperty serializedProperty =
                    serializedComponent.FindProperty( property.PropertyPath );

                serializedProperty.objectReferenceValue =
                    ObjectReference.FindRelative(
                        serializedProperty.objectReferenceValue,
                        FindRootForRelativeReference( destination, componentInfo ) );
            }

            if( serializedComponent != null )
                serializedComponent.SaveModifications();
        }

        private bool IsRelativeReference( PropertyInfo property )
        {
            if( property.ObjectReference == null )
                return false;

            if( property.ObjectReference.Type != ObjectReferenceType.Relative )
                return false;

            if( property.PropertyPath == "m_Father" )
                return false;

            return true;
        }

        private Transform FindRootForRelativeReference(
            GameObject destination,
            ComponentInfo sourceComponent )
        {
            return
                ObjectReference.FindRootForRelativeReference(
                    destination,
                    sourceComponent.Serialized.targetObject );
        }
    }
}