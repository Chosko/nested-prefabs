﻿using UnityEditor;
using UnityEngine;

namespace VisualDesignCafe.Editor.Prefabs.Comparing
{
    internal class ObjectModifier
    {
        public void AddChild(
            SerializedObject sourceObject, SerializedObject destinationObject )
        {
            GameObject source =
                (GameObject) sourceObject.targetObject;

            Transform sourceParent =
                ( (GameObject) sourceObject.targetObject ).transform.parent;

            GameObject destination =
                (GameObject) destinationObject.targetObject;

            Prefab destinationPrefab =
                PrefabHierarchyUtility.GetPrefab( destination );

            // Find the parent for the new child in the destination object.
            Transform destinationParent =
                Guid.FindChildWithGuid(
                    destinationPrefab.CachedTransform,
                    sourceParent.GetComponent<Guid>() );

            if( destinationParent == null )
            {
                if( Config.DEBUG_WARNING && Config.VERBOSE )
                    Console.LogWarningFormat(
                        "Could not find parent for added child. Using default parent." );

                destinationParent = destination.transform;
            }

            // Check if the child already exists in the destination prefab.
            if( Guid.FindChildWithGuid(
                    destinationPrefab.CachedTransform,
                    source.GetComponent<Guid>() ) != null )
            {
                if( Config.DEBUG_WARNING && Config.VERBOSE )
                    Console.LogWarningFormat(
                        "Skipping AddChild. Child already exists on object. ({0})\n{1}",
                        source,
                        destinationPrefab.gameObject.PrintHierarchy() );

                return;
            }

            var instance = new GameObject( source.name );
            {
                instance.transform.SetParent( destinationParent.transform, false );
                instance.transform.localPosition = source.transform.localPosition;
                instance.transform.localRotation = source.transform.localRotation;
                instance.transform.localScale = source.transform.localScale;
                instance.transform.SetSiblingIndex( source.transform.GetSiblingIndex() );
                instance.name = source.name;
                instance.layer = source.layer;
                instance.tag = source.tag;
                instance.SetActive( source.activeSelf );
                GameObjectUtility.SetStaticEditorFlags(
                    instance, GameObjectUtility.GetStaticEditorFlags( source ) );
            }
            Clone( source, instance );

            destinationPrefab.ReferenceCollection.Add( instance, true );

            Prefab instancePrefab = instance.GetComponent<Prefab>();
            if( instancePrefab != null )
            {
                instancePrefab.ReferenceCollection.Add( instance );

                Prefab sourcePrefab = source.GetComponent<Prefab>();
                if( sourcePrefab != null )
                    instancePrefab.CopyFrom( sourcePrefab );
            }

            instance.GetComponent<Guid>().Cache();
        }

        public void RemoveChild( GameObject child )
        {
            GameObject.DestroyImmediate( child );
        }

        private void Clone( GameObject source, GameObject destination )
        {
            Component[] sourceComponents = source.GetComponents<Component>();
            foreach( Component sourceComponent in sourceComponents )
            {
                if( sourceComponent == null )
                    continue;

                try
                {
                    CloneComponent(
                        destination,
                        sourceComponent );
                }
                catch( System.Exception e )
                {
                    if( Config.RETHROW )
                        throw e;
                    else if( Config.DEBUG_EXCEPTION )
                    {
                        Debug.LogException( e );
                    }
                }
            }
        }

        private void CloneComponent(
            GameObject destination, Component sourceComponent )
        {
            Component component =
                        destination.GetComponent( sourceComponent.GetType() );

            if( component == null )
                component = destination.AddComponent( sourceComponent.GetType() );

            if( component != null && sourceComponent != null )
                EditorUtility.CopySerialized( sourceComponent, component );
        }
    }
}