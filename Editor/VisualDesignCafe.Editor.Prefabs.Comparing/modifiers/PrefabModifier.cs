﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace VisualDesignCafe.Editor.Prefabs.Comparing
{
    using Object = UnityEngine.Object;

    internal sealed class PrefabModifier
    {
        public readonly Prefab Prefab;

        private PropertyModifier _recycledPropertyModifier =
            new PropertyModifier();

        private ComponentModifier _recycledComponentModifier =
            new ComponentModifier();

        private ObjectModifier _recycledObjectModifier =
            new ObjectModifier();

        private Dictionary<Object, SerializedObject> _targetCache =
            new Dictionary<Object, SerializedObject>();

        public PrefabModifier( Prefab prefab )
        {
            Prefab = prefab;
        }

        public void ApplyModifications(
            ModificationList modifications, bool keepPrefabOverride )
        {
            _targetCache.Clear();

            foreach( Modification modification in modifications )
            {
                // Switching of targets is not yet fully supported, so for now we will pass a value of null.
                // ApplyModification( modification, keepPrefabOverride, Prefab );
                ApplyModification( modification, keepPrefabOverride, null );
            }
        }

        public void ApplyModification(
            Modification modification, bool keepPrefabOverride, Prefab target )
        {
            if( Config.DEBUG_INFO )
                Console.Log( "Applying modification " + modification.ToString() );

            Console.Indent++;

            try
            {
                if( target != null )
                {
                    if( !modification.SetTarget( target, _targetCache ) )
                    {
                        if( Config.DEBUG_WARNING )
                            Console.LogWarning( "Failed to set target of modification." );

                        return;
                    }
                }

                switch( modification.Type )
                {
                    case ModificationType.PropertyAdded:
                        // --> Nothing to copy over, we can ignore this. 
                        // This shouldn't happen with prefabs in normal cases,
                        // unless it is a property that can be ignored (internal stuff)
                        break;
                    case ModificationType.PropertyRemoved:
                        // --> Nothing to copy over, we can ignore this
                        // This shouldn't happen with prefabs in normal cases,
                        // unless it is a property that can be ignored (internal stuff)
                        break;

                    case ModificationType.PropertyValueChanged:
                        ApplyModifiedProperty(
                            modification,
                            keepPrefabOverride );
                        break;

                    case ModificationType.ChildAdded:
                        ApplyAddedChild( modification );
                        break;

                    case ModificationType.ChildRemoved:
                        ApplyRemovedChild( modification );
                        break;

                    case ModificationType.ComponentAdded:
                        ApplyAddedComponent( modification );
                        break;

                    case ModificationType.ComponentRemoved:
                        ApplyRemovedComponent( modification );
                        break;
                }
            }
            catch( NullReferenceException e )
            {
                if( Config.RETHROW )
                    throw e;
                else if( Config.DEBUG_EXCEPTION )
                {
                    Debug.LogException( e );
                }
            }
            catch( InvalidCastException e )
            {
                if( Config.RETHROW )
                    throw e;
                else if( Config.DEBUG_EXCEPTION )
                {
                    Debug.LogException( e );
                }
            }
            finally
            {
                Console.Indent--;
            }
        }

        private void ApplyRemovedComponent( Modification modification )
        {
            _recycledComponentModifier.RemoveComponent(
                (Component) modification.DestinationObject.targetObject );
        }

        private void ApplyAddedComponent( Modification modification )
        {
            modification.DestinationObject.UpdateIfRequired();

            bool transformChanged = false;
            _recycledComponentModifier.AddComponent(
                modification.DestinationObject.targetObject.AsGameObject(),
                (Component) modification.SourceObject.targetObject,
                modification.CachedSourceComponent,
                out transformChanged );

            modification.DestinationObject.SaveModifications();
        }

        private void ApplyRemovedChild( Modification modification )
        {
            _recycledObjectModifier.RemoveChild(
                (GameObject) modification.DestinationObject.targetObject );
        }

        private void ApplyAddedChild( Modification modification )
        {
            modification.DestinationObject.UpdateIfRequired();

            _recycledObjectModifier.AddChild(
                modification.SourceObject,
                modification.DestinationObject );

            modification.DestinationObject.SaveModifications();
        }

        private void ApplyModifiedProperty(
            Modification modification,
            bool keepPrefabOverride )
        {
            if( modification.DestinationObject == null
                || modification.DestinationObject.targetObject == null )
            {
                if( Config.DEBUG_INFO )
                    Console.Log(
                        "Skipping property modification because destination component was removed." );
            }

            if( modification.DestinationProperty != null
                && modification.DestinationProperty.serializedObject != null )
            {
                modification.DestinationProperty.serializedObject.UpdateIfRequired();
            }

            _recycledPropertyModifier.ModifyProperty(
                modification.SourceProperty,
                modification.DestinationProperty,
                modification.DestinationObject,
                modification.SourceObject,
                modification.CachedSourceProperty,
                keepPrefabOverride );

            if( modification.DestinationProperty != null
                && modification.DestinationProperty.serializedObject != null )
            {
                modification.DestinationProperty.serializedObject.SaveModifications();
            }
        }
    }
}