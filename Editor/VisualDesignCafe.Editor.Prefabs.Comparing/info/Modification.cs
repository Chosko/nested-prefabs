﻿using System.Collections.Generic;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace VisualDesignCafe.Editor.Prefabs.Comparing
{

    public enum ModificationType
    {
        ComponentAdded,
        ComponentRemoved,
        ChildAdded,
        ChildRemoved,
        PropertyValueChanged,
        PropertyAdded,
        PropertyRemoved,
    }

    public sealed class Modification
    {
        public readonly ModificationType Type;
        public readonly SerializedObject SourceObject;
        public readonly SerializedProperty SourceProperty;

        public SerializedObject DestinationObject
        {
            get;
            private set;
        }

        public SerializedProperty DestinationProperty
        {
            get;
            private set;
        }

        internal readonly PropertyInfo CachedSourceProperty;
        internal readonly ObjectInfo CachedSourceObject;
        internal readonly ComponentInfo CachedSourceComponent;

        private readonly PropertyInfo CachedDestinationProperty;
        private readonly ObjectInfo CachedDestinationObject;
        private readonly ComponentInfo CachedDestinationComponent;

        internal Modification( ModificationType type, PropertyInfo destination, PropertyInfo source )
        {
            Type = type;
            CachedDestinationProperty = destination;
            CachedSourceProperty = source;

            SourceObject = source != null ? source.Component.Serialized : null;
            DestinationObject = destination != null ? destination.Component.Serialized : null;

            SourceProperty = source != null ? SourceObject.FindProperty( source.PropertyPath ) : null;
            DestinationProperty = destination != null ? DestinationObject.FindProperty( destination.PropertyPath ) : null;
        }

        internal Modification( ModificationType type, ObjectInfo destination, ObjectInfo source )
        {
            Type = type;
            CachedDestinationObject = destination;
            CachedSourceObject = source;

            SourceProperty = null;
            DestinationProperty = null;
            SourceObject = source != null ? source.Object.Serialized : null;
            DestinationObject = destination != null ? destination.Object.Serialized : null;
        }

        internal Modification( ModificationType type, ComponentInfo destination, ComponentInfo source )
        {
            Type = type;
            CachedDestinationComponent = destination;
            CachedSourceComponent = source;

            SourceProperty = null;
            DestinationProperty = null;
            SourceObject = source != null ? source.Serialized : null;
            DestinationObject = destination != null ? destination.Serialized : null;
        }

        internal bool SetTarget(
            Prefab prefab, Dictionary<Object, SerializedObject> targetCache )
        {
            if( Config.DEBUG_INFO && Config.VERBOSE )
                Console.Log(
                    "Switching target of modification to " + prefab.gameObject.ToLog() );

            if( DestinationObject == null )
                return false;

            GameObject target =
                ObjectReference.FindRelative(
                    DestinationObject.targetObject,
                    prefab.CachedTransform ).AsGameObject();

            if( target == null )
            {
                if( Config.DEBUG_WARNING )
                    Console.LogWarning( "Could not find target." );

                return false;
            }

            Component component = DestinationObject.targetObject as Component;

            if( component != null )
            {
                component =
                    ComponentUtility.GetComponentAtIndex(
                        target,
                        component.GetType(),
                        ComponentUtility.GetDuplicateComponentIndex( component ) );

                if( component == null )
                    return false;

                SetDestinationObject( component, targetCache );
            }
            else
            {
                SetDestinationObject( target, targetCache );
            }

            if( DestinationProperty != null )
            {
                DestinationProperty =
                    DestinationObject.FindProperty( DestinationProperty.propertyPath );

                if( DestinationProperty == null )
                    return false;
            }

            return true;
        }

        private void SetDestinationObject(
            Object obj, Dictionary<Object, SerializedObject> targetCache )
        {
            if( targetCache == null )
            {
                DestinationObject = new SerializedObject( obj );
            }
            else
            {
                SerializedObject serializedTarget;
                if( targetCache.TryGetValue( obj, out serializedTarget ) )
                {
                    DestinationObject = serializedTarget;
                }
                else
                {
                    DestinationObject = new SerializedObject( obj );
                    targetCache[ obj ] = DestinationObject;
                }
            }
        }

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();

            builder.Append( Type.ToString() );
            builder.Append( " " );
            builder.Append( "<b>" );

            try
            {
                switch( Type )
                {
                    case ModificationType.ChildAdded:
                        builder.Append( ( (GameObject) CachedSourceObject.Object.Serialized.targetObject ).ToLog() );
                        break;
                    case ModificationType.ChildRemoved:
                        builder.Append( ( (GameObject) CachedSourceObject.Object.Serialized.targetObject ).ToLog() );
                        break;
                    case ModificationType.ComponentAdded:
                        builder.Append( CachedSourceComponent.Serialized.targetObject.ToString() );
                        break;
                    case ModificationType.ComponentRemoved:
                        builder.Append( CachedDestinationComponent.Serialized.targetObject.ToString() );
                        break;
                    case ModificationType.PropertyAdded:
                        builder.Append( CachedSourceProperty.PropertyPath );
                        break;
                    case ModificationType.PropertyRemoved:
                        builder.Append( CachedDestinationProperty.PropertyPath );
                        break;
                    case ModificationType.PropertyValueChanged:
                        builder.Append( CachedDestinationProperty.PropertyPath );
                        builder.Append( ": " );
                        builder.Append( CachedSourceProperty.GetValueAsString() );
                        builder.Append( " -> " );
                        builder.Append( CachedDestinationProperty.GetValueAsString() );
                        break;
                }
            }
            catch( System.NullReferenceException )
            {
                builder.Append( "null" );
            }

            builder.Append( "</b>" );

            return builder.ToString();
        }
    }
}