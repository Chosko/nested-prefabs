﻿using UnityEditor;
using UnityEngine;

namespace VisualDesignCafe.Editor.Prefabs.Comparing
{

    internal enum ObjectReferenceType
    {
        /// <summary>
        /// The reference value is null.
        /// </summary>
        Null,

        /// <summary>
        /// The reference is invalid. 
        /// This can happen if the referenced object is a parent of a nested prefab 
        /// and the component containing the reference property is inside the nested prefab.
        /// </summary>
        Invalid,

        /// <summary>
        /// The referenced object is part of the prefab.
        /// </summary>
        Relative,

        /// <summary>
        /// The reference object is a prefab asset.
        /// </summary>
        Asset,

        /// <summary>
        /// The referenced object is an instance of a non-GameObject Object (ScriptableObject, Texture, Material, etc.).
        /// </summary>
        ObjectInstance,

        /// <summary>
        /// The referenced object is a non-GameObject asset (ScriptableObject, Texture, Material, etc.).
        /// </summary>
        ObjectAsset,

        /// <summary>
        /// The referenced object is an object in the scene outside of the prefab hierarchy.
        /// </summary>
        SceneObject,
    }

    internal sealed class ObjectReference
    {

        public Object Reference
        {
            get;
            private set;
        }

        public ObjectReferenceType Type
        {
            get;
            private set;
        }

        public readonly PropertyInfo PropertyInfo;

        public static Transform FindRootForRelativeReference(
            GameObject destination,
            Object source )
        {
            if( destination == null )
                return null;

            Prefab destinationPrefab =
                PrefabHierarchyUtility.GetPrefab( destination );

            if( destinationPrefab == null )
                return destination.transform;

            bool destinationIsModel =
                NestedPrefabUtility.IsModelPrefab( destinationPrefab );

            destinationPrefab =
                destinationIsModel
                    ? ( PrefabHierarchyUtility.GetParentPrefab( destinationPrefab ) ?? destinationPrefab )
                    : destinationPrefab;

            if( source.GetType() == typeof( PrefabOverrides ) )
            {
                destinationPrefab =
                    PrefabHierarchyUtility.GetParentPrefab( destinationPrefab )
                    ?? destinationPrefab;
            }

            return destinationPrefab != null
                ? destinationPrefab.transform
                : null;
        }

        public static Object FindRelative( Object objectReference, Transform root )
        {
            if( objectReference == null )
                return null;

            GameObject destinationGameObject =
                FindRelative( objectReference.AsGameObject(), root );

            if( objectReference is GameObject )
                return destinationGameObject;

            if( destinationGameObject == null )
                return null;

            return
                ComponentUtility.GetComponentAtIndex(
                    destinationGameObject,
                    objectReference.GetType(),
                    ComponentUtility.GetDuplicateComponentIndex( objectReference as Component ) );
        }

        private static GameObject FindRelative( GameObject objectReference, Transform root )
        {
            if( objectReference == null )
                return null;

            Guid guid = objectReference.GetComponent<Guid>();
            Guid[] children = root.GetComponentsInChildren<Guid>( true );

            foreach( Guid child in children )
            {
                if( Guid.HierarchyEquals( child, guid, root ) )
                    return child.gameObject;
            }

            return null;
        }

        public ObjectReference(
            PropertyInfo property, Prefab rootPrefab, Prefab nestedPrefab, Object reference )
        {
            this.PropertyInfo = property;
            this.Reference = reference;
            this.Type = GetObjectReferenceType( nestedPrefab, rootPrefab );
        }

        public ObjectReference( Prefab rootPrefab, Prefab prefab, Object reference )
        {
            this.PropertyInfo = null;
            this.Reference = reference;
            this.Type = GetObjectReferenceType( prefab, rootPrefab );
        }

        private ObjectReferenceType GetObjectReferenceType( Prefab prefab, Prefab rootPrefab )
        {
            if( Reference == null )
                return ObjectReferenceType.Null;

            if( this.PropertyInfo != null && this.PropertyInfo.PropertyPath == "m_Father" )
                return ObjectReferenceType.Relative;

            GameObject referencedGameObject = Reference.AsGameObject();

            if( referencedGameObject == null )
            {
                if( EditorUtility.IsPersistent( Reference ) )
                    return ObjectReferenceType.ObjectAsset;
                else
                    return ObjectReferenceType.ObjectInstance;
            }

            Transform tranform = referencedGameObject.transform;
            Transform nestedPrefabRoot = prefab != null ? prefab.CachedTransform : null;
            Transform prefabRoot = rootPrefab != null ? rootPrefab.CachedTransform : null;
            Prefab referencedPrefab = PrefabHierarchyUtility.GetPrefab( referencedGameObject );

            // Overrides can reference to the parent of the nested prefab.
            // We have to adjust the root for this.
            if( PropertyInfo != null
                && prefab != null
                && PropertyInfo.Component.Type == typeof( PrefabOverrides ) )
            {
                Prefab parentPrefab = PrefabHierarchyUtility.GetParentPrefab( prefab );
                if( parentPrefab != null )
                    nestedPrefabRoot = parentPrefab.CachedTransform;
            }

            // If the current prefab and the referenced prefab share the same asset 
            // then the reference must be a relative reference. 
            // (or to the asset itself, but in that case we can handle it as a relative reference)
            if( referencedPrefab != null && prefab != null )
            {
                if( referencedPrefab.AssetGuid == prefab.AssetGuid )
                    return ObjectReferenceType.Relative;
            }

            while( tranform != null )
            {
                // If one of the parents of the referenced object is the nested prefab 
                // then it is a relative reference within the nested prefab.
                if( ( tranform == nestedPrefabRoot && nestedPrefabRoot != null ) )
                {
                    return ObjectReferenceType.Relative;
                }

                // If one of the parents of the referenced object is the root of the prefab 
                // hierarchy and not the nested prefab then the referenced object is invalid. 
                // This is because the reference can't be saved when saving the nested prefab 
                // because the referenced object does not exist in its scope.
                if( tranform == prefabRoot && prefabRoot != null )
                {
                    return ObjectReferenceType.Invalid;
                }

                tranform = tranform.parent;
            }

            // The referenced object is not part of prefab hierarchy. 
            // Check if the referenced object is an asset, if it is not then it must be an object in the scene.
            return EditorUtility.IsPersistent( referencedGameObject )
                ? ObjectReferenceType.Asset
                : ObjectReferenceType.SceneObject;
        }
    }
}