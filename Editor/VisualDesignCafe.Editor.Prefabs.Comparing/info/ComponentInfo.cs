﻿using System;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace VisualDesignCafe.Editor.Prefabs.Comparing
{
    using Object = UnityEngine.Object;

    /// <summary>
    /// Contains all (serialized) data of a Component.
    /// </summary>
    internal sealed class ComponentInfo
    {

        /// <summary>
        /// Total number of milliseconds it took to cache the component.
        /// </summary>
        public long CacheTime
        {
            get;
            private set;
        }

        /// <summary>
        /// Should this component be saved in the parent prefab?
        /// </summary>
        public readonly bool SaveInParent;

        /// <summary>
        /// Custom processors for this component.
        /// </summary>
        public readonly Postprocessor[] Processors;

        /// <summary>
        /// Component index.
        /// </summary>
        public readonly int Index;

        /// <summary>
        /// The object that this component is attached to.
        /// </summary>
        public ObjectInfo Object
        {
            get;
            private set;
        }

        /// <summary>
        /// The type of this component.
        /// </summary>
        public Type Type
        {
            get;
            private set;
        }

        /// <summary>
        /// SerializedObject for the component.
        /// </summary>
        public SerializedObject Serialized
        {
            get;
            private set;
        }

        /// <summary>
        /// List of properties in this component.
        /// </summary>
        public PropertyInfo[] Properties
        {
            get;
            private set;
        }

        public DrivenTransformProperties DrivenProperties
        {
            get;
            private set;
        }

        /// <summary>
        /// List of ignored components and properties.
        /// </summary>
        public readonly IgnoreList IgnoreList;

        /// <summary>
        /// Creates a new class holding cached information about the component.
        /// </summary>
        public ComponentInfo(
            int index,
            Object target,
            ObjectInfo objectInfo,
            IgnoreList ignoreList )
        {
            var stopwatch = new System.Diagnostics.Stopwatch();
            stopwatch.Start();
            {
                IgnoreList = ignoreList;
                Object = objectInfo;
                Type = target.GetType();
                Index = index;
                Serialized = new SerializedObject( target );

                CacheDrivenProperties( target, Properties );

                SaveInParent =
                    target
                    .GetType()
                    .GetCustomAttributes( typeof( SaveInParent ), true )
                    .Length > 0;

                Processors =
                    Postprocessor.GetCachedPostprocessorsForType( Type );

                Properties = GatherProperties( Serialized, ignoreList );
            }
            stopwatch.Stop();
            CacheTime = stopwatch.ElapsedMilliseconds;
        }

        /// <summary>
        /// Checks if this component equals the other component.
        /// </summary>
        /// <param name="other"></param>
        /// <param name="comparison"></param>
        /// <param name="ignoreList"></param>
        /// <returns>True if the components are equal.</returns>
        public bool Equals(
            ComponentInfo other,
            PrefabComparison comparison )
        {
            return new ComponentComparer().AreEqual( this, other, comparison );
        }

        /// <summary>
        /// Compares the component to the other component.
        /// </summary>
        /// <param name="other"></param>
        /// <param name="modificationCallback"></param>
        /// <param name="comparison"></param>
        /// <param name="ignoreList"></param>
        /// <param name="shouldExit"></param>
        public ModificationList Compare(
            ComponentInfo other,
            Func<Modification, bool> modificationCallback,
            PrefabComparison comparison,
            out bool shouldExit )
        {
            var comparer = new ComponentComparer()
            {
                ModificationCallback = modificationCallback,
            };

            return
                comparer.Compare(
                    this,
                    other,
                    comparison,
                    out shouldExit );
        }

        private PropertyInfo[] GatherProperties(
            SerializedObject component, IgnoreList ignoreList )
        {
            return
                ComponentLayout.GetCachedLayout( component, ignoreList, true )
                    .GetProperties( this, ignoreList );
        }

        private void CacheDrivenProperties(
            Object target, PropertyInfo[] properties )
        {
            var rectTransform = target as RectTransform;
            if( rectTransform == null )
                return;

            DrivenProperties =
                (DrivenTransformProperties) typeof( RectTransform )
                    .GetProperty(
                        "drivenProperties",
                        BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic )
                    .GetValue( rectTransform, null );
        }
    }
}
