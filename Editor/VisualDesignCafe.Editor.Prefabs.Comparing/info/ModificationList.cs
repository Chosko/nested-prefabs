﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace VisualDesignCafe.Editor.Prefabs.Comparing
{
    public class ModificationList : IEnumerable<Modification>
    {

        public static explicit operator Modification[] ( ModificationList list )
        {
            return list._isSorted
                ? list._sortedModifications
                : list._modifications.ToArray();
        }

        private static readonly List<ModificationType> _modificationSortOrder = new List<ModificationType>()
        {
            ModificationType.ChildAdded,
            ModificationType.ComponentRemoved,
            ModificationType.ComponentAdded,
            ModificationType.PropertyAdded,
            ModificationType.PropertyValueChanged,
            ModificationType.PropertyRemoved,
            ModificationType.ChildRemoved
        };

        public int Count
        {
            get
            {
                return _isSorted
                    ? _sortedModifications.Length
                    : _modifications.Count;
            }
        }

        public Modification this[ int index ]
        {
            get
            {
                return _isSorted
                    ? _sortedModifications[ index ]
                    : _modifications[ index ];
            }
        }

        private List<Modification> _modifications = new List<Modification>();
        private Modification[] _sortedModifications = new Modification[ 0 ];
        private bool _isSorted = false;

        public ModificationList()
        { }

        public ModificationList( IEnumerable<Modification> modifications )
        {
            AddRange( modifications );
        }

        public void Add( Modification modification )
        {
            _modifications.Add( modification );
            _isSorted = false;
        }

        public void AddRange( IEnumerable<Modification> modifications )
        {
            _modifications.AddRange( modifications );
            _isSorted = false;
        }

        public bool ContainsHierarchyModifications()
        {
            return _sortedModifications.Any(
                m =>
                {
                    return
                        m.Type == ModificationType.ChildAdded
                        || m.Type == ModificationType.ChildRemoved
                        || ( m.Type == ModificationType.PropertyValueChanged
                            && m.SourceProperty != null
                            && m.SourceProperty.propertyPath == "m_Father" );
                } );
        }

        public IEnumerator<Modification> GetEnumerator()
        {
            return ( ( _isSorted
                ? (IEnumerable<Modification>) _sortedModifications
                : (IEnumerable<Modification>) _modifications ) ).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ( ( _isSorted
                ? (IEnumerable<Modification>) _sortedModifications
                : (IEnumerable<Modification>) _modifications ) ).GetEnumerator();
        }

        public void Sort()
        {
            if( _isSorted )
                return;

            if( _modifications == null || _modifications.Count == 0 )
            {
                _sortedModifications = new Modification[ 0 ];
                return;
            }

            _isSorted = true;

            var modifications = new List<Modification>();
            modifications.AddRange( _modifications );

            for( int i = modifications.Count - 1; i > -1; i-- )
            {
                // Remove all children of gameobjects that have been removed. 
                // If we destroy the parent all its children will be destroyed automatically.
                if( modifications[ i ].Type == ModificationType.ChildRemoved )
                {
                    GameObject target =
                        (GameObject) modifications[ i ]
                            .CachedSourceObject
                            .Object
                            .Serialized
                            .targetObject;

                    Transform parent = target.transform.parent;

                    if( modifications.Any(
                        m =>
                            m.Type == ModificationType.ChildRemoved
                            && ( (GameObject) m.CachedSourceObject.Object.Serialized.targetObject ).transform == parent ) )
                    {
                        modifications.RemoveAt( i );
                    }

                    modifications.RemoveAll(
                        m =>
                        {
                            if( m == null )
                                return true;

                            if( m.Type == ModificationType.PropertyValueChanged
                                && m.DestinationProperty != null )
                            {
                                Component component =
                                    m.DestinationProperty.serializedObject != null
                                        ? m.DestinationProperty.serializedObject.targetObject as Component
                                        : null;

                                if( component != null && component.gameObject == target )
                                    return true;
                            }

                            return false;
                        } );

                    // Make sure the index is not out of bounds after removing items.
                    i = Mathf.Min( i, modifications.Count );
                }
                else if( modifications[ i ].Type == ModificationType.ComponentRemoved )
                {
                    Component target = (Component) modifications[ i ].DestinationObject.targetObject;

                    // Transform and RectTransform components should never be removed. 
                    // Instead they are replaced with an AddComponent modification.
                    if( target.GetType() == typeof( Transform ) || target.GetType() == typeof( RectTransform ) )
                    {
                        modifications.RemoveAt( i );
                    }
                    else
                    {
                        modifications.RemoveAll(
                            m =>
                            {
                                if( m == null )
                                    return true;

                                if( m.Type == ModificationType.PropertyValueChanged
                                    && m.DestinationProperty != null )
                                {
                                    Component component =
                                        m.DestinationProperty.serializedObject != null
                                            ? m.DestinationProperty.serializedObject.targetObject as Component
                                            : null;

                                    if( component != null && component == target )
                                        return true;
                                }

                                return false;
                            } );
                    }

                    // Make sure the index is not out of bounds after removing items.
                    i = Mathf.Min( i, modifications.Count );
                }
            }

            _sortedModifications = modifications
                .OrderBy(
                    m => _modificationSortOrder.IndexOf( m.Type ) * 10000

                        // Move removed children to the top based on their depth,
                        // this way the children with the highest depth will be deleted first.
                        // Otherwise all children will be deleted when a parent object was deleted
                        // and we will end up with a bunch of NullReferenceExceptions for the children.
                        - ( m.Type == ModificationType.ChildRemoved
                            ? GetDepth( m.CachedSourceObject.PrefabTransform, (GameObject) m.CachedSourceObject.Object.Serialized.targetObject )
                            : 0 ) * 100

                        // Move added children to the end based on their depth,
                        // this way the children with the lowest depth will be added first.
                        // Otherwise the parent won't exist when its child is added first.
                        + ( m.Type == ModificationType.ChildAdded
                            ? GetDepth( m.CachedSourceObject.PrefabTransform, (GameObject) m.CachedSourceObject.Object.Serialized.targetObject )
                            : 0 ) * 100

                        // Move children with a higher sibling index to the end of the list, 
                        // if we add them first then there are not enough children on the new object 
                        // to set the correct sibling index and the object will become corrupt.
                        + ( m.Type == ModificationType.ChildAdded
                            ? ( (GameObject) m.CachedSourceObject.Object.Serialized.targetObject ).transform.GetSiblingIndex()
                            : 0 )

                        // Move Root Order changes all the way to the end of the list. We have to make sure the entire hierarchy
                        // is correct before making any changes to the order.
                        + ( m.Type == ModificationType.PropertyValueChanged && m.CachedSourceProperty.PropertyPath == "m_RootOrder"
                            ? 100000
                            : 0 )

                        // Order the parenting changes based on the depth of the GameObject. Top parents should be changed first.
                        + ( m.Type == ModificationType.PropertyValueChanged && m.CachedSourceProperty.PropertyPath == "m_Father"
                            ? GetDepth( m.CachedSourceProperty.Component.Object.PrefabTransform, ( (Component) m.CachedSourceProperty.Component.Serialized.targetObject ).gameObject ) * 100
                            : 0 )

                        // Move object reference changes to the end of the property changes.
                        + ( m.Type == ModificationType.PropertyValueChanged && m.CachedSourceProperty.PropertyType == SerializedPropertyType.ObjectReference
                            ? 100
                            : 0 )

                        // Move objects with a higher component dependency back in the queue. This way the required components are added first.
                        + ( m.Type == ModificationType.ComponentAdded
                            ? GetRequiredComponentSum( m.CachedSourceComponent.Serialized.targetObject as Component )
                            : 0 )

                        // Move reordering of components to end of the queue.
                        + ( m.Type == ModificationType.PropertyValueChanged && m.CachedSourceProperty.PropertyPath == "m_ComponentIndex"
                            ? 100000
                            : 0 )

                    ).ToArray();
        }

        private int GetDepth( Transform root, GameObject gameObject )
        {
            if( gameObject == null )
                return -1;

            int depth = 0;
            Transform parent = gameObject.transform;
            while( parent != null && parent != root )
            {
                depth++;
                parent = parent.parent;
            }

            return depth;
        }

        private int GetRequiredComponentSum( Component component )
        {
            if( component == null )
                return 0;

            return GetRequiredComponentSum( component.GetType() );
        }

        private int GetRequiredComponentSum( System.Type component )
        {
            RequireComponent[] attributes =
                component.GetCustomAttributes( typeof( RequireComponent ), true ) as RequireComponent[];

            if( attributes.Length == 0 )
                return 0;

            int sum = 0;

            foreach( RequireComponent attribute in attributes )
            {
                if( attribute.m_Type0 != null )
                    sum += GetRequiredComponentSum( attribute.m_Type0 ) + 1;

                if( attribute.m_Type1 != null )
                    sum += GetRequiredComponentSum( attribute.m_Type1 ) + 1;

                if( attribute.m_Type2 != null )
                    sum += GetRequiredComponentSum( attribute.m_Type2 ) + 1;
            }

            return sum;
        }
    }
}