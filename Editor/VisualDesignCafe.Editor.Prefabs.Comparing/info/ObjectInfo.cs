﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace VisualDesignCafe.Editor.Prefabs.Comparing
{
    /// <summary>
    /// Contains all (serialized) data for a GameObject.
    /// </summary>
    internal sealed class ObjectInfo
    {

        /// <summary>
        /// The time (in ms) it took to create and cache data for this ObjectInfo.
        /// </summary>
        public long CacheTime
        {
            get;
            private set;
        }

        /// <summary>
        /// ObjectInfo class of the root GameObject in this prefab.
        /// </summary>
        public ObjectInfo PrefabInfo
        {
            get;
            private set;
        }

        /// <summary>
        /// The GameObject's properties as a ComponentInfo.
        /// </summary>
        public ComponentInfo Object
        {
            get;
            private set;
        }

        /// <summary>
        /// All components on this GameObject.
        /// </summary>
        public ComponentInfo[] Components
        {
            get;
            private set;
        }

        /// <summary>
        /// All direct children of this GameObject.
        /// </summary>
        public ObjectInfo[] Children
        {
            get;
            private set;
        }

        /// <summary>
        /// All children in the hierarchy of this GameObject.
        /// </summary>
        public ObjectInfo[] CachedChildren
        {
            get { return _cachedChildren; }
        }

        /// <summary>
        /// The root transform of the prefab this GameObject is part of.
        /// </summary>
        public Transform PrefabTransform
        {
            get { return ( (GameObject) this.PrefabInfo.Object.Serialized.targetObject ).transform; }
        }

        /// <summary>
        /// Should nested prefabs be included in this ObjectInfo's data?
        /// If set to false no children will be cached in nested prefabs but the nested prefab itself will still be included.
        /// </summary>
        public bool IncludeNestedChildren
        {
            get;
            private set;
        }

        /// <summary>
        /// The nested prefab component on this object. Null if the object is not a nested prefab.
        /// </summary>
        public NestedPrefab NestedPrefab
        {
            get;
            private set;
        }

        public bool IsNestedPrefab
        {
            get;
            private set;
        }

        public Prefab ParentPrefab
        {
            get;
            private set;
        }

        public Prefab ParentPrefabOrPrefab
        {
            get { return ParentPrefab ?? Prefab; }
        }

        /// <summary>
        /// The topmost prefab in the entire hierarchy.
        /// </summary>
        public Prefab RootPrefab
        {
            get;
            private set;
        }

        /// <summary>
        /// The prefab this object is a part of.
        /// </summary>
        public Prefab Prefab
        {
            get;
            private set;
        }

        /// <summary>
        /// The object GUID of this object.
        /// </summary>
        public Guid Guid
        {
            get;
            private set;
        }

        /// <summary>
        /// Is this object the root of a prefab or nested prefab?
        /// </summary>
        public bool IsRootObject
        {
            get;
            private set;
        }

        public readonly IgnoreList IgnoreList;


        /// <summary>
        /// Is the prefab being reloaded from the original model prefab?
        /// </summary>
        public readonly bool IsReloadedFromModelPrefab;

        public readonly bool IsModelPrefab;

        private ObjectInfo[] _cachedChildren;

        /// <summary>
        /// Creates a new serialized representation of the GameObject,
        /// can be used to compare GameObjects against each other to find modifications.
        /// </summary>
        public ObjectInfo(
            GameObject target,
            Transform root,
            ObjectInfo rootInfo,
            IgnoreList ignoreList,
            bool includeNestedChildren = true,
            bool isReloadedFromModelPrefab = false,
            bool isModelPrefab = false )
        {
            var stopwatch = new System.Diagnostics.Stopwatch();
            stopwatch.Start();
            {
                IgnoreList = ignoreList;
                IsModelPrefab = isModelPrefab;
                IsReloadedFromModelPrefab = isReloadedFromModelPrefab;
                PrefabInfo = rootInfo ?? this;
                IncludeNestedChildren = includeNestedChildren;

                CacheHierarchy( target, root );
                Object = new ComponentInfo( -1, target, this, ignoreList );
                GatherComponents( target );
                RefreshChildren();
            }
            stopwatch.Stop();
            this.CacheTime = stopwatch.ElapsedMilliseconds;
        }

        private void GatherComponents( GameObject source )
        {
            if( IsNestedPrefab && !IncludeNestedChildren )
            {
                Components = FindComponentsToSaveInParent( source );
            }
            else
            {
                Components = GetAllComponents( source );
            }
        }

        private ModelComponentModification GetModelModifications( GameObject source )
        {
            return IsReloadedFromModelPrefab
                ? source.GetComponent<ModelComponentModification>()
                : null;
        }

        private ComponentInfo[] GetAllComponents( GameObject source )
        {
            ModelComponentModification modelModifications =
                GetModelModifications( source );

            int componentIndex = 0;
            var componentInfoCollection = new List<ComponentInfo>();

            foreach( var component in source.GetComponents<Component>() )
            {
                if( component == null )
                {
                    if( Config.DEBUG_WARNING )
                        Console.LogWarning( "Component is null" );

                    continue;
                }

                if( IsAddedToModel( component, modelModifications ) )
                    continue;

                if( IsNotSaved( component ) )
                    continue;

                if( IsRootObject && IsSavedInParent( component ) )
                    continue;

                if( IgnoreList.IsIgnored( component.GetType() ) )
                    continue;

                if( !NestedPrefabUtility.IsInternalComponent( component.GetType() ) )
                {
                    componentInfoCollection.Add(
                        new ComponentInfo( componentIndex, component, this, IgnoreList ) );
                    componentIndex++;
                }
                else
                {
                    componentInfoCollection.Add(
                        new ComponentInfo( -1, component, this, IgnoreList ) );
                }
            }

            return componentInfoCollection.ToArray();
        }

        private ComponentInfo[] FindComponentsToSaveInParent( GameObject source )
        {
            ModelComponentModification modelModifications =
                GetModelModifications( source );

            int componentIndex = 0;
            var componentInfoCollection = new List<ComponentInfo>();

            var rectTransform = source.GetComponent<RectTransform>();
            if( rectTransform != null )
            {
                componentInfoCollection.Add(
                    new ComponentInfo(
                        -1,
                        rectTransform,
                        this,
                        IgnoreList ) );
            }
            else
            {
                componentInfoCollection.Add(
                    new ComponentInfo(
                        -1,
                        source.GetComponent<Transform>(),
                        this,
                        IgnoreList ) );
            }

            foreach( var component in source.GetComponents<Component>() )
            {
                if( component == null )
                {
                    if( Config.DEBUG_WARNING )
                        Console.LogWarning( "Component is null" );

                    continue;
                }

                if( IsAddedToModel( component, modelModifications ) )
                    continue;

                if( IsNotSaved( component ) )
                    continue;

                if( !IsSavedInParent( component ) )
                    continue;

                if( IgnoreList.IsIgnored( component.GetType() ) )
                    continue;

                if( !NestedPrefabUtility.IsInternalComponent( component.GetType() ) )
                {
                    componentInfoCollection.Add(
                        new ComponentInfo( componentIndex, component, this, IgnoreList ) );

                    componentIndex++;
                }
                else
                {
                    componentInfoCollection.Add(
                        new ComponentInfo( -1, component, this, IgnoreList ) );
                }
            }

            return componentInfoCollection.ToArray();
        }

        private bool IsAddedToModel(
            Component component,
            ModelComponentModification modelModifications )
        {
            return
                IsReloadedFromModelPrefab
                    && modelModifications != null
                    && modelModifications.Contains( component );
        }

        private bool IsNotSaved( Component component )
        {
            return component.GetType().GetCustomAttributes( typeof( DontSaveInPrefab ), true ).Length > 0;
        }

        private bool IsSavedInParent( Component component )
        {
            return component.GetType().GetCustomAttributes( typeof( SaveInParent ), true ).Length > 0;
        }

        private void CacheHierarchy( GameObject source, Transform root )
        {
            Guid = source.GetComponent<Guid>();
            Prefab = PrefabHierarchyUtility.GetPrefab( source );
            NestedPrefab = source.GetComponent<NestedPrefab>();
            IsRootObject = source.transform.parent == null || source.transform == root;
            GameObject rootPrefab = PrefabHierarchyUtility.FindPrefabRoot( root.gameObject );
            RootPrefab = rootPrefab != null ? rootPrefab.GetComponent<Prefab>() : null;
            ParentPrefab = Prefab != null
                ? PrefabHierarchyUtility.GetParentPrefab( Prefab )
                : null;
            IsNestedPrefab = NestedPrefab != null && PrefabInfo != this;
        }

        private void RefreshChildren()
        {
            GameObject source = this.Object.Serialized.targetObject as GameObject;

            // Ignore child if it has been added to a model.
            if( IsReloadedFromModelPrefab
                && source.GetComponent<ModelModification>() != null )
            {
                this.Children = new ObjectInfo[ 0 ];
                _cachedChildren = new ObjectInfo[ 0 ];
            }

            // Find all children of the GameObject. Skip if the game object is a nested prefab.
            var objectInfoCollection = new List<ObjectInfo>();

            foreach( Transform child in source.transform )
            {
                if( child == null )
                {
                    if( Config.DEBUG_WARNING )
                        Console.LogWarning( "Transform child is null" );

                    continue;
                }

                if( IsReloadedFromModelPrefab
                    && child.GetComponent<ModelModification>() != null )
                {
                    continue;
                }

                objectInfoCollection.Add(
                    new ObjectInfo(
                        child.gameObject,
                        this.PrefabTransform,
                        this.PrefabInfo,
                        IgnoreList,
                        IncludeNestedChildren,
                        IsReloadedFromModelPrefab,
                        IsModelPrefab ) );
            }
            this.Children = objectInfoCollection.ToArray();

            _cachedChildren = GetChildrenRecursively();
        }

        /// <summary>
        /// Checks if this object equals the other object.
        /// </summary>
        public bool Equals(
            ObjectInfo other,
            PrefabComparison flags )
        {
            var comparer = new ObjectComparer();

            return comparer.AreEqual( this, other, flags );
        }

        /// <summary>
        /// Compares this object to the other object and returns the differences, if there are any.
        /// </summary>
        public ModificationList Compare(
            ObjectInfo other,
            Func<Modification, bool> modificationCallback,
            PrefabComparison flags,
            out bool shouldExit )
        {
            var comparer = new ObjectComparer()
            {
                ModificationCallback = modificationCallback
            };

            return comparer.Compare( this, other, flags, out shouldExit );
        }

        private ObjectInfo[] GetChildrenRecursively()
        {
            List<ObjectInfo> children = new List<ObjectInfo>();
            children.AddRange( Children );

            foreach( ObjectInfo child in Children )
            {
                if( !IncludeNestedChildren && child.NestedPrefab != null )
                    continue;

                children.AddRange( child.GetChildrenRecursively() );
            }

            return children.ToArray();
        }
    }
}