﻿using System;
using System.Collections.Generic;
using UnityEditor;

namespace VisualDesignCafe.Editor.Prefabs.Comparing
{
    public class ComponentLayout
    {
        private static Dictionary<Type, ComponentLayout> _layoutCollection =
            new Dictionary<Type, ComponentLayout>();

        public readonly IgnoreList IgnoreList;

        /// <summary>
        /// Collection of serialized properties in the component.
        /// </summary>
        private HashSet<string> _properties = new HashSet<string>();

        /// <summary>
        /// Gets the Component Layout for the given serialized component.
        /// </summary>
        /// <param name="component">Serialized component or gameobject.</param>
        /// <param name="createIfNull"></param>
        /// <returns></returns>
        internal static ComponentLayout GetCachedLayout(
            SerializedObject component,
            IgnoreList ignoreList,
            bool createIfNull = true )
        {
            var type = component.targetObject.GetType();
            ComponentLayout layout;

            if( _layoutCollection.TryGetValue( type, out layout ) )
            {
                if( layout.IgnoreList == ignoreList )
                {
                    return layout;
                }
                else
                {
                    return CreateLayout( component, ignoreList );
                }
            }

            return createIfNull
                ? CreateLayout( component, ignoreList )
                : null;
        }

        private static ComponentLayout CreateLayout(
            SerializedObject component,
            IgnoreList ignoreList )
        {
            var layout = new ComponentLayout( component, ignoreList );
            _layoutCollection[ component.targetObject.GetType() ] = layout;
            return layout;
        }

        /// <summary>
        /// Creates a new Component Layout for the serialized component.
        /// </summary>
        public ComponentLayout( SerializedObject component, IgnoreList ignoreList )
        {
            IgnoreList = ignoreList;
            SerializedProperty property = component.GetIterator();

            bool enterChildren = true;
            while( property.Next( enterChildren ) )
            {
                enterChildren = false;

                if( !ignoreList.IsIgnored(
                        property.propertyPath,
                        component.targetObject ) )
                {
                    _properties.Add( property.propertyPath );
                }

                if( property.hasChildren )
                {
                    if( property.isArray )
                    {
                        enterChildren = false;
                    }
                    else
                    {
                        enterChildren = true;
                    }
                }
                else
                {
                    enterChildren = false;
                }
            }
        }

        /// <summary>
        /// Gets all properties in the component.
        /// </summary>
        /// <param name="componentInfo"></param>
        /// <param name="component"></param>
        /// <returns></returns>
        internal PropertyInfo[] GetProperties(
            ComponentInfo componentInfo,
            IgnoreList ignoreList )
        {
            SerializedObject component = componentInfo.Serialized;
            List<PropertyInfo> properties = new List<PropertyInfo>();

            SerializedProperty p = component.GetIterator();

            bool didNext = false;
            while( didNext || p.Next( true ) )
            {
                didNext = false;

                if( !_properties.Contains( p.propertyPath ) )
                {
                    continue;
                }

                properties.Add( new PropertyInfo( componentInfo, p ) );

                if( p.isArray && p.propertyType != SerializedPropertyType.String )
                {
                    // add array elements.
                    int depth = p.depth;
                    bool enterChildren = true;
                    bool hasEnteredChildren = false;
                    bool didBreak = false;
                    while( p.Next( enterChildren ) )
                    {
                        if( p.depth > depth )
                            hasEnteredChildren = true;

                        if( p.depth <= depth && hasEnteredChildren )
                        {
                            didBreak = true;
                            break;
                        }

                        enterChildren = false;

                        if( !ignoreList.IsIgnored(
                                p.propertyPath, component.targetObject ) )
                        {
                            properties.Add(
                                new PropertyInfo( componentInfo, p ) );
                        }

                        if( p.hasChildren )
                        {
                            if( p.isArray && p.propertyType == SerializedPropertyType.String )
                            {
                                enterChildren = false;
                            }
                            else
                            {
                                enterChildren = true;
                            }
                        }
                        else
                        {
                            enterChildren = false;
                        }
                    }

                    // We passed all properties without breaking. 
                    // So we should exit the loop to prevent the "The operation is not possible when moved past all properties (Next returned false)" exception from Unity.
                    if( !didBreak )
                        break;
                    else
                        didNext = true;
                }
            }

            return properties.ToArray();
        }
    }
}