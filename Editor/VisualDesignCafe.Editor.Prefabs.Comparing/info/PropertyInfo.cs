﻿using System;
using UnityEditor;
using UnityEngine;

namespace VisualDesignCafe.Editor.Prefabs.Comparing
{
    using EqualityComparer = Func<PropertyInfo, PrefabComparison, bool>;

    /// <summary>
    /// Contains the (serialized) data of a single property
    /// </summary>
    internal sealed class PropertyInfo
    {
        public readonly ComponentInfo Component;
        public readonly SerializedPropertyType PropertyType;
        public readonly string PropertyPath;
        public readonly string DisplayName;
        public readonly bool PrefabOverride;
        public readonly bool IsArray;
        public readonly int ArraySize;
        public readonly ObjectReference ObjectReference;

        public EqualityComparer CachedEquals
        {
            get
            {
                return _equalityComparer;
            }
        }

        public System.Object Value
        {
            get;
            private set;
        }

        private readonly SerializedProperty _serialized;
        private EqualityComparer _equalityComparer;

        public static bool GetIsArray( SerializedProperty property )
        {
            if( !property.isArray )
                return false;

            if( property.propertyType != SerializedPropertyType.Generic )
                return false;

            if( property.propertyPath == "_referenceCollection._collection" )
                return false;

            return true;
        }

        public PropertyInfo(
            ComponentInfo component,
            string path,
            object value )
        {
            Component = component;
            _serialized = null;
            PropertyType = SerializedPropertyType.Integer;
            PropertyPath = path;
            DisplayName = path;
            PrefabOverride = false;
            IsArray = false;
            Value = value;
        }

        public PropertyInfo(
            ComponentInfo component,
            SerializedProperty property )
        {
            Component = component;
            _serialized = property;
            PropertyType = property.propertyType;
            PropertyPath = property.propertyPath;
            DisplayName = property.displayName;
            PrefabOverride = property.prefabOverride;
            IsArray = GetIsArray( property );

            CacheEqualityComparer();

            if( property.propertyType == SerializedPropertyType.ObjectReference )
            {
                ObjectReference =
                    new ObjectReference(
                        this,
                        component.Object.RootPrefab,
                        component.Object.IsModelPrefab
                            ? component.Object.ParentPrefabOrPrefab
                            : component.Object.Prefab,
                        _serialized.objectReferenceValue );
            }

            if( IsArray )
            {
                ArraySize = property.arraySize;
            }
        }

        public string GetValueAsString()
        {
            if( Value == null )
                return "null";

            if( PropertyType == SerializedPropertyType.ObjectReference
                && Value as GameObject != null )
            {
                return ( (GameObject) Value ).ToLog();
            }

            return Value.ToString();
        }

        public bool Equals(
            PropertyInfo otherProperty,
            PrefabComparison comparison )
        {
            var comparer = new PropertyComparer();

            return comparer.AreEqual( this, otherProperty, comparison );
        }

        /// <summary>
        /// Returns the root order, reduced by the number of ignored children in its parent's transform.
        /// </summary>
        public int SubtractIgnoredFromRootOrder()
        {
            GameObject gameObject =
                Component.Object.Object.Serialized.targetObject as GameObject;
            Transform parent = gameObject.transform.parent;

            int index = (int) this.Value;

            if( parent == null )
                return index;

            for( int i = 0; i < index; i++ )
            {
                var modelModification =
                    parent.GetChild( i ).GetComponent<ModelModification>();

                if( modelModification != null )
                    index--;
            }

            return index;
        }

        private void CacheEqualityComparer()
        {
            object value;
            _equalityComparer =
                PropertyComparer.CreateEqualityComparer(
                    this,
                    _serialized,
                    out value );
            Value = value ?? Value;
        }
    }
}
