﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace VisualDesignCafe.Editor.Prefabs.Comparing
{
    [Flags]
    public enum IgnoreLists
    {
        Components = 1,
        ComponentProperties = 2,
        GlobalProperties = 4,
    }

    public class IgnoreList
    {
        private HashSet<string> _ignoredComponents =
            new HashSet<string>();

        private HashSet<string> _ignoredPaths =
            new HashSet<string>();

        private List<Wildcard> _ignoredWildcards =
            new List<Wildcard>();

        private Dictionary<string, HashSet<string>> _ignoredPropertiesByComponent =
            new Dictionary<string, HashSet<string>>();

        public void AddComponent( Type type )
        {
            _ignoredComponents.Add( type.FullName );
        }

        public void AddComponent( Component component )
        {
            AddComponent( component.GetType() );
        }

        public void AddProperty( string propertyPath )
        {
            _ignoredPaths.Add( propertyPath );
        }

        public void AddProperty( string propertyPath, Type component )
        {
            AddProperty( propertyPath, component.FullName );
        }

        public void AddProperty( string propertyPath, string component )
        {
            if( !_ignoredPropertiesByComponent.ContainsKey( component ) )
                _ignoredPropertiesByComponent[ component ] =
                    new HashSet<string>();

            _ignoredPropertiesByComponent[ component ].Add( propertyPath );
        }

        public void AddProperty( Wildcard wildCard )
        {
            _ignoredWildcards.Add( wildCard );
        }

        public bool IsIgnored( Component component )
        {
            return IsIgnored( component.GetType() );
        }

        public bool IsIgnored( Type component )
        {
            return ContainsComponent( component.FullName );
        }

        public bool IsIgnored(
            string propertyPath,
            UnityEngine.Object component,
            IgnoreLists lists =
                IgnoreLists.Components
                | IgnoreLists.ComponentProperties
                | IgnoreLists.GlobalProperties )
        {
            return IsIgnored(
                propertyPath,
                component != null
                    ? component.GetType()
                    : null,
                lists );
        }

        public bool IsIgnored(
            string propertyPath,
            Type component,
            IgnoreLists lists =
                IgnoreLists.Components
                | IgnoreLists.ComponentProperties
                | IgnoreLists.GlobalProperties )
        {
            while( component != null )
            {
                if( FlagsUtility.IsSet( lists, IgnoreLists.Components ) )
                {
                    if( IsIgnored( component ) )
                        return true;
                }

                if( FlagsUtility.IsSet( lists, IgnoreLists.ComponentProperties ) )
                {
                    HashSet<string> ignoredProperties;
                    if( _ignoredPropertiesByComponent.TryGetValue(
                            component.FullName,
                            out ignoredProperties ) )
                    {
                        if( ignoredProperties.Contains( propertyPath ) )
                            return true;
                    }
                }

                component = component.BaseType;
            }

            if( FlagsUtility.IsSet( lists, IgnoreLists.GlobalProperties ) )
            {
                if( _ignoredPaths.Contains( propertyPath ) )
                    return true;

                foreach( var wildCard in _ignoredWildcards )
                {
                    if( wildCard.IsMatch( propertyPath ) )
                        return true;
                }
            }

            return false;
        }

        private bool ContainsComponent( string name )
        {
            return _ignoredComponents.Contains( name );
        }
    }
}