﻿using System;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace VisualDesignCafe.Editor.Prefabs.Building
{
    internal class AssetBackup
    {
        public const string DEFAULT_LOCATION = "Library/PrefabDatabase";

        public readonly string Location;

        /// <summary>
        /// Creates a new Prefab Asset Database at the given location.
        /// </summary>
        /// <param name="location">Directory for the asset backup.</param>
        public AssetBackup( string location )
        {
            Location = location.Trim( '/', '\\' );
        }

        /// <summary>
        /// Exports a copy of all prefabs and models in the project to the backup location.
        /// </summary>
        /// <param name="path">Only export the assets in this folder. Pass an empty string to include all assets.</param>
        /// <param name="progressCallback"></param>
        public void Export( string path, Action<int, int, string> progressCallback )
        {
            // Make sure the path uses forward slashes.
            path = path.Replace( '\\', '/' );

            // Find all prefabs and models in the project.
            string[] prefabs =
                AssetDatabase.GetAllAssetPaths()
                    .Where(
                        p =>
                            string.IsNullOrEmpty( path )
                            || p
                                .Replace( '\\', '/' )
                                .StartsWith( path, StringComparison.OrdinalIgnoreCase ) )
                    .Where(
                        p =>
                            AssetDatabaseUtility.IsPrefabAsset( p )
                            || AssetDatabaseUtility.IsModelAsset( p ) )
                    .ToArray();

            // Loop through all prefabs and models and make a copy of 
            // the asset file in the 'Library/NestedPrefabs' folder.
            int index = 0;
            int count = prefabs.Length;

            foreach( string prefab in prefabs )
            {
                try
                {
                    string sourcePath = prefab;
                    string destinationPath = Location + "/" + AssetDatabase.AssetPathToGUID( prefab );

                    if( progressCallback != null )
                        progressCallback( index, count, sourcePath );

                    File.Copy( sourcePath, destinationPath, true );
                }
                catch( Exception e )
                {
                    Console.LogException( e );
                }

                index++;
            }
        }

        /// <summary>
        /// Imports all backup files in the backup location in the project.
        /// </summary>
        /// <param name="deleteStoredData"></param>
        /// <param name="progressCallback"></param>
        public void Import( bool deleteStoredData, Action<int, int, string> progressCallback )
        {
            string[] files =
                Directory.GetFiles( Location )
                    .Select( f => f.Replace( '\\', '/' ) )
                    .ToArray();

            NestedPrefabsPostprocessor.IsEnabled = false;

            int index = 0;
            int count = files.Length;

            foreach( string file in files )
            {
                try
                {
                    string guid = Path.GetFileName( file );
                    string destinationPath = AssetDatabase.GUIDToAssetPath( guid );

                    if( string.IsNullOrEmpty( destinationPath ) )
                    {
                        Debug.LogWarning( "Could not find path for GUID. (" + guid + ")" );
                        continue;
                    }

                    if( progressCallback != null )
                        progressCallback( index, count, destinationPath );

                    if( File.Exists( destinationPath ) )
                    {
                        FileUtil.ReplaceFile( file, destinationPath );
                    }
                    else
                    {
                        Debug.LogWarning( "Prefab was deleted. (" + destinationPath + ")" );
                    }

                    if( deleteStoredData )
                        File.Delete( file );
                }
                catch( Exception e )
                {
                    Console.LogException( e );
                }

                index++;
            }

            NestedPrefabsPostprocessor.IsEnabled = true;
        }
    }
}