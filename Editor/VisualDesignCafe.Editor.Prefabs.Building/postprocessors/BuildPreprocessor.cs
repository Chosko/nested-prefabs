﻿using System;
using System.IO;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace VisualDesignCafe.Editor.Prefabs.Building
{
    public class BuildPreprocessor
    {
        public static bool SuppressDialogs
        {
            get;
            set;
        }

        /// <summary>
        /// Preprocesses the project for a build.
        /// </summary>
        /// <returns></returns>
        public static bool OnPreprocessBuild()
        {
            return PreprocessBuild( string.Empty );
        }

        /// <summary>
        /// Removes all internal components from assets in the project.
        /// </summary>
        /// <param name="suppressDialogs"></param>
        /// <returns></returns>
        public static bool RemoveInternalComponentsFromAssets( bool suppressDialogs = false )
        {
            bool result = true;

            // Remove all components from prefabs in the project.
            AssetDatabaseUtility.ForEachPrefabInAssetDatabase(
                ( GameObject asset, int index, int count ) =>
                {
                    if( !suppressDialogs )
                    {
                        EditorUtility.DisplayProgressBar(
                            "Removing Nested Prefab data...",
                            "Checking \"" + asset.name + "\"...",
                            (float) ( index ) / (float) count );
                    }

                    Transform[] hierarchy = asset.GetComponentsInChildren<Transform>( true );

                    foreach( Transform child in hierarchy )
                    {
                        RemoveComponent<PrefabOverrides>( child.gameObject );
                        RemoveComponent<NestedPrefab>( child.gameObject );
                        RemoveComponent<Prefab>( child.gameObject );
                        RemoveComponent<Guid>( child.gameObject );
                        RemoveComponent<ModelModification>( child.gameObject );
                        RemoveComponent<ModelComponentModification>( child.gameObject );
                    }
                },
                ( exception ) => Debug.LogException( exception ) );

            // Remove all components from models in the project.
            AssetDatabaseUtility.ForEachModelInAssetDatabase(
                ( GameObject asset, int index, int count ) =>
                {
                    if( !suppressDialogs )
                    {
                        EditorUtility.DisplayProgressBar(
                            "Removing Nested Prefab data...",
                            "Checking \"" + asset.name + "\"...",
                            (float) ( index ) / (float) count );
                    }

                    Transform[] hierarchy = asset.GetComponentsInChildren<Transform>( true );

                    foreach( Transform child in hierarchy )
                    {
                        RemoveComponent<PrefabOverrides>( child.gameObject );
                        RemoveComponent<NestedPrefab>( child.gameObject );
                        RemoveComponent<Prefab>( child.gameObject );
                        RemoveComponent<Guid>( child.gameObject );
                        RemoveComponent<ModelModification>( child.gameObject );
                        RemoveComponent<ModelComponentModification>( child.gameObject );
                    }
                },
                ( exception ) => Debug.LogException( exception ) );

            if( !suppressDialogs )
                EditorUtility.ClearProgressBar();

            return result;
        }

        /// <summary>
        /// Preprocesses the project for a build.
        /// </summary>
        /// <param name="path">
        /// Only processes assets in this path. 
        /// Use string.Empty to process the entire project.
        /// </param>
        /// <returns></returns>
        private static bool PreprocessBuild( string path )
        {
            EnsureNestedPrefabsIsInitialized();

            if( BuildPostprocessor.Postprocessor == null )
            {
                if( Config.DEBUG_WARNING )
                    Debug.LogWarning( "Nested Prefabs Postprocessor does not exist." );
            }

            if( BuildUtility.IsInBuildMode() )
                return true;

            // Destroy the post processor so prefabs are not processed while importing/exporting for the build.
            BuildPostprocessor.Postprocessor.Destroy();

            AssetDatabase.SaveAssets();

            if( !HasWriteAccess() )
            {
                Debug.LogError( "Could not process project for a build. Application has no write access to 'Library/'." );
                BuildPostprocessor.Postprocessor.Create();
                return false;
            }

            if( !PreprocessBuildFull( path ) )
            {
                BuildPostprocessor.Postprocessor.Create();
                return false;
            }

            BuildUtility.SetIsPreprocessed( true );

            if( !SuppressDialogs )
                EditorUtility.ClearProgressBar();

            AssetDatabase.SaveAssets();

            return true;
        }

        /// <summary>
        /// Checks if the application has write access to the Library folder.
        /// </summary>
        /// <returns></returns>
        private static bool HasWriteAccess()
        {
            try
            {
                EnsureDirectoryExists( AssetBackup.DEFAULT_LOCATION );

                string guid = System.Guid.NewGuid().ToString();

                var stream = File.Create( AssetBackup.DEFAULT_LOCATION + "/" + guid );
                stream.Close();
                stream.Dispose();

                File.Delete( AssetBackup.DEFAULT_LOCATION + "/" + guid );
            }
            catch( UnauthorizedAccessException )
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Does a full export of the Prefab Database (makes a backup copy of every prefab and model file).
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        private static bool PreprocessBuildFull( string path )
        {
            EnsureDirectoryExists( AssetBackup.DEFAULT_LOCATION );

            var database = new AssetBackup( AssetBackup.DEFAULT_LOCATION );
            database.Export(
                path,
                ( index, count, asset ) =>
                {
                    if( !SuppressDialogs )
                        EditorUtility.DisplayProgressBar(
                            "Exporting Prefab Database",
                            asset,
                            (float) index / (float) count );
                } );

            if( !RemoveInternalComponentsFromAssets( SuppressDialogs ) )
            {
                database.Import(
                    true,
                    ( index, count, asset ) =>
                    {
                        if( !SuppressDialogs )
                            EditorUtility.DisplayProgressBar(
                                "Restoring Prefab Database",
                                asset,
                                (float) index / (float) count );
                    } );

                Debug.LogError( "Failed to remove nested prefabs data for build." );
                return false;
            }

            return true;
        }

        private static void RemoveComponent<T>( GameObject gameObject )
            where T : Component
        {
            T component = gameObject.GetComponent<T>();

            if( component != null )
                Component.DestroyImmediate( component, true );
        }

        [InitializeOnLoadMethod]
        private static void Initialize()
        {
            // Check if the project is in build mode and make sure the 
            // post processor is destroyed while the extension is in build mode.
            // We don't want to add any new components or process any prefabs.
            if( BuildUtility.IsInBuildMode() )
            {
                Debug.LogWarning( "Nested Prefabs is in build mode. Prefabs should not be edited while in build mode." );

                EditorApplication.update += EditorUpdate;
            }
        }

        private static void EditorUpdate()
        {
            if( !BuildUtility.IsInBuildMode() )
            {
                EditorApplication.update -= EditorUpdate;
                return;
            }

            if( BuildPostprocessor.Postprocessor != null )
                BuildPostprocessor.Postprocessor.Destroy();
        }

        internal static void EnsureNestedPrefabsIsInitialized()
        {
            EditorApplication.Step();

            var nestedPrefabs = FindType( "VisualDesignCafe.NestedPrefabs.NestedPrefabs" );

            if( nestedPrefabs == null )
                throw new NullReferenceException( "Could not find type 'NestedPrefabs'." );

            nestedPrefabs
                .GetMethod(
                    "Initialize",
                    BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic )
                .Invoke( null, null );
        }

        private static void EnsureDirectoryExists( string path )
        {
            if( !Directory.Exists( path ) )
                Directory.CreateDirectory( path );
        }

        private static Type FindType( string typeName )
        {
            foreach( var assembly in AppDomain.CurrentDomain.GetAssemblies() )
            {
                foreach( var type in assembly.GetTypes() )
                {
                    if( type.FullName == typeName )
                        return type;
                }
            }

            return null;
        }
    }
}