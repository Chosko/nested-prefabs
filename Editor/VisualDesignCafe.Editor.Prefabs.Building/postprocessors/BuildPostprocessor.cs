﻿using System.IO;
using UnityEditor;
using UnityEngine;

namespace VisualDesignCafe.Editor.Prefabs.Building
{
    public class BuildPostprocessor
    {
        public static NestedPrefabsPostprocessor Postprocessor
        {
            get;
            set;
        }

        public static bool SuppressDialogs
        {
            get;
            set;
        }

        public static bool OnPostprocessBuild()
        {
            EnsureNestedPrefabsIsInitialized();

            if( Postprocessor == null )
            {
                if( Config.DEBUG_WARNING )
                    Debug.LogWarning( "Nested Prefabs Postprocessor does not exist." );
            }

            if( !BuildUtility.IsInBuildMode() )
            {
                // Make sure the postprocessor is created. 
                // Not having a postprocessor corrupts the project.
                if( Postprocessor != null )
                    Postprocessor.Create();

                Debug.LogError( "Trying to postprocess the project after a build but the project was not processed by the Preprocessor before building." );
                return false;
            }

            if( !PostprocessBuildFull() )
                return false;

            AssetDatabase.Refresh( ImportAssetOptions.ForceUpdate );
            AssetDatabase.SaveAssets();

            Postprocessor.Create();
            Postprocessor.ClearPrefabDatabase();

            // GUID data for models is persistent, 
            // so we can reimport them and regenerate internal data instead 
            // of having to make a backup for each model.
            ModelPostprocessor.SkipPreProcessor = true;
            NestedPrefabsPostprocessor.IsEnabled = false;
            {
                AssetDatabase.StartAssetEditing();
                {
                    AssetDatabaseUtility.ForEachModelInAssetDatabase(
                        ( string path, int index, int count ) =>
                        {
                            if( !SuppressDialogs )
                            {
                                EditorUtility.DisplayProgressBar(
                                    "Restoring Nested Prefab data...",
                                    "Restoring \"" + path + "\"...",
                                    (float) ( index ) / (float) count );
                            }

                            AssetDatabase.ImportAsset( path, ImportAssetOptions.ForceUpdate );
                        },
                        ( e ) =>
                        {
                            Debug.LogException( e );
                        } );
                }
                AssetDatabase.StopAssetEditing();

                AssetDatabase.Refresh( ImportAssetOptions.ForceUpdate );
                AssetDatabase.SaveAssets();
            }
            NestedPrefabsPostprocessor.IsEnabled = true;
            ModelPostprocessor.SkipPreProcessor = false;

            BuildUtility.SetIsPreprocessed( false );
            EditorUtility.ClearProgressBar();

            return true;
        }

        /// <summary>
        /// Restores the prefabs by copying the backup files back into the project.
        /// </summary>
        /// <returns></returns>
        private static bool PostprocessBuildFull()
        {
            EnsureDirectoryExists( AssetBackup.DEFAULT_LOCATION );

            AssetDatabase.SaveAssets();

            var database = new AssetBackup( AssetBackup.DEFAULT_LOCATION );
            database.Import(
                true,
                ( int index, int count, string path ) =>
                {
                    if( !SuppressDialogs )
                        EditorUtility.DisplayProgressBar(
                            "Importing Prefab Database",
                            path,
                            (float) index / (float) count );
                } );

            if( !SuppressDialogs )
                EditorUtility.ClearProgressBar();

            return true;
        }

        private static void EnsureNestedPrefabsIsInitialized()
        {
            BuildPreprocessor.EnsureNestedPrefabsIsInitialized();
        }

        private static void EnsureDirectoryExists( string path )
        {
            if( !Directory.Exists( path ) )
                Directory.CreateDirectory( path );
        }
    }
}