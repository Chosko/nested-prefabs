﻿using UnityEditor;
using UnityEngine;

namespace VisualDesignCafe.Editor.Prefabs.Building
{
    public static class BuildMenu
    {
        /// <summary>
        /// Opens the documentation for 'Making a Build' in the default internet browser.
        /// </summary>
        [MenuItem( "Assets/Nested Prefabs/Help", false, 200 )]
        public static void Help()
        {
            // TODO: Use the URL in the Nested Prefabs config file instead of a hard coded URL.
            Application.OpenURL( "https://www.visualdesigncafe.com/nestedprefabs/documentation/building/" );
        }

        /// <summary>
        /// Preprocesses the project for a build.
        /// </summary>
        [MenuItem( "Assets/Nested Prefabs/Preprocess build", false, 100 )]
        public static void PreprocessBuild()
        {
            if( !BuildUtility.PreprocessBuild() )
            {
                EditorUtility.DisplayDialog(
                    "Error",
                    "Failed to preprocess the project for a build.",
                    "Ok" );
            }
            else
            {
                EditorUtility.DisplayDialog(
                    "Done",
                    "Preprocessed the project for a build.\n\n"
                    + "The Nested Prefabs postprocessor is currently disabled and prefabs should not be modified to prevent corruption.\n\n"
                    + "Choose 'Postprocess build' from the menu after finishing building the project to restore all prefabs.",
                    "Ok" );
            }
        }

        /// <summary>
        /// Postprocesses the project after a build.
        /// </summary>
        [MenuItem( "Assets/Nested Prefabs/Postprocess build", false, 101 )]
        public static void PostprocessBuild()
        {
            if( !BuildUtility.PostprocessBuild() )
            {
                EditorUtility.DisplayDialog(
                    "Error",
                    "Failed to postprocess the project after a build.",
                    "Ok" );
            }
            else
            {
                EditorUtility.DisplayDialog(
                    "Done",
                    "Postprocessed the project after a build.\n\n"
                    + "All nested prefab connections have been restored.",
                    "Ok" );
            }
        }

        /// <summary>
        /// Checks if the project is in build mode and enables the Import prefab database option.
        /// </summary>
        /// <returns></returns>
        [MenuItem( "Assets/Nested Prefabs/Preprocess build", true, 100 )]
        private static bool ValidatePreprocessBuild()
        {
            return !BuildUtility.IsInBuildMode();
        }

        /// <summary>
        /// Checks if the project is in build mode and enables the Import prefab database option.
        /// </summary>
        /// <returns></returns>
        [MenuItem( "Assets/Nested Prefabs/Postprocess build", true, 101 )]
        private static bool ValidatePostprocessBuild()
        {
            return BuildUtility.IsInBuildMode();
        }
    }
}
