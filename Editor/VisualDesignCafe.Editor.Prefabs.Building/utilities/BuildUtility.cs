﻿using UnityEditor;
using UnityEngine;

namespace VisualDesignCafe.Editor.Prefabs.Building
{
    public static class BuildUtility
    {
        /// <summary>
        /// Checks if the project is in build mode.
        /// </summary>
        /// <returns></returns>
        public static bool IsInBuildMode()
        {
            return EditorPrefs.GetBool(
                "NestedPrefabs.IsPreprocessed."
                    + ( (System.UInt64) Application.dataPath.GetStableHashCode() ).ToString(),
                false );
        }

        /// <summary>
        /// Preprocesses the project for building with Unity Cloud Build. 
        /// Will remove all components added by the Nested Prefabs extension from the Asset Database.
        /// Does not create any backup data and does not support restoring the removed data.
        /// </summary>
        public static void PreprocessCloudBuild()
        {
            RemovePrefabDatabase();
        }

        /// <summary>
        /// Preprocesses the project for a regular build. 
        /// Will remove all components added by the Nested prefab extension from the Asset Database.
        /// Stores the removed components and all prefab connections on disk so the data can be restored after building.
        /// </summary>
        public static bool PreprocessBuild()
        {
            return BuildPreprocessor.OnPreprocessBuild();
        }

        /// <summary>
        /// Postprocesses the project after building. 
        /// Will restore all removed components and prefab connection from disk.
        /// </summary>
        public static bool PostprocessBuild()
        {
            return BuildPostprocessor.OnPostprocessBuild();
        }

        /// <summary>
        /// Sets if the current project is preprocessed for a build.
        /// </summary>
        /// <param name="isPreprocessed"></param>
        internal static void SetIsPreprocessed( bool isPreprocessed )
        {
            EditorPrefs.SetBool(
                "NestedPrefabs.IsPreprocessed."
                    + ( (System.UInt64) Application.dataPath.GetStableHashCode() ).ToString(),
                isPreprocessed );
        }

        /// <summary>
        /// Removes the prefab database from the project. 
        /// Will delete all stored data/components on assets in this project.
        /// </summary>
        internal static void RemovePrefabDatabase()
        {
            NestedPrefabsPostprocessor.IsEnabled = false;
            BuildPreprocessor.RemoveInternalComponentsFromAssets( true );
            NestedPrefabsPostprocessor.IsEnabled = true;
        }
    }
}