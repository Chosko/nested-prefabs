﻿using UnityEditor;
using UnityEngine;

namespace VisualDesignCafe.Editor.Prefabs.Building
{
    public class BuildModeSceneView
    {
        private static double _time;
        private static bool _isInBuildMode = false;
        private static GUIStyle _headerStyle;
        private static GUIStyle _labelStyle;

        [InitializeOnLoadMethod]
        public static void Initialize()
        {
            SceneView.onSceneGUIDelegate -= OnSceneGui;
            SceneView.onSceneGUIDelegate += OnSceneGui;

            EditorApplication.update -= OnEditorUpdate;
            EditorApplication.update += OnEditorUpdate;
        }

        public static void Poll()
        {
            _isInBuildMode = BuildUtility.IsInBuildMode();
        }

        private static void OnEditorUpdate()
        {
            if( EditorApplication.timeSinceStartup - _time >= 1 )
            {
                _time = EditorApplication.timeSinceStartup;
                Poll();
            }
        }

        private static void OnSceneGui( SceneView sceneView )
        {
            if( !_isInBuildMode )
                return;

            if( _headerStyle == null )
            {
                _headerStyle = (GUIStyle) "NotificationBackground";
            }

            if( _labelStyle == null )
            {
                _labelStyle = new GUIStyle( EditorStyles.whiteLabel );
                _labelStyle.wordWrap = true;
                _labelStyle.alignment = TextAnchor.MiddleCenter;
                _labelStyle.normal.textColor = new Color( 1, 1, 1, .5f );
            }

            Handles.BeginGUI();
            {
                Rect rect = new Rect( 0, 0, 240, 140 );
                rect.center =
                    new Vector2(
                        sceneView.position.width * 0.5f,
                        sceneView.position.height * 0.5f );

                GUI.Box( rect, "Build Mode", _headerStyle );
                GUI.Label(
                    rect.Contract( 10, rect.height * 0.5f, 10, 10 ),
                    "Go to 'Assets/Nested Prefabs/Postprocess build' to exit build mode.",
                    _labelStyle );
            }
            Handles.EndGUI();
        }
    }
}