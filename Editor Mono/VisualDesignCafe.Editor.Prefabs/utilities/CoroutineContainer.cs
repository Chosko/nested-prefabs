﻿using UnityEngine;

namespace VisualDesignCafe.Editor
{
    public sealed class CoroutineContainer : MonoBehaviour
    {

        private static CoroutineContainer _container;

        public static CoroutineContainer GetContainer()
        {
            if( object.ReferenceEquals( _container, null ) )
            {
                _container = CoroutineContainer.FindObjectOfType<CoroutineContainer>();

                if( object.ReferenceEquals( _container, null ) )
                {
                    var container = new GameObject( "CoroutineUtility" );
                    _container = container.AddComponent<CoroutineContainer>();
                    container.hideFlags = HideFlags.HideAndDontSave;
                }
            }
            return _container;
        }
    }
}
