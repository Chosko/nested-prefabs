﻿using UnityEngine;

namespace VisualDesignCafe
{
    public static class Console
    {
        public enum Priority
        {
            Low = 0,
            Medium = 1,
            High = 2,
        }

#if DEV
        public static Priority PriorityFilter = Priority.Low;
#else
        public static Priority PriorityFilter = Priority.High;
#endif

        public static int Indent
        {
            get
            {
                return _indentCount;
            }
            set
            {
                _indentCount = Mathf.Max( value, 0 );
                _cachedIndent = GetIndent();
            }
        }

        private static int _indentCount = 0;
        private static string _cachedIndent;

        /// <summary>
        /// Logs the message to the Unity console.
        /// </summary>
        public static void Log( object message, Object context = null )
        {
            Debug.Log( string.Concat( IndentMultiLine( message ), "\n" ), context );
        }

        /// <summary>
        /// Logs the message to the Unity console if the priority is 
        /// higher than the current priority filter.
        /// </summary>
        public static void Log( object message, Priority priority )
        {
            if( (int) priority < (int) PriorityFilter )
                return;

            Log( message );
        }

        /// <summary>
        /// Logs a formatted message to the Unity console.
        /// </summary>
        public static void LogFormat( string message, params object[] args )
        {
            Debug.LogFormat( string.Concat( IndentMultiLine( message ), "\n" ), args );
        }

        /// <summary>
        /// Logs a warning message to the Unity console.
        /// </summary>
        public static void LogWarning( object message, Object context = null )
        {
            Debug.LogWarning( string.Concat( IndentMultiLine( message ), "\n" ), context );
        }

        /// <summary>
        /// Logs a warning message to the Unity console
        /// if the priority is higher than the current priority filter.
        /// </summary>
        public static void LogWarning( object message, Priority priority )
        {
            if( (int) priority < (int) PriorityFilter )
                return;

            LogWarning( message );
        }

        /// <summary>
        /// Logs a formatted warning message to the Unity console.
        /// </summary>
        public static void LogWarningFormat( string message, params object[] args )
        {
            Debug.LogWarningFormat( string.Concat( IndentMultiLine( message ), "\n" ), args );
        }

        /// <summary>
        /// Logs an error message to the Unity console.
        /// </summary>
        public static void LogError( object message, Object context = null )
        {
            Debug.LogError( string.Concat( IndentMultiLine( message ), "\n" ), context );
        }

        /// <summary>
        /// Logs an error message to the Unity console
        /// if the priority is higher than the current priority filter.
        /// </summary>
        public static void LogError( object message, Priority priority )
        {
            if( (int) priority < (int) PriorityFilter )
                return;

            LogError( message );
        }

        /// <summary>
        /// Logs a formatted error message to the Unity console.
        /// </summary>
        public static void LogErrorFormat( string message, params object[] args )
        {
            Debug.LogErrorFormat( string.Concat( IndentMultiLine( message ), "\n" ), args );
        }

        /// <summary>
        /// Logs an exception to the Unity console in the form of an error message.
        /// </summary>
        public static void LogException( System.Exception e, Object context = null )
        {
            Debug.LogException( e, context );
        }

        /// <summary>
        /// Adds an indent to all lines in the message.
        /// </summary>
        private static string IndentMultiLine( object message )
        {
            return
                string.Concat( _cachedIndent, message )
                    .Replace( "\n", "\n" + _cachedIndent );
        }

        private static string GetIndent()
        {
            return new string( ' ', Mathf.Max( _indentCount * 4, 0 ) );
        }
    }
}