﻿using System.Collections.Generic;
using UnityEngine;

namespace VisualDesignCafe.Editor.Prefabs
{
    internal static class TransformExtensions
    {
        public static string PathTo( this Transform transform, Transform target )
        {
            List<string> path = new List<string>();

            while( target != transform && target != null )
            {
                path.Add( target.name );
                target = target.parent;
            }

            path.Reverse();

            return string.Join( "/", path.ToArray() );
        }
    }
}