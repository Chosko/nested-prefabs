﻿using System;

namespace VisualDesignCafe.Editor.Prefabs
{
    /// <summary>
    /// Ignores the component when applying changes to a prefab.
    /// </summary>
    [AttributeUsage( AttributeTargets.Class, AllowMultiple = false, Inherited = true )]
    public class DontSaveInPrefab : Attribute
    { }
}