﻿using System;

namespace VisualDesignCafe.Editor.Prefabs
{
    /// <summary>
    /// Saves the component in the parent prefab instead of the prefab the component is on.
    /// Main uses are for Transform, RectTransform, PrefabOverrides.
    /// Should only be used for components on the root of a (Nested) Prefab.
    /// </summary>
    [AttributeUsage( AttributeTargets.Class, AllowMultiple = false, Inherited = true )]
    public class SaveInParent : Attribute
    { }
}