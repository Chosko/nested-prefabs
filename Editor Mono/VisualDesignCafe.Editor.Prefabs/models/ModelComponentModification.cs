﻿using System.Collections.Generic;
using UnityEngine;

namespace VisualDesignCafe.Editor.Prefabs
{
    [ExecuteInEditMode]
    // Dont save this component in a prefab. It is added and removed by the postprocessor, no matter if we apply it or not.
    [DontSaveInPrefab]
    public class ModelComponentModification : MonoBehaviour
    {
        public Component[] Components
        {
            get { return _components.ToArray(); }
        }

        public int Count
        {
            get { return _components.Count; }
        }

        [SerializeField]
        private List<Component> _components = new List<Component>();

        public void Clear()
        {
            _components.Clear();
        }

        /// <summary>
        /// Checks if the component modifications collection contains the given component.
        /// </summary>
        public bool Contains( Component component )
        {
            return _components.Contains( component );
        }

        /// <summary>
        /// Adds the component to the component modifications collection.
        /// </summary>
        public void AddComponent( Component component )
        {
            if( _components.Contains( component ) )
                return;

            _components.Add( component );
        }

        /// <summary>
        /// Removes the component from the component modifications collection.
        /// </summary>
        public bool RemoveComponent( Component component )
        {
            return _components.Remove( component );
        }

        private void OnEnable()
        {
            if( Application.isPlaying )
            {
                Component.Destroy( this );
                return;
            }

            Config.SetDefaultHideFlags( this );

            // Make sure there is only a single ModelComponentModification component on the object.
            ModelComponentModification[] components = GetComponents<ModelComponentModification>();

            if( components.Length > 1 )
            {
                Debug.LogError( "Adding multiple ModelComponentModification components is not allowed." );

                for( int i = components.Length - 1; i > 0; i-- )
                {
                    Component.DestroyImmediate( components[ i ], true );
                }
            }
        }
    }
}