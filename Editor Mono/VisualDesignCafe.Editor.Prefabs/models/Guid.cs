﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace VisualDesignCafe.Editor.Prefabs
{

    [ExecuteInEditMode]
    public sealed class Guid : MonoBehaviour
    {
        public string Value
        {
            get
            {
                return _guid;
            }
            private set
            {
                _guid = value;
            }
        }

        public Prefab PrefabOrParentPrefab
        {
            get { return Prefab ?? ParentPrefab; }
        }

        public Prefab Prefab
        {
            get
            {
                if( !_isCached || ( _hasPrefab && _prefab == null ) )
                    Cache();

                return _prefab;
            }
            private set { _prefab = value; }
        }

        public Prefab ParentPrefab
        {
            get
            {
                if( _cachedParentPrefab == null )
                    _cachedParentPrefab = FindParentPrefab();

                return _cachedParentPrefab;
            }
            private set { _cachedParentPrefab = value; }
        }

#if !DEV
        [HideInInspector]
#endif
        [SerializeField]
        private string _guid;

        private Prefab _prefab;
        private Prefab _cachedParentPrefab;
        private bool _isCached;
        private bool _hasPrefab;
        private Prefab _parentPrefab;

        public static bool operator ==( Guid a, Guid b )
        {
            if( object.ReferenceEquals( a, b ) )
            {
                return true;
            }

            if( ( (object) a == null ) || ( (object) b == null ) )
            {
                return false;
            }

            // If both GUID's are a nested prefab we have to check the prefab guid as well.
            if( a.PrefabOrParentPrefab != null
                && b.PrefabOrParentPrefab != null
                && a.PrefabOrParentPrefab.AssetGuid == b.PrefabOrParentPrefab.AssetGuid
                && !string.IsNullOrEmpty( a.PrefabOrParentPrefab.PrefabGuid )
                && !string.IsNullOrEmpty( b.PrefabOrParentPrefab.PrefabGuid )
                && a.PrefabOrParentPrefab.transform.parent != null
                && b.PrefabOrParentPrefab.transform.parent != null )
            {
                if( a.Prefab != null && b.Prefab != null )
                    return a.Prefab.PrefabGuid == b.Prefab.PrefabGuid;

                if( a.Value == b.Value
                    && a.PrefabOrParentPrefab.PrefabGuid == b.PrefabOrParentPrefab.PrefabGuid )
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            return a.Value == b.Value;
        }

        public static bool operator !=( Guid a, Guid b )
        {
            return !( a == b );
        }

        public static string NewGuid()
        {
            return System.Guid.NewGuid().ToString();
        }

        public static string[] PathTo( Transform target, Transform root = null )
        {
            if( target == null )
                throw new ArgumentNullException( "Target" );

            Guid guid = target.GetComponent<Guid>();

            if( guid == null )
                throw new InvalidOperationException( "Target does not have a GUID." );

            var path = new List<string>();
            path.Add( guid.Value );

            while( target != root && target != null )
            {
                Prefab prefab = target.GetComponent<Prefab>();

                if( prefab != null )
                    path.Add( prefab.PrefabGuid );

                target = target.parent;
            }

            path.Reverse();

            return path.Where( id => !string.IsNullOrEmpty( id ) ).ToArray();
        }

        public static Transform FindChild( Transform target, string[] path )
        {
            if( target == null )
                throw new ArgumentNullException( "Target" );

            if( path == null || path.Length == 0 )
                throw new ArgumentNullException( "Path" );

            Prefab prefab = null;
            for( int i = 0; i < path.Length - 1; i++ )
            {
                prefab =
                    FindPrefabWithGuidRecursive(
                        prefab != null ? prefab.transform : target,
                        path[ i ] );
            }

            return
                FindChildWithGuidRecursive(
                    prefab != null ? prefab.transform : target,
                    path[ path.Length - 1 ] );
        }

        private static Prefab FindPrefabWithGuidRecursive(
            Transform transform, string prefabGuid )
        {
            var prefab = transform.GetComponent<Prefab>();
            if( prefab != null )
            {
                return prefab.PrefabGuid == prefabGuid
                    ? prefab
                    : null;
            }

            foreach( Transform child in transform )
            {
                prefab = FindPrefabWithGuidRecursive( child, prefabGuid );

                if( prefab != null )
                    return prefab;
            }

            return null;
        }

        private static Transform FindChildWithGuidRecursive(
            Transform transform, string objectGuid )
        {
            var guid = transform.GetComponent<Guid>();
            if( guid != null && guid.Value == objectGuid )
            {
                return guid.transform;
            }

            foreach( Transform child in transform )
            {
                if( child.GetComponent<Prefab>() != null )
                    continue;

                Transform result = FindChildWithGuidRecursive( child, objectGuid );

                if( result != null )
                    return result;
            }

            return null;
        }

        public override bool Equals( object obj )
        {
            Guid guid = obj as Guid;
            if( (object) guid == null )
            {
                return false;
            }

            return Equals( guid );
        }

        public bool Equals( Guid guid )
        {
            if( Prefab != null && guid.Prefab != null )
            {
                return Value == guid.Value && Prefab.PrefabGuid == guid.Prefab.PrefabGuid;
            }

            return base.Equals( guid ) && Value == guid.Value;
        }

        /// <summary>
        /// Compares the GUIDs and the ID's of all parent prefabs.
        /// </summary>
        public static bool HierarchyEquals( Guid a, Guid b, Transform root )
        {
            if( object.ReferenceEquals( a, b ) )
                return true;

            if( a == null || b == null )
                return false;

            if( a != b )
            {
                return false;
            }

            if( a.transform == root || b.transform == root )
                return true;

            Prefab parentPrefabA = FindParentPrefab( a.transform );
            Prefab parentPrefabB = FindParentPrefab( b.transform );

            while( parentPrefabA != null && parentPrefabB != null )
            {
                if( parentPrefabA.CachedTransform == root
                    || parentPrefabB.CachedTransform == root )
                {
                    break;
                }

                if( parentPrefabA.PrefabGuid != parentPrefabB.PrefabGuid )
                {
                    // Ignore if we reached a root prefab.
                    if( !string.IsNullOrEmpty( parentPrefabA.PrefabGuid )
                        && !string.IsNullOrEmpty( parentPrefabB.PrefabGuid ) )
                    {
                        return false;
                    }
                }

                parentPrefabA = FindParentPrefab( parentPrefabA.CachedTransform );
                parentPrefabB = FindParentPrefab( parentPrefabB.CachedTransform );
            }

            return true;
        }

        public static Transform FindChildWithGuid( Transform transform, Guid guid )
        {
            if( guid == null )
                return null;

            var childGuid = transform.GetComponent<Guid>();
            if( childGuid == guid )
                return transform;

            var prefab = transform.GetComponent<Prefab>();
            if( prefab != null && prefab.transform != transform )
                return null;

            foreach( Transform child in transform )
            {
                Transform result = FindChildWithGuid( child, guid );
                if( result != null )
                    return result;
            }

            return null;
        }

        private static Prefab FindParentPrefab( Transform transform )
        {
            Prefab prefab = null;
            Transform parent = transform.parent;

            while( parent != null )
            {
                prefab = parent.GetComponent<Prefab>();

                if( prefab != null )
                    return prefab;

                parent = parent.parent;
            }

            return null;
        }

        public override string ToString()
        {
            return gameObject.name + " (" + Value + ")";
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public void OverrideGuid( string value )
        {
            if( Config.DEBUG_INFO && Config.VERBOSE )
                Console.LogFormat( "Override guid {0} with {1}.", Value, value );

            Value = value;
        }

        public void CreateGuid()
        {
            _guid = NewGuid();

            if( Config.DEBUG_INFO && Config.VERBOSE )
                Console.LogFormat( "Assigning new guid to {0}; {1}.", gameObject.name, _guid );
        }

        public void CopyFrom( Guid other )
        {
            Value = other.Value;
        }

        public void Cache()
        {
            _isCached = true;
            _prefab = GetComponent<Prefab>();
            _hasPrefab = _prefab != null;
        }

        private void OnEnable()
        {
            if( Application.isPlaying )
            {
                Component.Destroy( this );
                return;
            }

            Config.SetDefaultHideFlags( this );
            Cache();

            // Make sure there is only a single Prefab component on the object.
            Guid[] guids = GetComponents<Guid>();

            if( guids.Length > 1 )
            {
                Debug.LogError( "Manually adding a Guid component is not allowed to prevent corruption of prefabs." );

                Guid originalGuid =
                    guids.FirstOrDefault( g => g != null && !string.IsNullOrEmpty( g._guid ) )
                        ?? guids.FirstOrDefault( g => g != null );

                for( int i = guids.Length - 1; i > -1; i-- )
                {
                    if( guids[ i ] != originalGuid )
                        Component.DestroyImmediate( guids[ i ], true );
                }
            }
        }

        private Prefab FindParentPrefab()
        {
            Transform parent = transform.parent;
            Prefab prefab = null;

            while( parent != null )
            {
                prefab = parent.GetComponent<Prefab>();
                if( prefab != null )
                    return prefab;

                parent = parent.parent;
            }

            return null;
        }
    }
}