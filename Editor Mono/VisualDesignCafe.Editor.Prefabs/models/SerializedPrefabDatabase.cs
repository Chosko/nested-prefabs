﻿using UnityEngine;

namespace VisualDesignCafe.Editor.Prefabs
{
    public class SerializedPrefabDatabase : MonoBehaviour, ISerializationCallbackReceiver
    {
        public static SerializedPrefabDatabase Instance
        {
            get
            {
                if( _instance == null )
                {
                    _instance = FindObjectOfType<SerializedPrefabDatabase>();

                    // Search for an object in the scene with the unique name of the PrefabDatabaseCache.
                    if( _instance == null )
                    {
                        GameObject container = GameObject.Find( "SerializedPrefabDatabase.325c5b2a7434372429c2ca9d61dba58e" );

                        if( container != null )
                            _instance = container.GetComponent<SerializedPrefabDatabase>();
                    }

                    // Create a new GameObject with a cache component.
                    if( _instance == null )
                    {
                        GameObject container = new GameObject( "SerializedPrefabDatabase.325c5b2a7434372429c2ca9d61dba58e" );
                        if( container != null )
                        {
                            container.hideFlags = HideFlags.HideAndDontSave;

                            _instance = container.AddComponent<SerializedPrefabDatabase>();

                            if( Config.DEBUG_INFO )
                                Console.Log( "Created new cache" );
                        }
                    }
                }

                return _instance;
            }
        }

        private static SerializedPrefabDatabase _instance;

        public System.Action OnSerialize;
        public System.Action OnDeserialize;

        private PrefabAsset[] _serializationData;

        /// <summary>
        /// Clears the cache.
        /// </summary>
        public void Clear()
        {
            _serializationData = new PrefabAsset[ 0 ];
        }

        public void Deserialize( out PrefabAsset[] collection )
        {
            collection = _serializationData;

            if( collection == null )
                collection = new PrefabAsset[ 0 ];
        }

        public void Serialize( PrefabAsset[] collection )
        {
            _serializationData = collection;
        }

        public void OnBeforeSerialize()
        {
            if( OnSerialize != null )
                OnSerialize.Invoke();
        }

        public void OnAfterDeserialize()
        {
            if( OnDeserialize != null )
                OnDeserialize.Invoke();
        }
    }
}