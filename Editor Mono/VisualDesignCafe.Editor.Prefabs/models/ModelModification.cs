﻿using UnityEngine;

namespace VisualDesignCafe.Editor.Prefabs
{

    [ExecuteInEditMode]
    // Dont save this component in a prefab. It is added and removed by the postprocessor, no matter if we apply it or not.
    [DontSaveInPrefab]
    public class ModelModification : MonoBehaviour
    {
        private void OnEnable()
        {
            if( Application.isPlaying )
            {
                Component.Destroy( this );
                return;
            }

            Config.SetDefaultHideFlags( this );

            // Make sure there is only a single ModelModification component on the object.
            ModelModification[] components = GetComponents<ModelModification>();

            if( components.Length > 1 )
            {
                Debug.LogError( "Adding multiple ModelModification components is not allowed." );

                for( int i = components.Length - 1; i > 0; i-- )
                {
                    Component.DestroyImmediate( components[ i ], true );
                }
            }
        }
    }
}