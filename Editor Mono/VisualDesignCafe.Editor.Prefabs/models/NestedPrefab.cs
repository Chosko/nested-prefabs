﻿using UnityEngine;

#pragma warning disable 618

namespace VisualDesignCafe.Editor.Prefabs
{

    [ExecuteInEditMode]
    public sealed class NestedPrefab : MonoBehaviour, INestedPrefab
    {

        /// <summary>
        /// The internal Prefab object of the Nested Prefab.
        /// </summary>
        public Prefab Prefab
        {
            get
            {
                if( _cachedPrefab == null && this != null )
                    _cachedPrefab = GetComponent<Prefab>();

                return _cachedPrefab;
            }
        }

        /// <summary>
        /// The original prefab asset for this nested prefab.
        /// </summary>
        public GameObject Asset
        {
            get
            {
                return Prefab != null ? Prefab.Asset : null;
            }
        }

        /// <summary>
        /// The GUID of the original prefab asset for this Nested Prefab.
        /// </summary>
        public string AssetGuid
        {
            get
            {
                if( Prefab == null )
                    return string.Empty;

                return Prefab.AssetGuid;
            }
        }

        /// <summary>
        /// The (cached) GameObject of the nested prefab.
        /// </summary>
        public GameObject CachedGameObject
        {
            get
            {
                if( _cachedGameObject == null && this != null )
                    _cachedGameObject = this.gameObject;

                return _cachedGameObject;
            }
        }

        /// <summary>
        /// The (cached) Transform of the nested prefab
        /// </summary>
        public Transform CachedTransform
        {
            get
            {
                if( _cachedTransform == null && this != null )
                    _cachedTransform = transform;

                return _cachedTransform;
            }
        }

        /// <summary>
        /// Reference collection of the nested prefab containing references for all children, 
        /// linking them to the parent prefab object.
        /// </summary>
        public ReferenceCollection ReferenceCollection
        {
            get
            {
                if( Prefab == null )
                    return null;

                return Prefab.ReferenceCollection;
            }
        }

        private GameObject _cachedGameObject;
        private Transform _cachedTransform;
        private Prefab _cachedPrefab;
        private GameObject _cachedRoot;

        /// <summary>
        /// Called when the NestedPrefab component is enabled. 
        /// Should usually only be called once when the component is created or on editor startup
        /// </summary>
        private void OnEnable()
        {
            if( Application.isPlaying )
            {
                Component.Destroy( this );
                return;
            }

            Config.SetDefaultHideFlags( this );

            // Make sure there is only a single NestedPrefab component on the object.
            NestedPrefab[] nestedPrefabs = GetComponents<NestedPrefab>();
            if( nestedPrefabs.Length > 1 )
            {
                Debug.LogError( "Manually adding a Nested Prefab component is not allowed to prevent corruption of prefabs." );

                for( int i = nestedPrefabs.Length - 1; i > 0; i-- )
                {
                    Component.DestroyImmediate( nestedPrefabs[ i ], true );
                }
            }
        }
    }
}