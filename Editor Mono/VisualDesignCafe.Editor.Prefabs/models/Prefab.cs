﻿using System.Linq;
using UnityEngine;

#pragma warning disable 618

namespace VisualDesignCafe.Editor.Prefabs
{

    [ExecuteInEditMode]
    public sealed class Prefab : MonoBehaviour, IPrefab
    {

        public delegate GameObject GetAsset( string guid );
        public static GetAsset GetAssetForPrefab;

        /// <summary>
        /// Limits the prefab postprocessor to only change the GUID of Nested Prefabs up to this depth.
        /// The first Nested Prefab in the hierarchy has a depth of 1. 
        /// </summary>
        public static int PostprocessPrefabGuidDepthLimit
        {
            get { return _postprocessPrefabGuidDepthLimit; }
            set { _postprocessPrefabGuidDepthLimit = value; }
        }

        private static int _postprocessPrefabGuidDepthLimit = int.MaxValue;

        /// <summary>
        /// Reference collection containing a list of all children in the hierarchy.
        /// </summary>
        public ReferenceCollection ReferenceCollection
        {
            get
            {
                if( _referenceCollection == null )
                    _referenceCollection = new ReferenceCollection( this );
                else if( _referenceCollection.Prefab == null )
                    _referenceCollection.SetPrefab( this );

                return _referenceCollection;
            }
        }

        /// <summary>
        /// The original asset of this Prefab.
        /// </summary>
        public GameObject Asset
        {
            get
            {
                if( _cachedAsset == null || AssetGuid != _cachedAssetGuid
                    // HACK: Duplicating a prefab, breaking the connection and nesting it as a child will end up with the wrong cached asset for some reason. It will have the child of the prefab instead of the root cached. With this hack the cached asset will be refreshed if is linked to a child.
                    || _cachedAsset.transform.parent != null )
                {
                    if( GetAssetForPrefab != null )
                    {
                        _cachedAsset = GetAssetForPrefab.Invoke( AssetGuid );
                        _cachedAssetGuid = AssetGuid;
                    }
                }

                return _cachedAsset;
            }
        }

        /// <summary>
        /// The GUID of the original prefab asset.
        /// </summary>
        public string AssetGuid
        {
            get { return _assetGuid; }
        }

        /// <summary>
        /// A unique ID for this prefab.
        /// </summary>
        public string PrefabGuid
        {
            get { return _guid; }
        }

        /// <summary>
        /// A unique ID for this GameObject.
        /// </summary>
        public Guid ObjectGuid
        {
            get
            {
                if( _cachedObjectGuid == null && this != null )
                    _cachedObjectGuid = GetComponent<Guid>();

                return _cachedObjectGuid;
            }
        }

        /// <summary>
        /// The Transform component of this Prefab.
        /// </summary>
        public Transform CachedTransform
        {
            get
            {
                if( _cachedTransform == null && this != null )
                    _cachedTransform = this.transform;

                return _cachedTransform;
            }
        }

        /// <summary>
        /// The GameObject this Prefab object is part of.
        /// </summary>
        public GameObject CachedGameObject
        {
            get
            {
                if( _cachedGameObject == null && this != null )
                    _cachedGameObject = this.gameObject;

                return _cachedGameObject;
            }
        }

#if !DEV
        [HideInInspector]
#endif
        [SerializeField]
        private string _guid;

#if !DEV
        [HideInInspector]
#endif
        [SerializeField]
        private ReferenceCollection _referenceCollection;

#if !DEV
        [HideInInspector]
#endif
        [SerializeField]
        private string _assetGuid;

        private GameObject _cachedAsset;
        private string _cachedAssetGuid;
        private Guid _cachedObjectGuid;
        private Transform _cachedTransform;
        private GameObject _cachedGameObject;
        private GameObject _cachedRoot;

        public static bool IsNull( Prefab prefab )
        {
            return prefab == null;
        }

        public static bool IsMissing( Prefab prefab )
        {
            return prefab.Asset == null;
        }

        public static bool IsNullOrMissing( Prefab prefab )
        {
            return IsNull( prefab ) || IsMissing( prefab );
        }

        public void OverrideAsset( string assetGuid )
        {
            _assetGuid = assetGuid;
            _cachedAsset = null;
            _cachedAssetGuid = string.Empty;
        }

        /// <summary>
        /// Creates a new Guid for this prefab
        /// </summary>
        public void CreateGuid()
        {
            _guid = Guid.NewGuid();

            if( Config.DEBUG_INFO )
                Console.Log( "Created new GUID for prefab: " + _guid );
        }

        /// <summary>
        /// Copies the Guid from the other prefab.
        /// </summary>
        public void CopyFrom( Prefab other )
        {
            _guid = other.PrefabGuid;
        }

        /// <summary>
        /// Overrides the existing Guid with a new value.
        /// </summary>
        public void OverrideGuid( string value )
        {
            _guid = value;
        }

        /// <summary>
        /// Resets the Guid of this prefab to an empty value.
        /// </summary>
        public void ResetGuid()
        {
            _guid = string.Empty;
        }

        /// <summary>
        /// Refreshes the cached values of the prefab.
        /// </summary>
        public void Cache()
        {
            _cachedGameObject = null;
            _cachedTransform = null;
            _cachedObjectGuid = null;
            _cachedAsset = null;
        }

        public override string ToString()
        {
            return gameObject.name;
        }

        /// <summary>
        /// Called when this component is enabled.
        /// Will automatically hide the component in the inspector.
        /// </summary>
        private void OnEnable()
        {
            if( Application.isPlaying )
            {
                Component.Destroy( this );
                return;
            }

            Config.SetDefaultHideFlags( this );

            // Make sure there is only a single Prefab component on the object.
            Prefab[] prefabs = GetComponents<Prefab>();

            if( prefabs.Length > 1 )
            {
                Debug.LogError( "Manually adding a Prefab component is not allowed to prevent corruption of prefabs." );

                Prefab originalPrefab =
                    prefabs.FirstOrDefault( p => p != null && !string.IsNullOrEmpty( p._assetGuid ) )
                        ?? prefabs.FirstOrDefault( p => p != null );

                for( int i = prefabs.Length - 1; i > -1; i-- )
                {
                    if( prefabs[ i ] != originalPrefab )
                        Component.DestroyImmediate( prefabs[ i ], true );
                }
            }
        }
    }
}