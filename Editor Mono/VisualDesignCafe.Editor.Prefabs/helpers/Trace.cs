﻿using System;

namespace VisualDesignCafe
{
    public static class Trace
    {
        public static void AssertArgumentNotNull<T>( T argument, string name )
        {
            if( argument == null )
                throw new ArgumentNullException( name );
        }

        public static void AssertArgumentNotNull<T1, T2>(
            T1 arg1, string name1,
            T2 arg2, string name2 )
        {
            AssertArgumentNotNull( arg1, name1 );
            AssertArgumentNotNull( arg2, name2 );
        }

        public static void AssertArgumentNotNull<T1, T2, T3>(
            T1 arg1, string name1,
            T2 arg2, string name2,
            T3 arg3, string name3 )
        {
            AssertArgumentNotNull( arg1, name1 );
            AssertArgumentNotNull( arg2, name2 );
            AssertArgumentNotNull( arg3, name3 );
        }

        public static void AssertArgumentNotNull<T1, T2, T3, T4>(
            T1 arg1, string name1,
            T2 arg2, string name2,
            T3 arg3, string name3,
            T4 arg4, string name4 )
        {
            AssertArgumentNotNull( arg1, name1 );
            AssertArgumentNotNull( arg2, name2 );
            AssertArgumentNotNull( arg3, name3 );
            AssertArgumentNotNull( arg4, name4 );
        }

        public static void AssertArgumentNotNull<T>( T arg )
        {
            if( arg == null )
                throw new ArgumentNullException(
                    string.Format(
                        "Argument of type '{0}'.",
                        typeof( T ).Name ) );
        }

        public static void AssertArgumentNotNull<T1, T2>( T1 arg1, T2 arg2 )
        {
            AssertArgumentNotNull( arg1, 0 );
            AssertArgumentNotNull( arg2, 1 );
        }

        public static void AssertArgumentNotNull<T1, T2, T3>(
            T1 arg1, T2 arg2, T3 arg3 )
        {
            AssertArgumentNotNull( arg1, 0 );
            AssertArgumentNotNull( arg2, 1 );
            AssertArgumentNotNull( arg3, 2 );
        }

        public static void AssertArgumentNotNull<T1, T2, T3, T4>(
            T1 arg1, T2 arg2, T3 arg3, T4 arg4 )
        {
            AssertArgumentNotNull( arg1, 0 );
            AssertArgumentNotNull( arg2, 1 );
            AssertArgumentNotNull( arg3, 2 );
            AssertArgumentNotNull( arg4, 3 );
        }

        private static void AssertArgumentNotNull<T>( T argument, int index )
        {
            if( argument == null )
                throw new ArgumentNullException(
                    string.Format(
                        "Argument of type '{0}' at position {1}.",
                        typeof( T ).Name,
                        index.ToString() ) );
        }
    }
}
