﻿using UnityEngine;

namespace VisualDesignCafe.Editor.Prefabs
{
    public static class Config
    {

#if DEV
        public static bool IS_UNIT_TEST = false;
#endif

        /// <summary>
        /// When set to true an exception will be thrown instead of logging an error.
        /// Should be used when running editor tests to make the tests fail if any error occurs.
        /// </summary>
#if DEV
        public static bool STRICT = false;
#else
        public static readonly bool STRICT = false;
#endif
        /// <summary>
        /// Rethrows any caught exceptions. 
        /// Should be used when running editor tests to make sure any exception will cause the test to fail
        /// </summary>
#if DEV
        public static bool RETHROW = false;
#else
        public static readonly bool RETHROW = false;
#endif

        /// <summary>
        /// When set to true additional debug information will be included in debug logs
        /// </summary>
#if DEV
        public static bool VERBOSE = true;
#else
        public static readonly bool VERBOSE = false;
#endif

#if DEV
        public static bool DEBUG_WARNING = true;
        public static bool DEBUG_ERROR = true;
        public static bool DEBUG_INFO = true;
        public static bool DEBUG_EXCEPTION = true;

        /// <summary>
        /// When set to true tags for rich text will be logged to the console.
        /// </summary>
        public static bool USE_RICH_TEXT = true;
#else
        public static readonly bool DEBUG_WARNING = false;
        public static readonly bool DEBUG_ERROR = true;
        public static readonly bool DEBUG_INFO = false;
        public static readonly bool DEBUG_EXCEPTION = true;

        public static readonly bool USE_RICH_TEXT = false;
#endif

        // TODO: Obsolete?
#if DEV
        public static bool APPLY_CHANGES_TO_ASSET = true;
#else
        public static readonly bool APPLY_CHANGES_TO_ASSET = true;
#endif

        /// <summary>
        /// When set to true, the creation of a circular prefab reference is prevented immediately after parenting by performing an Undo.
        /// </summary>
#if DEV
        public static readonly bool PREVENT_CIRCULAR_REFERENCES = false;
#else
        public static readonly bool PREVENT_CIRCULAR_REFERENCES = false;
#endif

        // TODO: Obsolete?
        /// <summary>
        /// When set to true any changes to a prefab will be delayed by 0.5 seconds before applying them.
        /// This will give the user time to modify things like strings or input fields without the changes being applied while editing.
        /// Should be disabled for Unit Tests.
        /// </summary>
#if DEV
        public static bool DELAY_APPLY_CHANGES = false;
#else
        public static readonly bool DELAY_APPLY_CHANGES = false;
#endif

        /// <summary>
        /// When set to true all permanent prefab overrides will also be applied to the asset of the root prefab.
        /// Overrides will never be applied to the original asset.
        /// </summary>
        public static readonly bool APPLY_OVERRIDES_TO_ASSET = true;

        /// <summary>
        /// When set to true all assets in the project will be saved after applying changes to a prefab.
        /// </summary>
        public static readonly bool SAVE_ASSETS_AFTER_APPLY = false;

#if DEV
        public static readonly HideFlags DEFAULT_HIDEFLAGS = HideFlags.HideInInspector | HideFlags.NotEditable;
#else
        public static readonly HideFlags DEFAULT_HIDEFLAGS = HideFlags.HideInInspector | HideFlags.NotEditable;
#endif

        public static new string ToString()
        {
            var message = new System.Text.StringBuilder();

            message.AppendLine( "STRICT: " + STRICT );
            message.AppendLine( "RETHROW: " + RETHROW );
            message.AppendLine( "VERBOSE: " + VERBOSE );
            message.AppendLine( "DEBUG_INFO: " + DEBUG_INFO );
            message.AppendLine( "DEBUG_WARNING: " + DEBUG_WARNING );
            message.AppendLine( "DEBUG_ERROR: " + DEBUG_ERROR );
            message.AppendLine( "DEBUG_EXCEPTION: " + DEBUG_EXCEPTION );
            message.AppendLine( "USE_RICH_TEXT: " + USE_RICH_TEXT );
            message.AppendLine( "APPLY_CHANGES_TO_ASSET: " + APPLY_CHANGES_TO_ASSET );
            message.AppendLine( "DELAY_APPLY_CHANGES: " + DELAY_APPLY_CHANGES );

            return message.ToString();
        }

        public static void Print()
        {
            Console.Log( ToString() );
        }

        public static bool SetDefaultHideFlags( Object o )
        {
            bool fixedHideFlags = false;

            if( !IsSet( o.hideFlags, HideFlags.NotEditable ) && IsSet( DEFAULT_HIDEFLAGS, HideFlags.NotEditable ) )
            {
                o.hideFlags = Set( o.hideFlags, HideFlags.NotEditable );
                fixedHideFlags = true;
            }

            if( !IsSet( o.hideFlags, HideFlags.HideInInspector ) && IsSet( DEFAULT_HIDEFLAGS, HideFlags.HideInInspector ) )
            {
                o.hideFlags = Set( o.hideFlags, HideFlags.HideInInspector );
                fixedHideFlags = true;
            }

            return fixedHideFlags;
        }

        private static bool IsSet( HideFlags flags, HideFlags flag )
        {
            return ( (int) flags & (int) flag ) != 0;
        }

        private static HideFlags Set( HideFlags flags, HideFlags flag )
        {
            return (HideFlags) ( (int) flags | (int) flag );
        }
    }
}