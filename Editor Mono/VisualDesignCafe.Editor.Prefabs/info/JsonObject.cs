// TODO: Remove this class and use Newtonsoft to match with the rest of the VDC framework.

#define PRETTY		//Comment out when you no longer need to read JSON to disable pretty Print system-wide
//Using doubles will cause errors in VectorTemplates.cs; Unity speaks floats
#define USEFLOAT	//Use floats for numbers instead of doubles	(enable if you're getting too many significant digits in string output)
//#define POOLING	//Currently using a build setting for this one (also it's experimental)

#if UNITY_2 || UNITY_3 || UNITY_4 || UNITY_5
using UnityEngine;
using Debug = UnityEngine.Debug;
#endif
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using System.Text;
/*
 * http://www.opensource.org/licenses/lgpl-2.1.php
 * JSONObject class v.1.4.1
 * for use with Unity
 * Copyright Matt Schoen 2010 - 2013
 */

namespace VisualDesignCafe.Serialization
{

    public class JsonObject
    {
#if POOLING
    const int MAX_POOL_SIZE = 10000;
    public static Queue<JSONObject> releaseQueue = new Queue<JSONObject>();
#endif

        const int MAX_DEPTH = 100;
        const string INFINITY = "\"INFINITY\"";
        const string NEGINFINITY = "\"NEGINFINITY\"";
        const string NaN = "\"NaN\"";
        const string NEWLINE = "\r\n";
        public static readonly char[] WHITESPACE = { ' ', '\r', '\n', '\t', '\uFEFF', '\u0009' };
        public enum Type { NULL, STRING, NUMBER, OBJECT, ARRAY, BOOL, BAKED }
        public bool isContainer { get { return ( type == Type.ARRAY || type == Type.OBJECT ); } }
        public Type type = Type.NULL;
        public int Count
        {
            get
            {
                if( _arrayValues == null )
                    return -1;
                return _arrayValues.Count;
            }
        }

        public string Json { get { return Print(); } }


        public List<JsonObject> AsArray { get { return _arrayValues ?? new List<JsonObject>(); } }
        public int AsInt { get { return (int) _intValue; } }
        public bool AsBool { get { return _boolValue; } }
        public string AsString { get { return _stringValue; } }
        public Dictionary<string, JsonObject> AsDictionary
        {
            get
            {
                Dictionary<string, JsonObject> collection = new Dictionary<string, JsonObject>();
                for( int i = 0; i < _arrayKeys.Count; i++ )
                {
                    if( i < _arrayValues.Count )
                        collection[ _arrayKeys[ i ] ] = _arrayValues[ i ];
                    else
                        collection[ _arrayKeys[ i ] ] = new JsonObject();
                }
                return collection;
            }
        }

        private List<JsonObject> _arrayValues;
        private List<string> _arrayKeys;
        private string _stringValue;
#if USEFLOAT
        public float n;
        public float AsFloat
        {
            get
            {
                return n;
            }
        }
#else
    public double n;
    public float f {
        get {
            return (float)n;
        }
    }
#endif
        public bool useInt;
        private long _intValue;
        private bool _boolValue;
        public delegate void AddJSONContents( JsonObject self );

        public static JsonObject nullJO { get { return Create( Type.NULL ); } } //an empty, null object
        public static JsonObject obj { get { return Create( Type.OBJECT ); } }      //an empty object
        public static JsonObject arr { get { return Create( Type.ARRAY ); } }       //an empty array

        public JsonObject( Type t )
        {
            type = t;
            switch( t )
            {
                case Type.ARRAY:
                    _arrayValues = new List<JsonObject>();
                    break;
                case Type.OBJECT:
                    _arrayValues = new List<JsonObject>();
                    _arrayKeys = new List<string>();
                    break;
            }
        }
        public JsonObject( bool b )
        {
            type = Type.BOOL;
            this._boolValue = b;
        }
#if USEFLOAT
        public JsonObject( float f )
        {
            type = Type.NUMBER;
            n = f;
        }
#else
    public JSONObject(double d) {
        type = Type.NUMBER;
        n = d;
    }
#endif
        public JsonObject( int i )
        {
            type = Type.NUMBER;
            this._intValue = i;
            useInt = true;
            n = i;
        }
        public JsonObject( long l )
        {
            type = Type.NUMBER;
            _intValue = l;
            useInt = true;
            n = l;
        }
        public JsonObject( Dictionary<string, string> dic )
        {
            type = Type.OBJECT;
            _arrayKeys = new List<string>();
            _arrayValues = new List<JsonObject>();
            //Not sure if it's worth removing the foreach here
            foreach( KeyValuePair<string, string> kvp in dic )
            {
                _arrayKeys.Add( kvp.Key );
                _arrayValues.Add( CreateStringObject( kvp.Value ) );
            }
        }
        public JsonObject( Dictionary<string, JsonObject> dic )
        {
            type = Type.OBJECT;
            _arrayKeys = new List<string>();
            _arrayValues = new List<JsonObject>();
            //Not sure if it's worth removing the foreach here
            foreach( KeyValuePair<string, JsonObject> kvp in dic )
            {
                _arrayKeys.Add( kvp.Key );
                _arrayValues.Add( kvp.Value );
            }
        }
        public JsonObject( AddJSONContents content )
        {
            content.Invoke( this );
        }
        public JsonObject( JsonObject[] objs )
        {
            type = Type.ARRAY;
            _arrayValues = new List<JsonObject>( objs );
        }
        //Convenience function for creating a JSONObject containing a string.  This is not part of the constructor so that malformed JSON data doesn't just turn into a string object
        public static JsonObject StringObject( string val ) { return CreateStringObject( val ); }
        public void Absorb( JsonObject obj )
        {
            _arrayValues.AddRange( obj._arrayValues );
            _arrayKeys.AddRange( obj._arrayKeys );
            _stringValue = obj._stringValue;
            n = obj.n;
            useInt = obj.useInt;
            _intValue = obj._intValue;
            _boolValue = obj._boolValue;
            type = obj.type;
        }
        public static JsonObject Create()
        {
#if POOLING
        JSONObject result = null;
        while(result == null && releaseQueue.Count > 0) {
            result = releaseQueue.Dequeue();
#if DEV
            //The following cases should NEVER HAPPEN (but they do...)
            if(result == null)
                Debug.WriteLine("wtf " + releaseQueue.Count);
            else if(result.list != null)
                Debug.WriteLine("wtflist " + result.list.Count);
#endif
        }
        if(result != null)
            return result;
#endif
            return new JsonObject();
        }
        public static JsonObject Create( Type t )
        {
            JsonObject obj = Create();
            obj.type = t;
            switch( t )
            {
                case Type.ARRAY:
                    obj._arrayValues = new List<JsonObject>();
                    break;
                case Type.OBJECT:
                    obj._arrayValues = new List<JsonObject>();
                    obj._arrayKeys = new List<string>();
                    break;
            }
            return obj;
        }
        public static JsonObject Create( bool val )
        {
            JsonObject obj = Create();
            obj.type = Type.BOOL;
            obj._boolValue = val;
            return obj;
        }
        public static JsonObject Create( float val )
        {
            JsonObject obj = Create();
            obj.type = Type.NUMBER;
            obj.n = val;
            return obj;
        }
        public static JsonObject Create( int val )
        {
            JsonObject obj = Create();
            obj.type = Type.NUMBER;
            obj.n = val;
            obj.useInt = true;
            obj._intValue = val;
            return obj;
        }
        public static JsonObject Create( long val )
        {
            JsonObject obj = Create();
            obj.type = Type.NUMBER;
            obj.n = val;
            obj.useInt = true;
            obj._intValue = val;
            return obj;
        }
        public static JsonObject CreateStringObject( string val )
        {
            JsonObject obj = Create();
            obj.type = Type.STRING;
            obj._stringValue = val;
            return obj;
        }
        public static JsonObject CreateBakedObject( string val )
        {
            JsonObject bakedObject = Create();
            bakedObject.type = Type.BAKED;
            bakedObject._stringValue = val;
            return bakedObject;
        }
        /// <summary>
        /// Create a JSONObject by parsing string data
        /// </summary>
        /// <param name="val">The string to be parsed</param>
        /// <param name="maxDepth">The maximum depth for the parser to search.  Set this to to 1 for the first level, 
        /// 2 for the first 2 levels, etc.  It defaults to -2 because -1 is the depth value that is parsed (see below)</param>
        /// <param name="storeExcessLevels">Whether to store levels beyond maxDepth in baked JSONObjects</param>
        /// <param name="strict">Whether to be strict in the parsing. For example, non-strict parsing will successfully 
        /// parse "a string" into a string-type </param>
        /// <returns></returns>
        public static JsonObject Create( string val, int maxDepth = -2, bool storeExcessLevels = false, bool strict = false )
        {
            JsonObject obj = Create();
            obj.Parse( val, maxDepth, storeExcessLevels, strict );
            return obj;
        }
        public static JsonObject Create( AddJSONContents content )
        {
            JsonObject obj = Create();
            content.Invoke( obj );
            return obj;
        }
        public static JsonObject Create( Dictionary<string, string> dic )
        {
            JsonObject obj = Create();
            obj.type = Type.OBJECT;
            obj._arrayKeys = new List<string>();
            obj._arrayValues = new List<JsonObject>();
            //Not sure if it's worth removing the foreach here
            foreach( KeyValuePair<string, string> kvp in dic )
            {
                obj._arrayKeys.Add( kvp.Key );
                obj._arrayValues.Add( CreateStringObject( kvp.Value ) );
            }
            return obj;
        }
        public JsonObject() { }
        #region PARSE
        public JsonObject( string str, int maxDepth = -2, bool storeExcessLevels = false, bool strict = false )
        {   //create a new JSONObject from a string (this will also create any children, and parse the whole string)
            Parse( str, maxDepth, storeExcessLevels, strict );
        }
        void Parse( string str, int maxDepth = -2, bool storeExcessLevels = false, bool strict = false )
        {
            if( !string.IsNullOrEmpty( str ) )
            {
                str = str.Trim( WHITESPACE );
                if( strict )
                {
                    if( str[ 0 ] != '[' && str[ 0 ] != '{' )
                    {
                        type = Type.NULL;
#if UNITY_2 || UNITY_3 || UNITY_4 || UNITY_5
                        Debug.LogWarning
#else
                    Debug.WriteLine
#endif
                        ( "Improper (strict) JSON formatting.  First character must be [ or {" );
                        return;
                    }
                }
                if( str.Length > 0 )
                {
#if UNITY_WP8 || UNITY_WSA
                if (str == "true") {
                    type = Type.BOOL;
                    b = true;
                } else if (str == "false") {
                    type = Type.BOOL;
                    b = false;
                } else if (str == "null") {
                    type = Type.NULL;
#else
                    if( string.Compare( str, "true", true ) == 0 )
                    {
                        type = Type.BOOL;
                        _boolValue = true;
                    }
                    else if( string.Compare( str, "false", true ) == 0 )
                    {
                        type = Type.BOOL;
                        _boolValue = false;
                    }
                    else if( string.Compare( str, "null", true ) == 0 )
                    {
                        type = Type.NULL;
#endif
#if USEFLOAT
                    }
                    else if( str == INFINITY )
                    {
                        type = Type.NUMBER;
                        n = float.PositiveInfinity;
                    }
                    else if( str == NEGINFINITY )
                    {
                        type = Type.NUMBER;
                        n = float.NegativeInfinity;
                    }
                    else if( str == NaN )
                    {
                        type = Type.NUMBER;
                        n = float.NaN;
#else
                } else if(str == INFINITY) {
                    type = Type.NUMBER;
                    n = double.PositiveInfinity;
                } else if(str == NEGINFINITY) {
                    type = Type.NUMBER;
                    n = double.NegativeInfinity;
                } else if(str == NaN) {
                    type = Type.NUMBER;
                    n = double.NaN;
#endif
                    }
                    else if( str[ 0 ] == '"' )
                    {
                        type = Type.STRING;
                        this._stringValue = str.Substring( 1, str.Length - 2 );
                    }
                    else
                    {
                        int tokenTmp = 1;
                        /*
                         * Checking for the following formatting (www.json.org)
                         * object - {"field1":value,"field2":value}
                         * array - [value,value,value]
                         * value - string	- "string"
                         *		 - number	- 0.0
                         *		 - bool		- true -or- false
                         *		 - null		- null
                         */
                        int offset = 0;
                        switch( str[ offset ] )
                        {
                            case '{':
                                type = Type.OBJECT;
                                _arrayKeys = new List<string>();
                                _arrayValues = new List<JsonObject>();
                                break;
                            case '[':
                                type = Type.ARRAY;
                                _arrayValues = new List<JsonObject>();
                                break;
                            default:
                                try
                                {
#if USEFLOAT
                                    n = System.Convert.ToSingle( str );
#else
                                n = System.Convert.ToDouble(str);				 
#endif
                                    if( !str.Contains( "." ) )
                                    {
                                        _intValue = System.Convert.ToInt64( str );
                                        useInt = true;
                                    }
                                    type = Type.NUMBER;
                                }
                                catch( System.FormatException )
                                {
                                    type = Type.NULL;
#if UNITY_2 || UNITY_3 || UNITY_4 || UNITY_5
                                    Debug.LogWarning
#else
                                Debug.WriteLine
#endif
                                ( "improper JSON formatting:" + str );
                                }
                                return;
                        }
                        string propName = "";
                        bool openQuote = false;
                        bool inProp = false;
                        int depth = 0;
                        while( ++offset < str.Length )
                        {
                            if( System.Array.IndexOf( WHITESPACE, str[ offset ] ) > -1 )
                                continue;
                            if( str[ offset ] == '\\' )
                            {
                                offset += 1;
                                continue;
                            }
                            if( str[ offset ] == '"' )
                            {
                                if( openQuote )
                                {
                                    if( !inProp && depth == 0 && type == Type.OBJECT )
                                        propName = str.Substring( tokenTmp + 1, offset - tokenTmp - 1 );
                                    openQuote = false;
                                }
                                else
                                {
                                    if( depth == 0 && type == Type.OBJECT )
                                        tokenTmp = offset;
                                    openQuote = true;
                                }
                            }
                            if( openQuote )
                                continue;
                            if( type == Type.OBJECT && depth == 0 )
                            {
                                if( str[ offset ] == ':' )
                                {
                                    tokenTmp = offset + 1;
                                    inProp = true;
                                }
                            }

                            if( str[ offset ] == '[' || str[ offset ] == '{' )
                            {
                                depth++;
                            }
                            else if( str[ offset ] == ']' || str[ offset ] == '}' )
                            {
                                depth--;
                            }
                            //if  (encounter a ',' at top level)  || a closing ]/}
                            if( ( str[ offset ] == ',' && depth == 0 ) || depth < 0 )
                            {
                                inProp = false;
                                string inner = str.Substring( tokenTmp, offset - tokenTmp ).Trim( WHITESPACE );
                                if( inner.Length > 0 )
                                {
                                    if( type == Type.OBJECT )
                                        _arrayKeys.Add( propName );
                                    if( maxDepth != -1 )                                                            //maxDepth of -1 is the end of the line
                                        _arrayValues.Add( Create( inner, ( maxDepth < -1 ) ? -2 : maxDepth - 1 ) );
                                    else if( storeExcessLevels )
                                        _arrayValues.Add( CreateBakedObject( inner ) );

                                }
                                tokenTmp = offset + 1;
                            }
                        }
                    }
                }
                else type = Type.NULL;
            }
            else type = Type.NULL;  //If the string is missing, this is a null
                                    //Profiler.EndSample();
        }
        #endregion
        public bool IsNumber { get { return type == Type.NUMBER; } }
        public bool IsNull { get { return type == Type.NULL; } }
        public bool IsString { get { return type == Type.STRING; } }
        public bool IsBool { get { return type == Type.BOOL; } }
        public bool IsArray { get { return type == Type.ARRAY; } }
        public bool IsObject { get { return type == Type.OBJECT || type == Type.BAKED; } }
        public void Add( bool val )
        {
            Add( Create( val ) );
        }
        public void Add( float val )
        {
            Add( Create( val ) );
        }
        public void Add( int val )
        {
            Add( Create( val ) );
        }
        public void Add( string str )
        {
            Add( CreateStringObject( str ) );
        }
        public void Add( AddJSONContents content )
        {
            Add( Create( content ) );
        }
        public void Add( JsonObject obj )
        {
            if( obj )
            {       //Don't do anything if the object is null
                if( type != Type.ARRAY )
                {
                    type = Type.ARRAY;      //Congratulations, son, you're an ARRAY now
                    if( _arrayValues == null )
                        _arrayValues = new List<JsonObject>();
                }
                _arrayValues.Add( obj );
            }
        }
        public JsonObject AddField( string name, bool val )
        {
            return AddField( name, Create( val ) );
        }
        public JsonObject AddField( string name, float val )
        {
            return AddField( name, Create( val ) );
        }
        public JsonObject AddField( string name, int val )
        {
            return AddField( name, Create( val ) );
        }
        public JsonObject AddField( string name, long val )
        {
            return AddField( name, Create( val ) );
        }
        public JsonObject AddField( string name, AddJSONContents content )
        {
            return AddField( name, Create( content ) );
        }
        public JsonObject AddField( string name, string val )
        {
            AddField( name, CreateStringObject( val ) );
            return this;
        }
        public JsonObject AddField( string name, JsonObject obj )
        {
            if( obj )
            {       //Don't do anything if the object is null
                if( type != Type.OBJECT )
                {
                    if( _arrayKeys == null )
                        _arrayKeys = new List<string>();
                    if( type == Type.ARRAY )
                    {
                        for( int i = 0; i < _arrayValues.Count; i++ )
                            _arrayKeys.Add( i + "" );
                    }
                    else if( _arrayValues == null )
                        _arrayValues = new List<JsonObject>();
                    type = Type.OBJECT;     //Congratulations, son, you're an OBJECT now
                }
                _arrayKeys.Add( name );
                _arrayValues.Add( obj );
            }

            return this;
        }
        public void SetField( string name, string val ) { SetField( name, CreateStringObject( val ) ); }
        public void SetField( string name, bool val ) { SetField( name, Create( val ) ); }
        public void SetField( string name, float val ) { SetField( name, Create( val ) ); }
        public void SetField( string name, int val ) { SetField( name, Create( val ) ); }
        public void SetField( string name, JsonObject obj )
        {
            if( HasField( name ) )
            {
                _arrayValues.Remove( this[ name ] );
                _arrayKeys.Remove( name );
            }
            AddField( name, obj );
        }
        public void RemoveField( string name )
        {
            if( _arrayKeys.IndexOf( name ) > -1 )
            {
                _arrayValues.RemoveAt( _arrayKeys.IndexOf( name ) );
                _arrayKeys.Remove( name );
            }
        }
        public delegate void FieldNotFound( string name );
        public delegate void GetFieldResponse( JsonObject obj );

        public string GetStringValue( string name, string fallback )
        {
            string value;
            GetField( out value, name, fallback );
            return value;
        }

        public bool GetBoolValue( string name, bool fallback )
        {
            bool value;
            GetField( out value, name, fallback );
            return value;
        }

        public int GetIntValue( string name, int fallback )
        {
            int value;
            GetField( out value, name, fallback );
            return value;
        }

        public float GetFloatValue( string name, float fallback )
        {
            float value;
            GetField( out value, name, fallback );
            return value;
        }

        public bool GetField( out bool field, string name, bool fallback )
        {
            field = fallback;
            return GetField( ref field, name );
        }
        public bool GetField( ref bool field, string name, FieldNotFound fail = null )
        {
            if( type == Type.OBJECT )
            {
                int index = _arrayKeys.IndexOf( name );
                if( index >= 0 )
                {
                    field = _arrayValues[ index ]._boolValue;
                    return true;
                }
            }
            if( fail != null ) fail.Invoke( name );
            return false;
        }
#if USEFLOAT
        public bool GetField( out float field, string name, float fallback )
        {
#else
    public bool GetField(out double field, string name, double fallback) {
#endif
            field = fallback;
            return GetField( ref field, name );
        }
#if USEFLOAT
        public bool GetField( ref float field, string name, FieldNotFound fail = null )
        {
#else
    public bool GetField(ref double field, string name, FieldNotFound fail = null) {
#endif
            if( type == Type.OBJECT )
            {
                int index = _arrayKeys.IndexOf( name );
                if( index >= 0 )
                {
                    field = _arrayValues[ index ].n;
                    return true;
                }
            }
            if( fail != null ) fail.Invoke( name );
            return false;
        }
        public bool GetField( out int field, string name, int fallback )
        {
            field = fallback;
            return GetField( ref field, name );
        }
        public bool GetField( ref int field, string name, FieldNotFound fail = null )
        {
            if( IsObject )
            {
                int index = _arrayKeys.IndexOf( name );
                if( index >= 0 )
                {
                    field = (int) _arrayValues[ index ].n;
                    return true;
                }
            }
            if( fail != null ) fail.Invoke( name );
            return false;
        }
        public bool GetField( out long field, string name, long fallback )
        {
            field = fallback;
            return GetField( ref field, name );
        }
        public bool GetField( ref long field, string name, FieldNotFound fail = null )
        {
            if( IsObject )
            {
                int index = _arrayKeys.IndexOf( name );
                if( index >= 0 )
                {
                    field = (long) _arrayValues[ index ].n;
                    return true;
                }
            }
            if( fail != null ) fail.Invoke( name );
            return false;
        }
        public bool GetField( out uint field, string name, uint fallback )
        {
            field = fallback;
            return GetField( ref field, name );
        }
        public bool GetField( ref uint field, string name, FieldNotFound fail = null )
        {
            if( IsObject )
            {
                int index = _arrayKeys.IndexOf( name );
                if( index >= 0 )
                {
                    field = (uint) _arrayValues[ index ].n;
                    return true;
                }
            }
            if( fail != null ) fail.Invoke( name );
            return false;
        }
        public bool GetField( out string field, string name, string fallback )
        {
            field = fallback;
            return GetField( ref field, name );
        }
        public bool GetField( ref string field, string name, FieldNotFound fail = null )
        {
            if( IsObject )
            {
                int index = _arrayKeys.IndexOf( name );
                if( index >= 0 )
                {
                    field = _arrayValues[ index ]._stringValue;
                    return true;
                }
            }
            if( fail != null ) fail.Invoke( name );
            return false;
        }
        public void GetField( string name, GetFieldResponse response, FieldNotFound fail = null )
        {
            if( response != null && IsObject )
            {
                int index = _arrayKeys.IndexOf( name );
                if( index >= 0 )
                {
                    response.Invoke( _arrayValues[ index ] );
                    return;
                }
            }
            if( fail != null ) fail.Invoke( name );
        }
        public JsonObject GetField( string name )
        {
            if( IsObject )
                for( int i = 0; i < _arrayKeys.Count; i++ )
                    if( _arrayKeys[ i ] == name )
                        return _arrayValues[ i ];
            return new JsonObject();
        }
        public bool HasFields( string[] names )
        {
            if( !IsObject )
                return false;
            for( int i = 0; i < names.Length; i++ )
                if( !_arrayKeys.Contains( names[ i ] ) )
                    return false;
            return true;
        }
        public bool HasField( string name )
        {
            if( !IsObject )
                return false;
            for( int i = 0; i < _arrayKeys.Count; i++ )
                if( _arrayKeys[ i ] == name )
                    return true;
            return false;
        }
        public void Clear()
        {
            type = Type.NULL;
            if( _arrayValues != null )
                _arrayValues.Clear();
            if( _arrayKeys != null )
                _arrayKeys.Clear();
            _stringValue = "";
            n = 0;
            _boolValue = false;
        }
        /// <summary>
        /// Copy a JSONObject. This could probably work better
        /// </summary>
        /// <returns></returns>
        public JsonObject Copy()
        {
            return Create( Print() );
        }
        /*
         * The Merge function is experimental. Use at your own risk.
         */
        public void Merge( JsonObject obj )
        {
            MergeRecur( this, obj );
        }
        /// <summary>
        /// Merge object right into left recursively
        /// </summary>
        /// <param name="left">The left (base) object</param>
        /// <param name="right">The right (new) object</param>
        static void MergeRecur( JsonObject left, JsonObject right )
        {
            if( left.type == Type.NULL )
                left.Absorb( right );
            else if( left.type == Type.OBJECT && right.type == Type.OBJECT )
            {
                for( int i = 0; i < right._arrayValues.Count; i++ )
                {
                    string key = right._arrayKeys[ i ];
                    if( right[ i ].isContainer )
                    {
                        if( left.HasField( key ) )
                            MergeRecur( left[ key ], right[ i ] );
                        else
                            left.AddField( key, right[ i ] );
                    }
                    else
                    {
                        if( left.HasField( key ) )
                            left.SetField( key, right[ i ] );
                        else
                            left.AddField( key, right[ i ] );
                    }
                }
            }
            else if( left.type == Type.ARRAY && right.type == Type.ARRAY )
            {
                if( right.Count > left.Count )
                {
#if UNITY_2 || UNITY_3 || UNITY_4 || UNITY_5
                    Debug.LogError
#else
                Debug.WriteLine
#endif
                ( "Cannot merge arrays when right object has more elements" );
                    return;
                }
                for( int i = 0; i < right._arrayValues.Count; i++ )
                {
                    if( left[ i ].type == right[ i ].type )
                    {           //Only overwrite with the same type
                        if( left[ i ].isContainer )
                            MergeRecur( left[ i ], right[ i ] );
                        else
                        {
                            left[ i ] = right[ i ];
                        }
                    }
                }
            }
        }
        public void Bake()
        {
            if( type != Type.BAKED )
            {
                _stringValue = Print();
                type = Type.BAKED;
            }
        }
        public IEnumerable BakeAsync()
        {
            if( type != Type.BAKED )
            {
                foreach( string s in PrintAsync() )
                {
                    if( s == null )
                        yield return s;
                    else
                    {
                        _stringValue = s;
                    }
                }
                type = Type.BAKED;
            }
        }
#pragma warning disable 219
        public string Print( bool pretty = false )
        {
            StringBuilder builder = new StringBuilder();
            Stringify( 0, builder, pretty );
            return builder.ToString();
        }
        public IEnumerable<string> PrintAsync( bool pretty = false )
        {
            StringBuilder builder = new StringBuilder();
            printWatch.Reset();
            printWatch.Start();
            foreach( IEnumerable e in StringifyAsync( 0, builder, pretty ) )
            {
                yield return null;
            }
            yield return builder.ToString();
        }
#pragma warning restore 219
        #region STRINGIFY
        const float maxFrameTime = 0.008f;
        static readonly Stopwatch printWatch = new Stopwatch();
        IEnumerable StringifyAsync( int depth, StringBuilder builder, bool pretty = false )
        {   //Convert the JSONObject into a string
            //Profiler.BeginSample("JSONprint");
            if( depth++ > MAX_DEPTH )
            {
#if UNITY_2 || UNITY_3 || UNITY_4 || UNITY_5
                Debug.Log
#else
            Debug.WriteLine
#endif
            ( "reached max depth!" );
                yield break;
            }
            if( printWatch.Elapsed.TotalSeconds > maxFrameTime )
            {
                printWatch.Reset();
                yield return null;
                printWatch.Start();
            }
            switch( type )
            {
                case Type.BAKED:
                    builder.Append( _stringValue );
                    break;
                case Type.STRING:
                    builder.AppendFormat( "\"{0}\"", _stringValue );
                    break;
                case Type.NUMBER:
                    if( useInt )
                    {
                        builder.Append( _intValue.ToString() );
                    }
                    else
                    {
#if USEFLOAT
                        if( float.IsInfinity( n ) )
                            builder.Append( INFINITY );
                        else if( float.IsNegativeInfinity( n ) )
                            builder.Append( NEGINFINITY );
                        else if( float.IsNaN( n ) )
                            builder.Append( NaN );
#else
                if(double.IsInfinity(n))
                    builder.Append(INFINITY);
                else if(double.IsNegativeInfinity(n))
                    builder.Append(NEGINFINITY);
                else if(double.IsNaN(n))
                    builder.Append(NaN);
#endif
                        else
                            builder.Append( n.ToString() );
                    }
                    break;
                case Type.OBJECT:
                    builder.Append( "{" );
                    if( _arrayValues.Count > 0 )
                    {
#if( PRETTY )     //for a bit more readability, comment the define above to disable system-wide
                        if( pretty )
                            builder.Append( NEWLINE );
#endif
                        for( int i = 0; i < _arrayValues.Count; i++ )
                        {
                            string key = _arrayKeys[ i ];
                            JsonObject obj = _arrayValues[ i ];
                            if( obj )
                            {
#if( PRETTY )
                                if( pretty )
                                    for( int j = 0; j < depth; j++ )
                                        builder.Append( "\t" ); //for a bit more readability
#endif
                                builder.AppendFormat( "\"{0}\":", key );
                                foreach( IEnumerable e in obj.StringifyAsync( depth, builder, pretty ) )
                                    yield return e;
                                builder.Append( "," );
#if( PRETTY )
                                if( pretty )
                                    builder.Append( NEWLINE );
#endif
                            }
                        }
#if( PRETTY )
                        if( pretty )
                            builder.Length -= 2;
                        else
#endif
                            builder.Length--;
                    }
#if( PRETTY )
                    if( pretty && _arrayValues.Count > 0 )
                    {
                        builder.Append( NEWLINE );
                        for( int j = 0; j < depth - 1; j++ )
                            builder.Append( "\t" ); //for a bit more readability
                    }
#endif
                    builder.Append( "}" );
                    break;
                case Type.ARRAY:
                    builder.Append( "[" );
                    if( _arrayValues.Count > 0 )
                    {
#if( PRETTY )
                        if( pretty )
                            builder.Append( NEWLINE ); //for a bit more readability
#endif
                        for( int i = 0; i < _arrayValues.Count; i++ )
                        {
                            if( _arrayValues[ i ] )
                            {
#if( PRETTY )
                                if( pretty )
                                    for( int j = 0; j < depth; j++ )
                                        builder.Append( "\t" ); //for a bit more readability
#endif
                                foreach( IEnumerable e in _arrayValues[ i ].StringifyAsync( depth, builder, pretty ) )
                                    yield return e;
                                builder.Append( "," );
#if( PRETTY )
                                if( pretty )
                                    builder.Append( NEWLINE ); //for a bit more readability
#endif
                            }
                        }
#if( PRETTY )
                        if( pretty )
                            builder.Length -= 2;
                        else
#endif
                            builder.Length--;
                    }
#if( PRETTY )
                    if( pretty && _arrayValues.Count > 0 )
                    {
                        builder.Append( NEWLINE );
                        for( int j = 0; j < depth - 1; j++ )
                            builder.Append( "\t" ); //for a bit more readability
                    }
#endif
                    builder.Append( "]" );
                    break;
                case Type.BOOL:
                    if( _boolValue )
                        builder.Append( "true" );
                    else
                        builder.Append( "false" );
                    break;
                case Type.NULL:
                    builder.Append( "null" );
                    break;
            }
            //Profiler.EndSample();
        }
        //TODO: Refactor Stringify functions to share core logic
        /*
         * I know, I know, this is really bad form.  It turns out that there is a
         * significant amount of garbage created when calling as a coroutine, so this
         * method is duplicated.  Hopefully there won't be too many future changes, but
         * I would still like a more elegant way to optionaly yield
         */
        void Stringify( int depth, StringBuilder builder, bool pretty = false )
        {   //Convert the JSONObject into a string
            //Profiler.BeginSample("JSONprint");
            if( depth++ > MAX_DEPTH )
            {
#if UNITY_2 || UNITY_3 || UNITY_4 || UNITY_5
                Debug.Log
#else
            Debug.WriteLine
#endif
            ( "reached max depth!" );
                return;
            }
            switch( type )
            {
                case Type.BAKED:
                    builder.Append( _stringValue );
                    break;
                case Type.STRING:
                    builder.AppendFormat( "\"{0}\"", _stringValue );
                    break;
                case Type.NUMBER:
                    if( useInt )
                    {
                        builder.Append( _intValue.ToString() );
                    }
                    else
                    {
#if USEFLOAT
                        if( float.IsInfinity( n ) )
                            builder.Append( INFINITY );
                        else if( float.IsNegativeInfinity( n ) )
                            builder.Append( NEGINFINITY );
                        else if( float.IsNaN( n ) )
                            builder.Append( NaN );
#else
                if(double.IsInfinity(n))
                    builder.Append(INFINITY);
                else if(double.IsNegativeInfinity(n))
                    builder.Append(NEGINFINITY);
                else if(double.IsNaN(n))
                    builder.Append(NaN);
#endif
                        else
                            builder.Append( n.ToString() );
                    }
                    break;
                case Type.OBJECT:
                    builder.Append( "{" );
                    if( _arrayValues.Count > 0 )
                    {
#if( PRETTY )     //for a bit more readability, comment the define above to disable system-wide
                        if( pretty )
                            builder.Append( "\n" );
#endif
                        for( int i = 0; i < _arrayValues.Count; i++ )
                        {
                            string key = _arrayKeys[ i ];
                            JsonObject obj = _arrayValues[ i ];
                            if( obj )
                            {
#if( PRETTY )
                                if( pretty )
                                    for( int j = 0; j < depth; j++ )
                                        builder.Append( "\t" ); //for a bit more readability
#endif
                                builder.AppendFormat( "\"{0}\":", key );
                                obj.Stringify( depth, builder, pretty );
                                builder.Append( "," );
#if( PRETTY )
                                if( pretty )
                                    builder.Append( "\n" );
#endif
                            }
                        }
#if( PRETTY )
                        if( pretty )
                            builder.Length -= 2;
                        else
#endif
                            builder.Length--;
                    }
#if( PRETTY )
                    if( pretty && _arrayValues.Count > 0 )
                    {
                        builder.Append( "\n" );
                        for( int j = 0; j < depth - 1; j++ )
                            builder.Append( "\t" ); //for a bit more readability
                    }
#endif
                    builder.Append( "}" );
                    break;
                case Type.ARRAY:
                    builder.Append( "[" );
                    if( _arrayValues.Count > 0 )
                    {
#if( PRETTY )
                        if( pretty )
                            builder.Append( "\n" ); //for a bit more readability
#endif
                        for( int i = 0; i < _arrayValues.Count; i++ )
                        {
                            if( _arrayValues[ i ] )
                            {
#if( PRETTY )
                                if( pretty )
                                    for( int j = 0; j < depth; j++ )
                                        builder.Append( "\t" ); //for a bit more readability
#endif
                                _arrayValues[ i ].Stringify( depth, builder, pretty );
                                builder.Append( "," );
#if( PRETTY )
                                if( pretty )
                                    builder.Append( "\n" ); //for a bit more readability
#endif
                            }
                        }
#if( PRETTY )
                        if( pretty )
                            builder.Length -= 2;
                        else
#endif
                            builder.Length--;
                    }
#if( PRETTY )
                    if( pretty && _arrayValues.Count > 0 )
                    {
                        builder.Append( "\n" );
                        for( int j = 0; j < depth - 1; j++ )
                            builder.Append( "\t" ); //for a bit more readability
                    }
#endif
                    builder.Append( "]" );
                    break;
                case Type.BOOL:
                    if( _boolValue )
                        builder.Append( "true" );
                    else
                        builder.Append( "false" );
                    break;
                case Type.NULL:
                    builder.Append( "null" );
                    break;
            }
            //Profiler.EndSample();
        }
        #endregion
#if UNITY_2 || UNITY_3 || UNITY_4 || UNITY_5
        public static implicit operator WWWForm( JsonObject obj )
        {
            WWWForm form = new WWWForm();
            for( int i = 0; i < obj._arrayValues.Count; i++ )
            {
                string key = i + "";
                if( obj.type == Type.OBJECT )
                    key = obj._arrayKeys[ i ];
                string val = obj._arrayValues[ i ].ToString();
                if( obj._arrayValues[ i ].type == Type.STRING )
                    val = val.Replace( "\"", "" );
                form.AddField( key, val );
            }
            return form;
        }
#endif
        public JsonObject this[ int index ]
        {
            get
            {
                if( _arrayValues.Count > index ) return _arrayValues[ index ];
                return new JsonObject();
            }
            set
            {
                if( _arrayValues.Count > index )
                    _arrayValues[ index ] = value;
            }
        }
        public JsonObject this[ string index ]
        {
            get
            {
                return GetField( index );
            }
            set
            {
                SetField( index, value );
            }
        }
        public override string ToString()
        {
            return Print();
        }
        public string ToString( bool pretty )
        {
            return Print( pretty );
        }
        public Dictionary<string, string> ToDictionary()
        {
            if( type == Type.OBJECT )
            {
                Dictionary<string, string> result = new Dictionary<string, string>();
                for( int i = 0; i < _arrayValues.Count; i++ )
                {
                    JsonObject val = _arrayValues[ i ];
                    switch( val.type )
                    {
                        case Type.STRING: result.Add( _arrayKeys[ i ], val._stringValue ); break;
                        case Type.NUMBER: result.Add( _arrayKeys[ i ], val.n + "" ); break;
                        case Type.BOOL: result.Add( _arrayKeys[ i ], val._boolValue + "" ); break;
                        default:
#if UNITY_2 || UNITY_3 || UNITY_4 || UNITY_5
                            Debug.LogWarning
#else
                        Debug.WriteLine
#endif
                        ( "Omitting object: " + _arrayKeys[ i ] + " in dictionary conversion" );
                            break;
                    }
                }
                return result;
            }
#if UNITY_2 || UNITY_3 || UNITY_4 || UNITY_5
            Debug.Log
#else
        Debug.WriteLine
#endif
        ( "Tried to turn non-Object JSONObject into a dictionary" );
            return null;
        }
        public static implicit operator bool( JsonObject o )
        {
            return o != null;
        }
#if POOLING
    static bool pool = true;
    public static void ClearPool() {
        pool = false;
        releaseQueue.Clear();
        pool = true;
    }

    ~JSONObject() {
        if(pool && releaseQueue.Count < MAX_POOL_SIZE) {
            type = Type.NULL;
            list = null;
            keys = null;
            str = "";
            n = 0;
            b = false;
            releaseQueue.Enqueue(this);
        }
    }
#endif
    }
}