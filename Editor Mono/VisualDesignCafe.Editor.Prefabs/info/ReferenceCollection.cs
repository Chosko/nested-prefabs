﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

#pragma warning disable 618

namespace VisualDesignCafe.Editor.Prefabs
{

    [Serializable]
    public sealed class ReferenceCollection : IReferenceCollection
    {
        public Prefab Prefab
        {
            get;
            private set;
        }

        public int Count
        {
            get { return _collection.Count; }
        }

        public bool IsValidCached
        {
            get { return _cachedGetIsValid; }
            set { _cachedGetIsValid = value; }
        }

        public int CacheId
        {
            get { return _hasCachedValidation; }
            set { _hasCachedValidation = value; }
        }

        public GameObject this[ int index ]
        {
            get { return _collection[ index ]; }
        }

        [SerializeField]
        private List<GameObject> _collection = new List<GameObject>();

        private int _hasCachedValidation = -1;
        private bool _cachedGetIsValid = true;

        /// <summary>
        /// Creates a new Reference Collection for the Prefab
        /// </summary>
        public ReferenceCollection( Prefab prefab )
        {
            Prefab = prefab;
        }

        /// <summary>
        /// Changes the target prefab for the reference collection.
        /// </summary>
        public void SetPrefab( Prefab prefab )
        {
            Prefab = prefab;
        }

        /// <summary>
        /// Validates the reference collection according to the given validation method.
        /// </summary>
        public void Validate( Func<GameObject, GameObject> validation )
        {
            for( int i = _collection.Count - 1; i > -1; i-- )
            {
                _collection[ i ] = validation( _collection[ i ] );

                if( _collection[ i ] == null )
                {
                    _collection.RemoveAt( i );
                    continue;
                }
            }
        }

        /// <summary>
        /// Finds an object in the collection with the given GUID.
        /// </summary>
        public GameObject FindObjectWithGuid( Guid guid )
        {
            for( int i = 0; i < _collection.Count; i++ )
            {
                if( _collection[ i ] == null )
                    continue;

                if( _collection[ i ].GetComponent<Guid>() == guid )
                    return _collection[ i ];
            }

            return null;
        }

        /// <summary>
        /// Is this reference collection equal to the other collection?
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Equals( ReferenceCollection other )
        {
            if( object.ReferenceEquals( this, other ) )
                return true;

            ReferenceCollection collection = other as ReferenceCollection;

            if( collection == null )
                return false;

            Clean();
            collection.Clean();

            if( _collection.Count != collection._collection.Count )
                return false;

            foreach( GameObject gameObject in _collection )
            {
                Guid guid = gameObject.GetComponent<Guid>();

                if( !collection._collection.Any( c => c.GetComponent<Guid>() == guid ) )
                    return false;
            }

            return true;
        }

        public override string ToString()
        {
            if( _collection == null )
                return string.Empty;

            return string.Join(
                "\n",
                _collection
                    .Select(
                        g =>
                            g != null
                                ? ( g.name + "(" + g.GetInstanceID() + ")" + " [" + g.GetComponent<Guid>() + "]" )
                                : "null" )
                    .ToArray() );
        }

        public bool Contains( GameObject key )
        {
            return _collection.Contains( key );
        }

        public void Clean()
        {
            _collection = _collection.Where( c => c != null ).Distinct().ToList();
        }

        public void Clear()
        {
            _collection.Clear();
        }

        public void Remove( GameObject child )
        {
            Remove( child, true );
        }

        public void Remove( GameObject child, bool destroyGuid )
        {
            if( !_collection.Remove( child ) )
                return;

            if( destroyGuid )
            {
                Guid guid = child.GetComponent<Guid>();

                if( guid != null )
                    Component.DestroyImmediate( guid );
            }
        }

        /// <summary>
        /// Adds a new child to the reference collection. Should only be used on an instanced child 
        /// when the child is exactly the same as the source prefab. Using this on any modified object will
        /// corrupt the reference collection.
        /// </summary>
        public void Add( GameObject child )
        {
            Add( child, false );
        }

        public void Add( GameObject child, bool keepGuid )
        {
            if( Contains( child ) )
                return;

            _collection.Add( child );

            var guid = child.GetComponent<Guid>();
            if( guid == null )
            {
                guid = child.AddComponent<Guid>();
                guid.CreateGuid();
            }
            else if( !keepGuid && guid.GetComponent<NestedPrefab>() == null )
            {
                guid.CreateGuid();
            }
        }
    }
}