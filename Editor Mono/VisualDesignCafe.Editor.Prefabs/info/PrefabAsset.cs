﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using VisualDesignCafe.Serialization;

namespace VisualDesignCafe.Editor.Prefabs
{
    [Serializable]
    public class PrefabAsset
    {
        public static Func<GameObject, string> GetAssetPath;
        public static Func<string, string> AssetPathToGUID;

        public string Guid
        {
            get { return _guid; }
            private set { _guid = value; }
        }

        public string Path
        {
            get { return _path; }
            private set { _path = value; }
        }

        public string[] References
        {
            get { return _references; }
            private set { _references = value; }
        }

        [SerializeField]
        private string _guid;
        [SerializeField]
        private string _path;
        [SerializeField]
        private string[] _references;

        public PrefabAsset( GameObject asset )
        {
            Path = GetAssetPath.Invoke( asset ); ;
            Guid = AssetPathToGUID.Invoke( Path );
            References = FindNestedPrefabs( asset ).ToArray();
        }

        public PrefabAsset( JsonObject json )
        {
            Guid = json.GetStringValue( "Guid", string.Empty );
            Path = json.GetStringValue( "Path", string.Empty );
            References = json[ "References" ].AsArray.Select( r => r.AsString ).ToArray();
        }

        public bool HasReferenceTo( PrefabAsset asset )
        {
            return HasReferenceTo( asset.Guid );
        }

        public bool HasReferenceTo( string assetGuid )
        {
            return References.Contains( assetGuid );
        }

        public JsonObject ToJson()
        {
            var json = new JsonObject();
            json.AddField( "Guid", Guid );
            json.AddField( "Path", Path );

            var array = new JsonObject();
            foreach( string guid in References )
                array.Add( guid );

            json.AddField( "References", array );

            return json;
        }

        private List<string> FindNestedPrefabs( GameObject asset )
        {
            var nestedPrefabs = new List<string>();
            Prefab[] prefabs = asset.GetComponentsInChildren<Prefab>( true );
            foreach( Prefab prefab in prefabs )
            {
                if( prefab.gameObject == asset )
                    continue;

                string guid = prefab.AssetGuid;
                if( !string.IsNullOrEmpty( guid ) && !nestedPrefabs.Contains( guid ) )
                    nestedPrefabs.Add( guid );
            }

            return nestedPrefabs;
        }
    }
}
