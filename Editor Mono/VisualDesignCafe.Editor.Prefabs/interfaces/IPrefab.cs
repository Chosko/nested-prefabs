﻿using System;
using UnityEngine;

namespace VisualDesignCafe.Editor.Prefabs
{
    [Obsolete( "Use 'Prefab' instead." )]
    public interface IPrefab
    {
        /// <summary>
        /// The Transform component of this prefab.
        /// </summary>
        Transform CachedTransform { get; }

        /// <summary>
        /// The GameObject this prefab object is part of.
        /// </summary>
        GameObject CachedGameObject { get; }

        /// <summary>
        /// Reference collection containing a list of all children in the prefab's hierarchy.
        /// </summary>
        ReferenceCollection ReferenceCollection { get; }

        /// <summary>
        /// The original prefab asset object.
        /// </summary>
        GameObject Asset { get; }

        /// <summary>
        /// The GUID of the original prefab asset.
        /// </summary>
        string AssetGuid { get; }

        /// <summary>
        /// A unique ID for this prefab.
        /// </summary>
        string PrefabGuid { get; }

        /// <summary>
        /// A unique ID for this GameObject.
        /// </summary>
        Guid ObjectGuid { get; }

        string ToString();
    }
}