﻿using System;
using UnityEngine;

namespace VisualDesignCafe.Editor.Prefabs
{
    [Obsolete( "Use 'NestedPrefab' instead." )]
    public interface INestedPrefab
    {
        string AssetGuid { get; }

        Prefab Prefab { get; }

        /// <summary>
        /// The source prefab of this nested prefab.
        /// </summary>
        GameObject Asset { get; }

        /// <summary>
        /// The GameObject root of the nested prefab
        /// </summary>
        GameObject CachedGameObject { get; }

        /// <summary>
        /// The Transform root of the nested prefab
        /// </summary>
        Transform CachedTransform { get; }

        /// <summary>
        /// Reference collection of the nested prefab containing a list of all children in its hierarchy.
        /// </summary>
        ReferenceCollection ReferenceCollection { get; }
    }
}
