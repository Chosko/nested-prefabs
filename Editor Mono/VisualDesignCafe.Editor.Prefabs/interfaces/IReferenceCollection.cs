﻿using UnityEngine;

namespace VisualDesignCafe.Editor.Prefabs
{
    [System.Obsolete( "Use 'ReferenceCollection' instead." )]
    public interface IReferenceCollection
    {
        Prefab Prefab { get; }
        int Count { get; }

        bool IsValidCached { get; set; }
        int CacheId { get; set; }

        bool Equals( ReferenceCollection other );

        void Add( GameObject key );
        void Add( GameObject key, bool keepGuid );
        void Remove( GameObject key );

        bool Contains( GameObject key );

        void Clean();
        void Clear();

        void Validate( System.Func<GameObject, GameObject> validation );

        GameObject FindObjectWithGuid( Guid guid );
    }
}