﻿using System.Linq;
using UnityEngine;

namespace VisualDesignCafe.Editor.Prefabs
{
    [System.Obsolete( "Use 'ComponentOverride' instead." )]
    [System.Serializable]
    public sealed class PrefabOverrideComponent
    {
        public Object Target
        {
            get;
            set;
        }

        public bool IsExpanded
        {
            get { return _isExpanded; }
            set { _isExpanded = value; }
        }

        public int Count
        {
            get { return _overrideProperties.Length; }
        }

        public string TypeName
        {
            get { return _typeName; }
        }

        public PrefabOverride[] Properties
        {
            get { return _overrideProperties; }
        }

        [SerializeField]
        private bool _isExpanded = true;

        [SerializeField]
        private string _typeName;

        [SerializeField]
        private PrefabOverride[] _overrideProperties = new PrefabOverride[ 0 ];

        public PrefabOverrideComponent( string typeName )
        {
            _typeName = typeName;
        }

        public PrefabOverride AddProperty( string property )
        {
            var @override = new PrefabOverride( property );
            var newProperties = new PrefabOverride[ _overrideProperties.Length + 1 ];
            _overrideProperties.CopyTo( newProperties, 0 );
            newProperties[ newProperties.Length - 1 ] = @override;
            _overrideProperties = newProperties;

            return @override;
        }

        public void RemoveProperty( string property )
        {
            _overrideProperties =
                _overrideProperties.Where( p => p.Path != property ).ToArray();
        }

        public int IndexOf( string property )
        {
            for( int i = 0; i < _overrideProperties.Length; i++ )
            {
                if( _overrideProperties[ i ].Path == property )
                    return i;
            }
            return -1;
        }

        public PrefabOverride GetPropertyAt( int index )
        {
            if( index == -1 )
                return null;

            return _overrideProperties[ index ];
        }

        public PrefabOverride GetProperty( string property )
        {
            int index = IndexOf( property );

            if( index == -1 )
                return null;

            return _overrideProperties[ index ];
        }
    }
}
