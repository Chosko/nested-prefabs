﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace VisualDesignCafe.Editor.Prefabs
{
    [System.Obsolete( "Use 'ObjectOverride' instead." )]
    [System.Serializable]
    public sealed class PrefabOverrideObject
    {
        public GameObject Target
        {
            get { return _target; }
        }

        public bool IsExpanded
        {
            get { return _isExpanded; }
            set { _isExpanded = value; }
        }

        public int Count
        {
            get { return _overrideComponents.Length; }
        }

        public string[] ObjectPath
        {
            get { return _path; }
        }

        public string HierarchyPath
        {
            get { return _hierarchyPath; }
        }

        public int Index
        {
            get { return _index; }
        }

        public PrefabOverrideComponent[] Components
        {
            get { return _overrideComponents; }
        }

        [SerializeField]
        private GameObject _target;

        [SerializeField]
        private bool _isExpanded = true;

        [SerializeField]
        private string[] _path = new string[ 0 ];

        [SerializeField]
        private string _hierarchyPath = string.Empty;

        [SerializeField]
        private int _index = 0;

        [UnityEngine.Serialization.FormerlySerializedAs( "_components" )]
        [SerializeField]
        private PrefabOverrideComponent[] _overrideComponents = new PrefabOverrideComponent[ 0 ];

        public PrefabOverrideObject( GameObject target, string[] path )
        {
            _target = target;
            _path = path;
        }

        public PrefabOverrideObject( string[] path )
        {
            _path = path;
        }

        public void SetPath( string[] path )
        {
            _path = path;
        }

        public void SetHierarchy( string hierarchy, int index )
        {
            _hierarchyPath = hierarchy;
            _index = index;
        }

        public void SetTarget( GameObject target )
        {
            _target = target;
        }

        public PrefabOverrideComponent AddComponent( string component )
        {
            var componentOverride = new PrefabOverrideComponent( component );
            var newProperties = new PrefabOverrideComponent[ _overrideComponents.Length + 1 ];
            _overrideComponents.CopyTo( newProperties, 0 );
            newProperties[ newProperties.Length - 1 ] = componentOverride;
            _overrideComponents = newProperties;

            return componentOverride;
        }

        public void RemoveComponent( int index )
        {
            List<PrefabOverrideComponent> components = _overrideComponents.ToList();
            components.RemoveAt( index );
            _overrideComponents = components.ToArray();
        }

        public void RemoveComponent( string componentType )
        {
            _overrideComponents =
                _overrideComponents
                    .Where( p => p.TypeName != componentType )
                    .ToArray();
        }

        public void RemoveComponent( PrefabOverrideComponent component )
        {
            var newComponents = _overrideComponents.ToList();
            newComponents.Remove( component );
            _overrideComponents = newComponents.ToArray();
        }

        public int IndexOf( string componentType )
        {
            for( int i = 0; i < _overrideComponents.Length; i++ )
            {
                if( _overrideComponents[ i ].TypeName == componentType )
                    return i;
            }
            return -1;
        }

        public PrefabOverrideComponent GetComponentAt( int index )
        {
            if( index == -1 )
                return null;

            return _overrideComponents[ index ];
        }

        public PrefabOverrideComponent GetComponent( string componentType )
        {
            int index = IndexOf( componentType );

            if( index == -1 )
                return null;

            return _overrideComponents[ index ];
        }
    }
}
