﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace VisualDesignCafe.Editor.Prefabs.Overrides
{
    using Object = UnityEngine.Object;

    [Serializable]
    public class ObjectOverride
    {
        public GameObject Target
        {
            get { return _target; }
        }

        public string Path
        {
            get { return _path; }
        }

        public string[] GuidPath
        {
            get { return ( _guidPath ?? string.Empty ).Split( '/' ); }
        }

        public bool IsOverridden
        {
            get { return _isOverridden; }
        }

        public ComponentOverride[] Components
        {
            get { return _components.ToArray(); }
        }

        public int ComponentCount
        {
            get { return _components.Count; }
        }

        [SerializeField]
        private GameObject _target;

        [SerializeField]
        private string _path;

        [SerializeField]
        private string _guidPath;

        [SerializeField]
        private bool _isOverridden;

        [SerializeField]
        private List<ComponentOverride> _components =
            new List<ComponentOverride>();

        public ObjectOverride(
            GameObject target, string path, string[] guidPath, bool isOverridden )
        {
            if( target == null )
                throw new ArgumentNullException( "Target" );

            if( string.IsNullOrEmpty( path ) )
                throw new ArgumentNullException( "Path" );

            if( guidPath == null || guidPath.Any( s => string.IsNullOrEmpty( s ) ) )
                throw new ArgumentNullException( "GUID Path" );

            _target = target;
            _isOverridden = isOverridden;

            UpdatePath( path );
            UpdateGuidPath( guidPath );
        }

        public void UpdateTarget( GameObject target )
        {
            _target = target;
        }

        public void UpdatePath( string path )
        {
            _path = path;
        }

        public void UpdateGuidPath( string[] path )
        {
            _guidPath = string.Join( "/", path ?? new string[ 0 ] );
        }

        public ComponentOverride Add( ComponentOverride componentOverride )
        {
            if( !_components.Contains( componentOverride ) )
                _components.Add( componentOverride );

            return componentOverride;
        }

        public bool Remove( ComponentOverride componentOverride )
        {
            return _components.Remove( componentOverride );
        }

        public ComponentOverride FindComponentOverride( Object target )
        {
            return _components.FirstOrDefault( c => c.Target == target );
        }

        public ComponentOverride GetComponentOverride( int index )
        {
            return _components[ index ];
        }
    }
}