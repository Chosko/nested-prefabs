﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace VisualDesignCafe.Editor.Prefabs.Overrides
{
    using Object = UnityEngine.Object;

    [Serializable]
    public class ComponentOverride
    {
        public Object Target
        {
            get { return _target; }
        }

        public string Type
        {
            get { return _type; }
        }

        public int Index
        {
            get { return _index; }
        }

        public bool IsOverridden
        {
            get { return _isOverridden; }
        }

        public int PropertyCount
        {
            get { return _properties.Count; }
        }

        public PropertyOverride[] Properties
        {
            get { return _properties.ToArray(); }
        }

        [SerializeField]
        private Object _target;

        [SerializeField]
        private string _type;

        [SerializeField]
        private int _index;

        [SerializeField]
        private bool _isOverridden;

        [SerializeField]
        private List<PropertyOverride> _properties =
            new List<PropertyOverride>();

        public ComponentOverride(
            Object target, string type, int index, bool isOverridden )
        {
            if( target == null )
                throw new ArgumentNullException( "Target" );

            if( string.IsNullOrEmpty( type ) )
                throw new ArgumentNullException( "Type" );

            if( index < 0 )
                throw new ArgumentOutOfRangeException( "Index" );

            if( type.Contains( '.' ) )
                throw new InvalidOperationException( "Type name should not contain namespace." );

            _target = target;
            _index = index;
            _type = type;
            _isOverridden = isOverridden;
        }

        public void UpdateTarget( Object target )
        {
            _target = target;
        }

        public void UpdateIndex( int index )
        {
            _index = index;
        }

        public PropertyOverride Add( PropertyOverride propertyOverride )
        {
            if( propertyOverride == null )
                throw new ArgumentNullException( "Property Override" );

            if( !_properties.Contains( propertyOverride ) )
                _properties.Add( propertyOverride );

            return propertyOverride;
        }

        public bool Remove( PropertyOverride propertyOverride )
        {
            return _properties.Remove( propertyOverride );
        }

        public PropertyOverride FindPropertyOverride( string path )
        {
            return _properties.FirstOrDefault( c => c.Path == path );
        }

        public PropertyOverride GetPropertyOverride( int index )
        {
            return _properties[ index ];
        }
    }
}