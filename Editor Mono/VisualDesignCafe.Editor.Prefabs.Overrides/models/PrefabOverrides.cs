﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using VisualDesignCafe.Editor.Prefabs.Overrides;

// Ignore warnings of obsolete methods. 
// The obsolete PrefabOverrideObject and PrefabOverrideComponent classes are still
// used for backwards compatibility and should not show a warning.
#pragma warning disable 618  

// Should be in the 'Overrides' namespace, but changing the namespace
// will cause all scripts to go missing because the ID in the plugin changes.
namespace VisualDesignCafe.Editor.Prefabs
{
    using Object = UnityEngine.Object;

    [SaveInParent]
    [ExecuteInEditMode]
    public sealed class PrefabOverrides : MonoBehaviour
    {
        public ObjectOverride[] Objects
        {
            get { return _objects.ToArray(); }
        }

        [SerializeField]
        private List<ObjectOverride> _objects =
            new List<ObjectOverride>();

        /// <summary>
        /// Holds the serialized data for previous versions of Nested Prefabs.
        /// Only exists for backwards compatibility.
        /// </summary>
#if !DEV
        [HideInInspector]
#endif
        [SerializeField]
        private List<PrefabOverrideObject> _overrideObjects = new List<PrefabOverrideObject>();

        /// <summary>
        /// Returns the serialized data for previous versions of Nested Prefabs.
        /// </summary>
        public List<PrefabOverrideObject> GetCompatibilityOverrides()
        {
            return _overrideObjects;
        }

        /// <summary>
        /// Updates the stored references to the prefab override's targets.
        /// Any paths or indices are updated if the target was moved or renamed.
        /// </summary>
        public void UpdateAll()
        {
            for( int i = _objects.Count - 1; i > -1; i-- )
            {
                ObjectOverride objectOverride = _objects[ i ];

                if( !UpdateObjectOverride( objectOverride ) )
                {
                    _objects.RemoveAt( i );
                    continue;
                }

                for( int c = objectOverride.ComponentCount - 1; c > -1; c-- )
                {
                    ComponentOverride componentOverride =
                        objectOverride.GetComponentOverride( c );

                    if( !UpdateComponentOverride(
                            objectOverride.Target, componentOverride ) )
                    {
                        objectOverride.Remove( componentOverride );
                        continue;
                    }
                }
            }
        }

        /// <summary>
        /// Adds a new object override for the target.
        /// </summary>
        public ObjectOverride AddObjectOverride( GameObject target )
        {
            return GetObjectOverride( target, true );
        }

        /// <summary>
        /// Adds a new component override for the target.
        /// </summary>
        public ComponentOverride AddComponentOverride( Object target )
        {
            return GetComponentOverride( target, true );
        }

        /// <summary>
        /// Adds a new property override for the property in the target.
        /// </summary>
        public PropertyOverride AddPropertyOverride( Object target, string property )
        {
            return GetPropertyOverride( target, property, true );
        }

        /// <summary>
        /// Adds a new object override.
        /// </summary>
        public ObjectOverride Add( ObjectOverride objectOverride )
        {
            if( objectOverride == null )
                throw new ArgumentNullException( "Object Override" );

            if( !_objects.Contains( objectOverride ) )
                _objects.Add( objectOverride );

            return objectOverride;
        }

        /// <summary>
        /// Adds a new component override.
        /// </summary>
        public ComponentOverride Add( ComponentOverride componentOverride )
        {
            if( componentOverride == null )
                throw new ArgumentNullException( "Component Override" );

            GameObject target =
                componentOverride.Target as GameObject
                ?? ( (Component) componentOverride.Target ).gameObject;

            return GetObjectOverride( target, true ).Add( componentOverride );
        }

        /// <summary>
        /// Adds a new property override.
        /// </summary>
        public PropertyOverride Add(
            Object target, PropertyOverride propertyOverride )
        {
            if( target == null )
                throw new ArgumentNullException( "Target" );

            if( propertyOverride == null )
                throw new ArgumentNullException( "Property Override" );

            return GetComponentOverride( target, true ).Add( propertyOverride );
        }

        /// <summary>
        /// Removes the object override.
        /// </summary>
        public bool Remove( ObjectOverride objectOverride )
        {
            return _objects.Remove( objectOverride );
        }

        /// <summary>
        /// Removes the component override.
        /// </summary>
        public bool Remove( ComponentOverride componentOverride )
        {
            ObjectOverride objectOverride =
                _objects.FirstOrDefault(
                    o => o.Remove( componentOverride ) );

            if( objectOverride == null )
                return false;

            if( !objectOverride.IsOverridden
                && objectOverride.ComponentCount == 0 )
            {
                Remove( objectOverride );
            }

            return true;
        }

        /// <summary>
        /// Removes the property override.
        /// </summary>
        public bool Remove( PropertyOverride propertyOverride )
        {
            ObjectOverride objectOverride =
                _objects.FirstOrDefault(
                    o => o.Components.FirstOrDefault(
                        c => c.Remove( propertyOverride ) ) != null );

            if( objectOverride == null )
                return false;

            ComponentOverride componentOverride =
                objectOverride.Components.FirstOrDefault(
                    c => c.PropertyCount == 0 );

            if( componentOverride != null && !componentOverride.IsOverridden )
                objectOverride.Remove( componentOverride );

            if( !objectOverride.IsOverridden
                && objectOverride.ComponentCount == 0 )
            {
                Remove( objectOverride );
            }

            return true;
        }

        /// <summary>
        /// Returns the object override for the target if the object is overridden.
        /// </summary>
        /// <param name="createIfNull">Add a new object override if the object is not overridden.</param>
        public ObjectOverride GetObjectOverride(
            GameObject target, bool createIfNull = false )
        {
            if( target == null )
                throw new ArgumentNullException( "Target" );

            ObjectOverride objectOverride =
                _objects.FirstOrDefault( o => o.Target == target );

            if( objectOverride == null && createIfNull )
            {
                objectOverride =
                    Add(
                        new ObjectOverride(
                            target,
                            PathTo( target.transform ),
                            GuidPathTo( target.transform ),
                            false ) );
            }

            return objectOverride;
        }

        /// <summary>
        /// Returns the component override for the target if the component is overridden.
        /// </summary>
        /// <param name="createIfNull">Add a new component override if the component is not overridden.</param>
        public ComponentOverride GetComponentOverride(
            Object target, bool createIfNull = false )
        {
            if( target == null )
                throw new ArgumentNullException( "Target" );

            GameObject targetGameObject =
                target as GameObject ?? ( (Component) target ).gameObject;

            ObjectOverride objectOverride =
                GetObjectOverride( targetGameObject, createIfNull );

            if( objectOverride == null )
                return null;

            ComponentOverride componentOverride =
                objectOverride.FindComponentOverride( target );

            if( componentOverride == null && createIfNull )
            {
                componentOverride =
                    objectOverride.Add(
                        new ComponentOverride(
                            target,
                            target.GetType().Name,
                            ComponentIndexOf( target ),
                            false ) );
            }

            return componentOverride;
        }

        /// <summary>
        /// Returns the property override for the target if the property is overridden.
        /// </summary>
        /// <param name="createIfNull">Add a new property override if the property is not overridden.</param>
        public PropertyOverride GetPropertyOverride(
            Object target, string property, bool createIfNull = false )
        {
            ComponentOverride componentOverride =
                GetComponentOverride( target, createIfNull );

            if( componentOverride == null )
                return null;

            PropertyOverride propertyOverride =
                componentOverride.FindPropertyOverride( property );

            if( propertyOverride == null && createIfNull )
            {
                propertyOverride =
                    componentOverride.Add( new PropertyOverride( property ) );
            }

            return propertyOverride;
        }

        private string PathTo( Transform target )
        {
            return transform.PathTo( target ) + "/" + ChildIndexOf( target );
        }

        private int ChildIndexOf( Transform target )
        {
            Transform parent = target.parent;
            int index = 0;
            for( int i = 0; i < parent.childCount; i++ )
            {
                if( parent.GetChild( i ) == target )
                    break;

                if( parent.GetChild( i ).name != target.name )
                    continue;

                index++;
            }

            return index;
        }

        private string[] GuidPathTo( Transform target )
        {
            return Guid.PathTo( target, transform );
        }

        private int ComponentIndexOf( Object target )
        {
            if( target == null || target is GameObject )
                return 0;

            int index = 0;
            Component[] components =
                ( (Component) target ).GetComponents<Component>();

            for( int i = 0; i < components.Length; i++ )
            {
                if( components[ i ] == target )
                    return index;

                if( components[ i ].GetType() == target.GetType() )
                    index++;
            }

            return 0;
        }

        private Transform FindChildAtIndex( Transform parent, string name, int index )
        {
            if( index < 0 )
                return null;

            int currentIndex = 0;
            for( int i = 0; i < parent.childCount; i++ )
            {
                if( parent.GetChild( i ).name == name )
                {
                    if( currentIndex == index )
                        return parent.GetChild( i );

                    currentIndex++;
                }
            }

            return null;
        }

        private void ExtractIndex(
            string source, out string path, out int index )
        {
            int indexOfIndex = source.LastIndexOf( '/' );

            if( indexOfIndex == -1 )
            {
                index = -1;
                path = source;
                return;
            }

            string indexString =
                source.Substring(
                    indexOfIndex + 1,
                    source.Length - indexOfIndex - 1 );

            path = source.Substring( 0, indexOfIndex );

            if( !int.TryParse( indexString, out index ) )
                index = -1;
        }

        private GameObject FindTarget( string path, string[] guidPath )
        {
            Transform target = Guid.FindChild( transform, guidPath );

            if( target != null )
                return target.gameObject;

            if( string.IsNullOrEmpty( path ) )
                return null;

            int index;
            ExtractIndex( path, out path, out index );

            target = transform.Find( path );
            if( target != null && index > 0 )
            {
                target =
                    FindChildAtIndex( target.parent, target.name, index )
                    ?? target;
            }

            return target != null ? target.gameObject : null;
        }

        private Object FindTarget( GameObject target, string type, int index )
        {
            if( index < 0 )
                return null;

            if( string.IsNullOrEmpty( type ) )
                return target;

            if( index == 0 )
                return target.GetComponent( type );

            Object[] components =
                target.GetComponents( typeof( Component ) )
                .Where( c => c.GetType().FullName == type )
                .ToArray();

            if( index < components.Length )
                return components[ index ];

            return null;
        }

        private bool UpdateObjectOverride( ObjectOverride objectOverride )
        {
            if( objectOverride.Target == null )
            {
                objectOverride.UpdateTarget(
                    FindTarget(
                        objectOverride.Path,
                        objectOverride.GuidPath ) );
            }

            if( objectOverride.Target == null )
                return false;

            objectOverride.UpdateGuidPath(
                GuidPathTo( objectOverride.Target.transform ) );

            objectOverride.UpdatePath(
                PathTo( objectOverride.Target.transform ) );

            return true;
        }

        private bool UpdateComponentOverride(
            GameObject target, ComponentOverride componentOverride )
        {
            if( componentOverride.Target == null )
            {
                componentOverride.UpdateTarget(
                    FindTarget(
                        target,
                        componentOverride.Type,
                        componentOverride.Index ) );
            }

            if( componentOverride.Target == null )
                return false;

            componentOverride.UpdateIndex(
                ComponentIndexOf( componentOverride.Target ) );

            return true;
        }

        private void OnEnable()
        {
            // Destroy component at runtime.
            if( Application.isPlaying )
            {
                Destroy( this );
                return;
            }

            if( transform.parent == null )
            {
                Debug.LogWarning( "PrefabOverrides can only be added to a nested prefab" );

                Component.DestroyImmediate( this );
                return;
            }

            if( GetComponent<Prefab>() == null )
            {
                Debug.LogWarning( "PrefabOverrides can only be added to a nested prefab" );

                Component.DestroyImmediate( this );
                return;
            }

            if( GetComponent<NestedPrefab>() == null )
            {
                Debug.LogWarning( "PrefabOverrides can only be added to a nested prefab" );

                Component.DestroyImmediate( this );
                return;
            }

            if( transform.parent.GetComponentInParent<Prefab>() == null )
            {
                Debug.LogWarning( "PrefabOverrides can only be added to a nested prefab" );

                Component.DestroyImmediate( this );
                return;
            }

            // Make sure there is only a single PrefabOverrides component on the object.
            PrefabOverrides[] overrides = GetComponents<PrefabOverrides>();

            if( overrides.Length > 1 )
            {
                Debug.LogError( "Multiple PrefabOverrides components on a GameObject are not allowed.", this );

                for( int i = overrides.Length - 1; i > 0; i-- )
                    Component.DestroyImmediate( overrides[ i ], true );
            }

        }
    }
}