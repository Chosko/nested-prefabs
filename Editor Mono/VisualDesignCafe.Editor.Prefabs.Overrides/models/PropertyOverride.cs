﻿using System;
using UnityEngine;

namespace VisualDesignCafe.Editor.Prefabs.Overrides
{
    using Object = UnityEngine.Object;

    [Serializable]
    public class PropertyOverride
    {
        /// <summary>
        /// Property path of the overridden property.
        /// </summary>
        public string Path
        {
            get { return _path; }
        }

        public AnimationCurve AnimationCurveValue { get { return _animationCurveValue; } }
        public bool BooleanValue { get { return _booleanValue; } }
        public Bounds BoundsValue { get { return _boundsValue; } }
        public Color ColorValue { get { return _colorValue; } }
        public double DoubleValue { get { return _doubleValue; } }
        public float FloatValue { get { return _floatValue; } }
        public int IntValue { get { return _intValue; } }
        public long LongValue { get { return _longValue; } }
        public Object ObjectReferenceValue { get { return _objectReferenceValue; } }
        public Quaternion QuaternionValue { get { return _quaternionValue; } }
        public Rect RectValue { get { return _rectValue; } }
        public string StringValue { get { return _stringValue; } }
        public Vector2 Vector2Value { get { return _vector2Value; } }
        public Vector3 Vector3Value { get { return _vector3Value; } }
        public Vector4 Vector4Value { get { return _vector4Value; } }

        [SerializeField]
        private string _path;
        [SerializeField]
        private AnimationCurve _animationCurveValue;
        [SerializeField]
        private bool _booleanValue;
        [SerializeField]
        private Bounds _boundsValue;
        [SerializeField]
        private Color _colorValue;
        [SerializeField]
        private double _doubleValue;
        [SerializeField]
        private float _floatValue;
        [SerializeField]
        private int _intValue;
        [SerializeField]
        private long _longValue;
        [SerializeField]
        private Object _objectReferenceValue;
        [SerializeField]
        private Quaternion _quaternionValue;
        [SerializeField]
        private Rect _rectValue;
        [SerializeField]
        private string _stringValue;
        [SerializeField]
        private Vector2 _vector2Value;
        [SerializeField]
        private Vector3 _vector3Value;
        [SerializeField]
        private Vector4 _vector4Value;

        public PropertyOverride( string path )
        {
            if( string.IsNullOrEmpty( path ) )
                throw new ArgumentNullException( "Path" );

            _path = path;
        }

        [Obsolete( "Set the value directly on the target property instead." )]
        public void SetValue( AnimationCurve value )
        {
            _animationCurveValue = value;
        }

        [Obsolete( "Set the value directly on the target property instead." )]
        public void SetValue( bool value )
        {
            _booleanValue = value;
        }

        [Obsolete( "Set the value directly on the target property instead." )]
        public void SetValue( Bounds value )
        {
            _boundsValue = value;
        }

        [Obsolete( "Set the value directly on the target property instead." )]
        public void SetValue( Color value )
        {
            _colorValue = value;
        }

        [Obsolete( "Set the value directly on the target property instead." )]
        public void SetValue( double value )
        {
            _doubleValue = value;
        }

        [Obsolete( "Set the value directly on the target property instead." )]
        public void SetValue( float value )
        {
            _floatValue = value;
        }

        [Obsolete( "Set the value directly on the target property instead." )]
        public void SetValue( int value )
        {
            _intValue = value;
        }

        [Obsolete( "Set the value directly on the target property instead." )]
        public void SetValue( long value )
        {
            _longValue = value;
        }

        [Obsolete( "Set the value directly on the target property instead." )]
        public void SetValue( Object value )
        {
            _objectReferenceValue = value;
        }

        [Obsolete( "Set the value directly on the target property instead." )]
        public void SetValue( Quaternion value )
        {
            _quaternionValue = value;
        }

        [Obsolete( "Set the value directly on the target property instead." )]
        public void SetValue( Rect value )
        {
            _rectValue = value;
        }

        [Obsolete( "Set the value directly on the target property instead." )]
        public void SetValue( string value )
        {
            _stringValue = value;
        }

        [Obsolete( "Set the value directly on the target property instead." )]
        public void SetValue( Vector2 value )
        {
            _vector2Value = value;
        }

        [Obsolete( "Set the value directly on the target property instead." )]
        public void SetValue( Vector3 value )
        {
            _vector3Value = value;
        }

        [Obsolete( "Set the value directly on the target property instead." )]
        public void SetValue( Vector4 value )
        {
            _vector4Value = value;
        }
    }
}
