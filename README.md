# README #

[Nested Prefabs](https://www.assetstore.unity3d.com/#!/content/84495?aid=1011lK8Q) for Unity is a fully integrated and completely automatic nested prefab solution for Unity. The plugin itself can be found on the [Unity Asset Store](https://www.assetstore.unity3d.com/#!/content/84495?aid=1011lK8Q).

> Nested Prefabs version 1.3.0 or higher is required in order to build the plugin with the source in this repository.

The Nested Prefabs plugin consists of three separate parts:

* __Assemblies (Subset)__  
Contains shared code between multiple Visual Design Cafe projects, named the *Assemblies* project. Nested Prefabs only contains a subset of the full library, as not all code is required to run the plugin.
* __Nested Prefabs Library__  
Contains the core functionality for Nested Prefabs. All code for nesting prefabs, applying changes, overriding properties, and UI is present in this library.
* __Nested Prefabs Package Manager__  
Installs, updates, uninstalls, and starts the Nested Prefabs plugin. Miscellaneous code to install and run the plugin. Does not directly influence any of the functionality for nesting prefabs, however, it is essential for installing and running the plugin.

This repository contains all code for the __Nested Prefabs Library__.
The other parts are currently not available for download.

### What and who is this repository for? ###

* To extend the plugin with custom functionality tailored to a specific project or organization.
* For those who want to have a copy of the source code as backup.
* Anyone who wishes to improve the plugin with new features, bug fixes, or improvements.

### How do I build Nested Prefabs? ###

1. Download the source code or clone this repository.
2. Open the Visual Studio Solution in the __Visual Studio~__ folder.
3. Add references to the _Assemblies_ folder from the [Asset Store Package](https://www.assetstore.unity3d.com/#!/content/84495?aid=1011lK8Q). (__VisualDesignCafe.Editor.dll__ and __VisualDesignCafe.Editor.Analytics.dll__). By default the solution contains references to these assemblies in an _Assemblies~_ folder. You can either replace these references, or copy the _Assemblies_ folder from the Asset Store Package to the root folder of this repository and rename it to _Assemblies~_.
3. Build the solution.
4. Copy the created assemblies (__NestedPrefabs.dll__ and __NestedPrefabs.Editor.dll__) into your Unity project.

### Contribution guidelines ###

* All code in the plugin should target Unity __5.0__. If a feature is targeted at a higher version of Unity and uses functionality that is not available in Unity 5.0 then please use reflection to retrieve the required methods and make sure the feature can be ignored safely in older versions of Unity.
* All new features should come with Unit Tests. This repository does not contain the unit tests for Nested Prefabs at the moment. These will be added soon along with a testing API.
* Pull requests will be tested with our full list of Unit Tests. The pull request will only be accepted if __all__ tests pass. These tests contain most user-actions such as adding/removing components, children, properties, etc.

### Who do I talk to? ###

* For specific issues with the plugin please create a new issue or write a comment on an existing issue in the [public issue tracker](https://bitbucket.org/VisualDesignCafe/nested-prefabs/issues).
* For other questions and/or inquiries please send an email to support@visualdesigncafe.com